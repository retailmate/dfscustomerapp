/*
package com.cognizant.iot.logout;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.X509TrustManager;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.cognizant.iot.activity.Login_Screen;
import com.cognizant.retailmate.R;

public class Logout_Activity extends Activity {

	// Alert Dialog Manager
	// /AlertDialogManager alert = new AlertDialogManager();

	// Session Manager Class
	SessionManager session;
	ProgressDialog mProgressDialog;
	// Button Logout

	static InputStream is = null;
	static JSONObject jobj = null;
	static String jsondata = "";
	Button btnLogout;
	String jsonUrl;
	String jresponse = null;
	String adr, bID;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.session_main);

		// Session class instance
		session = new SessionManager(getApplicationContext());

		TextView lblName = (TextView) findViewById(R.id.user_name);
		TextView lblEmail = (TextView) findViewById(R.id.email_value);

		// Button logout
		btnLogout = (Button) findViewById(R.id.logout_button);
		Toast.makeText(getApplicationContext(),
				"User Login Status: " + session.isLoggedIn(), Toast.LENGTH_LONG)
				.show();

		*/
/**
		 * Call this function whenever you want to check user login This will
		 * redirect user to LoginActivity is he is not logged in
		 * *//*

		session.checkLogin();

		// get user data from session
		HashMap<String, String> user = session.getUserDetails();

		// name
		String name = user.get(SessionManager.KEY_NAME);

		// email
		String email = user.get(SessionManager.KEY_EMAIL);

		// displaying user data
		lblName.setText(Html.fromHtml("MAC Address: <b>" + name + "</b>"));
		lblEmail.setText(Html.fromHtml("Email: <b>" + email + "</b>"));

		*/
/**
		 * Logout button click event
		 * *//*

		btnLogout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// Clear the session data
				// This will clear all session data and
				// redirect user to LoginActivity
				session.logoutUser();

			}
		});
	}

}
*/
