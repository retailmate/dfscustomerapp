package com.cognizant.iot.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cognizant.iot.activity.CatalogActivity;
import com.cognizant.iot.activityfindproduct.JSONFunctionsDetails_findprod;
import com.cognizant.iot.activityproductlist.ImageLoaderprod;
import com.cognizant.iot.activityproductlist.JSONfunctions_deletefrmwishlist_prod;
import com.cognizant.iot.activityproductlist.MainActivityprod;
import com.cognizant.iot.activityproductlist.Product_description_productscreen;
import com.cognizant.iot.activitywishlist.JSONfunctions_wish_deleteproduct;
import com.cognizant.iot.mycart.JsonFuntionCart;
import com.cognizant.iot.utils.AccountState;
import com.cognizant.retailmate.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cz.msebera.android.httpclient.Header;

public class ListViewAdapter_prod extends BaseAdapter {

    TextView product_category;

    ProgressDialog mProgressDialog;
    JSONObject jsonobject;
    JSONArray jsonarray;

    String prodid1, itemid;
    ProgressDialog prgDialog;
    ListViewAdapter_prod listViewAdapter_prod;

    Context context;
    LayoutInflater inflater;

    ArrayList<HashMap<String, String>> arraylist;
    ArrayList<HashMap<String, String>> data;
    ArrayList<HashMap<String, String>> initialDisplayedValues;

    HashMap<String, String> resultp = new HashMap<String, String>();
    HashMap<String, String> resultp2 = new HashMap<String, String>();

    private SalesPartFilter filter;
    ImageLoaderprod imageLoader;

    Boolean har = false;

    int product_should_be_added = 0;

    String ime;

    public ListViewAdapter_prod(Context context,
                                ArrayList<HashMap<String, String>> arraylistpassed, String imei) {
        this.context = context;
        data = arraylistpassed;
        imageLoader = new ImageLoaderprod(context);
        inflater = LayoutInflater.from(context);
        initialDisplayedValues = arraylistpassed;
        ime = imei;
    }

    @Override
    public void notifyDataSetChanged() // Create this function in your
    // adapter class
    {
        super.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        TextView prodid;
        TextView prodname;
        TextView prodprice;
        ImageView flag, tag_product_screen;
        // TextView product_category;
        TextView product_offers;
        final ImageView wish_prsnt_img, tag_cart_icon;
        TextView product_unit, offer_in_tag_product_list;

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.listview_itemprod, parent,
                false);
        resultp = data.get(position);
        prodname = (TextView) itemView.findViewById(R.id.prodname);
        prodprice = (TextView) itemView.findViewById(R.id.prodprice);
        wish_prsnt_img = (ImageView) itemView.findViewById(R.id.wishlist_btn1);
        tag_cart_icon = (ImageView) itemView.findViewById(R.id.tag_cart_icon);
        product_category = (TextView) itemView
                .findViewById(R.id.product_category);
        product_offers = (TextView) itemView.findViewById(R.id.product_offers);
        product_unit = (TextView) itemView.findViewById(R.id.per_items);
        offer_in_tag_product_list = (TextView) itemView
                .findViewById(R.id.offer_in_tag_product_list);
        tag_product_screen = (ImageView) itemView
                .findViewById(R.id.tag_product_list);

        // Locate the ImageView in listview_item.xml
        flag = (ImageView) itemView.findViewById(R.id.flag);

        // Capture position and set results to the TextViews
        // prodid.setText(resultp.get(MainActivity.PRODID));
        prodname.setText(resultp.get(MainActivityprod.PRODNAME));
        prodprice.setText(resultp.get(MainActivityprod.PRODPRICE));
        product_category.setText(resultp.get(MainActivityprod.CATEGORY));
        if (resultp.get(MainActivityprod.OFFER).isEmpty()) {
            product_offers.setText("No Offers");
            tag_product_screen.setVisibility(View.INVISIBLE);
            offer_in_tag_product_list.setVisibility(View.INVISIBLE);
        } else {
            product_offers.setText(resultp.get(MainActivityprod.OFFER)
                    + " Available till "
                    + resultp.get(MainActivityprod.LASTDATE));
            offer_in_tag_product_list.setText(resultp
                    .get(MainActivityprod.OFFER));
        }

        product_unit.setText(resultp.get(MainActivityprod.UNIT));

        if (resultp.get(MainActivityprod.WISH_PRSNT).toString().equals("0")) {
            wish_prsnt_img.setImageResource(R.drawable.no_crl_mywish_icon);
            har = false;
        } else {
            wish_prsnt_img.setImageResource(R.drawable.hearticonnn);
            har = true;
        }


        imageLoader.DisplayImage(resultp.get(MainActivityprod.FLAG), flag);

        tag_cart_icon.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if (R.drawable.cart_red_icon == tag_cart_icon.getId()) {
                    tag_cart_icon.setImageResource(R.drawable.no_crl_cart_icon);
                } else {
                    tag_cart_icon.setImageResource(R.drawable.cart_red_icon);
                }

                resultp2 = data.get(position);
                prodid1 = resultp2
                        .get(com.cognizant.iot.activity.CatalogActivity.PRODID);

                itemid = resultp2.get("itemid");
                Log.d("@@##", "itemid = " + itemid + ", prodid1 = " + prodid1);
                new DownloadJSONforProductVariant().execute();

            }
        });

        wish_prsnt_img.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (R.drawable.hearticonnn == wish_prsnt_img.getId()) {
                    wish_prsnt_img
                            .setImageResource(R.drawable.no_crl_mywish_icon);
                } else {
                    wish_prsnt_img.setImageResource(R.drawable.hearticonnn);
                }

                resultp2 = data.get(position);
                prodid1 = resultp2
                        .get(com.cognizant.iot.activity.CatalogActivity.PRODID);
                itemid = resultp2.get("itemid");
                Long prodDouble = Long.parseLong(prodid1);

                if (resultp2.get(MainActivityprod.WISH_PRSNT).toString()
                        .equals("0")) {
                    RequestParams params = new RequestParams();
                    // params.put("macaddr", CatalogActivity.imei);
                    // params.put("wishname", "Marriage");
                    // params.put("products", prodid1);

                    params.put("Customer", "2001");
                    // params.put("Product", prodDouble);
                    params.put("product_name", itemid);
                    // addtowishlistService(params);

                    new DownloadJSONforAXADDwishlist().execute();

                    // Intent in = new Intent(context, CatalogActivity.class);
                    // in.putExtra("imeinmbr", ime);
                    // in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    // context.startActivity(in);
                } else {

                    // new DownloadJSONforAXADDwishlist().execute();
                    //  new DownloadJSONfordelete().execute();
                    // Intent hh = new Intent(context, CatalogActivity.class);
                    // hh.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    // hh.putExtra("imeinmbr", ime);
                    // context.startActivity(hh);
                }
            }
        });

        prodname.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {


                if (AccountState.getOfflineMode()) {

                } else {
                    // Get the position
                    resultp = data.get(position);
                    Intent intent = new Intent(context,
                            Product_description_productscreen.class);
                    // Pass all data prodid
                    intent.putExtra("prodid", resultp.get(MainActivityprod.PRODID));
                    // Pass all data prodname
                    intent.putExtra("prodname",
                            resultp.get(MainActivityprod.PRODNAME));
                    // Pass all data prodprice
                    intent.putExtra("prodprice",
                            resultp.get(MainActivityprod.PRODPRICE));
                    // Pass all data flag
                    intent.putExtra("flag", resultp.get(MainActivityprod.FLAG));
                    intent.putExtra("desc", resultp.get(MainActivityprod.DESC));
                    System.out.println("@@## DESCRIPTION SEND PAGE"
                            + resultp.get(MainActivityprod.DESC));

                    intent.putExtra("offer", resultp.get(MainActivityprod.OFFER));
                    intent.putExtra("category",
                            resultp.get(MainActivityprod.CATEGORY));
                    intent.putExtra("wish_present",
                            resultp.get(MainActivityprod.WISH_PRSNT));
                    intent.putExtra("unit", resultp.get(MainActivityprod.UNIT));
                    // Start SingleItemView Class
                    context.startActivity(intent);

                }
            }
        });
        return itemView;
    }



    /*
      *      Call to get Product Variant for adding to cart
     */

    private class DownloadJSONforProductVariant extends AsyncTask<Void, Void, Void> {
        String[] products = new String[1];
        String variantID = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


            products[0] = prodid1;


        }

        @Override
        protected Void doInBackground(Void... params) {
            String productRequestUrl = String.format("https://etihaddemodevret.cloudax.dynamics.com/Commerce/Products/Search?$top=250&api-version=7.1");
            Log.d("@@##", "productRequestUrl = " + productRequestUrl);
            ArrayList<HashMap<String, String>> arraylistprod = new ArrayList<HashMap<String, String>>();

            JSONObject jsonobject1detail = JSONFunctionsDetails_findprod
                    .getMultipleJSONfromURLAX(productRequestUrl, products, "");

            System.out.println("@@## VariantProductsResponseObject = " + jsonobject1detail.toString());
            try {

                JSONArray jsonarray = jsonobject1detail
                        .getJSONArray("value");

                for (int iir = 0; iir < jsonarray.length(); iir++) {
                    JSONObject jsonobjectAX = jsonarray
                            .getJSONObject(iir);

                    try {
                        JSONObject compositionInfo = new JSONObject();


                        compositionInfo = jsonobjectAX.getJSONObject("CompositionInformation");
                        JSONObject variantInfo = new JSONObject();

                        variantInfo = compositionInfo.getJSONObject("VariantInformation");

                        JSONArray variants = new JSONArray();
                        variants = variantInfo.getJSONArray("Variants");

                        JSONObject insideVariantObj = new JSONObject();


                        // Loop kept just to iterate once as we are picking up the first variant ID
                        for (int jk = 0; jk < 1; jk++) {

                            insideVariantObj = variants
                                    .getJSONObject(jk);


                            variantID = insideVariantObj
                                    .getString("DistinctProductVariantId");
                        }

                    } catch (JSONException | NullPointerException | IndexOutOfBoundsException e) {
                        variantID = jsonobjectAX
                                .getString("RecordId");

                    }

                }

            } catch (JSONException e) {

                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return null;

        }

        @Override
        protected void onPostExecute(Void args) {

            new DownloadJSONforaddCart(variantID).execute();
        }
    }


    /*
     * add to cart
     */


    // DownloadJSON AsyncTask
    private class DownloadJSONforaddCart extends AsyncTask<Void, Void, Void> {
        String str4 = null, str3 = null, str5 = null;

        String variantID = "";

        DownloadJSONforaddCart(String variantid) {
            this.variantID = variantid;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {

			/*
             * System.setProperty("https.proxyHost", "10.243.115.76");
			 * System.setProperty("https.proxyPort", "6050");
			 * System.setProperty("http.proxyHost", "10.243.115.76");
			 * System.setProperty("http.proxyPort", "6050");
			 */
            Log.d("@@##", "itemid ## = " + itemid + ", prodid1 = " + prodid1);
            // Create an array
            arraylist = new ArrayList<HashMap<String, String>>();
            // Retrieve JSON Objects from the given URL address
            jsonobject = JsonFuntionCart
                    .getJSONfromURL(
                            "http://ethcustomervalidtionax.azurewebsites.net/api/cart/CreateCart?idToken=" + AccountState.getTokenID(),
                            variantID, "1");

            System.out.println("@@## ADD CART AX " + jsonobject);

            try {

                String message = jsonobject.getString("Id");

            } catch (JSONException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            Toast.makeText(
                    context,
                    "Request processed successfully\nProduct "
                            + " added to the Cart", Toast.LENGTH_LONG).show();

        }
    }

    // DownloadJSON AsyncTask
    private class DownloadJSONfordelete extends AsyncTask<Void, Void, Void> {
        String str4 = null, str3 = null, str5 = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {

			/*
             * System.setProperty("https.proxyHost", "10.243.115.76");
			 * System.setProperty("https.proxyPort", "6050");
			 * System.setProperty("http.proxyHost", "10.243.115.76");
			 * System.setProperty("http.proxyPort", "6050");
			 */

            // Create an array
            arraylist = new ArrayList<HashMap<String, String>>();
            // Retrieve JSON Objects from the given URL address
            jsonobject = JSONfunctions_deletefrmwishlist_prod.getJSONfromURL(
                    CatalogActivity.urlPart
                            + "/api/WishList/DeleteWishlistProducts", prodid1);

            try {

                JSONObject obj1 = new JSONObject(jsonobject.getString("header"));
                String message = obj1.getString("message");
                if (message.equalsIgnoreCase("Request processed successfully")) {

                    String status = obj1.getString("status");
                    if (status.equalsIgnoreCase("success")) {

                        JSONArray obj3 = (JSONArray) jsonobject
                                .get("cusdetails");
                        try {
                            str3 = (String) obj3.getJSONObject(0).get("prodid");

                        } catch (JSONException j) {
                            j.printStackTrace();
                        }
                    }

                }
            } catch (JSONException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            Toast.makeText(
                    context,
                    "Request processed successfully\nProduct " + str3
                            + " from the wishlist", Toast.LENGTH_LONG).show();
            // mProgressDialog.dismiss();
        }
    }

    // This is Async task for adding to wishlist

    // Web service for ADD to WISHLIST

    /**
     * Method that performs RESTful webservice invocations
     *
     * @param params
     */
    public void addtowishlistService(RequestParams params) {

        // Toast.makeText(getApplicationContext(),"JSON request params is" +
        // params.toString(),Toast.LENGTH_LONG).show(); //to show the REQUEST

        AsyncHttpClient client = new AsyncHttpClient();
        client.addHeader("Content-Type", "application/json");
        //client.addHeader("Authorization", CatalogActivity.access_token_AX);
        client.addHeader("Authorization", CatalogActivity.google_token);
        client.post("https://etihaddemodevret.cloudax.dynamics.com/data/tests",
                params, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                        try {
                            String response = new String(responseBody, "UTF-8");
                            // JSON Object
                            JSONObject obj = new JSONObject(response);

                            JSONObject obj1 = new JSONObject(obj.getString("header"));

                            String msg = obj1.getString("message");
                            // Toast.makeText(getApplicationContext(),"Response is\n"
                            // +obj1.toString(),Toast.LENGTH_LONG).show();

                            if (msg.equalsIgnoreCase("Request processed successfully")) {
                                // Set Default Values for Edit View controls
                                // setDefaultValues();

                                // Toast.makeText(getApplicationContext(),"Request processed successfully",Toast.LENGTH_LONG).show();

                                String status = obj1.getString("status");

                                String str4 = null, str3 = null, str5 = null;
                                if (status.equalsIgnoreCase("success")) {
                                    JSONArray obj3 = (JSONArray) obj.get("cusdetails");

                                    str3 = (String) obj3.getJSONObject(0).get("customer");
                                    str4 = (String) obj3.getJSONObject(0).get("Products added");
                                    Toast.makeText(context, "Request processed successfully\nProduct " + str4 + " added to wishlist for Customer: " + str3, Toast.LENGTH_LONG).show();
                                    str5 = (String) obj3.getJSONObject(0).get("product");
                                    Toast.makeText(context, "Product " + str5 + " in the wishlist", Toast.LENGTH_LONG).show();
                                }
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException je) {
                            Toast.makeText(
                                    context,
                                    "Error Occured [Server's JSON response might be invalid]!",
                                    Toast.LENGTH_LONG).show();
                        }

                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        System.out.println("ERROR " + error);
                        prgDialog.hide();

                        if (statusCode == 404) {
                            Toast.makeText(context,
                                    "Requested resource not found",
                                    Toast.LENGTH_LONG).show();
                        }
                        // When Http response code is '500'
                        else if (statusCode == 500) {
                            Toast.makeText(context,
                                    "Something went wrong at server end",
                                    Toast.LENGTH_LONG).show();
                        }
                        // When Http response code other than 404, 500
                        else {
                            Toast.makeText(
                                    context,
                                    "Device might not be connected to Internet",
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    public Filter getFilter() {
        if (filter == null) {
            filter = new SalesPartFilter();
        }
        return filter;
    }

    ArrayList<HashMap<String, String>> productDisplayedValues;

    private class SalesPartFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            constraint = constraint.toString().toLowerCase();
            FilterResults result = new FilterResults();
            if (data == null) {
                data = new ArrayList<HashMap<String, String>>(
                        initialDisplayedValues);
            }
            if (constraint == null || constraint.length() == 0) {
                result.count = data.size();
                result.values = data;
            }
            if (constraint != null && constraint.toString().length() > 0) {
                productDisplayedValues = new ArrayList<HashMap<String, String>>();

                for (HashMap<String, String> map : data) {
                    product_should_be_added = 0;
                    for (Map.Entry<String, String> entry : map.entrySet()) {

                        if (entry.getKey() == MainActivityprod.PRODNAME
                                && entry.getValue().toString().toLowerCase()
                                .contains(constraint)) {
                            product_should_be_added = 1;
                        }
                    }
                    if (product_should_be_added == 1) {
                        productDisplayedValues.add(map);
                    }
                }

                result.count = productDisplayedValues.size();
                result.values = productDisplayedValues;
            } else {
                synchronized (this) {
                    result.values = initialDisplayedValues;
                    result.count = data.size();
                }
            }

            return result;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            if (results.count > 0) {
                data = (ArrayList<HashMap<String, String>>) results.values;
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }

        }
    }

	/*
     * FOR AX customized
	 */

	/*
     * ADD FROM AX wishlist
	 */

    // DownloadJSON AsyncTask
    private class DownloadJSONforAXADDwishlist extends
            AsyncTask<Void, Void, JSONObject> {
        String str4 = null, str3 = null, str5 = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected JSONObject doInBackground(Void... params) {
            /*
             * System.setProperty("https.proxyHost", "10.243.115.76");
			 * System.setProperty("https.proxyPort", "6050");
			 * System.setProperty("http.proxyHost", "10.243.115.76");
			 * System.setProperty("http.proxyPort", "6050");
			 */

            // Create an array
            arraylist = new ArrayList<HashMap<String, String>>();
            // Retrieve JSON Objects from the given URL address


            jsonobject = JSONfunctions_wish_deleteproduct
                    .getJSONfromURLAXADDwishlist(
                            "http://ethcustomervalidtionax.azurewebsites.net/api/Commerce/CreateWishList?idtoken=" + AccountState.getTokenID(),
                            prodid1, itemid);

            System.out.println("@@## REsponse addwishlist \n"
                    + jsonobject);

            return jsonobject;
        }

        @Override
        protected void onPostExecute(JSONObject jsonobject) {

            try {
                if (jsonobject.has("Data")) {
                    JSONObject dataObject = jsonobject.getJSONObject("Data");
                    if (dataObject.getBoolean("success")) {
                        Toast.makeText(context, "Item has been added to your Wishlist",
                                Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(context, "Unable to update Wishlist, please try again",
                                Toast.LENGTH_LONG).show();
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


}