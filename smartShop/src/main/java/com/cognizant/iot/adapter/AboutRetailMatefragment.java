package com.cognizant.iot.adapter;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.cognizant.retailmate.R;

public class AboutRetailMatefragment extends Fragment {
	LinearLayout cataloglayout;

	public AboutRetailMatefragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		super.onCreate(savedInstanceState);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_whats_hot,
				container, false);

		cataloglayout = (LinearLayout) getActivity().findViewById(
				R.id.catalog_activity_layout);

		cataloglayout.setVisibility(View.INVISIBLE);

		return rootView;
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub

		super.onDestroy();
	}

}
