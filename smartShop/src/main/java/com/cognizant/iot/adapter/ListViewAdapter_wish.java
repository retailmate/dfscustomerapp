package com.cognizant.iot.adapter;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cognizant.iot.activity.CatalogActivity;
import com.cognizant.iot.activitywishlist.ImageLoader_wish;
import com.cognizant.iot.activitywishlist.JSONfunctions_wish_deleteproduct;
import com.cognizant.iot.insidestore.RecommendedInStore;
import com.cognizant.iot.mycart.JsonFuntionCart;
import com.cognizant.iot.productlist.ListViewAdapter;
import com.cognizant.iot.utils.AccountState;
import com.cognizant.iot.wishlist.activity.Product_description_wishlistscreen;
import com.cognizant.iot.wishlist.activity.Wishlist_Screen;
import com.cognizant.retailmate.R;

public class ListViewAdapter_wish extends BaseAdapter {

    // Declare Variables
    Context context;
    LayoutInflater inflater;
    ArrayList<HashMap<String, String>> data;
    ImageLoader_wish imageLoader;
    HashMap<String, String> resultp = new HashMap<String, String>();
    ProgressDialog mProgressDialog;
    JSONObject jsonobject;
    JSONArray jsonarray;

    ArrayList<HashMap<String, String>> arraylist;
    String prodid1, itemID, lineId;

    String ime;

    View wishlist_outofstockColorView;
    ImageView wishlist_outofstockrecommendation;

    public ListViewAdapter_wish(Context context,
                                ArrayList<HashMap<String, String>> arraylist, String imei) {
        this.context = context;
        data = arraylist;
        imageLoader = new ImageLoader_wish(context);
        ime = imei;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        // Declare Variables
        TextView category;
        TextView unit;
        TextView prodname;
        TextView prodprice, offer, offer_in_tag;
        ImageView flag;
        final ImageView delete_img_btn;
        final ImageView tag_cart_icon;
        ImageView tag;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View itemView = inflater.inflate(R.layout.listview_item_wish, parent,
                false);
        // Get the position
        resultp = data.get(position);

        // Locate the TextViews in listview_item.xml
        category = (TextView) itemView.findViewById(R.id.prodcategory_wish);
        prodname = (TextView) itemView.findViewById(R.id.prodname_wish);
        prodprice = (TextView) itemView.findViewById(R.id.prodprice_wish1);
        delete_img_btn = (ImageView) itemView
                .findViewById(R.id.delete_small_trash1);
        tag_cart_icon = (ImageView) itemView.findViewById(R.id.tag_cart_icon);
        // Locate the ImageView in listview_item.xml
        flag = (ImageView) itemView.findViewById(R.id.flag_wish);
        offer = (TextView) itemView.findViewById(R.id.offers_wish);
        offer_in_tag = (TextView) itemView.findViewById(R.id.offer_in_tag);
        unit = (TextView) itemView.findViewById(R.id.unit);
        tag = (ImageView) itemView.findViewById(R.id.tag);

        wishlist_outofstockColorView = itemView.findViewById(R.id.wishlist_outofstockColorView);
        wishlist_outofstockrecommendation = (ImageView) itemView.findViewById(R.id.wishlist_outofstockrecommendation);

        // Capture position and set results to the TextViews
        category.setText(resultp.get(CatalogActivity.CATEGORY));
        prodname.setText(resultp.get(CatalogActivity.PRODNAME));
        prodprice.setText(resultp.get(CatalogActivity.PRODPRICE));
        unit.setText(resultp.get(CatalogActivity.UNIT));

        if (resultp.get("prodid").equals("68719483872") || resultp.get("prodid").equals("68719483125")) {
            wishlist_outofstockColorView.setVisibility(View.VISIBLE);
            wishlist_outofstockrecommendation.setVisibility(View.VISIBLE);

            if (resultp.get("prodid").equals("68719483872")) {
                wishlist_outofstockrecommendation.setTag("68719483872");
            } else if (resultp.get("prodid").equals("68719483125")) {
                wishlist_outofstockrecommendation.setTag("68719483125");
            }
        }

        wishlist_outofstockrecommendation.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                String tag = v.getTag().toString();

                if (tag.equals("68719483872")) {

                    new RecommendedInStore("68719483873", context).getPopUp(v, context);
                }
                if (tag.equals("68719483125")) {
                    new RecommendedInStore("68719483124", context).getPopUp(v, context);
                }
            }
        });


        if (resultp.get(CatalogActivity.OFFER).length() > 0) {
            offer.setTextSize(15);
            offer.setText(resultp.get(CatalogActivity.OFFER));
            offer_in_tag.setText(resultp.get(CatalogActivity.OFFER));
        } else {

            tag.setVisibility(View.INVISIBLE);
            offer_in_tag.setVisibility(View.INVISIBLE);
            offer.setTextColor(Color.DKGRAY);
            offer.setText("No offers for this product");
        }


        // Capture position and set results to the ImageView
        // Passes flag images URL into ImageLoader.class
        imageLoader.DisplayImage(resultp.get(CatalogActivity.FLAG), flag);
        // Capture ListView item click

        tag_cart_icon.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if (R.drawable.cart_red_icon == tag_cart_icon.getId()) {
                    tag_cart_icon.setImageResource(R.drawable.no_crl_cart_icon);
                } else {
                    tag_cart_icon.setImageResource(R.drawable.cart_red_icon);
                }

                resultp = data.get(position);
                prodid1 = resultp.get(CatalogActivity.PRODID);
                lineId = resultp.get(CatalogActivity.LINEID);
                itemID = resultp.get("itemid");
                new DownloadJSONforAXaddCart().execute();

            }
        });

		/*
         * delete_img_btn.setBackgroundColor(Color.RED);
		 */
        delete_img_btn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                resultp = data.get(position);
                prodid1 = resultp.get(CatalogActivity.PRODID);
                lineId = resultp.get(CatalogActivity.LINEID);
                itemID = resultp.get("itemid");
                // TODO Auto-generated method stub

                delete_img_btn.setImageResource(R.drawable.no_crl_mywish_icon);
                new DownloadJSONforAXdelete().execute();


                data.remove(position);
                ListViewAdapter_wish.this.notifyDataSetChanged();

            }
        });

        prodname.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {

                if (AccountState.getOfflineMode()) {

                } else {

                    resultp = data.get(position);
                    Intent intent = new Intent(context,
                            Product_description_wishlistscreen.class);
                    // Pass all data prodid
                    intent.putExtra("prodid", resultp.get(Wishlist_Screen.PRODID));
                    System.out
                            .println("@@@##PRODID CLICKED IN WISHLIST HORIZONTAL SCROLL"
                                    + resultp.get(Wishlist_Screen.PRODID));
                    // Pass all data prodname
                    intent.putExtra("prodname",
                            resultp.get(Wishlist_Screen.PRODNAME));
                    // Pass all data prodprice
                    intent.putExtra("prodprice",
                            resultp.get(Wishlist_Screen.PRODPRICE));
                    // Pass all data flag
                    intent.putExtra("flag", resultp.get(Wishlist_Screen.FLAG));
                    intent.putExtra("desc", resultp.get(Wishlist_Screen.DESC));
                    intent.putExtra("beacon", resultp.get(Wishlist_Screen.BEACON));
                    intent.putExtra("offer", resultp.get(Wishlist_Screen.OFFER));
                    intent.putExtra("category",
                            resultp.get(Wishlist_Screen.CATEGORY));
                    intent.putExtra("wish_present", "1");
                    intent.putExtra("unit", resultp.get(Wishlist_Screen.UNIT));
                    // Start SingleItemView Class
                    context.startActivity(intent);

                }
            }
        });
        return itemView;
    }

	/*
     * ADD TO AX CART
	 */

    /*
     * add to cart
     */
    // DownloadJSON AsyncTask
    public class DownloadJSONforAXaddCart extends AsyncTask<Void, Void, Void> {
        String str4 = null, str3 = null, str5 = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {

			/*
             * System.setProperty("https.proxyHost", "10.243.115.76");
			 * System.setProperty("https.proxyPort", "6050");
			 * System.setProperty("http.proxyHost", "10.243.115.76");
			 * System.setProperty("http.proxyPort", "6050");
			 */

            // Create an array
            arraylist = new ArrayList<HashMap<String, String>>();
            // Retrieve JSON Objects from the given URL address

            jsonobject = JsonFuntionCart
                    .getJSONfromURL(
                            "http://ethcustomervalidtionax.azurewebsites.net/api/cart/CreateCart?idToken=" + AccountState.getTokenID(),
                            resultp.get("DistinctProductVariantId"), "1");

            System.out.println("@@## ADD CART AX " + jsonobject);

            try {


                String message = jsonobject.getString("Id");

            } catch (JSONException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            Toast.makeText(
                    context,
                    "Request processed successfully\nProduct "
                            + " added to the Cart", Toast.LENGTH_LONG).show();

        }
    }





	/*
     * DELETE FROM AX wishlist
	 */

    // DownloadJSON AsyncTask
    private class DownloadJSONforAXdelete extends AsyncTask<Void, JSONObject, JSONObject> {
        String str4 = null, str3 = null, str5 = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected JSONObject doInBackground(Void... params) {
            /*
             * System.setProperty("https.proxyHost", "10.243.115.76");
			 * System.setProperty("https.proxyPort", "6050");
			 * System.setProperty("http.proxyHost", "10.243.115.76");
			 * System.setProperty("http.proxyPort", "6050");
			 */

            // Create an array
            arraylist = new ArrayList<HashMap<String, String>>();
            // Retrieve JSON Objects from the given URL address

            //String jsonUrl = "https://etihaddemodevret.cloudax.dynamics.com/data/tests";
            String jsonUrl = "http://ethcustomervalidtionax.azurewebsites.net/api/Commerce/DeleteWishListItems?idToken=" + AccountState.getTokenID();

            try {

                Log.d("@@##", "lineId = " + lineId);
                jsonobject = com.cognizant.iot.wishlist.activity.JSONfunctions_wish_deleteproduct.getJSONfromURL(jsonUrl, lineId);


            } catch (NullPointerException | IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonobject;
        }

        @Override
        protected void onPostExecute(JSONObject args) {


            try {
                if (jsonobject.has("Data")) {
                    JSONObject dataObject = jsonobject.getJSONObject("Data");
                    if (dataObject.getBoolean("success")) {
                        Toast.makeText(context, "Wishlist deleted successfully",
                                Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(context, "Unable to delete wishlist item",
                                Toast.LENGTH_LONG).show();
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
    }

}
