package com.cognizant.iot.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cognizant.iot.activity.Login_Screen;
import com.cognizant.iot.helper.LocaleHelper;
import com.cognizant.iot.model.SpinnerModel;
import com.cognizant.retailmate.R;

import java.util.ArrayList;

/**
 * Created by Guest_User on 23/03/17.
 */

public class SpinnerAdapter extends ArrayAdapter<SpinnerModel> {

    int groupId;
    ArrayList<SpinnerModel> spinnerModels;
    LayoutInflater inflater;
    Context contextL;

    public SpinnerAdapter(Activity context, int groupId, int id, ArrayList<SpinnerModel> spinnerModels) {
        super(context, id, spinnerModels);
        this.spinnerModels = spinnerModels;
        this.contextL = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.groupId = groupId;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final View itemview = inflater.inflate(groupId, parent, false);
        ImageView country_image = (ImageView) itemview.findViewById(R.id.nation_flag);
        TextView country_name = (TextView) itemview.findViewById(R.id.nation_name);
        if (position == 0) {
            country_image.setVisibility(View.GONE);
            country_name.setVisibility(View.GONE);
        } else {
            country_image.setImageResource(spinnerModels.get(position).getImageid());
            country_name.setText(spinnerModels.get(position).getName());
        }
        itemview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Login_Screen.flag.setImageResource(spinnerModels.get(position).getImageid());

                String languageCode = getLanguagecode(spinnerModels.get(position).getName());

                Context context = LocaleHelper.setLocale(contextL, languageCode);

                Resources resources = context.getResources();

                Login_Screen.email_login_btn.setText(resources.getString(R.string.login));
                Login_Screen.register_btn.setText(resources.getString(R.string.register));
                Login_Screen.changelanguage.setText(resources.getString(R.string.changelanguage));

                View root = v.getRootView();                                                                //onclick spinner close
                root.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_BACK));
                root.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_BACK));
            }
        });
        return itemview;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getView(position, convertView, parent);
    }

    public String getLanguagecode(String s) {
        String language = "us";
        if (s.equals("Arabic")) {
            language = "ar";
        } else if (s.equals("Hindi")) {
            language = "hi";
        } else {
            language = "us";
        }

        return language;
    }
}
