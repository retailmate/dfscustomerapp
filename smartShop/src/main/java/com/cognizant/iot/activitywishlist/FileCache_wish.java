package com.cognizant.iot.activitywishlist;

import java.io.File;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;



public class FileCache_wish {
 
	private File cacheDir;

 
	public FileCache_wish(Context context) {






	// Find the dir to save cached images
	if (android.os.Environment.getExternalStorageState().equals(
			android.os.Environment.MEDIA_MOUNTED))
		cacheDir = new File(
				android.os.Environment.getExternalStorageDirectory(),
				"retailmateCache");
	else
		cacheDir = context.getCacheDir();
	if (!cacheDir.exists())
		cacheDir.mkdirs();

	}
 
	public File getFile(String url) {
		String filename = String.valueOf(url.hashCode());
		// String filename = URLEncoder.encode(url);
		File f = new File(cacheDir, filename);
		return f;
 
	}
 
	public void clear() {
		File[] files = cacheDir.listFiles();
		if (files == null)
			return;
		for (File f : files)
			f.delete();
	}



 
}
