package com.cognizant.iot.activity;

import java.util.List;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.cognizant.retailmate.R;

/*ProductAdapter class is designed to be attached to a list view, 
 and populate the data for the different products that will be in a list view
 It uses the Item layout to populate the data in the list view.
 */

public class ProductAdapter extends BaseAdapter {
	private List<Product> mProductList;
	private LayoutInflater mInflater;
	private boolean mShowCheckbox;
	private boolean mShowName;

	// constructor of this class
	public ProductAdapter(List<Product> list, LayoutInflater inflater,
			boolean showCheckbox, boolean showName) {
		mProductList = list;
		mInflater = inflater;
		mShowCheckbox = showCheckbox;
		mShowName = showName;
	}

	@Override
	public int getCount() {
		return mProductList.size();
	}

	@Override
	public Object getItem(int position) {
		return mProductList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		final ViewItem item;

		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.item, null); // gets the
																	// view of
																	// the
																	// item.xml
																	// layout

			item = new ViewItem();
			item.productImageView = (ImageView) convertView // gets the image
					.findViewById(R.id.ImageViewItem);
			item.productTitle = (TextView) convertView
					.findViewById(R.id.TextViewItem); // gets the textView
			item.productCheckbox = (CheckBox) convertView
					.findViewById(R.id.CheckBoxSelected); // gets the checkBox

			convertView.setTag(item);

		} else {
			item = (ViewItem) convertView.getTag(); // if something is already
													// there
		}

		Product curProduct = mProductList.get(position);

		item.productImageView.setImageDrawable(curProduct.productImage);
		item.productTitle.setText(curProduct.title);

		if (!mShowName) // make the text box invisible
			item.productTitle.setVisibility(View.GONE);

		if (!mShowCheckbox) {
			item.productCheckbox.setVisibility(View.GONE); // make the check box
															// invisible
		}

		else {
			if (curProduct.selected == true) // if we press on the product then
												// the checkBox get checked
				item.productCheckbox.setChecked(true);
			else
				item.productCheckbox.setChecked(false);
		}
		return convertView;
	}

	private class ViewItem {
		ImageView productImageView;
		TextView productTitle;
		CheckBox productCheckbox;
	}
}