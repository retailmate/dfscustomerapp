package com.cognizant.iot.activity;

import android.Manifest;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.cognizant.iot.aboutus.AboutRetailMate;
import com.cognizant.iot.activityfindproduct.JSONFunctionsDetails_findprod;
import com.cognizant.iot.activityfindproduct.JSONFunctions_findprod;
import com.cognizant.iot.activityfindproduct.ProductDetails_findprod;
import com.cognizant.iot.activityfindproduct.SuggestGetSet_findprod;
import com.cognizant.iot.activityproductlist.JSONfunctionsprod;
import com.cognizant.iot.activityproductlist.MainActivityprod;
import com.cognizant.iot.activitywishlist.JSONfunctions_wish;
import com.cognizant.iot.adapter.ListViewAdapter_prod;
import com.cognizant.iot.adapter.ListViewAdapter_prodPersonal;
import com.cognizant.iot.adapter.ListViewAdapter_wish;
import com.cognizant.iot.adapter.NavDrawerListAdapter;
import com.cognizant.iot.ar.Splash;
import com.cognizant.iot.camera.CameraActivity;
import com.cognizant.iot.chatbot.ChatMain;
import com.cognizant.iot.insidestore.AssociateCall;
import com.cognizant.iot.insidestore.StoreOffers;
import com.cognizant.iot.json.AssetsExtracter;
import com.cognizant.iot.loyaltypoints.multipleloyalty.LoyaltyHomeActivity;
import com.cognizant.iot.model.OneRetailWishlistCheckModel;
import com.cognizant.iot.mycart.MyCart;
import com.cognizant.iot.receiver.ConnectivityReceiver;
import com.cognizant.iot.scanner.ZBarScannerActivity;
import com.cognizant.iot.slidingmenu.model.NavDrawerItem;
import com.cognizant.iot.utils.AccountState;
import com.cognizant.iot.utils.GlobalClass;

import com.cognizant.iot.wearable.companion.service.BeaconService;
import com.cognizant.iot.wishlist.activity.Wishlist_Screen;
import com.cognizant.retailmate.R;
import com.devsmart.android.ui.HorizontalListView;
import com.google.gson.Gson;
import com.microsoft.azure.engagement.EngagementAgent;
import com.microsoft.azure.engagement.EngagementAgent.Callback;
import com.microsoft.azure.engagement.EngagementAgentUtils;
import com.microsoft.azure.engagement.EngagementConfiguration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class CatalogActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {

    private static final String TAG = "@@##";
    static Boolean login = false;


    Gson gson;
    /*
    FAB
     */
    FloatingActionButton fab, fabOffers;

    FloatingActionButton fab_p1us, fab_one, fab_two, fab_three;
    Animation FabOpen, FabClose, FabClockwise, FabAntiClockwise;

    String whichClass;
    boolean isOpen = false;


    /*
     * SLIDER MENU
     */
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    // nav drawer title
    private CharSequence mDrawerTitle;

    // used to store app title
    private CharSequence mTitle;

    // slide menu items
    private String[] navMenuTitles;
    private TypedArray navMenuIcons;

    private ArrayList<NavDrawerItem> navDrawerItems;
    private NavDrawerListAdapter navadapter;
    /*
     * SLIDER MENU END
	 */

    AssetsExtracter mTask;
    com.cognizant.iot.json.Logout mTasklogout;

	/*
     * FOR PROGGRESS BARS
	 */

    ProgressBar progressProd, progressWish, progressBar_search, progressBar1_productPersonal;
    private int mProgressStatus = 0;

	/*
     *
	 * 
	 */
    /*
     * New Code
	 */

    public String imeinmb, token, tokenAXAzure;



    // public CatalogActivity() {
    // imei = imeinmb;
    // }

    /*
     * FOR SEARCH AND TWO LISTS
     */
    // Declare Variables for wishlist
    JSONObject jsonobject, jsonobject1, jsonobjectAX, jsonobjectAX2,
            jsonobjectdetail, jsonobjectdetailPersonal;
    JSONArray jsonarray, jsonarraydetails, jsonarraydetailsPersonal;
    HorizontalListView listview;
    ListViewAdapter_wish adapter;
    ProgressDialog mProgressDialog;
    ArrayList<HashMap<String, String>> arraylist;
    public static String PRODID = "prodid";
    public static String LINEID = "LineId";
    public static String CATEGORY = "category";
    public static String UNIT = "unit";
    public static String PRODNAME = "prodname";
    public static String PRODPRICE = "prodprice";
    public static String FLAG = "flag";
    Boolean arrayempty = false;
    public static String DESC = "desc";
    public static String BEACON = "beacon";
    public static String OFFER = "offer";
    public static String imei = "";
    public static String access_token = "";
    public static String google_token = "";
    public static String access_token_AX = "";
    Intent wishlist_intent;
    TextView moreproduct;
    TextView morewishlist;
    Intent intentmorewish, intentmoreprod;//No detail found
    TextView textViewproductlistPersonal, textViewproductlist;
    TextView welcomeText;

    // for product list
    JSONObject jsonobject2, jsonobject3;
    JSONArray jsonarrayprod;
    HorizontalListView listviewprod, listviewprodpersonal;
    ListViewAdapter_prod adapterprod;
    ListViewAdapter_prodPersonal adapterprodpersonal;
    ProgressDialog mProgressDialogprod;
    ArrayList<HashMap<String, String>> arraylistprod, arraylistprodpersonal;
    public static String WISH_PRSNT = "wish_prsnt";

    // for find a product:
    AutoCompleteTextView autoText;
    ArrayAdapter<String> adaptersearch;

    ArrayList<String> productDisplayedValues;
    ArrayList<String> initialDisplayedValues;

    public static String imei4;

    private FilterMyResult filter;
    final Context context = this;

    JSONObject jsonobjectsearch;
    JSONArray jsonarraysearch;

    ArrayList<String> suggestions;

    private static final int ADDRESS_TRESHOLD = 2;

    Intent intentObject;

	/*
     * END
	 */

    GlobalClass mApplication;
    public static String urlPart = null;

    ImageView mic;

    int count = 0;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

		/*
         * Azure Mobile Engagement
		 */

        EngagementConfiguration engagementConfiguration = new EngagementConfiguration();
        engagementConfiguration
                .setConnectionString("Endpoint=AzureMobileEngagement.device.mobileengagement.windows.net;SdkKey=a4f7bea01379eb0c6e63de8d975d93ea;AppId=cua000424");
        EngagementAgent.getInstance(this).init(engagementConfiguration);
        EngagementAgent.getInstance(this).getDeviceId(new Callback<String>() {
            @Override
            public void onResult(String deviceId) {
                Log.v("myapp", "Got my device id:" + deviceId);
            }
        });

		/*
         * END
		 */

        gson = new Gson();

        mApplication = (GlobalClass) getApplicationContext();
        /*
         * For server call
		 */

        urlPart = mApplication.getServer();

		/*
         *
		 */

        setContentView(R.layout.product_list);


        google_token = getIntent().getStringExtra("google_token");

        if (!login) {
            Intent intent = new Intent(getApplicationContext(), Splash.class);
            startActivity(intent);
            login = true;
        }

		/*
         * SLIDER MENU
		 */
        mTitle = mDrawerTitle = getTitle();

        // load slide menu items
        navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);

        // nav drawer icons from resources
        navMenuIcons = getResources()
                .obtainTypedArray(R.array.nav_drawer_icons);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.list_slidermenu);

        navDrawerItems = new ArrayList<NavDrawerItem>();

        // adding nav drawer items to array
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons
                .getResourceId(0, -1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], navMenuIcons
                .getResourceId(1, -1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], navMenuIcons
                .getResourceId(2, -1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[3], navMenuIcons
                .getResourceId(3, -1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[4], navMenuIcons
                .getResourceId(4, -1), true, "5"));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[5], navMenuIcons
                .getResourceId(5, -1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[6], navMenuIcons
                .getResourceId(6, -1)));

        navDrawerItems.add(new NavDrawerItem(navMenuTitles[7], navMenuIcons
                .getResourceId(7, -1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[8], navMenuIcons
                .getResourceId(8, -1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[9], navMenuIcons
                .getResourceId(
                        9, -1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[10], navMenuIcons
                .getResourceId(10, -1)));
        // Recycle the typed array
        navMenuIcons.recycle();

        mDrawerList.setOnItemClickListener(new SlideMenuClickListener());

        // setting the nav drawer list adapter
        navadapter = new NavDrawerListAdapter(getApplicationContext(),
                navDrawerItems);
        mDrawerList.setAdapter(navadapter);


        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setCustomView(R.layout.actionbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,

                R.string.app_name, // nav drawer open - description for
                // accessibility
                R.string.app_name // nav drawer close - description for
                // accessibility
        ) {
            public void onDrawerClosed(View view) {
//                getActionBar().setTitle(mTitle);
                // calling onPrepareOptionsMenu() to show action bar icons
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
//                getActionBar().setTitle(mDrawerTitle);
                // calling onPrepareOptionsMenu() to hide action bar icons
                invalidateOptionsMenu();
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        if (savedInstanceState == null) {
            // on first time display view for first nav item
            // displayView(0);PRODUCT ID ADD INDICATOR:
        }
        /*
         * SLIDER MENU END
		 */

		/*
         * Progress bar
		 */
        progressProd = (ProgressBar) findViewById(R.id.progressBar1_product);
        progressWish = (ProgressBar) findViewById(R.id.progressBar1_wishlist);
        progressBar_search = (ProgressBar) findViewById(R.id.progressBar_search);
        progressBar1_productPersonal = (ProgressBar) findViewById(R.id.progressBar1_productPersonal);
        /*
         *
		 */

		/*
         * FOR SEARCH AND LISTS
		 */


        welcomeText = (TextView) findViewById(R.id.welcomeText);

        welcomeText.setText("Welcome, " + AccountState.getUserName() + "!");


        textViewproductlistPersonal = (TextView) findViewById(R.id.textViewproductlistPersonal);
        textViewproductlist = (TextView) findViewById(R.id.textViewproductlist);


        listviewprodpersonal = (HorizontalListView) findViewById(R.id.listview_prodPersonal);

        textViewproductlist.setTextSize(15);
        textViewproductlistPersonal.setTextSize(10);
/*
    For the implementation of toggle between product list and recommended list
 */
        textViewproductlistPersonal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listviewprod.setVisibility(View.INVISIBLE);
                listviewprodpersonal.setVisibility(View.VISIBLE);


                textViewproductlist.setTextSize(10);
                textViewproductlistPersonal.setTextSize(15);
            }
        });

        textViewproductlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listviewprod.setVisibility(View.VISIBLE);
                listviewprodpersonal.setVisibility(View.INVISIBLE);
                textViewproductlist.setTextSize(15);
                textViewproductlistPersonal.setTextSize(10);

            }
        });


        initialize();
        autoText.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                if (autoText.length() == 2) {
                    // call async task for find a prod
                    if (checkConnection()) {
                        new AdapterUpdaterTasksearch().execute();
                    }
                } else {
                    adaptersearch.getFilter().filter(s.toString());
                    adaptersearch.setNotifyOnChange(true);
                    autoText.setAdapter(adaptersearch);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (autoText.length() > 2) {
                    adaptersearch.getFilter().filter(s.toString());
                }
            }

        });

        mic = (ImageView) findViewById(R.id.mic);
        mic.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                displaySpeechRecognizer();
            }
        });

        ImageView button = (ImageView) findViewById(R.id.search);
        button.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                String information = autoText.getText().toString();
                if (information.isEmpty()) {
                    Toast.makeText(getApplicationContext(),
                            "Search field empty", Toast.LENGTH_SHORT).show();
                } else {
                    if (information.substring(information
                            .lastIndexOf("/") + 1).length() < 11) {

                        Toast.makeText(getApplicationContext(), "Not a valid product, please try again", Toast.LENGTH_SHORT).show();

                    } else {

                        String pid = information.substring(information
                                .lastIndexOf("/") + 1);
                        intentObject = new Intent(getApplicationContext(),
                                ProductDetails_findprod.class);
                        intentObject.putExtra("ProductId", pid);
                        intentObject.putExtra("class", "catalog");
                        startActivity(intentObject);
                    }
                }

            }
        });
        autoText.setText("");

        autoText.setHint("Search Product");

        // end of search a prod

        // for more button in welcome screen to view wish list
        morewishlist = (TextView) findViewById(R.id.morewishlist);
        morewishlist.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {


                if (checkConnection()) {

                    intentmorewish = new Intent(getApplicationContext(),
                            Wishlist_Screen.class);
                    startActivity(intentmorewish);
                }

            }
        });

        // for more button in welcome screen to view product list
        moreproduct = (TextView) findViewById(R.id.moreproduct);
        moreproduct.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                if (checkConnection()) {
                    intentmorewish = new Intent(getApplicationContext(),
                            MainActivityprod.class);
                    startActivity(intentmorewish);
                }

            }
        });

        // Execute DownloadJSON AsyncTask for GEtting Product LIST populated
        if (isStoragePermissionGranted() && checkConnection() || AccountState.getOfflineMode()) {

            new DownloadJSONprod().execute();
        }
        /*
         * END
		 */


        fab_p1us = (FloatingActionButton) findViewById(R.id.fab_plus);
        fab_one = (FloatingActionButton) findViewById(R.id.fab_one);
        fab_two = (FloatingActionButton) findViewById(R.id.fab_two);
        fab_three = (FloatingActionButton) findViewById(R.id.fab_three);
        FabOpen = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_open);
        FabClose = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_close);
        FabClockwise = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_clockwise);
        FabAntiClockwise = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_anticlockwise);

        fab_one.setImageResource(R.drawable.user_icon);
        fab_two.setImageResource(R.drawable.in_store_offers_icon);
        fab_three.setVisibility(View.INVISIBLE);

        fab_p1us.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (isOpen) {
                    fab_one.startAnimation(FabClose);
                    fab_two.startAnimation(FabClose);
//                    fab_three.startAnimation(FabClose);
                    fab_p1us.startAnimation(FabAntiClockwise);
                    fab_one.setClickable(false);
                    fab_two.setClickable(false);
//                    fab_three.setClickable(false);
                    isOpen = false;
                } else {
                    fab_one.startAnimation(FabOpen);
                    fab_two.startAnimation(FabOpen);
//                    fab_three.startAnimation(FabOpen);
                    fab_p1us.startAnimation(FabClockwise);
                    fab_one.setClickable(true);
                    fab_two.setClickable(true);
//                    fab_three.setClickable(true);
                    isOpen = true;
                }


            }

        });


//        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab_one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Click action
                Intent intent = new Intent(CatalogActivity.this, AssociateCall.class);
                intent.putExtra("whichClass", "catalog");
                startActivity(intent);
            }
        });

//        fabOffers = (FloatingActionButton) findViewById(R.id.fabOffers);
        fab_two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Click action
                Intent intent = new Intent(CatalogActivity.this, StoreOffers.class);
                startActivity(intent);
            }
        });

    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        initialize();

             /*
        For AZME
         */
        String activityNameOnEngagement = EngagementAgentUtils.buildEngagementActivityName(getClass()); // Uses short class name and removes "Activity" at the end.
        EngagementAgent.getInstance(this).startActivity(this, activityNameOnEngagement, null);
        /*
        AZME END
         */


        // register connection status listener
        GlobalClass.getInstance().setConnectivityListener(this);


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.v(TAG, "Permission: " + permissions[0] + "was " + grantResults[0]);
            //resume tasks needing this permission
        }
    }


    /*
     * SEARCH AND LISTs
     */
    // initialise for find a product:
    public void initialize() {
        autoText = (AutoCompleteTextView) findViewById(R.id.autoComplete_tv);
        TelephonyManager mngr = (TelephonyManager) context
                .getSystemService(context.TELEPHONY_SERVICE);
        imei4 = mngr.getDeviceId();
        autoText.setThreshold(ADDRESS_TRESHOLD);
        autoText.setText("");

        autoText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                autoText.setTypeface(Typeface.DEFAULT);
                v.setFocusable(true);
                v.setFocusableInTouchMode(true);
                return false;
            }
        });

        autoText.setFocusable(false);
        adaptersearch = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line) {
        };
    }

    public Filter getFilter() {
        if (filter == null) {
            filter = new FilterMyResult();
        }
        return filter;
    }

    private class FilterMyResult extends Filter {
        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            if (results.count > 0) {
                suggestions = (ArrayList<String>) results.values;
                adaptersearch.notifyDataSetChanged();
                autoText.showDropDown();
            } else {
                adaptersearch.notifyDataSetInvalidated();
            }
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults result = new FilterResults();
            if (suggestions == null) {
                suggestions = new ArrayList<String>(initialDisplayedValues); // saves
                // the
                // original
                // data
                // in
                // mOriginalValues
            }
            if (constraint == null || constraint.length() == 0) {
                result.count = suggestions.size();
                result.values = suggestions;
            }

            if (constraint != null && constraint.toString().length() > 0) {
                productDisplayedValues = new ArrayList<String>();
                for (int i = 0, l = suggestions.size(); i < l; i++) {
                    String loopString = suggestions.get(i);
                    if (loopString.toString().toLowerCase()
                            .contains(constraint.toString()))
                        productDisplayedValues.add(loopString);
                }
                result.count = productDisplayedValues.size();
                result.values = productDisplayedValues;
            } else {
                synchronized (this) {
                    result.values = initialDisplayedValues;
                    result.count = suggestions.size();
                }
            }
            return result;
        }

    }

	/*
     * END
	 */

	/*
     * SLIDER MENU
	 */

    /**
     * Slide menu item click listener
     */
    private class SlideMenuClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            // display view for selected nav drawer item
            System.out.println("@## position clicked" + position);
            displayView(position);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        // setMenuBackground();

        return true;
    }

//    private void setMenuBackground() {
//        // TODO Auto-generated method stub
//        // Log.d(TAG, "Enterting setMenuBackGround");
//        getLayoutInflater().setFactory(new Factory() {
//
//            @Override
//            public View onCreateView(String name, Context context,
//                                     AttributeSet attrs) {
//                // TODO Auto-generated method stub
//
//                if (name.equalsIgnoreCase("com.android.internal.view.menu.IconMenuItemView")) {
//                    try { // Ask our inflater to create the view
//                        LayoutInflater f = getLayoutInflater();
//                        final View view = f.createView(name, null, attrs);
//                        /*
//                         * The background gets refreshed each time a new item is
//						 * added the options menu. So each time Android applies
//						 * the default background we need to set our own
//						 * background. This is done using a thread giving the
//						 * background change as runnable object
//						 */
//                        new Handler().post(new Runnable() {
//                            public void run() {
//                                // sets the background color
//                                // view.setBackgroundColor(Color
//                                // .parseColor("#ffffff"));
//
//                                view.setBackgroundResource(R.drawable.background_blue_orignal);
//                                // sets the text color
//                                ((TextView) view).setTextColor(Color.WHITE);
//                                // sets the text size
//                                ((TextView) view).setTextSize(18);
//                            }
//                        });
//                        return view;
//                    } catch (InflateException e) {
//                    } catch (ClassNotFoundException e) {
//                    }
//                }
//
//                return null;
//            }
//        });
//    }


    @Override
    protected void onPause() {
        super.onPause();
        EngagementAgent.getInstance(this).endActivity();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar app icon/title
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle action bar actions click
        switch (item.getItemId()) {

            case R.id.qrscan:
                Intent intent = new Intent(getApplicationContext(),
                        ZBarScannerActivity.class);
                startActivity(intent);
                return true;

            case R.id.arscan:
                Intent intentar = new Intent(getApplicationContext(), Splash.class);
                startActivity(intentar);
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /* *
     * Called when invalidateOptionsMenu() is triggered
     */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // if nav drawer is opened, hide the action items
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        // menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }

    /**
     * Diplaying fragment view for selected nav drawer list item
     */
    private void displayView(int position) {
        // update the main content by replacing fragments
        Fragment fragment = null;
        Intent wishlist_intent;
        switch (position) {
            case 0:
                if (checkConnection() || AccountState.getOfflineMode()) {
                    getBaseContext().stopService(
                            new Intent(getBaseContext(), BeaconService.class));
                    mTask = new AssetsExtracter("empty", CatalogActivity.this);
                    mTask.execute();
                }
                break;
            case 1:
                if (checkConnection()) {
                    wishlist_intent = new Intent(CatalogActivity.this,
                            com.cognizant.iot.wishlist.activity.Wishlist_Screen.class);

                    wishlist_intent.putExtra("imeinmbr", imeinmb);
                    startActivity(wishlist_intent);
                    mDrawerLayout.closeDrawer(mDrawerList);
                }
                break;
            case 2:
                if (checkConnection() || AccountState.getOfflineMode()) {
                    wishlist_intent = new Intent(CatalogActivity.this,
                            com.cognizant.iot.orderhistory.activity.Order_Screen.class);
                    wishlist_intent.putExtra("imeinmbr", imeinmb);
                    wishlist_intent.putExtra("wichclass", "catalog");
                    startActivity(wishlist_intent);
                    mDrawerLayout.closeDrawer(mDrawerList);
                }
                break;
            case 3:
                if (checkConnection() || AccountState.getOfflineMode()) {
                    Intent cameraintent = new Intent(CatalogActivity.this,
                            CameraActivity.class);
                    startActivity(cameraintent);
                    mDrawerLayout.closeDrawer(mDrawerList);
                }
                break;
            case 4:
                if (checkConnection() || AccountState.getOfflineMode()) {
                    Intent intent = new Intent(CatalogActivity.this, MyCart.class);
                    intent.putExtra("whichClass", "catalog");
                    startActivity(intent);
                    mDrawerLayout.closeDrawer(mDrawerList);
                }
                break;
            case 5:
                if (checkConnection() || AccountState.getOfflineMode()) {
                    Intent intentLP = new Intent(CatalogActivity.this,
                            LoyaltyHomeActivity.class);
                    startActivity(intentLP);
                    mDrawerLayout.closeDrawer(mDrawerList);
                }
                break;

            case 6:
                wishlist_intent = new Intent(CatalogActivity.this,
                        com.cognizant.iot.storelocation.GoogleMapInitial.class);
                startActivity(wishlist_intent);
                mDrawerLayout.closeDrawer(mDrawerList);
                break;
            case 7:
                if (checkConnection() || AccountState.getOfflineMode()) {
                    wishlist_intent = new Intent(CatalogActivity.this,
                            ChatMain.class);
                    startActivity(wishlist_intent);
                    mDrawerLayout.closeDrawer(mDrawerList);
                }
                break;
            case 8:
                if (checkConnection() || AccountState.getOfflineMode()) {
                    wishlist_intent = new Intent(CatalogActivity.this,
                            VideoActivity.class);
                    startActivity(wishlist_intent);
                    mDrawerLayout.closeDrawer(mDrawerList);
                }
                break;
            case 9:
                wishlist_intent = new Intent(CatalogActivity.this,
                        AboutRetailMate.class);
                startActivity(wishlist_intent);
                mDrawerLayout.closeDrawer(mDrawerList);
                break;
            case 10:
//                mTasklogout = new Logout("empty", CatalogActivity.this, imeinmb);
//                System.out.println("LOGOUT IMEI" + imeinmb);


                finish();
//                mTasklogout.execute();
                break;
            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.frame_container, fragment).commit();

            // update selected item and title, then close the drawer
            mDrawerList.setItemChecked(position, true);
            mDrawerList.setSelection(position);
            setTitle(navMenuTitles[position]);
            mDrawerLayout.closeDrawer(mDrawerList);

        } else {
            // error in creating fragment
            Log.e("MainActivity", "Error in creating fragment");
        }
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

	/*
     * SLIDER MENU END
	 */

    // DownloadJSON AsyncTask for wishlist
    public class DownloadJSON extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.d("@@##", "wish list request initiated");
            progressWish.setProgress(mProgressStatus);

            progressBar1_productPersonal.setProgress(mProgressStatus);

        }

        @Override
        protected Void doInBackground(Void... params) {

            // Create an array
            arraylist = new ArrayList<HashMap<String, String>>();

            // jsonUrl = "https://etihaddemodevret.cloudax.dynamics.com/data/tests?$filter=Customer";
            String jsonUrl = "http://ethcustomervalidtionax.azurewebsites.net/api/Commerce/GetWishList?idToken=" + AccountState.getTokenID();
            try {

                if (AccountState.getOfflineMode()) {
                    jsonobject1 = new JSONObject(getResponse("wish"));
                } else {
                    jsonobject1 = JSONfunctions_wish
                            .getJSONfromAXURLGETWishlist(jsonUrl);
                }
                System.out.println("whislist returned \n" + jsonobject1);

                // Locate the array name in JSON
                JSONObject dataObject = jsonobject1.getJSONObject("Data");
                if (dataObject != null) {
                    if (dataObject.get("success") != null && (boolean) dataObject.get("success")) {
                        JSONObject wishListObject = dataObject.getJSONObject("wishlistdata");
                        JSONArray jsonarray = wishListObject.getJSONArray("CommerceListLines");

                        Log.d("@@##", "jsonarray length = " + jsonarray.length());
                        if (jsonarray.length() == 0) {
                            arrayempty = true;
                        } else {

                            for (int i = 0; i < jsonarray.length(); i++) {
                                HashMap<String, String> map = new HashMap<String, String>();
                                jsonobject = jsonarray.getJSONObject(i);
                                // Retrive JSON Objects
                                String productId = Long.toString((Long) jsonobject.get("ProductId"));
                                Log.d("@@##", "productId@ = " + productId);
                                map.put("prodid", productId);
                                String lineId = Long.toString((Long) jsonobject.get("LineId"));
                                Log.d("@@##", "CatlogActivity lineId@ = " + lineId);
                                Log.d("@@##", "productId@ = " + productId);
                                map.put("LineId", lineId);
                                map.put("itemid", "");

                                map.put("desc", "");
                                // map.put("beacon", jsonobject.getString("Beacon"));
                                map.put("offer", "");
                                // Set the JSON Objects into the array


                                arraylist.add(map);
                            }
                        }

                    }
                }

            } catch (JSONException | NullPointerException | ArrayIndexOutOfBoundsException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }

            progressWish.setProgress(50);


            return null;

        }

        @Override
        protected void onPostExecute(Void args) {


            new DownloadJSONforProducts().execute();

        }
    }


    /*
      *      Call to get extra details of products in WISHLIST
     */

    private class DownloadJSONforProducts extends AsyncTask<Void, Void, Void> {
        String[] products = new String[(arraylist.size())];
        String productIds = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            Collections.reverse(arraylist);

            for (int i = 0; i < arraylist.size(); i++) {
                products[i] = arraylist.get(i).get("prodid");

            }


        }

        @Override
        protected Void doInBackground(Void... params) {
            String productRequestUrl = String.format("https://etihaddemodevret.cloudax.dynamics.com/Commerce/Products/Search?$top=250&api-version=7.1");
            Log.d("@@##", "productRequestUrl = " + productRequestUrl);
            ArrayList<HashMap<String, String>> arraylistprod = new ArrayList<HashMap<String, String>>();
//			JSONObject cartResponseObject = JSONfunctionsprod
//					.getJSONfromAXURLGETProducts(productRequestUrl);
            JSONObject jsonobject1detail = null;
            try {
                if (AccountState.getOfflineMode()) {
                    jsonobject1detail = new JSONObject(getResponse("wishdetail"));
                } else {

                    jsonobject1detail = JSONFunctionsDetails_findprod
                            .getMultipleJSONfromURLAX(productRequestUrl, products, "");

                }

                System.out.println("@@## WishLISTProductsResponseObject = " + jsonobject1detail.toString());

                jsonarraydetails = jsonobject1detail
                        .getJSONArray("value");

                for (int ii = 0; ii < jsonarraydetails.length(); ii++) {
                    jsonobjectdetail = jsonarraydetails
                            .getJSONObject(ii);

                    JSONObject insideImage = new JSONObject();
                    insideImage = jsonobjectdetail
                            .getJSONObject("Image");
                    JSONArray insideitemimage = new JSONArray();
                    insideitemimage = insideImage
                            .getJSONArray("Items");


                    JSONObject insideImageURL = new JSONObject();

                    // Loop kept just to iterate once as we are picking up the first IMAGE URL
                    for (int j = 0; j < 1; j++) {

                        insideImageURL = insideitemimage
                                .getJSONObject(j);

                        arraylist.get(ii).put("flag",
                                "https://etihaddemodevret.cloudax.dynamics.com/MediaServer/"
                                        + insideImageURL
                                        .getString("Url"));

                        System.out.println("@@## Wishlist IMAGES " + "https://etihaddemodevret.cloudax.dynamics.com/MediaServer/"
                                + insideImageURL
                                .getString("Url"));
                    }
                    arraylist.get(ii).put("prodname", jsonobjectdetail
                            .getString("ProductName"));

                    arraylist.get(ii).put("category", jsonobjectdetail
                            .getString("SearchName"));
                    arraylist.get(ii).put("unit", "$");

//                    arraylist.get(ii).put("prodprice",
//                            Html.fromHtml("<small>$</small>")
//                                    + jsonobjectdetail
//                                    .getString("AdjustedPrice"));

                    arraylist.get(ii).put("prodprice",
                            jsonobjectdetail
                                    .getString("AdjustedPrice"));

                    arraylist.get(ii).put("beacon", "CE:0B:89:EE:21:9A");
                    arraylist.get(ii).put("offer", "0");

                    try {


                        JSONObject compositionInfo = new JSONObject();

                        compositionInfo = jsonobjectdetail.getJSONObject("CompositionInformation");
                        JSONObject variantInfo = new JSONObject();

                        variantInfo = compositionInfo.getJSONObject("VariantInformation");

                        JSONArray variants = new JSONArray();
                        variants = variantInfo.getJSONArray("Variants");

                        JSONObject insideVariantObj = new JSONObject();


                        // Loop kept just to iterate once as we are picking up the first variant ID
                        for (int jk = 0; jk < 1; jk++) {

                            insideVariantObj = variants
                                    .getJSONObject(jk);

                            arraylist.get(ii).put("DistinctProductVariantId", insideVariantObj
                                    .getString("DistinctProductVariantId"));

                        }

                    } catch (JSONException | NullPointerException | IndexOutOfBoundsException e) {
                        arraylist.get(ii).put("DistinctProductVariantId", jsonobjectdetail
                                .getString("RecordId"));

                        System.out.println("@@## NO DistinctProductVariantId FOR wishlist " + jsonobjectdetail
                                .getString("RecordId"));

                    }
                }

            } catch (JSONException | NullPointerException | ArrayIndexOutOfBoundsException e) {

                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return null;

        }

        @Override
        protected void onPostExecute(Void args) {

            progressWish.setProgress(100);
            progressWish.setVisibility(View.GONE);


            if (arrayempty == true)
                Toast.makeText(getApplicationContext(),
                        "Your wishlist is empty", Toast.LENGTH_LONG).show();

            // Locate the listview in listview_main.xml
            listview = (HorizontalListView) findViewById(R.id.listview_wish);


            // Pass the results into ListViewAdapter.java
            adapter = new ListViewAdapter_wish(CatalogActivity.this, arraylist,
                    imei);
            // Set the adapter to the ListView
            adapter.notifyDataSetChanged();
            listview.setAdapter(adapter);


            //Get recommended products
            new DownloadJSONprodPersonal().execute();

        }
    }

    // DownloadJSON AsyncTask for productlist
    private class DownloadJSONprod extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressProd.setProgress(mProgressStatus);

        }

        @Override
        protected Void doInBackground(Void... params) {

            // Create an array
            arraylistprod = new ArrayList<HashMap<String, String>>();
            try {
                if (AccountState.getOfflineMode()) {
                    jsonobject3 = new JSONObject(getResponse("products"));
                } else {

                    jsonobject3 = JSONfunctionsprod
                            .getJSONfromAXURLGETProducts("https://etihaddemodevret.cloudax.dynamics.com/Commerce/Products/SearchByCategory(channelId=68719476778,catalogId=0,categoryId=68719477291)?$top=60&api-version=7.1");
                }


                System.out.println("@@## RESPONSE FROM AX for product list" + jsonobject3);
                // Locate the array name in JSON
                jsonarrayprod = jsonobject3.getJSONArray("value");

                for (int i = 0; i < jsonarrayprod.length(); i++) {
                    HashMap<String, String> map = new HashMap<String, String>();
                    jsonobject1 = jsonarrayprod.getJSONObject(i);
                    // Retrive JSON Objects
                    map.put("prodid", jsonobject1.getString("RecordId"));
                    map.put("itemid", jsonobject1.getString("ItemId"));
                    map.put("prodname", jsonobject1.getString("Name"));

                    map.put("prodprice", jsonobject1.getString("Price"));
                    map.put("flag",
                            "https://etihaddemodevret.cloudax.dynamics.com/MediaServer/"
                                    + jsonobject1.getString("PrimaryImageUrl"));


                    map.put("wish_prsnt", "0");

                    map.put("offers", "0");


                    // Set the JSON Objects into the array
                    arraylistprod.add(map);
                }
            } catch (JSONException | NullPointerException | ArrayIndexOutOfBoundsException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            progressProd.setProgress(50);
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {

            progressProd.setProgress(100);
            progressProd.setVisibility(View.GONE);

            listviewprodpersonal.setVisibility(View.INVISIBLE);

            // Locate the listview in listview_main.xml
            listviewprod = (HorizontalListView) findViewById(R.id.listview_prod);
            // Pass the results into ListViewAdapter.java
            adapterprod = new ListViewAdapter_prod(CatalogActivity.this,
                    arraylistprod, imei);
            // Set the adapter to the ListView
            listviewprod.setAdapter(adapterprod);

            count = count + 1;

            new DownloadJSON().execute();
        }
    }


    /*
           Download the recommended list of products
     */
    private class DownloadJSONprodPersonal extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //  progressProd.setProgress(mProgressStatus);
        }

        @Override
        protected Void doInBackground(Void... params) {

            // Create an array
            arraylistprodpersonal = new ArrayList<HashMap<String, String>>();
            try {

                if (AccountState.getOfflineMode()) {
                    jsonobject3 = new JSONObject(getResponse("recommended"));
                } else {
                    jsonobject3 = JSONfunctionsprod
                            .getJSONfromURLRecommendedProducts("http://rmethapi.azurewebsites.net/api/CognitiveServices/GetUserToItemRecommendationsAPI");
                }


                System.out.println("@@## RESPONSE FOR RECOMMENDED PRODUCTS" + jsonarrayprod);
                // Locate the array name in JSON
                jsonarrayprod = jsonobject3.getJSONArray("value");

                for (int i = 0; i < jsonarrayprod.length(); i++) {
                    HashMap<String, String> map = new HashMap<String, String>();
                    jsonobject1 = jsonarrayprod.getJSONObject(i);
                    // Retrive JSON Objects
                    map.put("prodid", jsonobject1.getString("ProductId"));
                    map.put("itemid", jsonobject1.getString("ItemId"));
                    map.put("prodname", jsonobject1.getString("ProductName"));

                    map.put("prodprice", jsonobject1.getString("Custprice"));
                    map.put("flag",
                            "https://etihaddemodevret.cloudax.dynamics.com/MediaServer/"
                                    + jsonobject1.getString("Image"));


                    map.put("wish_prsnt", "0");

                    map.put("offers", "0");
                    // Set the JSON Objects into the array
                    arraylistprodpersonal.add(map);
                }
            } catch (JSONException | NullPointerException | ArrayIndexOutOfBoundsException e) {

                e.printStackTrace();
            }
            progressBar1_productPersonal.setProgress(50);
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {


            if (arraylistprodpersonal.isEmpty()) {
                progressBar1_productPersonal.setProgress(100);
                progressBar1_productPersonal.setVisibility(View.GONE);
                textViewproductlist.setTextSize(15);
                textViewproductlistPersonal.setTextSize(10);
                listviewprodpersonal.setVisibility(View.INVISIBLE);

                Toast.makeText(getApplicationContext(), "Not able to get recommended product", Toast.LENGTH_SHORT).show();

            } else {
                new DownloadJSONforRecommendedProducts().execute();
            }

        }
    }


    /*
      *      Call to get extra details of products in recommendations
     */

    private class DownloadJSONforRecommendedProducts extends AsyncTask<Void, Void, Void> {
        String[] products = new String[(arraylistprodpersonal.size())];
        String productIds = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            for (int i = 0; i < arraylistprodpersonal.size(); i++) {
                products[i] = arraylistprodpersonal.get(i).get("prodid");

            }

        }

        @Override
        protected Void doInBackground(Void... params) {
            String productRequestUrl = String.format("https://etihaddemodevret.cloudax.dynamics.com/Commerce/Products/Search?$top=250&api-version=7.1");
            Log.d("@@##", "productRequestUrl = " + productRequestUrl);
            ArrayList<HashMap<String, String>> arraylistprod = new ArrayList<HashMap<String, String>>();
            JSONObject jsonobject1detail = null;
            try {
                if (AccountState.getOfflineMode()) {
                    jsonobject1detail = new JSONObject(getResponse("recommendedextra"));
                } else {
                    jsonobject1detail = JSONFunctionsDetails_findprod
                            .getMultipleJSONfromURLAX(productRequestUrl, products, "");

                }

                System.out.println("@@## RecommendedProductsResponseObject = " + jsonobject1detail.toString());


                jsonarraydetailsPersonal = jsonobject1detail
                        .getJSONArray("value");

                for (int iir = 0; iir < jsonarraydetailsPersonal.length(); iir++) {
                    jsonobjectdetailPersonal = jsonarraydetailsPersonal
                            .getJSONObject(iir);


                    arraylistprodpersonal.get(iir).put("category", jsonobjectdetailPersonal
                            .getString("SearchName"));


                    arraylistprodpersonal.get(iir).put("beacon", "CE:0B:89:EE:21:9A");
                    arraylistprodpersonal.get(iir).put("offer", "0");

                    try {
                        JSONObject compositionInfo = new JSONObject();

                        compositionInfo = jsonobjectdetailPersonal.getJSONObject("CompositionInformation");
                        JSONObject variantInfo = new JSONObject();

                        variantInfo = compositionInfo.getJSONObject("VariantInformation");

                        JSONArray variants = new JSONArray();
                        variants = variantInfo.getJSONArray("Variants");

                        JSONObject insideVariantObj = new JSONObject();


                        // Loop kept just to iterate once as we are picking up the first variant ID
                        for (int jk = 0; jk < 1; jk++) {

                            insideVariantObj = variants
                                    .getJSONObject(jk);

                            arraylistprodpersonal.get(iir).put("DistinctProductVariantId", insideVariantObj
                                    .getString("DistinctProductVariantId"));

                            System.out.println("@@## DistinctProductVariantId Recommended product " + insideVariantObj
                                    .getString("DistinctProductVariantId"));

                        }
                    } catch (JSONException | NullPointerException | IndexOutOfBoundsException e) {
                        arraylistprodpersonal.get(iir).put("DistinctProductVariantId", jsonobjectdetailPersonal
                                .getString("RecordId"));

                        System.out.println("@@## NO DistinctProductVariantId FOR Recommended product " + jsonobjectdetailPersonal
                                .getString("RecordId"));
                    }


                }

            } catch (JSONException | NullPointerException | ArrayIndexOutOfBoundsException e) {

                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return null;

        }

        @Override
        protected void onPostExecute(Void args) {

            progressBar1_productPersonal.setProgress(100);
            progressBar1_productPersonal.setVisibility(View.GONE);


            try {
                System.out.println("@@## DistinctProductVariantId Recommended product sent " + arraylistprodpersonal.get(0).get("DistinctProductVariantId"));
            } catch (IndexOutOfBoundsException e) {
                Toast.makeText(getApplicationContext(), "Not able to get recommended product", Toast.LENGTH_SHORT).show();
            }

            // Pass the results into ListViewAdapter.java
            adapterprodpersonal = new ListViewAdapter_prodPersonal(CatalogActivity.this,
                    arraylistprodpersonal, imei);
            // Set the adapter to the ListView
            listviewprodpersonal.setAdapter(adapterprodpersonal);

            count = count + 1;

            listviewprod.setVisibility(View.VISIBLE);
            listviewprodpersonal.setVisibility(View.INVISIBLE);
            textViewproductlist.setTextSize(15);
            textViewproductlistPersonal.setTextSize(10);


        }
    }



    /*
      *  async call for finding a prod using text by text search:
      */

    private class AdapterUpdaterTasksearch extends AsyncTask<Void, Void, Void> {

        List<SuggestGetSet_findprod> ListData = new ArrayList<SuggestGetSet_findprod>();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar_search.setProgress(mProgressStatus);
            progressBar_search.setVisibility(View.VISIBLE);

        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.i("UPDATE", "1");
//            jsonobjectsearch = JSONFunctions_findprod.getJSONfromURL(urlPart
//                            + "/api/Product/FindProduct",
//                    autoText.getText().toString(), imei4);

            String searchURL = "https://etihaddemodevret.cloudax.dynamics.com/Commerce/Products/SearchByText(channelId=68719476778,catalogId=0,searchText=" + "\'" + autoText.getText().toString() + "\'" + ")?$top=20&api-version=7.1";
            System.out.println("@@## URL for seraching product" + searchURL);
            jsonobjectsearch = JSONFunctions_findprod.getJSONfromAXURLGETSearchProducts(searchURL);


            System.out.println("@@## JSON result for seraching product" + jsonobjectsearch);
            try {
                jsonarraysearch = jsonobjectsearch.getJSONArray("value");
                for (int i = 0; i < jsonarraysearch.length(); i++) {
                    jsonobjectsearch = jsonarraysearch.getJSONObject(i);
                    ListData.add(new SuggestGetSet_findprod(jsonobjectsearch
                            .getString("RecordId"), jsonobjectsearch
                            .getString("Name"), jsonobjectsearch
                            .getString("ItemId")));


                }
                progressBar_search.setProgress(50);
            } catch (JSONException | NullPointerException | ArrayIndexOutOfBoundsException e) {

                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            Log.i("UPDATE", "2");
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            int size = ListData.size();
            if (size > 0) {
                adaptersearch.clear();
                Log.i("ADAPTER_SIZE", "" + size);
                suggestions = new ArrayList<String>();
                suggestions.clear();
                for (int i = 0; i < ListData.size(); i++) {
                    suggestions.add(ListData.get(i).getName() + " / "
                            + ListData.get(i).getId());
                }
                for (int i = 0; i < suggestions.size(); i++) {
                    adaptersearch.add(suggestions.get(i));
                }
                Log.i("UPDATE", "4");
                initialDisplayedValues = new ArrayList<String>(suggestions);
                autoText.setAdapter(adaptersearch);
                // adapter.setNotifyOnChange(true);
                autoText.showDropDown();
            }

            progressBar_search.setProgress(100);
            progressBar_search.setVisibility(View.GONE);
            super.onPostExecute(aVoid);
        }
    }

    private static final int SPEECH_REQUEST_CODE = 0;

    // Create an intent that can start the Speech Recognizer activity
    private void displaySpeechRecognizer() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        // Start the activity, the intent will be populated with the speech text
        startActivityForResult(intent, SPEECH_REQUEST_CODE);
    }

    // This callback is invoked when the Speech Recognizer returns.
    // This is where you process the intent and extract the speech text from the
    // intent.
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SPEECH_REQUEST_CODE && resultCode == RESULT_OK) {
            List<String> results = data
                    .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            final String spokenText = results.get(0);
            System.out.println("@@## spokenText " + spokenText);

            autoText.postDelayed(new Runnable() {
                @Override
                public void run() {
                    autoText.setText(spokenText.toString());
                    new AdapterUpdaterTasksearch().execute();
                }
            }, 500);

            // autoText.setText(spokenText.toString());
            // autoText.setSelection(autoText.getText().length());
            // autoText.setText(spokenText);
            // autoText.setHint(spokenText);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /*
For permission to access storage
 */
    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG, "Permission is granted");
                return true;
            } else {

                Log.v(TAG, "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG, "Permission is granted");
            return true;
        }


    }

    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        makeToast(isConnected);
    }


    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        makeToast(isConnected);

        return isConnected;
    }

    // Showing the status in Toast
    private void makeToast(boolean isConnected) {
        String message = "";
        int color;
        if (isConnected) {

        } else {
            message = "Sorry! Not connected to internet";
            color = Color.RED;
            if (!AccountState.getOfflineMode())
                Toast.makeText(CatalogActivity.this, message, Toast.LENGTH_SHORT).show();
        }
    }

    /*
    ONE RETAIL WISHLIST CHECK
     */
    public void makeOneRetailWishlistCheckCall() {


        String url = "http://104.43.192.216:8180/api/Cust?CID=%279902%27&SID=%27s1%27&WID=%27w1%27";

        StringRequest req = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("@@## inbound:%n %s", response);
                OneRetailWishlistCheckModel[] oneRetailWishlistCheckModel = gson.fromJson(response, OneRetailWishlistCheckModel[].class);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {


            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                int mStatusCode = response.statusCode;

                Log.d("@@##", "Response Back " + response.statusCode);
                if (mStatusCode == HttpURLConnection.HTTP_OK) {

                } else {

                }
                return super.parseNetworkResponse(response);
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(req);

    }


    public String getResponse(String type) {
        String json = null;
        InputStream is = null;
        try {

            switch (type) {

                case "wishdetail":
                    is = getAssets().open("wishlistproductsoffline.json");
                    break;
                case "wish":
                    is = getAssets().open("wishlistoffline.json");
                    break;
                case "products":
                    is = getAssets().open("productsoffline.json");
                    break;
                case "recommended":
                    is = getAssets().open("recommendedoffline.json");
                    break;
                case "recommendedextra":
                    is = getAssets().open("recommendedextraoffline.json");
                    break;
            }
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }

        return json;
    }

}
