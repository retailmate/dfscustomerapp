package com.cognizant.iot.activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.cognizant.iot.adapter.SpinnerAdapter;
import com.cognizant.iot.geolocation.ProximityReciever;
import com.cognizant.iot.helper.LocaleHelper;
import com.cognizant.iot.homepage.HomePageActivity;
import com.cognizant.iot.model.SpinnerModel;
import com.cognizant.iot.service.CustomerSignalRService;
import com.cognizant.iot.service.notifications;
import com.cognizant.iot.utils.AccountState;
import com.cognizant.iot.utils.DialogFactory;
import com.cognizant.iot.utils.GlobalClass;
import com.cognizant.iot.wearable.companion.Constants;
import com.cognizant.iot.wearable.companion.service.BeaconService;
import com.cognizant.retailmate.R;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.Asset;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class Login_Screen extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {

    private static final int REQUEST_READ_PHONE_STATE = 111;
    private static final int REQUEST_WRITE_EXTERNAL_STORAGE = 123;
    private static final int REQUEST_CAMERA = 777;
    /*
         * NEW FOR WATCH
         */
    private static GoogleApiClient mGoogleApiClient;
    private static final String TAG = "@@##";

    static Context context = null;
    private final static int LOCATION_REQUEST_CODE = 998;
    String tokenAXAzure;

    /*
     * NEW END
     */
    // Progress Dialog Object
    ProgressDialog progressDialog;
    private NotificationManager notificationManager;

    // create 2 private variables i.e. username and password and generate
    // getters and setters
    private EditText login_email = null;
    private EditText login_password = null;

    // create a private buttons i.e login and newuserreg of type Button for
    // submit
//    private Button login;
    private TextView newuser;
    Intent intentObject;
    String imeinmbr;

    // final Context context = this;
    Intent main_intent_catalog;
    TextView click_forgot;
    public String imei;
//    EditText imei_login_screen = null;

    GlobalClass mApplication;
    public static String urlPart = null;

    Switch serverSwitch;
    private static final int RC_GET_TOKEN = 9002;
    /*
     * (non-Javadoc)
     *
     * @see android.app.Activity#onDestroy() new
     */
    static Boolean firstEntranceCheck = true;

    ImageView imageView1;
    Switch offlineswitch;

    public static TextView register_btn;
    public static TextView email_login_btn,changelanguage;

    String[] SPINNERLIST = {"English", "Arabic", "Hindi"};

    public static ImageView flag;

    @Override
    /* onCreate method is called when the activity is first created. */
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getBaseContext();
        notificationManager = (NotificationManager) this
                .getSystemService(Context.NOTIFICATION_SERVICE);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        if (!GlobalClass.checkPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            requestPermission(this, LOCATION_REQUEST_CODE, Manifest.permission.ACCESS_FINE_LOCATION);
        }

        // set up full screen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        mApplication = (GlobalClass) getApplicationContext();

        /*
         * For server call
		 */

        urlPart = mApplication.getServer();

		/*
             *
			 */
        if (progressDialog != null) {
            progressDialog = null;
        }

        // Instantiate Progress Dialog object
        progressDialog = new ProgressDialog(this, R.style.TransparentDialogTheme);
        // Set Progress Dialog Text
        progressDialog.setMessage("Please wait...");
        // Set Cancelable as False
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);

        setContentView(R.layout.login_page);
        login_email = (EditText) findViewById(R.id.emailid);
        login_password = (EditText) findViewById(R.id.loginPassword);
//        imei_login_screen = (EditText) findViewById(R.id.imei);

        imageView1 = (ImageView) findViewById(R.id.imageView1);
        offlineswitch = (Switch) findViewById(R.id.offlineswitch);
        register_btn = (TextView) findViewById(R.id.register_btn);
        email_login_btn = (TextView) findViewById(R.id.email_login);

        changelanguage = (TextView) findViewById(R.id.changelanguage);

        offlineswitch.setChecked(AccountState.getOfflineMode());


        imageView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (offlineswitch.getVisibility() == View.INVISIBLE) {
                    offlineswitch.setVisibility(View.VISIBLE);
                } else {
                    offlineswitch.setVisibility(View.INVISIBLE);
                }
            }
        });

        offlineswitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    offlineswitch.setChecked(true);
                    AccountState.setOfflineMode(true);
                } else {
                    offlineswitch.setChecked(false);
                    AccountState.setOfflineMode(false);

                }
            }
        });


//        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
//
//        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
//            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, REQUEST_READ_PHONE_STATE);
//
//        } else {
//            //TODO
//            TelephonyManager mngr = (TelephonyManager) context.getSystemService(context.TELEPHONY_SERVICE);
//            imei = mngr.getDeviceId(); // IMEI number string
//        }

        if (isContactPermissionGranted()) {
            TelephonyManager mngr = (TelephonyManager) context.getSystemService(context.TELEPHONY_SERVICE);
            imei = mngr.getDeviceId();
        }


        //imei = "000000000000011";
//        imei_login_screen.setText(imei);
        addListenerOnButton();

//        click_forgot = (TextView) findViewById(R.id.forgot_textview_clickable);

//        click_forgot.setOnClickListener(new OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//                // Intent intent = new Intent(context, ForgotPassword.class);
//                // intent.putExtra("imei",
//                // imei_login_screen.getText().toString());
//                // startActivity(intent);
//                Intent intent = new Intent(context, AR_activity.class);
//                startActivity(intent);
//
//            }
//        });
        /*
         *
		 * NEW FOR WATCH
		 */

        Log.d("@@##", "BeaconService going to start soon ");
        getBaseContext().startService(
                new Intent(getBaseContext(), BeaconService.class));

        Log.d("@@##", "SignalR going to start soon ");
        getBaseContext().startService(
                new Intent(getBaseContext(), CustomerSignalRService.class));

/*
Google Auth start
 */
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.server_client_id))
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .addApi(Wearable.API).addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();

        if (!mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
        }


        // Button click listeners
        findViewById(R.id.sign_in_button).setOnClickListener(this);
        /*
         * END
		 */

         /*
        google auth END
         */

        /**
         *
         * ALARM
         *
         */
        if (GlobalClass.checkPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            startAlarm(Login_Screen.this);
        } else {
            requestPermission(this, LOCATION_REQUEST_CODE, Manifest.permission.ACCESS_FINE_LOCATION);
        }


        flag = (ImageView) findViewById(R.id.flag);
        ArrayList<SpinnerModel> spinnerModelArrayList = new ArrayList<>();
        spinnerModelArrayList.add(new SpinnerModel("", R.drawable.usa));
        spinnerModelArrayList.add(new SpinnerModel("Arabic", R.drawable.uae));
        spinnerModelArrayList.add(new SpinnerModel("English", R.drawable.usa));
        spinnerModelArrayList.add(new SpinnerModel("Hindi", R.drawable.india));

        Spinner materialDesignSpinner = (Spinner)
                findViewById(R.id.language_spinner);
        SpinnerAdapter spinnerAdapter = new SpinnerAdapter(this, R.layout.spinner_single_row, R.id.nation_name, spinnerModelArrayList);
        materialDesignSpinner.setAdapter(spinnerAdapter);

/*    ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, SPINNERLIST);


        materialDesignSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String languageCode = getLanguagecode(String.valueOf(parent.getSelectedItem()));

                Context context = LocaleHelper.setLocale(Login_Screen.this, languageCode);

                Resources resources = context.getResources();

                email_login_btn.setText(resources.getString(R.string.login));
                register_btn.setText(resources.getString(R.string.register));



            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/


    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base));
    }

    private String getLanguagecode(String s) {
        String language = "us";
        if (s.equals("Arabic")) {
            language = "ar";
        } else if (s.equals("Hindi")) {
            language = "hi";
        } else {
            language = "us";
        }

        return language;
    }


    /*
     * ALARM CLASS START
     */
    public void startAlarm(Context context) {

        if (!firstEntranceCheck) {
            try {
                unregisterReceiver(new ProximityReciever());
            } catch (IllegalArgumentException e) {

            }
        }

        Intent intent = new Intent(context, notifications.class);
        PendingIntent sender = PendingIntent.getService(context, 0, intent, 0);

        System.out.println("ALARM GENERATED RETAIL MATE");
        AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);
        am.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, 100000, 200000,
                sender);
        firstEntranceCheck = false;

    }

	/*
     * ALARM END
	 */

    public void addListenerOnButton() {
        // we want to attach the on click events to 2 buttons i.e. login and new
        // user reg
//        login = (Button) findViewById(R.id.login_btn);
        newuser = (TextView) findViewById(R.id.register_btn);

        // if newuser reg button is clicked the below method executes
        newuser.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // Toast.makeText(getApplicationContext(),
                // "Your Device imei number is " + imei,
                // Toast.LENGTH_SHORT).show();

                intentObject = new Intent(getApplicationContext(),
                        RegisterActivity.class);

                intentObject.putExtra("IMEI", imei);

                startActivity(intentObject);

            }

        });

        // if login reg button is clicked the below method executes
//        login.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View arg0) {
//
//                // new DownloadAccessToken().execute();
//
//            }
//        });

    }

//    public String getIMEI(Context context) {
//
//        TelephonyManager mngr = (TelephonyManager) context
//                .getSystemService(context.TELEPHONY_SERVICE);
//        String imei = mngr.getDeviceId();
//        return imei;
//
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        return true;
    }


    /*
    Google Auth func
     */
    private void getIdToken() {
        // Show an account picker to let the user choose a Google account from the device.
        // If the GoogleSignInOptions only asks for IDToken and/or profile and/or email then no
        // consent screen will be shown here.
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_GET_TOKEN);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_in_button:

                if (GlobalClass.checkPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) && GlobalClass.checkPermission(this, Manifest.permission.READ_PHONE_STATE)) {
                    progressDialog.show();
                    if (AccountState.getOfflineMode()) {

                        AccountState.setUserNAME("Amer Hasan");
                        AccountState.setUserID("004021");
                        progressDialog.hide();
                        progressDialog.dismiss();

                        Intent main_intent_catalog = new Intent(Login_Screen.this,
                                CatalogActivity.class);

                        startActivity(main_intent_catalog);


                    } else {
                        getIdToken();
                    }
                } else {
                    requestPermission(this, LOCATION_REQUEST_CODE, Manifest.permission.ACCESS_FINE_LOCATION);
                }
                break;
        }
    }


    public static void sendMessage(String response, String path) {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            PutDataMapRequest putDataMapRequest = PutDataMapRequest
                    .create(path);

            // Add data to the request
            // putDataMapRequest.getDataMap().putString(Constants.KEY_TITLE,
            // String.format("hello world! %d", count++));
            putDataMapRequest.getDataMap().putString(Constants.KEY_TITLE,
                    response);
            putDataMapRequest.getDataMap().putString(Constants.KEY_TIMESTAMP,
                    String.valueOf(System.currentTimeMillis()));

            Bitmap icon = BitmapFactory.decodeResource(context.getResources(),
                    R.drawable.retail_mate_app_icon);
            Asset asset = createAssetFromBitmap(icon);
            putDataMapRequest.getDataMap().putAsset(Constants.KEY_IMAGE, asset);

            PutDataRequest request = putDataMapRequest.asPutDataRequest();
            Log.d(TAG, "sending response data to wear");
            Wearable.DataApi.putDataItem(mGoogleApiClient, request)
                    .setResultCallback(
                            new ResultCallback<DataApi.DataItemResult>() {
                                @Override
                                public void onResult(
                                        DataApi.DataItemResult dataItemResult) {
                                    Log.d(TAG,
                                            "response data sent :: putDataItem status: "
                                                    + dataItemResult
                                                    .getStatus()
                                                    .toString());
                                }
                            });
        } else {
            Log.d(TAG, "mGoogleApiClient not connected  ");
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        // TODO Auto-generated method stub
        Log.e(TAG, "Failed to connect to Google Api Client with error code "
                + connectionResult.getErrorCode());

    }

    @Override
    public void onConnected(Bundle arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onConnectionSuspended(int arg0) {
        // TODO Auto-generated method stub

    }

    private static Asset createAssetFromBitmap(Bitmap bitmap) {
        final ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteStream);
        return Asset.createFromBytes(byteStream.toByteArray());
    }


    /*
    Google auth fun start
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_GET_TOKEN) {
            // [START get_id_token]
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            Log.d(TAG, "onActivityResult:GET_TOKEN:success:" + result.getStatus().isSuccess());

            if (result.isSuccess()) {
                GoogleSignInAccount acct = result.getSignInAccount();
                String idToken = acct.getIdToken();
                String personName = acct.getDisplayName();
                AccountState.setUserNAME(personName);
                String personEmail = acct.getEmail();
                String personId = acct.getId();
                Uri personPhoto = acct.getPhotoUrl();
                // Show signed-in UI.
                Log.d(TAG, "idToken:\n" + idToken);
                Log.d(TAG, "personName:\n" + personName);
                Log.d(TAG, "personEmail:\n" + personEmail);
                Log.d(TAG, "personId:\n" + personId);
                postData(idToken);
            } else {
                // Show signed-out UI.
                //updateUI(false);
                Log.d(TAG, "idToken false");
                progressDialog.hide();
                progressDialog.dismiss();
            }
        }
    }


    /*
Google auth send data to RetailSDK
 */
    private void postData(String googleAuthToken) {
        progressDialog.show();
        String url = com.cognizant.iot.utils.Constants.REGISTRATION_ENDPOINT + googleAuthToken;

        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (progressDialog != null) {
                            progressDialog.hide();
                            progressDialog.dismiss();
                        }
                        // response
                        Log.d("@@##", "login response = " + response);

                        try {
                            JSONObject responseObject = new JSONObject(response);
                            if (responseObject.has("Data")) {
                                JSONObject responseDataObject = responseObject.getJSONObject("Data");
                                if (responseDataObject.has("success")) {
                                    if (responseDataObject.getBoolean("success")) {
                                        Log.d("@@##", "Save token in session");
                                        if (responseDataObject.has("tokenid")) {
                                            String tokenId = responseDataObject.getString("tokenid");
                                            AccountState.setTokenID(tokenId);
                                            Log.d("@@##", "AccountState.getTokenID = " + AccountState.getTokenID());
                                        }

                                        if (responseDataObject.has("customerid")) {
                                            String customerId = responseDataObject.getString("customerid");
                                            AccountState.setUserID(customerId);
                                            Log.d("@@##", "AccountState.getUserID = " + AccountState.getUserID());
                                        }


//                                        AlertDialog dialog = DialogFactory.getDialog("Login Success", Login_Screen.this);
//                                        //dialog.setOwnerActivity(activity);
//                                        dialog.show();
//                                        Toast.makeText(Login_Screen.this, "Login Success", Toast.LENGTH_LONG).show();
                                        /*
                                        new code added
                                         */
//                                        Intent main_intent_catalog = new Intent(Login_Screen.this,
//                                                CatalogActivity.class);
                                        Intent main_intent_catalog = new Intent(Login_Screen.this,
                                                HomePageActivity.class);
//                                        System.out.println("@@## SENDING IMEI" + imeinmbr);
//
//                                        main_intent_catalog.putExtra("imeinmbr", imeinmbr);
                                        main_intent_catalog.putExtra("google_token", "id_token " + AccountState.getTokenID());
//                                        main_intent_catalog.putExtra("access_token_AX", "Bearer "
//                                                + tokenAXAzure);
                                        startActivity(main_intent_catalog);
                                        /*
                                        END
                                         */
                                    } else {
                                        AlertDialog dialog = DialogFactory.getDialog("Something went wrong, please try again", Login_Screen.this);
                                        //dialog.setOwnerActivity(activity);
                                        dialog.show();
                                        //Toast.makeText(Login_Screen.this, "Something went wrong, please try again", Toast.LENGTH_LONG).show();
                                    }
                                }
                            }
                        } catch (JSONException | NullPointerException | ArrayIndexOutOfBoundsException e) {

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (progressDialog != null) {
                            progressDialog.hide();
                            progressDialog.dismiss();
                        }
                        AlertDialog dialog = DialogFactory.getDialog("Something went wrong, please try again", Login_Screen.this);
                        //dialog.setOwnerActivity(activity);
                        dialog.show();
                        // error
                        Log.d("@@##", "login VolleyError = " + error);
                        if (error.networkResponse != null) {
                            Log.d("@@##", "Error: " + error
                                    + "\nstatusCode >> " + error.networkResponse.statusCode
                                    + "\ndata >> " + error.networkResponse.data.toString()
                                    + "\ndata class >> " + error.networkResponse.data.getClass()
                                    + "\ngetCause >> " + error.getCause()
                                    + "\ngetMessage >> " + error.getMessage());

                            byte[] byteArray = error.networkResponse.data;
                            try {
                                String byteArrayStr = new String(byteArray, "UTF-8");
                                Log.d("@@##", "byteArrayStr = " + byteArrayStr);
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }

                    }
                }
        ) {

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                String output = "";
                return output.getBytes();
            }
        };

        // Adding request to request queue
        postRequest.setShouldCache(false);
        postRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        GlobalClass.getInstance().addToRequestQueue(postRequest, com.cognizant.iot.utils.Constants.REGISTRATION_REQUEST);
    }


    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        notificationManager.cancelAll();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
        Log.d("@@##", "SignalR going to stop soon ");
        getBaseContext().stopService(
                new Intent(getBaseContext(), CustomerSignalRService.class));
        super.onDestroy();

    }

    private void requestPermission(Activity activity, int permissionRequestCode, String permission) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
            Toast.makeText(activity, "Please allow permission in App Settings for additional functionality.", Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(activity, new String[]{permission}, permissionRequestCode);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case LOCATION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (!GlobalClass.checkPermission(this, Manifest.permission.READ_PHONE_STATE)) {
                        requestPermission(this, REQUEST_READ_PHONE_STATE, Manifest.permission.READ_PHONE_STATE);
                    }
                } else {

                    Toast.makeText(this, "Permission Denied, You cannot access location data.", Toast.LENGTH_LONG).show();

                }


                break;

            case REQUEST_READ_PHONE_STATE:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    //TODO

                    if (!GlobalClass.checkPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        requestPermission(this, REQUEST_WRITE_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE);
                    }
                }


                break;


            case REQUEST_WRITE_EXTERNAL_STORAGE:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    //TODO
                    if (!GlobalClass.checkPermission(this, Manifest.permission.CAMERA)) {
                        requestPermission(this, REQUEST_CAMERA, Manifest.permission.CAMERA);
                    }
                }

                break;
            case REQUEST_CAMERA:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    //TODO
                }
                break;
        }
    }

    /*
For permission to access CONTACTS
*/
    public boolean isContactPermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG, "Permission is granted");
                return true;
            } else {

                Log.v(TAG, "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, REQUEST_READ_PHONE_STATE);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG, "Permission is granted");
            return true;
        }


    }

}
