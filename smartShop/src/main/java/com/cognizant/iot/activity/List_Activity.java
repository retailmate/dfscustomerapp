/*package com.cognizant.iot.activity;

import java.io.File;
import java.util.ArrayList;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cognizant.iot.ar.AR_activity;
import com.cognizant.iot.ar.R;
import com.cognizant.iot.json.AssetsExtracter;
import com.cognizant.iot.model.AssetModel;
import com.cognizant.iot.scanner.ZBarScannerActivity;

public class List_Activity extends Activity {

	public static ArrayList<AssetModel> assetList = new ArrayList<AssetModel>();
	MyassetAdapter myassetAdapter;
	ListView asset_list;
	RelativeLayout scanner_view;
	TextView count;
	AssetsExtracter mTask;
	String assetID;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		assetList = (ArrayList<AssetModel>) getIntent().getSerializableExtra(
				"list");

		assetID = getIntent().getStringExtra("assetID");
		setContentView(R.layout.list_layout);

		asset_list = (ListView) findViewById(R.id.asset_list);
		scanner_view = (RelativeLayout) findViewById(R.id.first_list);
		count = (TextView) findViewById(R.id.asset_count);

		scanner_view.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(getApplicationContext(),
						ZBarScannerActivity.class);
				startActivity(intent);

			}
		});

		System.out.println("LIST" + assetList.get(1).getAssetName());

		myassetAdapter = new MyassetAdapter(getApplicationContext(),
				R.layout.list_layout, assetList);
		asset_list.setAdapter(myassetAdapter);

		asset_list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub

				Intent intent = new Intent(getApplicationContext(),
						Details_Activity.class);
				intent.putExtra("assetData", assetList.get(position));
				startActivity(intent);

			}

		});

		if (assetID.equals("123456")) {
			finish();
			Intent intent = new Intent(getApplicationContext(),
					Details_Activity.class);
			intent.putExtra("assetData", assetList.get(0));
			startActivity(intent);
		} else if (assetID.equals("1234")) {
			finish();
			Intent intent = new Intent(getApplicationContext(),
					Details_Activity.class);
			intent.putExtra("assetData", assetList.get(1));
			startActivity(intent);
		} else if (assetID.equals("12345")) {
			finish();
			Intent intent = new Intent(getApplicationContext(),
					Details_Activity.class);
			intent.putExtra("assetData", assetList.get(2));
			startActivity(intent);
		} else if (assetID.equals("empty")) {

		} else {
			Toast.makeText(getApplicationContext(), "No assets details found",
					Toast.LENGTH_SHORT).show();
			finish();
		}
	}

	public class MyassetAdapter extends ArrayAdapter<AssetModel> {
		ArrayList<AssetModel> actioninfo = new ArrayList<AssetModel>();
		LayoutInflater inflater;
		Context context;

		public MyassetAdapter(Context context, int resource,
				ArrayList<AssetModel> actioninfo) {
			super(context, resource);
			// TODO Auto-generated constructor stub
			this.actioninfo = actioninfo;
			this.context = context;
		}

		@Override
		public void notifyDataSetChanged() // Create this function in your
											// adapter class
		{
			super.notifyDataSetChanged();
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return actioninfo.size();
		}

		@Override
		public AssetModel getItem(int position) {
			// TODO Auto-generated method stub
			return actioninfo.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return super.getItemId(position);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			View vitems = convertView;
			AssetModel action = actioninfo.get(position);
			ImageView assetImage;
			TextView assetName, assetPriority, assetTime;
			TextView distance;

			if (convertView == null) {
				inflater = (LayoutInflater) context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				vitems = inflater.inflate(R.layout.list_row_layout, parent,
						false);
			}

			assetImage = (ImageView) vitems.findViewById(R.id.assetimage);
			if (position == 0) {
				assetImage.setImageResource(R.drawable.tv_switch_icon);
			} else if (position == 1) {
				assetImage.setImageResource(R.drawable.heater_icon);
			} else if (position == 2) {
				assetImage.setImageResource(R.drawable.dish_tv_icon);
			} else if (position == 3) {
				assetImage.setImageResource(R.drawable.cd_player);
			}
			assetName = (TextView) vitems.findViewById(R.id.assetname);
			assetPriority = (TextView) vitems.findViewById(R.id.priority);
			assetTime = (TextView) vitems.findViewById(R.id.time);
			distance = (TextView) vitems.findViewById(R.id.distance);

			assetName.setText(assetList.get(position).getAssetName());
			assetPriority.setText(assetList.get(position).getAssetPriority());
			if (assetList.get(position).getAssetPriority().equals("High")) {
				assetPriority.setTextColor(Color.RED);
			} else {
				assetPriority.setTextColor(Color.YELLOW);
			}
			assetTime.setText("Issue raised at "
					+ assetList.get(position).getAssetTime());
			count.setText(getCount() + " " + "Assets found");
			distance.setText("Aprx. " + assetList.get(position).getDistance()
					+ "m");
			return vitems;
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.refresh, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		switch (item.getItemId()) {
		case R.id.refresh:

			// mTask = new AssetsExtracter("empty", List_Activity.this);
			// mTask.execute();
			// finish();
			myassetAdapter.notifyDataSetChanged();

			break;
		}
		return super.onOptionsItemSelected(item);
	}
}
*/