package com.cognizant.iot.activity;

import android.app.ProgressDialog;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import com.cognizant.iot.utils.AccountState;
import com.cognizant.retailmate.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import cz.msebera.android.httpclient.Header;

/**
 * Created by 452781 on 4/10/2017.
 */


public class PIV_activity extends AppCompatActivity {

    private static final String TAG = "video";
    private VideoView myVideoView;
    private int position = 0;
    private ProgressDialog progressDialog;
    private MediaController mediaControls;
    MediaPlayer mMediaPlayer;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Get the layout from video_main.xml
        setContentView(R.layout.activity_video);

        if (mediaControls == null) {
            mediaControls = new MediaController(PIV_activity.this);

        }

        // Find your VideoView in your video_main.xml layout
        myVideoView = (VideoView) findViewById(R.id.video_view);


        RequestParams requestParams = new RequestParams();
        requestParams.put("UserID", AccountState.getUserID());
        requestParams.put("Name", AccountState.getUserName());

        getPersonalizedVideo();

        if (AccountState.getOfflineMode()) {
            getPersonalizedVideo();
        } else {
            getPersonalizedVideo(requestParams);
        }
        // Create a progressbar
        progressDialog = new ProgressDialog(PIV_activity.this);
        progressDialog.setMessage("Creating your personalized video..");
        progressDialog.setCancelable(false);
        progressDialog.show();


        myVideoView.requestFocus();
        myVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            public void onPrepared(MediaPlayer mp) {
                progressDialog.dismiss();
                myVideoView.seekTo(position);
                if (position == 0) {
                    myVideoView.start();
                } else {
                    myVideoView.pause();
                }
            }
        });
    }


    private void getPersonalizedVideo(RequestParams params) {
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(60000);
        client.post("http://rmethapi.azurewebsites.net/api/CognitiveServices/GetPIVDetailsAPI", params,
                new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        try {
                            progressDialog.dismiss();
                            String response = new String(responseBody, "UTF-8");
                            JSONObject responseObject = new JSONObject(response);

                            if (responseObject.has("videoUrl")) {
                                Uri videoPath = Uri.parse(responseObject.getString("videoUrl"));
                                myVideoView.setMediaController(mediaControls);
                                myVideoView.setVideoURI(videoPath);
                            }
                        } catch (IOException io) {
                            io.printStackTrace();
                        } catch (JSONException | NullPointerException | ArrayIndexOutOfBoundsException je) {
                            Toast.makeText(
                                    getApplicationContext(),
                                    "Error Occured [Server's JSON response might be invalid]!",
                                    Toast.LENGTH_LONG).show();
                            je.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

                        // Hide Progress Dialog
                        progressDialog.dismiss();

                        // When Http response code is '404'
                        if (statusCode == 404) {
                            Toast.makeText(getApplicationContext(),
                                    "Requested resource not found",
                                    Toast.LENGTH_LONG).show();
                        }
                        // When Http response code is '500'
                        else if (statusCode == 500) {
                            Toast.makeText(getApplicationContext(),
                                    "Something went wrong at server end",
                                    Toast.LENGTH_LONG).show();
                        }
                        // When Http response code other than 404, 500
                        else {
                            Toast.makeText(
                                    getApplicationContext(),
                                    "Unexpected Error occcured! [Most common Error: Device might not be connected to Internet or remote server is not up and running]",
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putInt("Position", myVideoView.getCurrentPosition());
        myVideoView.pause();
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        position = savedInstanceState.getInt("Position");
        myVideoView.seekTo(position);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mediaControls.clearFocus();
        mediaControls.removeAllViews();
        mediaControls.destroyDrawingCache();

    }


    /*
    OFFLINE METHOD

     */

    //local video is placed in res/raw/piv_videoo.mp4
    private void getPersonalizedVideo() {
        String videoUrl = "android.resource://" + getPackageName() + "/" + R.raw.piv_video;
        Uri videoPath = Uri.parse(videoUrl);

        myVideoView.setMediaController(mediaControls);
        myVideoView.setVideoURI(videoPath);
    }


}
