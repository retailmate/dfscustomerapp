package com.cognizant.iot.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.cognizant.iot.orderhistory.activity.JSONfunctions_order;
import com.cognizant.iot.utils.Constants;
import com.cognizant.iot.utils.GlobalClass;
import com.cognizant.iot.utils.Utilsprod;
import com.cognizant.retailmate.R;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cz.msebera.android.httpclient.Header;

/**
 * Register Activity Class
 */
public class RegisterActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {

    RequestParams params;
    String token;
    // Progress Dialog Object
    ProgressDialog progressDialog;
    TextView selectedFavColors;
    // Name Edit View Object
    EditText mFirstName, mLastName, mPhoneNo, mEmail, mPassword, mPasswordConfirm, mIMEINumber;
    private GoogleApiClient mGoogleApiClient;
    private String googleAuthToken = null;
    private static final String TAG = "RegisterActivity";
    private static final int RC_GET_TOKEN = 9002;
    private ImageView addCategoryView, addColorView;
    private List<HashMap<String, Object>> mCategoriesList = new ArrayList<HashMap<String, Object>>();
    private List<HashMap<String, Object>> mColorsList = new ArrayList<HashMap<String, Object>>();
    private List<String> mCategories = new ArrayList<>();
    private List<String> mColors = new ArrayList<>();
    private List<Boolean> mCheckedColors = new ArrayList<>();
    private String mSelectedColorValues = "";

    private LinearLayout categoryScrollViewWrapper, knowledgeWrapper;
    private LayoutInflater mInflater;
    private AlertDialog alertDialogObject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_user_registration);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        mInflater = LayoutInflater.from(this);
        int titleId = getResources().getIdentifier("action_bar_title", "id",
                "android");

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.server_client_id))
                .requestEmail()
                .build();

        // Build GoogleAPIClient with the Google Sign-In API and the above options.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        // Button click listeners
        findViewById(R.id.sign_in_button).setOnClickListener(this);
        findViewById(R.id.signout_in_button).setOnClickListener(this);

        TextView abTitle = (TextView) findViewById(titleId);
        //abTitle.setTextColor(Color.parseColor("#ffffff"));

        mFirstName = (EditText) findViewById(R.id.firstname);
        mLastName = (EditText) findViewById(R.id.lastname);
        mPhoneNo = (EditText) findViewById(R.id.mobile);
        mEmail = (EditText) findViewById(R.id.email_register);
        mPassword = (EditText) findViewById(R.id.password_register);
        mPasswordConfirm = (EditText) findViewById(R.id.re_enter_pass);
        mIMEINumber = (EditText) findViewById(R.id.imei_number_display);
        addCategoryView = (ImageView) findViewById(R.id.addCategory);
        addColorView = (ImageView) findViewById(R.id.addColor);
        categoryScrollViewWrapper = (LinearLayout) findViewById(R.id.categoryScrollViewWrapper);
        knowledgeWrapper = (LinearLayout) findViewById(R.id.knowledgeWrapper);
        selectedFavColors = (TextView) findViewById(R.id.selectedFavColors);
        addCategoryView.setOnClickListener(this);
        addColorView.setOnClickListener(this);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String mIMEIValue = extras.getString("IMEI");
            mIMEINumber.setText(mIMEIValue);
        }

        // Instantiate Progress Dialog object
        progressDialog = new ProgressDialog(this);
        // Set Progress Dialog Text
        progressDialog.setMessage("Please wait...");
        // Set Cancelable as False
        progressDialog.setCancelable(false);
        getRegistrationCategoryAndColor();
    }


    private void getIdToken() {
        // Show an account picker to let the user choose a Google account from the device.
        // If the GoogleSignInOptions only asks for IDToken and/or profile and/or email then no
        // consent screen will be shown here.
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_GET_TOKEN);
    }

    /**
     * Method gets triggered when Register button is clicked
     *
     * @param view
     */
    public void registerUser(View view) {

        // Get NAme ET control value
        String fname = mFirstName.getText().toString();
        String lname = mLastName.getText().toString();
        String mobile = mPhoneNo.getText().toString();
        // Get Email ET control value
        String email = mEmail.getText().toString();
        // Get Password ET control value
        String password = mPassword.getText().toString();
        String imei = mIMEINumber.getText().toString();
        String re_password = mPasswordConfirm.getText().toString();

        // Instantiate Http Request Param Object
        params = new RequestParams();
        // When Name Edit View, Email Edit View and Password Edit View have
        // values other than Null
        if (Test_Utility.isNotNull(fname) && Test_Utility.isNotNull(lname)
                && Test_Utility.isNotNull(mobile)
                && Test_Utility.isNotNull(email))

        {
            // When Email entered is Valid
            if (mobile.length() == 10) {
                if (Test_Utility.validate(email)) {

                    if (googleAuthToken != null) {
                        postData();
                    } else {
                        if (Test_Utility.isNotNull(lname) && Test_Utility.isNotNull(re_password) && re_password.equals(password)) {
                            params.put("FirstName", fname);
                            params.put("LastName", lname);
                            params.put("Phone", mobile);
                            params.put("Email", email);
                            new DownloadJSONAXSession().execute();
                        } else {
                            Toast.makeText(getApplicationContext(), "Password mismatch !!!", Toast.LENGTH_LONG).show();
                        }
                    }

                }

                // When Email is invalid
                else {
                    Toast.makeText(getApplicationContext(),
                            "Please enter valid email", Toast.LENGTH_LONG)
                            .show();
                }
            } else
                Toast.makeText(getApplicationContext(),
                        "Please enter only 10 digits for mobile number",
                        Toast.LENGTH_LONG).show();

        }
        // When any of the Edit View control left blank
        else {
            Toast.makeText(getApplicationContext(),
                    "Please fill the form, don't leave any field blank",
                    Toast.LENGTH_LONG).show();
        }

    }

    /**
     * Method that performs RESTful webservice invocations
     *
     * @param params
     * @return
     */
    public void registerService(RequestParams params) {

        // Toast.makeText(getApplicationContext(),"JSON request params is" +
        // params.toString(),Toast.LENGTH_LONG).show();

        AsyncHttpClient client = new AsyncHttpClient(true, 80, 443);
        client.addHeader("Authorization", "id_token " + token);
//		client.addHeader(
//				"DeviceToken",
//				"HOUSTON-20:QwLAwLPNYGMWHtEda7Lm792/PnwNbbjlIvphlaFk8lLoRYnQocGf1itJzWzui4/t3DtzO58iOKDfA9dXVJUz1Q==:5637144592:5637146202");

        client.addHeader(
                "DeviceToken",
                "HOUSTON-20:tmOvkmUrFIBWY2ypKGc/Y7KKuIy2/LHEcsouvq7vZsUIHJ8EOy40Z+P38LUtjqK5awQ6rVVn8FspYdDjGmQLkg==:5637144592:5637146202");

        // String registration =
        // "http://c3a2351402de471d85e8257dd5d2f031.cloudapp.net/api/UserManagement/CreateUser";
        // String registrationNew =
        // "http://c3a2351402de471d85e8257dd5d2f031.cloudapp.net/api/UserManagement/CreateAllergicUser";
        String registrationNew = "https://etihaddemodevret.cloudax.dynamics.com/Commerce/Customers?api-version=7.1";

        client.post(registrationNew, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                // Hide Progress Dialog
                progressDialog.hide();

                try {
                    String response = new String(responseBody, "UTF-8");

                    System.out.println("@@#$# RESPONSE OF REGISTRATION" + response);

                    // JSON Object
                    JSONObject obj = new JSONObject(response);

                    String str = (String) obj.getString("AccountNumber");

                    Toast.makeText(
                            getApplicationContext(),
                            "You are successfully registered!\nYour Customer ID is = "
                                    + str, Toast.LENGTH_LONG).show();

                    finish();

                    setDefaultValues();
                } catch (IOException io) {
                    io.printStackTrace();
                } catch (JSONException je) {
                    Toast.makeText(
                            getApplicationContext(),
                            "Error Occured [Server's JSON response might be invalid]!",
                            Toast.LENGTH_LONG).show();
                    je.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

                // Hide Progress Dialog
                progressDialog.hide();


                if (statusCode == 404) {
                    Toast.makeText(getApplicationContext(),
                            "Requested resource not found", Toast.LENGTH_LONG)
                            .show();
                }
                // When Http response code is '500'
                else if (statusCode == 500) {
                    Toast.makeText(getApplicationContext(),
                            "Something went wrong at server end",
                            Toast.LENGTH_LONG).show();
                }
                // When Http response code other than 404, 500
                else {
                    System.out.println("@@## statusCode " + statusCode + " "
                            + error);
                    Toast.makeText(getApplicationContext(),
                            "Check Internet Connection", Toast.LENGTH_LONG)
                            .show();
                }
            }
        });

    }

    /**
     * Method which navigates from Register Activity to Login Activity
     */
    public void navigatetoLoginActivity(View view) {
        Intent loginIntent = new Intent(getApplicationContext(),
                Login_Screen.class);
        // Clears History of Activity
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(loginIntent);
    }

    /**
     * Set default values for Edit View controls
     */
    public void setDefaultValues() {
        mFirstName.setText("");
        mLastName.setText("");
        mPhoneNo.setText("");
        mEmail.setText("");
        mPassword.setText("");
        mPasswordConfirm.setText("");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar app icon/title

        // Handle action bar actions click
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_in_button:
                progressDialog.show();
                getIdToken();
                break;
            case R.id.signout_in_button:
                signOut();
                break;
            case R.id.addCategory:
                showCategoriesDialog();
                break;
            case R.id.addColor:
                showColorsDialog();
                break;
        }
    }

	/*
     * FOR GETTING SESSION ID
	 */

    private class DownloadJSONAXSession extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Show Progress Dialog
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            // Retrieve JSON Objects from the given URL address
            JSONObject jsonobject = JSONfunctions_order
                    .getJSONfromAXURLACESSTOKEN("https://etihaddemodevret.cloudax.dynamics.com/Auth/token");

            try {
                token = jsonobject.getString("access_token");

            } catch (JSONException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {

            registerService(params);
        }
    }

    private void sendTokenByMail() {
        Date today = Calendar.getInstance().getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy hh:mm");
        String folderName = formatter.format(today);
        // (4) this prints "Folder Name = 2009-09-06-08.23.23"
        System.out.println("Folder Name = " + folderName);

        Log.d("@@##", "sendTokenByMail");
        Intent mailer = new Intent(Intent.ACTION_SEND);
        mailer.setType("text/plain");
        mailer.putExtra(Intent.EXTRA_EMAIL, new String[]{"mathi.r@cognizant.com"});
        mailer.putExtra(Intent.EXTRA_SUBJECT, "Google Auth Token - " + folderName);
        mailer.putExtra(Intent.EXTRA_TEXT, googleAuthToken);
        mailer.setType("message/rfc822");
        startActivity(Intent.createChooser(mailer, "Send email..."));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_GET_TOKEN) {
            // [START get_id_token]
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            Log.d(TAG, "onActivityResult:GET_TOKEN:success:" + result.getStatus().isSuccess());
            progressDialog.hide();
            if (result.isSuccess()) {
                GoogleSignInAccount acct = result.getSignInAccount();
                googleAuthToken = acct.getIdToken();
                String personName = acct.getDisplayName();
                String personEmail = acct.getEmail();
                String personId = acct.getId();
                Uri personPhoto = acct.getPhotoUrl();
                // Show signed-in UI.
                Log.d(TAG, "googleAuthToken:\n" + googleAuthToken);
                Log.d(TAG, "personName:\n" + personName);
                Log.d(TAG, "personEmail:\n" + personEmail);
                Log.d(TAG, "personId:\n" + personId);
                String[] mSplitedName = personName.split("\\s+");
                if (mSplitedName[0] != null && !mSplitedName[0].isEmpty()) {
                    mFirstName.setText(mSplitedName[0]);
                }

                if (mSplitedName[1] != null && !mSplitedName[1].isEmpty()) {
                    mLastName.setText(mSplitedName[1]);
                }
                mEmail.setText(personEmail);

                mPasswordConfirm.setVisibility(View.GONE);
                mPassword.setVisibility(View.GONE);

                sendTokenByMail();

            } else {
                // Show signed-out UI.
                //updateUI(false);
                Log.d(TAG, "idToken false");
            }
        }
    }

    private void postData() {
        progressDialog.show();
        String url = Constants.REGISTRATION_ENDPOINT + googleAuthToken;

        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (progressDialog != null) {
                            progressDialog.hide();
                        }
                        // response
                        Log.d("@@##", "response = " + response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.has("Data")) {
                                JSONObject responseData = (JSONObject) jsonObject.get("Data");
                                if (responseData.has("success")) {
                                    if (responseData.getBoolean("success")) {
                                        Toast.makeText(RegisterActivity.this, "Registration success. Please login to proceed.", Toast.LENGTH_SHORT).show();
                                        RegisterActivity.this.finish();
                                    } else {
                                        Toast.makeText(RegisterActivity.this, "Registration failed. Please try again.", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                        } catch (JSONException je) {

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (progressDialog != null) {
                            progressDialog.hide();
                        }
                        // error
                        Log.d("@@##", "VolleyError = " + error);
                        if (error.networkResponse != null) {
                            Log.d("@@##", "Error: " + error
                                    + "\nstatusCode >> " + error.networkResponse.statusCode
                                    + "\ndata >> " + error.networkResponse.data.toString()
                                    + "\ndata class >> " + error.networkResponse.data.getClass()
                                    + "\ngetCause >> " + error.getCause()
                                    + "\ngetMessage >> " + error.getMessage());

                            byte[] byteArray = error.networkResponse.data;
                            try {
                                String byteArrayStr = new String(byteArray, "UTF-8");
                                Log.d("@@##", "byteArrayStr = " + byteArrayStr);
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }

                    }
                }
        ) {

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                String output = "";
                JSONObject rootObject = new JSONObject();

                try {
                    rootObject.put("AccountNumber", "00412");
                    rootObject.put("FirstName", mFirstName.getText().toString());
                    rootObject.put("LastName", mLastName.getText().toString());
                    rootObject.put("Email", mEmail.getText().toString());
                    rootObject.put("Phone", mPhoneNo.getText().toString());
                    rootObject.put("PhoneIMEI", mIMEINumber.getText().toString());
                    //rootObject.put("PhoneIMEI", "1234567890");
                    rootObject.put("Category", Utilsprod.getSelectedCategoriesInString(mCategoriesList));
                    //rootObject.put("CategoryHierarchy", "Retail Product Category");
                    rootObject.put("favColor", Utilsprod.getFavColorString(mSelectedColorValues));
                    Log.d("@@##", "SelectedCategoriesInString = " + Utilsprod.getSelectedCategoriesInString(mCategoriesList));
                    Log.d("@@##", "SelectedColorsInString = " + Utilsprod.getFavColorString(mSelectedColorValues));
                    output = rootObject.toString();
                    Log.d("@@##", "output = \n" + output);
                } catch (JSONException je) {

                }

                try {
                    return output.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                return null;
            }

        };

        // Adding request to request queue
        postRequest.setShouldCache(false);
        postRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        GlobalClass.getInstance().addToRequestQueue(postRequest, Constants.REGISTRATION_REQUEST);
    }

    private void getRegistrationCategoryAndColor() {
        progressDialog.show();
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, Constants.REGISTRATION_CATEGORY_ENDPOINT, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("@@##", "response = " + response);
                        //Toast.makeText(RegisterActivity.this, response.toString(), Toast.LENGTH_LONG).show();
                        if (progressDialog != null) {
                            progressDialog.hide();
                        }

                        try {
                            if (response.has("CustomerRegistration")) {
                                JSONObject customerRegistrationObject = response.getJSONObject("CustomerRegistration");
                                readCategoriesFromJSON(customerRegistrationObject);
                            }

                            if (response.has("FavoriteColor")) {
                                JSONObject favoriteColorObject = response.getJSONObject("FavoriteColor");
                                readColorsFromJSON(favoriteColorObject);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("@@##", "error = " + error);
                        Toast.makeText(RegisterActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                        if (progressDialog != null) {
                            progressDialog.hide();
                        }
                    }
                });

        jsonRequest.setShouldCache(false);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        GlobalClass.getInstance().addToRequestQueue(jsonRequest, Constants.REGISTRATION_CATEGORY_REQUEST);
    }

    private void readColorsFromJSON(JSONObject jsonRootObject) {
        mColorsList = new ArrayList<>();
        mColors = new ArrayList<>();
        mCheckedColors = new ArrayList<>();
        try {
            //Get the instance of JSONArray that contains JSONObjects
            JSONArray jsonArray = jsonRootObject.optJSONArray("value");
            //Iterate the jsonArray and print the info of JSONObjects
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);

                String colorName = jsonObject.optString("ColorId").toString();
                HashMap<String, Object> mColorMap = new HashMap<>();
                mColorMap.put("ColorId", colorName);
                mColorMap.put("IsSelected", false);
                mColors.add(colorName);
                mCheckedColors.add(false);
                mColorsList.add(mColorMap);
                Log.d("@@##", "ColorId = " + jsonObject.optString("ColorId").toString());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void showColorsDialog() {
        final CharSequence[] colors = mColors.toArray(new String[mColors.size()]);
        final boolean[] checkedColors = Utilsprod.convertBoolean(mCheckedColors);
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle("Favourite Colors");

        dialogBuilder.setMultiChoiceItems(colors, checkedColors, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                mCheckedColors.set(which, isChecked);
            }
        });

        // Set the positive/yes button click listener
        dialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                selectedFavColors.setText(getCheckedColorValues(checkedColors, colors));
            }
        });

        //Create alert dialog object via builder
        alertDialogObject = dialogBuilder.create();
        //Show the dialog
        alertDialogObject.show();
    }

    private String getCheckedColorValues(boolean[] checkedColors, CharSequence[] colors) {
        mSelectedColorValues = "";
        for (int i = 0; i < checkedColors.length; i++) {
            boolean checked = checkedColors[i];
            if (checked) {
                if (mSelectedColorValues.isEmpty()) {
                    mSelectedColorValues = (String) colors[i];
                } else {
                    mSelectedColorValues += "," + (String) colors[i];
                }
            }
            selectedFavColors.setText(mSelectedColorValues);
        }
        return mSelectedColorValues;
    }

    private void readCategoriesFromJSON(JSONObject jsonRootObject) {
        mCategoriesList = new ArrayList<>();

        try {
            //Get the instance of JSONArray that contains JSONObjects
            JSONArray jsonArray = jsonRootObject.optJSONArray("value");

            //Iterate the jsonArray and print the info of JSONObjects
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);

                HashMap<String, Object> mMap = new HashMap<>();
                mMap.put("Category", jsonObject.optString("Category").toString());
                mMap.put("CategoryHierarchy", jsonObject.optString("CategoryHierarchy"));
                mMap.put("HasKnowledge", false);
                if (jsonObject.has("knowledgeLevelRequired")) {
                    if (jsonObject.optString("knowledgeLevelRequired").equalsIgnoreCase("Yes")) {
                        mMap.put("HasKnowledge", true);
                    }
                }
                mMap.put("IsSelected", false);
                mMap.put("KnowledgeLevel", 1);
                mCategoriesList.add(mMap);
                mCategories.add(jsonObject.optString("Category").toString());
                //Log.d("@@##", "Category = "+jsonObject.optString("Category").toString()+"\n CategoryHierarchy = "+jsonObject.optString("CategoryHierarchy")+",\n knowledgeLevelRequired = "+jsonObject.optString("knowledgeLevelRequired")+"\nHasKnowledge = "+(boolean) mMap.get("HasKnowledge"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void showCategoriesDialog() {
        final CharSequence[] categories = mCategories.toArray(new String[mCategories.size()]);
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle("Category");
        dialogBuilder.setItems(categories, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                String selectedText = categories[item].toString();  //Selected item in listview

                //add flag in master list
                HashMap<String, Object> selectedCategory = mCategoriesList.get(item);
                if (selectedCategory != null) {
                    if (selectedCategory.get("IsSelected") != null) {
                        boolean isSelected = (boolean) selectedCategory.get("IsSelected");
                        if (!isSelected) {
                            selectedCategory.put("IsSelected", true);
                            addCategoryItem(item, selectedCategory);
                        }
                    }
                }

                alertDialogObject.hide();
            }
        });
        //Create alert dialog object via builder
        alertDialogObject = dialogBuilder.create();
        //Show the dialog
        alertDialogObject.show();
    }

    private void addCategoryItem(int index, HashMap<String, Object> selectedCategory) {
        View view = mInflater.inflate(R.layout.preference_category_item, null, false);
        view.setTag(index);
        TextView categoryName = (TextView) view.findViewById(R.id.categoryName);
        String categoryValue = "N/A";

        if (categoryValue != null) {
            categoryValue = (String) selectedCategory.get("Category");
        }

        categoryName.setText((String) categoryValue);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = (int) view.getTag();
                removeCategoryItem(position, view);
            }
        });
        int childCount = categoryScrollViewWrapper.getChildCount();
        categoryScrollViewWrapper.addView(view, (childCount - 1));
        if (selectedCategory.get("HasKnowledge") != null) {
            if ((boolean) selectedCategory.get("HasKnowledge")) {
                addCategoryKnowledge(categoryValue, index);
            }
        }
    }

    private void addCategoryKnowledge(String categoryValue, int index) {
        View view = mInflater.inflate(R.layout.preference_category_knowledge, null, false);
        view.setTag(index);
        RadioGroup knowledgeRadioGroup = (RadioGroup) view.findViewById(R.id.knowledgeRadioGroup);
        knowledgeRadioGroup.setTag(index);
        knowledgeRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                Log.d("@@##", "group = " + group + ", checkedId = " + checkedId + ", group tag = " + group.getTag());
                RadioButton mRadioButton = (RadioButton) findViewById(checkedId);

                String selectedString = (String) mRadioButton.getText();
                int knowledgeLevelInt = 3;
                if (selectedString.equalsIgnoreCase("Medium")) {
                    knowledgeLevelInt = 2;
                } else if (selectedString.equalsIgnoreCase("High")) {
                    knowledgeLevelInt = 3;
                }

                //update knowledgeLevel for category
                if (group.getTag() != null) {
                    int categoryIndex = (int) group.getTag();
                    HashMap<String, Object> selectedCategory = mCategoriesList.get(categoryIndex);
                    selectedCategory.put("KnowledgeLevel", knowledgeLevelInt);
                }

            }
        });
        TextView categoryTitle = (TextView) view.findViewById(R.id.categoryTitle);
        categoryTitle.setText("Knowledge level for " + categoryValue);
        knowledgeWrapper.addView(view);
    }

    private void removeCategoryItem(int index, View view) {
        //update flag in master list
        HashMap<String, Object> selectedCategory = mCategoriesList.get(index);
        if (selectedCategory != null) {
            selectedCategory.put("IsSelected", false);
            selectedCategory.put("KnowledgeLevel", 1);
        }
        categoryScrollViewWrapper.removeView(view);
        removeCategoryKnowledge(index);
    }

    private void removeCategoryKnowledge(int index) {
        int knowledgeWrapperChildCount = knowledgeWrapper.getChildCount();
        for (int i = 0; i < knowledgeWrapperChildCount; i++) {
            View childView = knowledgeWrapper.getChildAt(i);
            if (childView != null && childView.getTag() != null) {
                int childViewIndex = (int) childView.getTag();
                if (childViewIndex == index) {
                    Log.d("@@##", "delete child at = " + childViewIndex);
                    knowledgeWrapper.removeView(childView);
                }
            }
        }
    }

    public void signOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        Log.d(TAG, "signOut:onResult:" + status);
                    }
                });
    }
}