package com.cognizant.iot.homepage.models.productsearch;

/**
 * Created by 452781 on 2/22/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductSearchVariantItemModel {

    @SerializedName("ItemId")
    @Expose
    private String itemId;
    @SerializedName("MasterProductId")
    @Expose
    private Integer masterProductId;
    @SerializedName("InventoryDimensionId")
    @Expose
    private String inventoryDimensionId;
    @SerializedName("AdjustedPrice")
    @Expose
    private Integer adjustedPrice;
    @SerializedName("BasePrice")
    @Expose
    private Integer basePrice;
    @SerializedName("Price")
    @Expose
    private Integer price;
    @SerializedName("Color")
    @Expose
    private String color;
    @SerializedName("Style")
    @Expose
    private String style;
    @SerializedName("Size")
    @Expose
    private String size;
    @SerializedName("Configuration")
    @Expose
    private String configuration;
    @SerializedName("ColorId")
    @Expose
    private String colorId;
    @SerializedName("StyleId")
    @Expose
    private String styleId;
    @SerializedName("SizeId")
    @Expose
    private String sizeId;
    @SerializedName("ConfigId")
    @Expose
    private String configId;
    @SerializedName("VariantId")
    @Expose
    private String variantId;

    @SerializedName("DistinctProductVariantId")
    @Expose
    private Integer distinctProductVariantId;
    @SerializedName("ProductNumber")
    @Expose
    private String productNumber;

    @SerializedName("ExtensionProperties")
    @Expose
    private List<Object> extensionProperties = null;

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public Integer getMasterProductId() {
        return masterProductId;
    }

    public void setMasterProductId(Integer masterProductId) {
        this.masterProductId = masterProductId;
    }

    public String getInventoryDimensionId() {
        return inventoryDimensionId;
    }

    public void setInventoryDimensionId(String inventoryDimensionId) {
        this.inventoryDimensionId = inventoryDimensionId;
    }

    public Integer getAdjustedPrice() {
        return adjustedPrice;
    }

    public void setAdjustedPrice(Integer adjustedPrice) {
        this.adjustedPrice = adjustedPrice;
    }

    public Integer getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(Integer basePrice) {
        this.basePrice = basePrice;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getConfiguration() {
        return configuration;
    }

    public void setConfiguration(String configuration) {
        this.configuration = configuration;
    }

    public String getColorId() {
        return colorId;
    }

    public void setColorId(String colorId) {
        this.colorId = colorId;
    }

    public String getStyleId() {
        return styleId;
    }

    public void setStyleId(String styleId) {
        this.styleId = styleId;
    }

    public String getSizeId() {
        return sizeId;
    }

    public void setSizeId(String sizeId) {
        this.sizeId = sizeId;
    }

    public String getConfigId() {
        return configId;
    }

    public void setConfigId(String configId) {
        this.configId = configId;
    }

    public String getVariantId() {
        return variantId;
    }

    public void setVariantId(String variantId) {
        this.variantId = variantId;
    }


    public Integer getDistinctProductVariantId() {
        return distinctProductVariantId;
    }

    public void setDistinctProductVariantId(Integer distinctProductVariantId) {
        this.distinctProductVariantId = distinctProductVariantId;
    }

    public String getProductNumber() {
        return productNumber;
    }

    public void setProductNumber(String productNumber) {
        this.productNumber = productNumber;
    }


    public List<Object> getExtensionProperties() {
        return extensionProperties;
    }

    public void setExtensionProperties(List<Object> extensionProperties) {
        this.extensionProperties = extensionProperties;
    }

}