package com.cognizant.iot.homepage.models.wishlist;

/**
 * Created by 452781 on 2/23/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WishlistBriefDataModel {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("wishlistdata")
    @Expose
    private WishlistDataModel wishlistdata;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public WishlistDataModel getWishlistdata() {
        return wishlistdata;
    }

    public void setWishlistdata(WishlistDataModel wishlistdata) {
        this.wishlistdata = wishlistdata;
    }

}