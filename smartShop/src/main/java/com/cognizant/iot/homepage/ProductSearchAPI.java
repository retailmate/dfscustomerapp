package com.cognizant.iot.homepage;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.cognizant.iot.Network.VolleyHelper;
import com.cognizant.iot.Network.VolleyRequest;
import com.cognizant.iot.homepage.models.cart.CartModel;
import com.cognizant.iot.homepage.models.productsearch.ProductSearchModel;
import com.cognizant.iot.homepage.models.wishlist.WishlistModel;
import com.cognizant.iot.utils.AccountState;
import com.cognizant.iot.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 452781 on 2/22/2017.
 */
public class ProductSearchAPI {

    Context context;
    List<String> productArray;
    ProductSearchModel productSearchModel;
    String[] variantIDs;
    String type;

    String lineid;

    public ProductSearchAPI(Context context, List<String> productArray, String type) {
        this.context = context;
        this.productArray = productArray;
        this.type = type;
    }

    public ProductSearchAPI(Context context, String lineid) {
        this.context = context;
        this.lineid = lineid;
    }

    public void makeaCall() {

        String[] products = new String[(productArray.size())];
        for (int i = 0; i < productArray.size(); i++) {
            products[i] = productArray.get(i);

        }


        Map<String, String> headers = new HashMap<>();
        JSONObject finalObject = null;
        try {

            headers.put("Content-Type", "application/json");
            headers.put("Authorization", "id_token " + AccountState.getTokenID());
            Log.e("TAG", "AccountState.getTokenID()" + AccountState.getTokenID());
            headers.put(
                    "OUN",
                    "092");

            JSONArray parentData = new JSONArray();

            for (int i = 0; i < products.length; i++) {


                parentData.put(Double.valueOf(products[i]));
            }

            JSONObject childData = new JSONObject();

            childData.put("Ids", parentData);
            childData.put("DataLevelValue", 4);

            finalObject = new JSONObject();
            finalObject.put("productSearchCriteria", childData);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        VolleyRequest<ProductSearchModel> recommendationModelVolleyRequest =
                new VolleyRequest<ProductSearchModel>(Request.Method.POST, Constants.PRODUCT_SEARCH_BY_ID_API, ProductSearchModel.class, headers, finalObject,

                        new com.android.volley.Response.Listener<ProductSearchModel>() {
                            @Override
                            public void onResponse(ProductSearchModel response) {
                                Log.e("Your Recommendations \n", "" + response.getProductSearchValueModel().size());
                                productSearchModel = response;
                                variantIDs = new String[productSearchModel.getProductSearchValueModel().size()];
                                for (int i = 0; i < productSearchModel.getProductSearchValueModel().size(); i++) {
                                    try {
                                        variantIDs[i] = productSearchModel.getProductSearchValueModel().get(i).getCompositionInformation().getVariantInformation().getVariants().get(0).getProductNumber();
                                    } catch (NullPointerException e) {
                                        variantIDs[i] = productSearchModel.getProductSearchValueModel().get(i).getRecordId();
                                    }

                                    System.out.println("@@## variantID" + variantIDs[i]);
                                }

                                if (type.equals("cart")) {
                                    makeCartCall(variantIDs[0]);
                                } else if (type.equals("wishlist")) {
                                    makeWishlistCall(variantIDs[0]);
                                }
                            }
                        }
                        //API CALL GETTING MADE FOR Dynamic Version contd

                        , new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null && networkResponse.statusCode != 200) {
                            Log.e("TAG", "No network on update.");
                            Toast.makeText(context, "Network Error, please check internet", Toast.LENGTH_SHORT).show();

                        }

                    }
                });


        RetryPolicy policy = new DefaultRetryPolicy(VolleyHelper.Timeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        recommendationModelVolleyRequest.setRetryPolicy(policy);
        VolleyHelper.getInstance(context).addToRequestQueue(recommendationModelVolleyRequest);


    }


    public void makeCartCall(String productId) {

        String[] products = new String[(productArray.size())];
        for (int i = 0; i < productArray.size(); i++) {
            products[i] = productArray.get(i);

        }


        Map<String, String> headers = new HashMap<>();
        JSONObject finalObject = null;
        try {

            headers.put("Content-Type", "application/json");


            finalObject = new JSONObject();
            finalObject.put("ProductId", productId);
            finalObject.put("Quantity", 1);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        VolleyRequest<CartModel> recommendationModelVolleyRequest =
                new VolleyRequest<CartModel>(Request.Method.POST, Constants.CART_ADD_ITEM_REQUEST + AccountState.getTokenID(), CartModel.class, headers, finalObject,

                        new com.android.volley.Response.Listener<CartModel>() {
                            @Override
                            public void onResponse(CartModel response) {

                                Toast.makeText(context, "cart " + response.getCartBriefDataModel().getSuccess(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        //API CALL GETTING MADE FOR Dynamic Version contd

                        , new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null && networkResponse.statusCode != 200) {
                            Log.e("TAG", "No network on update.");
                            Toast.makeText(context, "Network Error, please check internet", Toast.LENGTH_SHORT).show();

                        }

                    }
                });


        RetryPolicy policy = new DefaultRetryPolicy(VolleyHelper.Timeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        recommendationModelVolleyRequest.setRetryPolicy(policy);
        VolleyHelper.getInstance(context).addToRequestQueue(recommendationModelVolleyRequest);


    }

    public void makeWishlistCall(String productId) {

        String[] products = new String[(productArray.size())];
        for (int i = 0; i < productArray.size(); i++) {
            products[i] = productArray.get(i);

        }


        Map<String, String> headers = new HashMap<>();
        JSONObject finalObject = null;
        try {

            headers.put("Content-Type", "application/json");


            finalObject = new JSONObject();
            finalObject.put("ProductId", productId);
            finalObject.put("Quantity", 1);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        VolleyRequest<WishlistModel> recommendationModelVolleyRequest =
                new VolleyRequest<WishlistModel>(Request.Method.POST, Constants.WISHLIST_ADD_ITEM_REQUEST + AccountState.getTokenID(), WishlistModel.class, headers, finalObject,

                        new com.android.volley.Response.Listener<WishlistModel>() {
                            @Override
                            public void onResponse(WishlistModel response) {

                                Toast.makeText(context, "wishlist " + response.getData().getSuccess(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        //API CALL GETTING MADE FOR Dynamic Version contd

                        , new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null && networkResponse.statusCode != 200) {
                            Log.e("TAG", "No network on update.");
                            Toast.makeText(context, "Network Error, please check internet", Toast.LENGTH_SHORT).show();

                        }

                    }
                });


        RetryPolicy policy = new DefaultRetryPolicy(VolleyHelper.Timeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        recommendationModelVolleyRequest.setRetryPolicy(policy);
        VolleyHelper.getInstance(context).addToRequestQueue(recommendationModelVolleyRequest);


    }

    public void makeWishlistdelteCall() {


        Map<String, String> headers = new HashMap<>();
        JSONObject finalObject = null;
        try {

            headers.put("Content-Type", "application/json");


            finalObject = new JSONObject();
            finalObject.put("lineid", lineid);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        VolleyRequest<WishlistModel> wishlistremoveVolleyRequest =
                new VolleyRequest<WishlistModel>(Request.Method.POST, Constants.WISHLIST_REMOVE_ITEM_REQUEST + AccountState.getTokenID(), WishlistModel.class, headers, finalObject,

                        new com.android.volley.Response.Listener<WishlistModel>() {
                            @Override
                            public void onResponse(WishlistModel response) {

                                Toast.makeText(context, "wishlist item deleted", Toast.LENGTH_SHORT).show();
                            }
                        }
                        //API CALL GETTING MADE FOR Dynamic Version contd

                        , new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null && networkResponse.statusCode != 200) {
                            Log.e("TAG", "No network on update.");
                            Toast.makeText(context, "Network Error, please check internet", Toast.LENGTH_SHORT).show();

                        }

                    }
                });


        RetryPolicy policy = new DefaultRetryPolicy(VolleyHelper.Timeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        wishlistremoveVolleyRequest.setRetryPolicy(policy);
        VolleyHelper.getInstance(context).addToRequestQueue(wishlistremoveVolleyRequest);


    }

    public void makecartdeleteCall() {


        Map<String, String> headers = new HashMap<>();
        JSONObject finalObject = null;
        try {

            headers.put("Content-Type", "application/json");


            finalObject = new JSONObject();
            finalObject.put("lineid", lineid);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        VolleyRequest<CartModel> cartremoveVolleyRequest =
                new VolleyRequest<CartModel>(Request.Method.POST, Constants.CART_REMOVE_ITEM_REQUEST + AccountState.getTokenID(), CartModel.class, headers, finalObject,

                        new com.android.volley.Response.Listener<CartModel>() {
                            @Override
                            public void onResponse(CartModel response) {

                                Toast.makeText(context, "cart item deleted ", Toast.LENGTH_SHORT).show();
                            }
                        }
                        //API CALL GETTING MADE FOR Dynamic Version contd

                        , new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null && networkResponse.statusCode != 200) {
                            Log.e("TAG", "No network on update.");
                            Toast.makeText(context, "Network Error, please check internet", Toast.LENGTH_SHORT).show();

                        }

                    }
                });


        RetryPolicy policy = new DefaultRetryPolicy(VolleyHelper.Timeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        cartremoveVolleyRequest.setRetryPolicy(policy);
        VolleyHelper.getInstance(context).addToRequestQueue(cartremoveVolleyRequest);


    }

}
