package com.cognizant.iot.homepage.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.cognizant.iot.Network.VolleyHelper;
import com.cognizant.iot.Network.VolleyRequest;
import com.cognizant.iot.homepage.adapter.ProductListRecyclerAdapter;
import com.cognizant.iot.homepage.models.productlist.ProductListModel;
import com.cognizant.iot.utils.AccountState;
import com.cognizant.iot.utils.Constants;
import com.cognizant.retailmate.R;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;


public class ProductCategoryFragment extends Fragment {

    private GridLayoutManager lLayout;

    String categoryID;
    Context context;

    ProductListModel productListModel = new ProductListModel();
    public ProductListRecyclerAdapter rcAdapter;
    RecyclerView rv;
    View rootView;
    String categoryName;

//    private List<ItemObject> getAllItemList() {
//
//        List<ItemObject> allItems = new ArrayList<ItemObject>();
//        allItems.add(new ItemObject("specs1", R.drawable.retail_mate_app_icon));
//        allItems.add(new ItemObject("spec2", R.drawable.retail_mate_app_icon));
//        allItems.add(new ItemObject("Uni", R.drawable.retail_mate_app_icon));
//        allItems.add(new ItemObject("Gy", R.drawable.retail_mate_app_icon));
//        allItems.add(new ItemObject("Sn", R.drawable.retail_mate_app_icon));
//        Log.e("##@@", String.valueOf(allItems.size()));
//
//
//        return allItems;
//    }

    public ProductCategoryFragment() {

    }


    public ProductCategoryFragment(Context context, String name, String recordId) {
        this.context = context;
        categoryID = recordId;
        categoryName = name;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
//        getAllItemList();
        lLayout = new GridLayoutManager(getContext(), 2);


//        List<ItemObject> rowListItem = getAllItemList();


        Gson gson = new Gson();
        productListModel = gson.fromJson(getResponse(), ProductListModel.class);

        rootView = inflater.inflate(R.layout.homepage_category1, container, false);

        rv = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        rv.setHasFixedSize(true);

        rcAdapter = new ProductListRecyclerAdapter(getContext(), productListModel.getValue(), categoryName);
        rv.setAdapter(rcAdapter);


        rv.setLayoutManager(lLayout);
        productlistApiCall();
        return rootView;
    }

    void productlistApiCall() {


        Map<String, String> headers = new HashMap<>();
        JSONObject params = null;


        headers.put("Authorization", "id_token " + AccountState.getTokenID());

        headers.put(
                "OUN",
                "092");


        VolleyRequest<ProductListModel> productListVolleyRequest =
                new VolleyRequest<ProductListModel>(Request.Method.GET, Constants.PRODUCT_LIST_BY_CATEGORY_API + categoryID + ")?$top=60&api-version=7.1", ProductListModel.class, headers,

                        new com.android.volley.Response.Listener<ProductListModel>() {
                            @Override
                            public void onResponse(ProductListModel response) {
                                Log.e("TAG", "Product list " + response.getValue().get(0));
                                System.out.println("@@## productlistApiCall" + response.getValue().get(0));


                                productListModel = response;
                                rcAdapter = new ProductListRecyclerAdapter(getContext(), productListModel.getValue(), categoryName);
                                rv.setAdapter(rcAdapter);
                                rv.getAdapter().notifyDataSetChanged();
                                rcAdapter.notifyDataSetChanged();


                            }
                        }
                        //API CALL GETTING MADE FOR Dynamic Version contd

                        , new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null && networkResponse.statusCode != 200) {
                            System.out.println("@@## productlistApiCall" + error);
                            Log.e("TAG", "No network on update.");
                            Toast.makeText(context, "Network Error, please check internet", Toast.LENGTH_SHORT).show();

                        }

                    }
                });


        RetryPolicy policy = new DefaultRetryPolicy(VolleyHelper.Timeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        productListVolleyRequest.setRetryPolicy(policy);
        VolleyHelper.getInstance(context).addToRequestQueue(productListVolleyRequest);
    }


    public String getResponse() {
        String json = null;
        InputStream is = null;
        try {

            is = getActivity().getApplicationContext().getAssets().open("productsoffline.json");

            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }

        return json;
    }
}


