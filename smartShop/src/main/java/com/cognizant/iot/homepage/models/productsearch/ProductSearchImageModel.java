package com.cognizant.iot.homepage.models.productsearch;

/**
 * Created by 452781 on 2/21/2017.
 */


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ProductSearchImageModel implements Serializable {

    @SerializedName("Items")
    @Expose
    private List<ProductSearchItemModel> items = null;

    public List<ProductSearchItemModel> getItems() {
        return items;
    }

    public void setItems(List<ProductSearchItemModel> items) {
        this.items = items;
    }

}
