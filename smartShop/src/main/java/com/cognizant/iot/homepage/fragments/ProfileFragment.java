package com.cognizant.iot.homepage.fragments;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cognizant.iot.loyaltypoints.multipleloyalty.LoyaltyHomeActivity;
import com.cognizant.retailmate.R;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by 452781 on 2/26/2017.
 */

public class ProfileFragment extends Fragment implements OnMapReadyCallback, LocationListener {


    TextView name, points;
    ImageView profile_settings, search;
    Button view_profile, social_vibes;
    MapFragment mapFragment;
    LatLng current, destination;
    GoogleMap googleMap;
    Double latitude, longitude;
    LocationManager locationManager;

    public static ProfileFragment newInstance(String text) {

        ProfileFragment f = new ProfileFragment();

        Bundle b = new Bundle();
        b.putString("text", text);
        f.setArguments(b);
        return f;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        View v = null;
        try {
            v = inflater.inflate(R.layout.profile_fragement, container, false);
            ((AppCompatActivity) getActivity()).getSupportActionBar().hide();
            ((TextView) v.findViewById(R.id.name)).setText(getArguments().getString("text"));
        } catch (InflateException | IllegalArgumentException | NullPointerException e) {

        }

        name = (TextView) v.findViewById(R.id.name);
        points = (TextView) v.findViewById(R.id.points);
        profile_settings = (ImageView) v.findViewById(R.id.profile_settings);
        search = (ImageView) v.findViewById(R.id.search);
        view_profile = (Button) v.findViewById(R.id.view_profile);
        social_vibes = (Button) v.findViewById(R.id.social_vibes);


        points.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentLP = new Intent(getContext(),
                        LoyaltyHomeActivity.class);
                startActivity(intentLP);
            }
        });
        profile_settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity().getApplicationContext(), "Profile Settings", Toast.LENGTH_SHORT).show();
            }
        });
        view_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity().getApplicationContext(), "View Profile", Toast.LENGTH_SHORT).show();
            }
        });

        social_vibes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity().getApplicationContext(), "Social Vibes", Toast.LENGTH_SHORT).show();
            }
        });


        //autocomplete textview setting up
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_dropdown_item_1line, STORES);
        final AutoCompleteTextView search_store = (AutoCompleteTextView)
                v.findViewById(R.id.search_store);
        search_store.setAdapter(adapter);
        search_store.setDropDownBackgroundResource(R.color.bgdark);


        //new destination search
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                show_users_store(search_store.getText().toString());                    //setting picker to destination

                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().getApplicationContext().INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(search.getApplicationWindowToken(), 0);
            }
        });

        //current location tracking

        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                int MY_LOCATION_PERMISSION_CODE = 101;
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_LOCATION_PERMISSION_CODE);
            }

        }
        locationManager = (LocationManager) getActivity().getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        String provider = locationManager.getBestProvider(criteria, true);
        Location location = locationManager.getLastKnownLocation(provider);


        mapFragment = (MapFragment) getActivity().getFragmentManager().findFragmentById(
                R.id.profile_map_fragment);
        mapFragment.getMapAsync(this);

        if (location != null) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();
            //map setting
            current = new LatLng(latitude, longitude);


        } else {
            //This is what you need:
            locationManager.requestLocationUpdates(provider, 1000, 0, (LocationListener) this);
        }

        return v;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


//        mapFragment = (SupportMapFragment) this.getChildFragmentManager().findFragmentById(
//                R.id.map);


//        mapFragment.getMapAsync(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        locationManager.removeUpdates((LocationListener) this);

    }

    @Override
    public void onDestroyView() {

        super.onDestroyView();


        MapFragment f = (MapFragment) getActivity().getFragmentManager()
                .findFragmentById(R.id.profile_map_fragment);
        if (f != null)
            getActivity().getFragmentManager().beginTransaction().remove(f).commit();
    }

    private void show_users_store(String store_name) {
        //if user searches without giving any input then picker will stay at it's initial position
        Double lat = latitude, lon = longitude;


        try {
            JSONObject stores = new JSONObject(loadLocationJSONFromAsset());
            JSONArray all_stores = stores.getJSONArray("locations");
            for (int i = 0; i < all_stores.length(); i++) {
                JSONObject single_store = all_stores.getJSONObject(i);
                if (single_store.getString("place").equals(store_name)) {
                    lat = single_store.getDouble("lat");
                    lon = single_store.getDouble("lng");
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        destination = new LatLng(lat, lon);
        CameraPosition cameraPosition = new CameraPosition.Builder().target(destination).zoom(15.0f).build();
        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
        googleMap.moveCamera(cameraUpdate);
    }


    private String loadLocationJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getActivity().getApplicationContext().getAssets().open("location.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        Log.e("JSONLOCATION", json);
        return json;
    }


    private static final String[] STORES = new String[]{                                   //Autocomplete Textview String array making
            "Hong Kong", "Abu Dhabi", "Singapore", "Mumbai"
    };

    @Override
    public void onMapReady(GoogleMap Map) {

        try {
            googleMap = Map;
            Log.e("poision", current.toString());
            googleMap.addMarker(new MarkerOptions().position(current).title("Your Current Location").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
            googleMap.addMarker(new MarkerOptions().position(new LatLng(22.293889, 114.171111)).title("Hong Kong").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
            googleMap.addMarker(new MarkerOptions().position(new LatLng(24.626379, 54.344577)).title("Abu Dhabi").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
            googleMap.addMarker(new MarkerOptions().position(new LatLng(1.314297, 103.947314)).title("Singapore").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
            googleMap.addMarker(new MarkerOptions().position(new LatLng(18.975, 72.8258)).title("Mumbai").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));

            CameraPosition cameraPosition = new CameraPosition.Builder().target(current).zoom(15.0f).build();
            CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
            googleMap.moveCamera(cameraUpdate);
        } catch (NullPointerException e) {
            Toast.makeText(getContext(), "Turn on your location services", Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}