package com.cognizant.iot.homepage.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cognizant.iot.homepage.ProductSearchAPI;
import com.cognizant.iot.homepage.models.recommendation.RecommendationValue;
import com.cognizant.iot.productdetails.activity.ProductDetailsActivity;
import com.cognizant.retailmate.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 599654 on 2/17/2017.
 */

public class RecommendationRecyclerAdapter extends RecyclerView.Adapter<RecommendationRecyclerAdapter.MyViewHolder> {

    List<RecommendationValue> productList;
    Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView name, category, price;
        public ImageView productImage, tag_cart_icon, wishlist_btn1;

        public MyViewHolder(View view) {
            super(view);

            view.setOnClickListener(this);
            name = (TextView) view.findViewById(R.id.prodname);
            category = (TextView) view.findViewById(R.id.category);
            price = (TextView) view.findViewById(R.id.prodprice);
            productImage = (ImageView) view.findViewById(R.id.flag);
            this.tag_cart_icon = (ImageView) view.findViewById(R.id.tag_cart_icon);
            this.wishlist_btn1 = (ImageView) view.findViewById(R.id.wishlist_btn1);
            tag_cart_icon.setOnClickListener(this);
            wishlist_btn1.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {


            RecommendationValue recommendationValue = new RecommendationValue();
            int position = getAdapterPosition();
            recommendationValue = productList.get(position);

            if (view.getId() == R.id.tag_cart_icon) {

                if (R.drawable.cart_red_icon == tag_cart_icon.getId()) {
                    tag_cart_icon.setImageResource(R.drawable.cart_grey);
                } else {
                    tag_cart_icon.setImageResource(R.drawable.cart_red_icon);
                }

                List<String> list = new ArrayList<String>();
                list.add(recommendationValue.getProductId());
                ProductSearchAPI productSearchAPI = new ProductSearchAPI(context, list, "cart");

                productSearchAPI.makeaCall();

            } else if (view.getId() == R.id.wishlist_btn1) {

                if (R.drawable.hearticonnn == wishlist_btn1.getId()) {
                    wishlist_btn1.setImageResource(R.drawable.heart_grey);
                } else {
                    wishlist_btn1.setImageResource(R.drawable.hearticonnn);
                }

                List<String> list = new ArrayList<String>();
                list.add(recommendationValue.getProductId());
                ProductSearchAPI productSearchAPI = new ProductSearchAPI(context, list, "wishlist");

                productSearchAPI.makeaCall();
            } else {

                Intent intent = new Intent(context,
                        ProductDetailsActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                // Pass all data prodid
                intent.putExtra("prodid", recommendationValue.getProductId());
                // Pass all data prodname
                intent.putExtra("prodname",
                        recommendationValue.getProductName());
                // Pass all data prodprice
                intent.putExtra("prodprice",
                        String.valueOf(recommendationValue.getCustprice()));
                // Pass all data flag
                intent.putExtra("flag", recommendationValue.getImage());
                intent.putExtra("desc", recommendationValue.getProductDescription());


//            intent.putExtra("offer", resultp.get(MainActivityprod.OFFER));
                intent.putExtra("category",
                        recommendationValue.getRetailCategoryName());

                // Start SingleItemView Class
                context.startActivity(intent);
            }


        }
    }

    public RecommendationRecyclerAdapter(Context context, List<RecommendationValue> productList) {
        this.productList = productList;
        this.context = context;
    }

    @Override
    public RecommendationRecyclerAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.homepage_item_details_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecommendationRecyclerAdapter.MyViewHolder holder, int position) {

        RecommendationValue model = productList.get(position);

        holder.name.setText(model.getProductName());
        holder.category.setText("Houseware");
        holder.price.setText(String.valueOf(model.getCustprice()));
        Log.e("TAG", "Image URL" + model.getImage());
        Picasso.with(context).load("https://etihaddemodevret.cloudax.dynamics.com/MediaServer/" + model.getImage()).into(holder.productImage);

    }

    @Override
    public int getItemCount() {
        return productList.size();
    }
}
