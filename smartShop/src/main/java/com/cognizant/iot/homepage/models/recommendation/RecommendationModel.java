package com.cognizant.iot.homepage.models.recommendation;

/**
 * Created by 452781 on 2/21/2017.
 */


import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RecommendationModel {

    @SerializedName("value")
    @Expose
    private List<RecommendationValue> value = null;

    public List<RecommendationValue> getValue() {
        return value;
    }

    public void setValue(List<RecommendationValue> value) {
        this.value = value;
    }

}


