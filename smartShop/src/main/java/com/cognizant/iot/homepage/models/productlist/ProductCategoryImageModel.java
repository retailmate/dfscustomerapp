package com.cognizant.iot.homepage.models.productlist;

/**
 * Created by 452781 on 2/21/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class ProductCategoryImageModel {

    @SerializedName("Uri")
    @Expose
    private String uri;
    @SerializedName("AltText")
    @Expose
    private Object altText;
    @SerializedName("IsDefault")
    @Expose
    private Boolean isDefault;
    @SerializedName("IsSelfHosted")
    @Expose
    private Boolean isSelfHosted;
    @SerializedName("Priority")
    @Expose
    private Integer priority;
    @SerializedName("ExtensionProperties")
    @Expose
    private List<Object> extensionProperties = null;

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public Object getAltText() {
        return altText;
    }

    public void setAltText(Object altText) {
        this.altText = altText;
    }

    public Boolean getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(Boolean isDefault) {
        this.isDefault = isDefault;
    }

    public Boolean getIsSelfHosted() {
        return isSelfHosted;
    }

    public void setIsSelfHosted(Boolean isSelfHosted) {
        this.isSelfHosted = isSelfHosted;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public List<Object> getExtensionProperties() {
        return extensionProperties;
    }

    public void setExtensionProperties(List<Object> extensionProperties) {
        this.extensionProperties = extensionProperties;
    }

}