package com.cognizant.iot.homepage;

import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.speech.RecognizerIntent;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.cognizant.iot.Network.VolleyHelper;
import com.cognizant.iot.Network.VolleyRequest;
import com.cognizant.iot.activity.PIV_activity;
import com.cognizant.iot.activityfindproduct.SuggestGetSet_findprod;
import com.cognizant.iot.ar.AR_VR;
import com.cognizant.iot.camera.CameraActivity;
import com.cognizant.iot.chatbot.ChatMain;
import com.cognizant.iot.homepage.adapter.CustomPagerAdapter;
import com.cognizant.iot.homepage.adapter.RecommendationRecyclerAdapter;
import com.cognizant.iot.homepage.adapter.ViewPagerAdapter;
import com.cognizant.iot.homepage.fragments.ProductCategoryFragment;
import com.cognizant.iot.homepage.fragments.ProfileFragment;
import com.cognizant.iot.homepage.models.ProductModel;
import com.cognizant.iot.homepage.models.productlist.ProductCategoryModel;
import com.cognizant.iot.homepage.models.productsearch.ProductSearchModel;
import com.cognizant.iot.homepage.models.recommendation.RecommendationModel;
import com.cognizant.iot.mycart.MyBagFragment;
import com.cognizant.iot.scanner.ZBarScannerActivity;
import com.cognizant.iot.utils.AccountState;
import com.cognizant.iot.utils.Constants;
import com.cognizant.retailmate.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HomePageActivity extends AppCompatActivity {

    private ViewPager mViewPager;
    private CustomPagerAdapter mCustomPagerAdapter;
    RecyclerView recyclerView;
    RecommendationRecyclerAdapter recommendationRecyclerAdapter;

    CollapsingToolbarLayout collapsingToolbarLayout;
    AppBarLayout appBarLayout;

    ImageView home_i, bag_i, home_menu, chat_i, profile_i;
    View home_v, bag_v, chat_v, profile_v;
    LinearLayout bottom_nav;
    View bottom_view;
    LinearLayout home, bag, chat, profile;

    View menu_more_container;
    LinearLayout storemode, arvr, selfie, pivideo, storego;
    View menu_alpha_view;

    TabLayout tabLayout;
    ViewPager viewPager;
    ViewPagerAdapter viewPagerAdapter;

    TabLayout tabLayout_dots;

    Animation slideUpAnimation, slideDownAnimation, rotate360;

    Animation slide, slide_close;

    int[] mResources = {
            R.drawable.watch_banner,
            R.drawable.gift_banner,
            R.drawable.scotch_banner,
            R.drawable.perfume_banner,
            R.drawable.beauty_banner,


    };

    List<ProductModel> productList = new ArrayList<>();

    DrawerLayout mainlayout;
    Fragment profileFragment, bagFragment;

    View search_action;
    ImageView search_query, search_product, ar, close, mic;
    private String searchtext, searchId;
    AutoCompleteTextView query;
    RequestQueue queue;
    private ArrayAdapter<String> adaptersearch;
    private ArrayList<String> suggestions;
    List<SuggestGetSet_findprod> ListData = new ArrayList<>();

    View search_icon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_activity);

        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.grey)));
        getSupportActionBar().setDisplayShowCustomEnabled(true);

        getSupportActionBar().setCustomView(R.layout.homepage_actionbar_new);
        getSupportActionBar().setDisplayShowTitleEnabled(false);


        search_action = findViewById(R.id.search_action);

        mainlayout = (DrawerLayout) findViewById(R.id.drawerlayout1);

        slideUpAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_up);
        slideDownAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_down);
        rotate360 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate360);
        slide = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_left);
        slide_close = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_right);


        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.CollapsingToolbarLayout);
        appBarLayout = (AppBarLayout) findViewById(R.id.appbar);

        bottom_view = findViewById(R.id.bottom_view);
        bottom_nav = (LinearLayout) findViewById(R.id.bottom_navigation);

        menu_more_container = findViewById(R.id.more_container);

        storemode = (LinearLayout) findViewById(R.id.instoremode);
        arvr = (LinearLayout) findViewById(R.id.arvr);
        selfie = (LinearLayout) findViewById(R.id.clickselfie);
        pivideo = (LinearLayout) findViewById(R.id.piv);
        storego = (LinearLayout) findViewById(R.id.storego);
        menu_alpha_view = findViewById(R.id.menu_alpha_view);


        home = (LinearLayout) findViewById(R.id.home);
        bag = (LinearLayout) findViewById(R.id.bag);
        chat = (LinearLayout) findViewById(R.id.chat);
        profile = (LinearLayout) findViewById(R.id.profile);

        home_menu = (ImageView) findViewById(R.id.home_menu);

        home_v = findViewById(R.id.home_view);
        home_i = (ImageView) findViewById(R.id.home_icon);

        bag_v = findViewById(R.id.bag_view);
        bag_i = (ImageView) findViewById(R.id.bag_icon);

        chat_v = findViewById(R.id.chat_view);
        chat_i = (ImageView) findViewById(R.id.chat_icon);

        profile_v = findViewById(R.id.profile_view);
        profile_i = (ImageView) findViewById(R.id.profile_icon);

        home_v.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
        home_i.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));

        bag_v.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.transparent));
        bag_i.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.black));

        chat_v.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.transparent));
        chat_i.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.transparent));

        profile_v.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.transparent));
        profile_i.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.black));

        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                Log.e("##$$", String.valueOf(collapsingToolbarLayout.getHeight() + verticalOffset) + " ** " + String.valueOf(2 * ViewCompat.getMinimumHeight(collapsingToolbarLayout)));
                if (collapsingToolbarLayout.getHeight() + verticalOffset < (2 * ViewCompat.getMinimumHeight(collapsingToolbarLayout))) {
                    Log.e("##$$", "Closed");
                    bottom_view.startAnimation(slideDownAnimation);
                    bottom_view.setVisibility(View.GONE);

                } else {
                    Log.e("##$$", "Opened");
                    bottom_view.setVisibility(View.VISIBLE);
//                    bottom_view.startAnimation(slideUpAnimation);
                }
            }
        });


        home_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (menu_more_container.getVisibility() == View.INVISIBLE) {
                    home_menu.startAnimation(rotate360);
                    menu_more_container.bringToFront();
                    menu_more_container.setVisibility(View.VISIBLE);
                    menu_more_container.startAnimation(slideUpAnimation);
                    home_menu.setImageResource(R.drawable.dfs_group_logo);
                    new CountDownTimer(800, 100) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {
                            menu_alpha_view.setVisibility(View.VISIBLE);
                        }
                    }.start();

                } else {
                    home_menu.startAnimation(rotate360);
                    menu_more_container.startAnimation(slideDownAnimation);
                    menu_more_container.setVisibility(View.INVISIBLE);
                    home_menu.setImageResource(R.drawable.dfs_group_logo);

                    menu_alpha_view.setVisibility(View.GONE);
                }


            }
        });


//        Log.e("@@##",String.valueOf(mResources.length));

        mViewPager = (ViewPager) findViewById(R.id.pager);
        tabLayout_dots = (TabLayout) findViewById(R.id.tabDots);


        mCustomPagerAdapter = new CustomPagerAdapter(this, mResources);
        mViewPager.setAdapter(mCustomPagerAdapter);

        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView = (RecyclerView) findViewById(R.id.products_container);
        recyclerView.setLayoutManager(layoutManager);

//        populateData();
        recommendationApiCall();
        productcategoryApiCall();

        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        viewPager = (ViewPager) findViewById(R.id.viewPager);

        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());

//        viewPagerAdapter.addFragments(new ProductCategoryFragment(), "Catogory 1");
//        viewPagerAdapter.addFragments(new ProductCategoryFragment(), "Category 2");
//        viewPagerAdapter.addFragments(new ProductCategoryFragment(), "Category 3");
//
//        viewPager.setAdapter(viewPagerAdapter);
//        tabLayout.setupWithViewPager(viewPager);
//        tabLayout_dots.setupWithViewPager(mViewPager);

        profileFragment = ProfileFragment.newInstance(AccountState.getUserName());
        bagFragment = new MyBagFragment();
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                home_v.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
                home_i.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));

                bag_v.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.transparent));
                bag_i.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.black));

                chat_v.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.transparent));
                chat_i.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.transparent));

                profile_v.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.transparent));
                profile_i.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.black));

                getSupportActionBar().show();
//                getSupportFragmentManager().beginTransaction().remove(profileFragment).commit();
//                getSupportFragmentManager().executePendingTransactions();


                if (bagFragment.isAdded()) {
//                    FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
//                    fragmentTransaction.replace(mainlayout.getId(), bagFragment);
//                    fragmentTransaction.addToBackStack("bag");
//                    getSupportFragmentManager().beginTransaction().remove(bagFragment).commit();
//                    fragmentTransaction.commit();

                    getSupportFragmentManager().beginTransaction().hide(bagFragment).commit();
                }
                if (profileFragment.isAdded()) {
//                    FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
//                    fragmentTransaction.replace(mainlayout.getId(), profileFragment);
//                    fragmentTransaction.addToBackStack("profile");
//                    getSupportFragmentManager().beginTransaction().remove(profileFragment).commit();
//                    fragmentTransaction.commit();

                    getSupportFragmentManager().beginTransaction().hide(profileFragment).commit();
                }


                for (int i = 0; i < getSupportFragmentManager().getBackStackEntryCount(); ++i) {
                    getSupportFragmentManager().popBackStack();
                }
            }
        });


        bag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                home_v.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.transparent));
                home_i.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.black));

                chat_v.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.transparent));
                chat_i.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.transparent));

                profile_v.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.transparent));
                profile_i.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.black));

                bag_v.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
                bag_i.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));

                if (profileFragment.isAdded()) {
//                    FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
//                    fragmentTransaction.replace(mainlayout.getId(), profileFragment);
//                    fragmentTransaction.addToBackStack("profile");
//                    getSupportFragmentManager().beginTransaction().remove(profileFragment).commit();
//                    fragmentTransaction.commit();

                    getSupportFragmentManager().beginTransaction().hide(profileFragment).commit();
                }

                if (bagFragment != null && bagFragment instanceof MyBagFragment & !bagFragment.isAdded()) {

                    Bundle b = new Bundle();
                    b.putString("whichClass", "catalog");
                    bagFragment.setArguments(b);
                    getSupportFragmentManager().beginTransaction().add(mainlayout.getId(), bagFragment, "bag").commit();
                } else {
                    getSupportFragmentManager().beginTransaction().show(bagFragment).commit();
                    getSupportActionBar().hide();
                }

//                Intent intent = new Intent(HomePageActivity.this, MyCart.class);
//                intent.putExtra("whichClass", "catalog");
//                startActivity(intent);
            }
        });

        chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                home_v.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.transparent));
//                home_i.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.black));
//
//                bag_v.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.transparent));
//                bag_i.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.black));
//
//                profile_v.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.transparent));
//                profile_i.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.black));
//
//                chat_v.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
//                chat_i.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.transparent));

                Intent intent = new Intent(HomePageActivity.this, ChatMain.class);
                startActivity(intent);
            }
        });

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                home_v.setBackgroundColor(getResources().getColor(R.color.transparent));
                home_i.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.black));

                bag_v.setBackgroundColor(getResources().getColor(R.color.transparent));
                bag_i.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.black));

                chat_v.setBackgroundColor(getResources().getColor(R.color.transparent));
                chat_i.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.transparent));

                profile_v.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                profile_i.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));

                if (bagFragment.isAdded()) {
//                    FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
//                    fragmentTransaction.replace(mainlayout.getId(), bagFragment);
//                    fragmentTransaction.addToBackStack("bag");
//                    getSupportFragmentManager().beginTransaction().remove(bagFragment).commit();
//                    fragmentTransaction.commit();

                    getSupportFragmentManager().beginTransaction().hide(bagFragment).commit();
                }

                if (profileFragment != null && profileFragment instanceof ProfileFragment & !profileFragment.isAdded()) {
                    getSupportFragmentManager().beginTransaction().add(mainlayout.getId(), profileFragment, "profile").commit();
                } else {
                    getSupportFragmentManager().beginTransaction().show(profileFragment).commit();
                    getSupportActionBar().hide();
                }

            }
        });

        /**More features selection**/
        storemode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomePageActivity.this, StoreMode.class);
                startActivity(intent);
//                Toast.makeText(HomePageActivity.this, "Store Mode", Toast.LENGTH_SHORT).show();
            }
        });

        arvr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomePageActivity.this, AR_VR.class);
                startActivity(intent);
//                Toast.makeText(HomePageActivity.this, "ARVR", Toast.LENGTH_SHORT).show();
            }
        });

        selfie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cameraintent = new Intent(HomePageActivity.this,
                        CameraActivity.class);
                startActivity(cameraintent);
            }
        });

        pivideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(HomePageActivity.this, "PIV", Toast.LENGTH_SHORT).show();
                Intent wishlist_intent = new Intent(HomePageActivity.this,
                        PIV_activity.class);
                startActivity(wishlist_intent);
            }
        });

        storego.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(HomePageActivity.this, "StoreGo", Toast.LENGTH_SHORT).show();
            }
        });


    }

    public void ScanQr(View view) {
//        Toast.makeText(HomePageActivity.this, "QR CODE clicked", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(getApplicationContext(),
                ZBarScannerActivity.class);
        startActivity(intent);
    }

    public void SearchProduct(View view) {
//        Toast.makeText(HomePageActivity.this, "Search clicked", Toast.LENGTH_SHORT).show();
        view.setEnabled(false);
        search_icon = view;
        searchId = "";
        searchtext = "";

        adaptersearch = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_dropdown_item_1line) {
        };


        startsearch();
    }


    private void startsearch() {
        search_action.setVisibility(View.VISIBLE);
        search_action.startAnimation(slide);
//        Toast.makeText(MainActivity.this, "Search", Toast.LENGTH_SHORT).show();

        close = (ImageView) findViewById(R.id.close);
        mic = (ImageView) findViewById(R.id.mic);
        search_query = (ImageView) findViewById(R.id.searchquery);
        query = (AutoCompleteTextView) findViewById(R.id.query);

        query.setText("");
        doInBackground("");

        query.setDropDownBackgroundResource(R.color.bgdark);
        query.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                query.setTypeface(Typeface.DEFAULT);
                v.setFocusable(true);
                v.setFocusableInTouchMode(true);
                return false;
            }
        });

        query.setFocusable(false);


        query.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (query.length() == 2) {
                    doInBackground(s.toString());
                } else {
                    adaptersearch.getFilter().filter(s.toString());
                    adaptersearch.setNotifyOnChange(true);
                    query.setAdapter(adaptersearch);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (query.length() > 2) {
                    adaptersearch.getFilter().filter(query.getText().toString());
                    adaptersearch.setNotifyOnChange(true);
                    query.showDropDown();
                }

            }
        });

        query.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (query.length() > 2) {
                    adaptersearch.getFilter().filter(query.getText().toString());
                    adaptersearch.setNotifyOnChange(true);
                    adaptersearch.notifyDataSetChanged();
                    query.showDropDown();
                }
            }
        });

        mic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displaySpeechRecognizer();
//                Toast.makeText(MainActivity.this, "Mic", Toast.LENGTH_SHORT).show();
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (query.getText().toString().equals("")) {
                    search_action.startAnimation(slide_close);
                    search_action.setVisibility(View.INVISIBLE);
                    search_icon.setEnabled(true);
                } else
                    query.setText("");
//                Toast.makeText(MainActivity.this, "Close", Toast.LENGTH_SHORT).show();
            }
        });

        query.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                searchtext = query.getText().toString();
                searchId = "";
                int index = adaptersearch.getPosition(searchtext);
                try {
                    searchId = suggestions.get(index);
                } catch (NullPointerException | ArrayIndexOutOfBoundsException e) {
                    Log.e("Error", e.getMessage());
                }


                Toast.makeText(HomePageActivity.this, "Name:" + searchtext + "\nId:" + searchId, Toast.LENGTH_SHORT).show();
            }
        });

        query.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    Toast.makeText(HomePageActivity.this, "Name:" + searchtext + "\nId:" + searchId, Toast.LENGTH_SHORT).show();
                    return true;
                }
                return false;
            }
        });

        search_query.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("@@## On Tap", String.valueOf(adaptersearch.getCount()));
                if (!searchId.equals(""))
                    Toast.makeText(HomePageActivity.this, "Name:" + searchtext + "\nId:" + searchId, Toast.LENGTH_SHORT).show();


            }
        });
    }


    private void populateData() {

        ProductModel model = new ProductModel();
        model.setName("Camera");
        model.setCategory("Camera Accessories");
        model.setPrice((float) 124.0);
        productList.add(model);

        model = new ProductModel();
        model.setName("Tripod");
        model.setCategory("Camera Accessories");
        model.setPrice((float) 12.0);
        productList.add(model);

        model = new ProductModel();
        model.setName("Camera");
        model.setCategory("Camera Accessories");
        model.setPrice((float) 124.0);
        productList.add(model);

        model = new ProductModel();
        model.setName("Tripod");
        model.setCategory("Camera Accessories");
        model.setPrice((float) 12.0);
        productList.add(model);

    }


    void recommendationApiCall() {

        Map<String, String> headers = new HashMap<>();
        JSONObject params = null;
        try {

            headers.put("Content-Type", "application/json");

            params = new JSONObject();
            params.put("UserID", AccountState.getUserID());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        VolleyRequest<RecommendationModel> recommendationModelVolleyRequest =
                new VolleyRequest<RecommendationModel>(Request.Method.POST, Constants.RECOMMENDATION_API, RecommendationModel.class, headers, params,

                        new com.android.volley.Response.Listener<RecommendationModel>() {
                            @Override
                            public void onResponse(RecommendationModel response) {
//                                Toast.makeText(HomePageActivity.this, "Your Recommendations \n" + response.getValue(), Toast.LENGTH_SHORT).show();

                                recommendationRecyclerAdapter = new RecommendationRecyclerAdapter(getApplicationContext(), response.getValue());
                                recyclerView.setAdapter(recommendationRecyclerAdapter);
//                                recommendationProductDetailsApiCall(response);


                            }
                        }
                        //API CALL GETTING MADE FOR Dynamic Version contd

                        , new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null && networkResponse.statusCode != 200) {
                            Log.e("TAG", "No network on update.");
                            Toast.makeText(HomePageActivity.this, "Network Error, please check internet", Toast.LENGTH_SHORT).show();

                        }

                    }
                });


        RetryPolicy policy = new DefaultRetryPolicy(VolleyHelper.Timeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        recommendationModelVolleyRequest.setRetryPolicy(policy);
        VolleyHelper.getInstance(getApplicationContext()).addToRequestQueue(recommendationModelVolleyRequest);
    }

    void recommendationProductDetailsApiCall(RecommendationModel recommendationModel) {

        String[] products = new String[(recommendationModel.getValue().size())];
        for (int i = 0; i < recommendationModel.getValue().size(); i++) {
            products[i] = recommendationModel.getValue().get(i).getProductId();

        }


        Map<String, String> headers = new HashMap<>();
        JSONObject finalObject = null;
        try {

            headers.put("Content-Type", "application/json");
            headers.put("Authorization", "id_token " + AccountState.getTokenID());
            Log.e("TAG", "AccountState.getTokenID()" + AccountState.getTokenID());
            headers.put(
                    "OUN",
                    "092");

            JSONArray parentData = new JSONArray();

            for (int i = 0; i < products.length; i++) {


                parentData.put(Double.valueOf(products[i]));
            }

            JSONObject childData = new JSONObject();

            childData.put("Ids", parentData);
            childData.put("DataLevelValue", 4);

            finalObject = new JSONObject();
            finalObject.put("productSearchCriteria", childData);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        VolleyRequest<ProductSearchModel> recommendationModelVolleyRequest =
                new VolleyRequest<ProductSearchModel>(Request.Method.POST, Constants.PRODUCT_SEARCH_BY_ID_API, ProductSearchModel.class, headers, finalObject,

                        new com.android.volley.Response.Listener<ProductSearchModel>() {
                            @Override
                            public void onResponse(ProductSearchModel response) {
                                Log.e("Your Recommendations \n", "" + response.getProductSearchValueModel().size());

                            }
                        }
                        //API CALL GETTING MADE FOR Dynamic Version contd

                        , new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null && networkResponse.statusCode != 200) {
                            Log.e("TAG", "No network on update.");
                            Toast.makeText(HomePageActivity.this, "Network Error, please check internet", Toast.LENGTH_SHORT).show();

                        }

                    }
                });


        RetryPolicy policy = new DefaultRetryPolicy(VolleyHelper.Timeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        recommendationModelVolleyRequest.setRetryPolicy(policy);
        VolleyHelper.getInstance(getApplicationContext()).addToRequestQueue(recommendationModelVolleyRequest);
    }


    void productcategoryApiCall() {

        System.out.println("@@## productcategoryApiCall");

        Map<String, String> headers = new HashMap<>();
        JSONObject params = null;
        try {

            headers.put("Content-Type", "application/json");
            headers.put("Authorization", "id_token " + AccountState.getTokenID());

            headers.put(
                    "OUN",
                    "092");

            params = new JSONObject();
            params.put("channelId", 68719476778L);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        VolleyRequest<ProductCategoryModel> categoryVolleyRequest =
                new VolleyRequest<ProductCategoryModel>(Request.Method.POST, Constants.PRODUCT_CATEGORY_API, ProductCategoryModel.class, headers, params,

                        new com.android.volley.Response.Listener<ProductCategoryModel>() {
                            @Override
                            public void onResponse(ProductCategoryModel response) {
                                Log.e("TAG", "Product categories " + response.getValue().get(0));
                                System.out.println("@@## productcategoryApiCall" + response.getValue().get(0));

                                for (int i = 0; i < response.getValue().size(); i++) {
                                    viewPagerAdapter.addFragments(new ProductCategoryFragment(HomePageActivity.this, response.getValue().get(i).getName(), response.getValue().get(i).getRecordId()), response.getValue().get(i).getName());
//                                    System.out.println("@@## productcategoryApiCall" + response.getValue().get(i).getName());
                                }
//                                viewPagerAdapter.addFragments(new ProductCategoryFragment(), "Category 2");
//                                viewPagerAdapter.addFragments(new ProductCategoryFragment(), "Category 3");

                                viewPager.setAdapter(viewPagerAdapter);
                                tabLayout.setupWithViewPager(viewPager);
                                tabLayout_dots.setupWithViewPager(mViewPager);
                            }
                        }
                        //API CALL GETTING MADE FOR Dynamic Version contd

                        , new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null && networkResponse.statusCode != 200) {
                            System.out.println("@@## productcategoryApiCall" + error);
                            Log.e("TAG", "No network on update.");
                            Toast.makeText(HomePageActivity.this, "Network Error, please check internet", Toast.LENGTH_SHORT).show();

                        }

                    }
                });


        RetryPolicy policy = new DefaultRetryPolicy(VolleyHelper.Timeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        categoryVolleyRequest.setRetryPolicy(policy);
        VolleyHelper.getInstance(getApplicationContext()).addToRequestQueue(categoryVolleyRequest);
    }

    private static final int SPEECH_REQUEST_CODE = 0;

    private void displaySpeechRecognizer() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        startActivityForResult(intent, SPEECH_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SPEECH_REQUEST_CODE && resultCode == RESULT_OK) {
            List<String> results = data
                    .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            final String spokenText = results.get(0);
            Log.e("@@## spokenText ", spokenText);

            query.postDelayed(new Runnable() {
                @Override
                public void run() {

                    query.setText(spokenText);
                    query.setFocusable(true);
                    query.setSelection(query.getText().length());
                    doInBackground(query.getText().toString().trim());
                    Log.e("@@## After Speech", String.valueOf(adaptersearch.getCount()) + " " + adaptersearch.toString());
                    adaptersearch.getFilter().filter(query.getText().toString());
                    adaptersearch.setNotifyOnChange(true);
                    adaptersearch.notifyDataSetChanged();
                    query.showDropDown();
                }
            }, 500);
            query.setFocusable(true);
            query.setFocusableInTouchMode(true);
            query.requestFocus();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void doInBackground(String query_text) {
        ListData = new ArrayList<>();

        queue = Volley.newRequestQueue(getApplicationContext());

        Log.e("@@## before CALL", query_text);
        String url = "https://etihaddemodevret.cloudax.dynamics.com/Commerce/Products/SearchByText(channelId=68719476778,catalogId=0,searchText="
                + "\'" + query_text + "\'" + ")?$top=20&api-version=7.1";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("@@##", response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("value");
                            Log.e("@@## Json Array", String.valueOf(jsonArray.length()));

                            for (int i = 0; i < jsonArray.length(); i++) {
                                jsonObject = jsonArray.getJSONObject(i);
                                ListData.add(new SuggestGetSet_findprod(jsonObject
                                        .getString("RecordId"), jsonObject
                                        .getString("Name"), jsonObject
                                        .getString("ItemId")));
                            }

                            Log.e("@@## ListData", String.valueOf(ListData.size()));

                            if (ListData.size() > 0) {
                                adaptersearch.clear();

                                suggestions = new ArrayList<>();
                                suggestions.clear();

                                for (int i = 0; i < ListData.size(); i++) {
                                    suggestions.add(ListData.get(i).getId());
                                }

                                Log.e("@@## Suggestions", String.valueOf(suggestions.size()));

                                for (int i = 0; i < ListData.size(); i++) {
                                    adaptersearch.add(ListData.get(i).getName());
                                }
                                adaptersearch.setNotifyOnChange(true);
                                query.setAdapter(adaptersearch);
                                query.showDropDown();
                                adaptersearch.notifyDataSetChanged();

                                if (query.length() > 2) {
                                    adaptersearch.getFilter().filter(query.getText().toString());
                                    adaptersearch.setNotifyOnChange(true);
                                    adaptersearch.notifyDataSetChanged();
                                    query.showDropDown();
                                }


                            }

                        } catch (NullPointerException | ArrayIndexOutOfBoundsException | JSONException e) {
                            Log.e("Error", e.getMessage());
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                adaptersearch.clear();
                adaptersearch.add("Connection Error");
                query.setAdapter(adaptersearch);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Authorization", "id_token " + AccountState.getTokenID());
                params.put("OUN", "092");
                return params;
            }
        };
        queue.add(stringRequest);

    }

}
