package com.cognizant.iot.homepage.models.wishlist;

/**
 * Created by 452781 on 2/23/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WishlistDataModel {

    @SerializedName("Id")
    @Expose
    private String id;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("CustomerId")
    @Expose
    private String customerId;
    @SerializedName("CustomerName")
    @Expose
    private String customerName;
    @SerializedName("IsFavorite")
    @Expose
    private Boolean isFavorite;
    @SerializedName("IsRecurring")
    @Expose
    private Boolean isRecurring;
    @SerializedName("IsPrivate")
    @Expose
    private Boolean isPrivate;
    @SerializedName("IsCollaborative")
    @Expose
    private Boolean isCollaborative;
    @SerializedName("CreatedDateTime")
    @Expose
    private String createdDateTime;
    @SerializedName("DueDateTime")
    @Expose
    private String dueDateTime;
    @SerializedName("CommerceListTypeValue")
    @Expose
    private Integer commerceListTypeValue;
    @SerializedName("CommerceListLines")
    @Expose
    private List<WishlistCommerceListLineModel> commerceListLines = null;
    @SerializedName("CommerceListContributors")
    @Expose
    private List<Object> commerceListContributors = null;
    @SerializedName("CommerceListInvitations")
    @Expose
    private List<Object> commerceListInvitations = null;
    @SerializedName("ExtensionProperties")
    @Expose
    private List<Object> extensionProperties = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public Boolean getIsFavorite() {
        return isFavorite;
    }

    public void setIsFavorite(Boolean isFavorite) {
        this.isFavorite = isFavorite;
    }

    public Boolean getIsRecurring() {
        return isRecurring;
    }

    public void setIsRecurring(Boolean isRecurring) {
        this.isRecurring = isRecurring;
    }

    public Boolean getIsPrivate() {
        return isPrivate;
    }

    public void setIsPrivate(Boolean isPrivate) {
        this.isPrivate = isPrivate;
    }

    public Boolean getIsCollaborative() {
        return isCollaborative;
    }

    public void setIsCollaborative(Boolean isCollaborative) {
        this.isCollaborative = isCollaborative;
    }

    public String getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(String createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public String getDueDateTime() {
        return dueDateTime;
    }

    public void setDueDateTime(String dueDateTime) {
        this.dueDateTime = dueDateTime;
    }

    public Integer getCommerceListTypeValue() {
        return commerceListTypeValue;
    }

    public void setCommerceListTypeValue(Integer commerceListTypeValue) {
        this.commerceListTypeValue = commerceListTypeValue;
    }

    public List<WishlistCommerceListLineModel> getCommerceListLines() {
        return commerceListLines;
    }

    public void setCommerceListLines(List<WishlistCommerceListLineModel> commerceListLines) {
        this.commerceListLines = commerceListLines;
    }

    public List<Object> getCommerceListContributors() {
        return commerceListContributors;
    }

    public void setCommerceListContributors(List<Object> commerceListContributors) {
        this.commerceListContributors = commerceListContributors;
    }

    public List<Object> getCommerceListInvitations() {
        return commerceListInvitations;
    }

    public void setCommerceListInvitations(List<Object> commerceListInvitations) {
        this.commerceListInvitations = commerceListInvitations;
    }

    public List<Object> getExtensionProperties() {
        return extensionProperties;
    }

    public void setExtensionProperties(List<Object> extensionProperties) {
        this.extensionProperties = extensionProperties;
    }

}