package com.cognizant.iot.homepage.models.wishlist;

/**
 * Created by 452781 on 2/23/2017.
 */

import com.cognizant.iot.homepage.models.productsearch.ProductSearchValueModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WishlistCommerceListLineModel {

    @SerializedName("CommerceListId")
    @Expose
    private String commerceListId;
    @SerializedName("LineId")
    @Expose
    private String lineId;
    @SerializedName("CustomerId")
    @Expose
    private String customerId;
    @SerializedName("CustomerName")
    @Expose
    private String customerName;
    @SerializedName("ProductId")
    @Expose
    private String productId;
    @SerializedName("Quantity")
    @Expose
    private Integer quantity;
    @SerializedName("UnitOfMeasure")
    @Expose
    private String unitOfMeasure;
    @SerializedName("IsFavorite")
    @Expose
    private Boolean isFavorite;
    @SerializedName("IsRecurring")
    @Expose
    private Boolean isRecurring;
    @SerializedName("IsPrivate")
    @Expose
    private Boolean isPrivate;
    @SerializedName("CreatedDateTime")
    @Expose
    private String createdDateTime;
    @SerializedName("ExtensionProperties")
    @Expose
    private List<Object> extensionProperties = null;


    ProductSearchValueModel productSearchValueModel;

    public ProductSearchValueModel getProductSearchValueModel() {
        return productSearchValueModel;
    }

    public void setProductSearchValueModel(ProductSearchValueModel productSearchValueModel) {
        this.productSearchValueModel = productSearchValueModel;
    }


    public String getCommerceListId() {
        return commerceListId;
    }

    public void setCommerceListId(String commerceListId) {
        this.commerceListId = commerceListId;
    }

    public String getLineId() {
        return lineId;
    }

    public void setLineId(String lineId) {
        this.lineId = lineId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }

    public void setUnitOfMeasure(String unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    public Boolean getIsFavorite() {
        return isFavorite;
    }

    public void setIsFavorite(Boolean isFavorite) {
        this.isFavorite = isFavorite;
    }

    public Boolean getIsRecurring() {
        return isRecurring;
    }

    public void setIsRecurring(Boolean isRecurring) {
        this.isRecurring = isRecurring;
    }

    public Boolean getIsPrivate() {
        return isPrivate;
    }

    public void setIsPrivate(Boolean isPrivate) {
        this.isPrivate = isPrivate;
    }

    public String getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(String createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public List<Object> getExtensionProperties() {
        return extensionProperties;
    }

    public void setExtensionProperties(List<Object> extensionProperties) {
        this.extensionProperties = extensionProperties;
    }

}