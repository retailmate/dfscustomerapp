package com.cognizant.iot.homepage.models.cart;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by 452781 on 2/23/2017.
 */

public class CartDiscountLineModel {

    @SerializedName("SaleLineNumber")
    @Expose
    private Integer saleLineNumber;
    @SerializedName("OfferId")
    @Expose
    private String offerId;
    @SerializedName("OfferName")
    @Expose
    private String offerName;
    @SerializedName("Amount")
    @Expose
    private Integer amount;
    @SerializedName("EffectiveAmount")
    @Expose
    private Integer effectiveAmount;
    @SerializedName("Percentage")
    @Expose
    private Integer percentage;
    @SerializedName("DealPrice")
    @Expose
    private Integer dealPrice;
    @SerializedName("DiscountLineTypeValue")
    @Expose
    private Integer discountLineTypeValue;
    @SerializedName("ManualDiscountTypeValue")
    @Expose
    private Integer manualDiscountTypeValue;
    @SerializedName("CustomerDiscountTypeValue")
    @Expose
    private Integer customerDiscountTypeValue;
    @SerializedName("PeriodicDiscountTypeValue")
    @Expose
    private Integer periodicDiscountTypeValue;
    @SerializedName("DiscountApplicationGroup")
    @Expose
    private String discountApplicationGroup;
    @SerializedName("ConcurrencyModeValue")
    @Expose
    private Integer concurrencyModeValue;
    @SerializedName("IsCompoundable")
    @Expose
    private Boolean isCompoundable;
    @SerializedName("DiscountCode")
    @Expose
    private String discountCode;
    @SerializedName("PricingPriorityNumber")
    @Expose
    private Integer pricingPriorityNumber;
    @SerializedName("IsDiscountCodeRequired")
    @Expose
    private Boolean isDiscountCodeRequired;
    @SerializedName("ThresholdAmountRequired")
    @Expose
    private Integer thresholdAmountRequired;
    @SerializedName("ExtensionProperties")
    @Expose
    private List<Object> extensionProperties = null;

    public Integer getSaleLineNumber() {
        return saleLineNumber;
    }

    public void setSaleLineNumber(Integer saleLineNumber) {
        this.saleLineNumber = saleLineNumber;
    }

    public String getOfferId() {
        return offerId;
    }

    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }

    public String getOfferName() {
        return offerName;
    }

    public void setOfferName(String offerName) {
        this.offerName = offerName;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getEffectiveAmount() {
        return effectiveAmount;
    }

    public void setEffectiveAmount(Integer effectiveAmount) {
        this.effectiveAmount = effectiveAmount;
    }

    public Integer getPercentage() {
        return percentage;
    }

    public void setPercentage(Integer percentage) {
        this.percentage = percentage;
    }

    public Integer getDealPrice() {
        return dealPrice;
    }

    public void setDealPrice(Integer dealPrice) {
        this.dealPrice = dealPrice;
    }

    public Integer getDiscountLineTypeValue() {
        return discountLineTypeValue;
    }

    public void setDiscountLineTypeValue(Integer discountLineTypeValue) {
        this.discountLineTypeValue = discountLineTypeValue;
    }

    public Integer getManualDiscountTypeValue() {
        return manualDiscountTypeValue;
    }

    public void setManualDiscountTypeValue(Integer manualDiscountTypeValue) {
        this.manualDiscountTypeValue = manualDiscountTypeValue;
    }

    public Integer getCustomerDiscountTypeValue() {
        return customerDiscountTypeValue;
    }

    public void setCustomerDiscountTypeValue(Integer customerDiscountTypeValue) {
        this.customerDiscountTypeValue = customerDiscountTypeValue;
    }

    public Integer getPeriodicDiscountTypeValue() {
        return periodicDiscountTypeValue;
    }

    public void setPeriodicDiscountTypeValue(Integer periodicDiscountTypeValue) {
        this.periodicDiscountTypeValue = periodicDiscountTypeValue;
    }

    public String getDiscountApplicationGroup() {
        return discountApplicationGroup;
    }

    public void setDiscountApplicationGroup(String discountApplicationGroup) {
        this.discountApplicationGroup = discountApplicationGroup;
    }

    public Integer getConcurrencyModeValue() {
        return concurrencyModeValue;
    }

    public void setConcurrencyModeValue(Integer concurrencyModeValue) {
        this.concurrencyModeValue = concurrencyModeValue;
    }

    public Boolean getIsCompoundable() {
        return isCompoundable;
    }

    public void setIsCompoundable(Boolean isCompoundable) {
        this.isCompoundable = isCompoundable;
    }

    public String getDiscountCode() {
        return discountCode;
    }

    public void setDiscountCode(String discountCode) {
        this.discountCode = discountCode;
    }

    public Integer getPricingPriorityNumber() {
        return pricingPriorityNumber;
    }

    public void setPricingPriorityNumber(Integer pricingPriorityNumber) {
        this.pricingPriorityNumber = pricingPriorityNumber;
    }

    public Boolean getIsDiscountCodeRequired() {
        return isDiscountCodeRequired;
    }

    public void setIsDiscountCodeRequired(Boolean isDiscountCodeRequired) {
        this.isDiscountCodeRequired = isDiscountCodeRequired;
    }

    public Integer getThresholdAmountRequired() {
        return thresholdAmountRequired;
    }

    public void setThresholdAmountRequired(Integer thresholdAmountRequired) {
        this.thresholdAmountRequired = thresholdAmountRequired;
    }

    public List<Object> getExtensionProperties() {
        return extensionProperties;
    }

    public void setExtensionProperties(List<Object> extensionProperties) {
        this.extensionProperties = extensionProperties;
    }
}