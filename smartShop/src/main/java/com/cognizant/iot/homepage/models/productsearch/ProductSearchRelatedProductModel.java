package com.cognizant.iot.homepage.models.productsearch;

/**
 * Created by 452781 on 2/23/2017.
 */


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductSearchRelatedProductModel {

    @SerializedName("ProductRecordId")
    @Expose
    private String productRecordId;
    @SerializedName("RelatedProductRecordId")
    @Expose
    private String relatedProductRecordId;
    @SerializedName("CatalogId")
    @Expose
    private Integer catalogId;
    @SerializedName("RelationName")
    @Expose
    private String relationName;
    @SerializedName("IsExcludedRelation")
    @Expose
    private Boolean isExcludedRelation;
    @SerializedName("IsDirectRelation")
    @Expose
    private Boolean isDirectRelation;
    @SerializedName("ExtensionProperties")
    @Expose
    private List<Object> extensionProperties = null;

    public String getProductRecordId() {
        return productRecordId;
    }

    public void setProductRecordId(String productRecordId) {
        this.productRecordId = productRecordId;
    }

    public String getRelatedProductRecordId() {
        return relatedProductRecordId;
    }

    public void setRelatedProductRecordId(String relatedProductRecordId) {
        this.relatedProductRecordId = relatedProductRecordId;
    }

    public Integer getCatalogId() {
        return catalogId;
    }

    public void setCatalogId(Integer catalogId) {
        this.catalogId = catalogId;
    }

    public String getRelationName() {
        return relationName;
    }

    public void setRelationName(String relationName) {
        this.relationName = relationName;
    }

    public Boolean getIsExcludedRelation() {
        return isExcludedRelation;
    }

    public void setIsExcludedRelation(Boolean isExcludedRelation) {
        this.isExcludedRelation = isExcludedRelation;
    }

    public Boolean getIsDirectRelation() {
        return isDirectRelation;
    }

    public void setIsDirectRelation(Boolean isDirectRelation) {
        this.isDirectRelation = isDirectRelation;
    }

    public List<Object> getExtensionProperties() {
        return extensionProperties;
    }

    public void setExtensionProperties(List<Object> extensionProperties) {
        this.extensionProperties = extensionProperties;
    }

}
