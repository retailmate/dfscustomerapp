package com.cognizant.iot.homepage.models.recommendation;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by 452781 on 2/21/2017.
 */

public class RecommendationValue {

    @SerializedName("ItemId")
    @Expose
    private String itemId;
    @SerializedName("ProductId")
    @Expose
    private String productId;
    @SerializedName("ProductName")
    @Expose
    private String productName;
    @SerializedName("RetailCategoryName")
    @Expose
    private String retailCategoryName;
    @SerializedName("ProductDescription")
    @Expose
    private String productDescription;
    @SerializedName("SizeGroupId")
    @Expose
    private String sizeGroupId;
    @SerializedName("StyleGroupId")
    @Expose
    private String styleGroupId;
    @SerializedName("ColorGroupId")
    @Expose
    private String colorGroupId;
    @SerializedName("Custprice")
    @Expose
    private Double custprice;
    @SerializedName("Image")
    @Expose
    private String image;

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getRetailCategoryName() {
        return retailCategoryName;
    }

    public void setRetailCategoryName(String retailCategoryName) {
        this.retailCategoryName = retailCategoryName;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getSizeGroupId() {
        return sizeGroupId;
    }

    public void setSizeGroupId(String sizeGroupId) {
        this.sizeGroupId = sizeGroupId;
    }

    public String getStyleGroupId() {
        return styleGroupId;
    }

    public void setStyleGroupId(String styleGroupId) {
        this.styleGroupId = styleGroupId;
    }

    public String getColorGroupId() {
        return colorGroupId;
    }

    public void setColorGroupId(String colorGroupId) {
        this.colorGroupId = colorGroupId;
    }

    public Double getCustprice() {
        return custprice;
    }

    public void setCustprice(Double custprice) {
        this.custprice = custprice;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}