package com.cognizant.iot.homepage;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Switch;

import com.cognizant.iot.json.AssetsExtracter;
import com.cognizant.iot.wearable.companion.service.BeaconService;
import com.cognizant.retailmate.R;

public class StoreMode extends AppCompatActivity {
    Switch simpleSwitch1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.homepage_store_mode);

//        getSupportActionBar().hide();
        simpleSwitch1 = (Switch) findViewById(R.id.simpleSwitch1);

        simpleSwitch1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getBaseContext().stopService(
                        new Intent(getBaseContext(), BeaconService.class));

                AssetsExtracter mTask = new AssetsExtracter("empty", StoreMode.this);
                mTask.execute();

            }
        });
    }
}
