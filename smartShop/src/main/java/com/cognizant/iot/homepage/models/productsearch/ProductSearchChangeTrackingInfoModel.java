package com.cognizant.iot.homepage.models.productsearch;

/**
 * Created by 452781 on 2/21/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductSearchChangeTrackingInfoModel {

    @SerializedName("ModifiedDateTime")
    @Expose
    private String modifiedDateTime;
    @SerializedName("ChangeActionValue")
    @Expose
    private Integer changeActionValue;
    @SerializedName("RequestedActionValue")
    @Expose
    private Integer requestedActionValue;

    public String getModifiedDateTime() {
        return modifiedDateTime;
    }

    public void setModifiedDateTime(String modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    public Integer getChangeActionValue() {
        return changeActionValue;
    }

    public void setChangeActionValue(Integer changeActionValue) {
        this.changeActionValue = changeActionValue;
    }

    public Integer getRequestedActionValue() {
        return requestedActionValue;
    }

    public void setRequestedActionValue(Integer requestedActionValue) {
        this.requestedActionValue = requestedActionValue;
    }

}
