package com.cognizant.iot.service;

import android.app.ActivityManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.IBinder;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.cognizant.iot.activity.Login_Screen;
import com.cognizant.iot.model.CustomerModel;
import com.cognizant.iot.utils.AccountState;
import com.cognizant.iot.utils.Constants;

import com.cognizant.retailmate.R;
import com.google.gson.Gson;
import com.google.gson.JsonElement;

import java.util.List;

import microsoft.aspnet.signalr.client.Action;
import microsoft.aspnet.signalr.client.ErrorCallback;
import microsoft.aspnet.signalr.client.MessageReceivedHandler;
import microsoft.aspnet.signalr.client.Platform;
import microsoft.aspnet.signalr.client.http.android.AndroidPlatformComponent;
import microsoft.aspnet.signalr.client.hubs.HubConnection;
import microsoft.aspnet.signalr.client.hubs.HubProxy;

/**
 * Created by 452781 on 10/21/2016.
 */
public class CustomerSignalRService extends Service {

    Gson gson = new Gson();

    Context context;
    private HubConnection signalR;
    private static HubProxy mHubProxy;
    MessageReceivedHandler messageReceivedHandler;
    private final IBinder mBinder = new LocalBinder(); // Binder given to clients
    Boolean foreground = false;

    public CustomerSignalRService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        int result = super.onStartCommand(intent, flags, startId);
        StartConnection();
        return result;
    }


    @Override
    public void onDestroy() {
        if (signalR != null) {
            try {
                signalR.stop();
            } catch (NullPointerException e) {
                Log.e("TAG", "Cannot stop service while starting");
            }


        }
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        // Return the communication channel to the service.
        StartConnection();
        return mBinder;
    }

    /**
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    public class LocalBinder extends Binder {
        public CustomerSignalRService getService() {
            return CustomerSignalRService.this;
        }
    }

    /**
     * method for clients (activities)
     */
    public static void sendMessage(String message, String type) {

        System.out.println("@@##  makeSignalRCall sendMessage called");
        String SERVER_METHOD_SEND = "SendCustomerPosition";

        if (type.equals("greeting")) {
            SERVER_METHOD_SEND = "SendCustomerGreeting";
        } else if (type.equals("geo")) {
            SERVER_METHOD_SEND = "SendCustomerDistance";
        } else if (type.equals("CustomerDetails")) {
            SERVER_METHOD_SEND = "SendCustomerDetails";
        }

        if (mHubProxy != null) {
            mHubProxy.invoke(SERVER_METHOD_SEND, message);
        }


    }

    private void StartConnection() {
        Platform.loadPlatformComponent(new AndroidPlatformComponent());

        Log.e("TAG", "Start Connection called.");


        String serverUrl = Constants.SignalRURL;
        signalR = new HubConnection(serverUrl);

        mHubProxy = signalR.createHubProxy(Constants.SignalRHubProxy);

//        signalR.setMessageId("2");

        signalR.start().done(new Action<Void>() {
            @Override
            public void run(Void aVoid) throws Exception {
                Log.e("TAG", "^^^^Customer Connected.");
//                SplashActivity.associateServiceReady = true;
                mHubProxy.invoke("JoinGroup", signalR.getConnectionId(), "Customer");

                PostCustomerDetails();

            }
        });

        signalR.error(new ErrorCallback() {
            @Override
            public void onError(Throwable throwable) {
                Log.e("TAG", "#@#@" + throwable.getMessage());
                if (throwable.getMessage().contains("Disconnected")) {
                    signalR.stop();
                }
            }
        });


        signalR.received(new MessageReceivedHandler() {
            @Override
            public void onMessageReceived(final JsonElement jsonElement) {
                Log.e("TAG", "onCustomerMessageReceived" + jsonElement);

                Gson gson = new Gson();
//                AddAssociatePosition getassociatedatafromsignalR = gson.fromJson(String.valueOf(jsonElement), AddAssociatePosition.class);
//
//                if (String.valueOf(getassociatedatafromsignalR.getM()).equals("BroadCastAssociateMessage")) {
//
//                    Log.e("****", "Received associate message ");
//                    AssociateSignalRJobCallOrderAssociateListModel[] associateSignalRJobCallOrderAssociateListModel = gson.fromJson(String.valueOf(getassociatedatafromsignalR.getA()), AssociateSignalRJobCallOrderAssociateListModel[].class);
//
//
//                    if (checkForAssociateTask(associateSignalRJobCallOrderAssociateListModel[0].getTaskAssignedTo())) {
//                        Log.e("****", "Received associate message for associate ID " + AccountState.getPrefUserId());
//                        new CheckIfForeground().execute();
//                    }

//                }


            }
        });


    }

    private void PostCustomerDetails() {
        CustomerModel customerModel = new CustomerModel();

        customerModel.setUserId(AccountState.getUserID());
        customerModel.setFirstName(AccountState.getUserName());


        sendMessage(gson.toJson(customerModel), "CustomerDetails");
    }


//    private boolean checkForAssociateTask(ArrayList<AssociateSignalRJobCallModel> associateSignalRJobCallModels) {
//        Log.e("****", "checkForAssociateTask called ");
//        for (int i = 0; i < associateSignalRJobCallModels.size(); i++) {
//            if (associateSignalRJobCallModels.get(i).getAssocID().equals(AccountState.getPrefUserId())) {
//                return true;
//            }
//        }
//        return false;
//    }


    /*
    Class to check activity is running in foreground or background
     */
    private class CheckIfForeground extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {

            ActivityManager activityManager = (ActivityManager) getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE);
            List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
                if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    Log.i("Foreground App", appProcess.processName);

                    if (getApplicationContext().getPackageName().equalsIgnoreCase(appProcess.processName)) {

                        foreground = true;
                        // close_app();
                    } else {
                        foreground = false;
                    }
                }
            }

            if (foreground) {

                showAlertDialog();


            } else {
                //if not foreground

                sendNotification();
            }

            return null;
        }

    }

    /*
    Create alert window
     */
    public void showAlertDialog() {

        Intent intent = new Intent(context, DialogActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
//        new Handler(Looper.getMainLooper()).post(new Runnable() {
//            @Override
//            public void run() {
//
//
//            }
//        });
    }

    /*

    create system notification
     */
    public void sendNotification() {

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this);

//Create the intent that’ll fire when the user taps the notification//

        Intent intent = new Intent(getApplicationContext(), Login_Screen.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        mBuilder.setContentIntent(pendingIntent);

        mBuilder.setSmallIcon(R.drawable.retail_mate_app_icon);
        mBuilder.setContentTitle("New Tasks Alert");
        mBuilder.setContentText("You have been assigned new tasks, please login to check!");

        NotificationManager mNotificationManager =

                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.notify(001, mBuilder.build());
    }
}