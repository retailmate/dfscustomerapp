package com.cognizant.iot.productdetails.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.cognizant.retailmate.R;


public class User_moredetails extends AppCompatActivity {
    TextView name;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_moredetails);

        name = (TextView) findViewById(R.id.name);
        name.setText(getIntent().getStringExtra("name"));
    }
}
