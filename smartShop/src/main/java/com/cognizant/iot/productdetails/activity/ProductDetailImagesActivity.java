package com.cognizant.iot.productdetails.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.cognizant.iot.homepage.models.productsearch.ProductSearchImageModel;
import com.cognizant.iot.productdetails.adapter.DataAdapter;
import com.cognizant.iot.productdetails.model.Imagemodel;
import com.cognizant.retailmate.R;

import java.util.ArrayList;

/**
 * Created by 599584 on 2/20/2017.
 */

public class ProductDetailImagesActivity extends AppCompatActivity {
    //    public List<IconData1> data1 = new ArrayList<>();
    TextView tv;
    ProductSearchImageModel imageModels;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_productdetails_images);
        tv = (TextView) findViewById(R.id.close);
        tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        imageModels = (ProductSearchImageModel) getIntent().getSerializableExtra("imageModel");

        RecyclerView recyclerView1 = (RecyclerView) findViewById(R.id.recyclerView1);

        ArrayList androidVersions = prepareData();
        DataAdapter adapter = new DataAdapter(getApplicationContext(), androidVersions);
        recyclerView1.setHasFixedSize(true);
        recyclerView1.setLayoutManager(new LinearLayoutManager(this));
        recyclerView1.setAdapter(adapter);


    }

    private final String android_image_urls[] = {
            "https://images-na.ssl-images-amazon.com/images/I/A1jbT0WKfZL._SL1500_.jpg",
            "https://d2cv4qiga7xhrv.cloudfront.net/media/catalog/product/cache/1/small_image/500x/9df78eab33525d08d6e5fb8d27136e95/d/r/dr5002-b00150_01-dry-red-no-5-laptop-backpack.jpg",
            "https://everlane-2.imgix.net/i/23a52272_ccb9.jpg",
            "https://3.imimg.com/data3/RA/MH/MY-7328797/pitthu-bags-250x250.jpg"

    };

    private ArrayList prepareData() {

        ArrayList image_model = new ArrayList<>();
        for (int i = 0; i < getImages().length; i++) {
            Imagemodel imagemodel = new Imagemodel();

            imagemodel.setImage_url(getImages()[i]);
            image_model.add(imagemodel);
        }
        return image_model;
    }

    String[] getImages() {
        String[] imgurls = new String[imageModels.getItems().size()];
        for (int i = 0; i < imageModels.getItems().size(); i++) {
            imgurls[i] = "https://etihaddemodevret.cloudax.dynamics.com/MediaServer/" + imageModels.getItems().get(i).getUrl();
        }

        return imgurls;
    }
}


