package com.cognizant.iot.productdetails.adapter;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cognizant.iot.productdetails.model.ReviewModel;
import com.cognizant.retailmate.R;

import java.util.List;


/**
 * Created by 599584 on 2/21/2017.
 */

public class ProductReviewRecyclerviewAdapter extends RecyclerView.Adapter<ProductReviewRecyclerviewAdapter.MyViewHolder> implements PopupMenu.OnMenuItemClickListener {

    List<ReviewModel> reviewList;
    Context context;


    LayoutInflater mlayoutinflater;
    ImageView call;
    ImageView chat;
    ImageView options;
    Snackbar snackbar;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView name, date, review;
        ImageView imageView;
        LinearLayout layout;

        public MyViewHolder(View view) {
            super(view);
            layout = (LinearLayout) view.findViewById(R.id.layout);
            name = (TextView) view.findViewById(R.id.name);
            date = (TextView) view.findViewById(R.id.date);
            review = (TextView) view.findViewById(R.id.review);
            imageView = (ImageView) view.findViewById(R.id.imageView);

            mlayoutinflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);

        }
    }

    public ProductReviewRecyclerviewAdapter(Context context, List<ReviewModel> list) {
        this.context = context;
        this.reviewList = list;
    }

    @Override
    public ProductReviewRecyclerviewAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.userreview, parent, false);
        snackbar = Snackbar.make(parent, "", Snackbar.LENGTH_INDEFINITE);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        final ReviewModel model = reviewList.get(position);

        holder.name.setText(model.getName());
        holder.date.setText(model.getDate());
        holder.review.setText(model.getReview());
        holder.imageView.setImageDrawable(model.getImage());

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(context, User_moredetails.class);
//                intent.putExtra("name", model.getName());
//                context.startActivity(intent);


                snack();


            }
        });
    }

    @Override
    public int getItemCount() {
        return reviewList.size();
    }

    public void snack() {


        // Get the Snackbar's layout view
        final Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
        // Inflate our custom view
        View snackView = mlayoutinflater.inflate(R.layout.productdetails_snackoptions, null);
        snackView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        // Configure the view
        call = (ImageView) snackView.findViewById(R.id.snack_phone);
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("TAG", "---------onclick call");
            }
        });
        chat = (ImageView) snackView.findViewById(R.id.snack_chat);
        chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(view.getContext(), "call in process", Toast.LENGTH_SHORT).show();
            }
        });
        options = (ImageView) snackView.findViewById(R.id.snack_options);
        options.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopup(view);
            }
        });

        // Add the view to the Snackbar's layout
        if (snackbar.isShown()) {
            layout.removeAllViews();
            snackbar.dismiss();
        } else {
            layout.addView(snackView, 0);
            snackbar.show();
        }


    }


    public void showPopup(View v) {
        PopupMenu popupMenu = new PopupMenu(context, v);
        popupMenu.setOnMenuItemClickListener(this);
        MenuInflater inflater = popupMenu.getMenuInflater();
        inflater.inflate(R.menu.snack_menu, popupMenu.getMenu());
        popupMenu.show();
    }


    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.profile:
                Log.e("##@@", "Profile clicked");
                Toast.makeText(context, "showing profile", Toast.LENGTH_LONG).show();
                return true;
            case R.id.ar_screen:
                Log.e("##@@", "orders clicked");

                return true;
            default:
                return false;
        }

    }

}
