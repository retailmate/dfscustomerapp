package com.cognizant.iot.wishlist.activity;



import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
 
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;
 
import android.util.Log;
 
public class JSONfunctions_sharewishlist {
 
	public static JSONObject getJSONfromURL(String url,String email) {
		InputStream is = null;
		String result = "";
		JSONObject jArray = null;
 
		// Download JSON data from URL
		try {
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(url);
			
			
			 List<NameValuePair> params = new ArrayList<NameValuePair>();
//	            params.add(new BasicNameValuePair("macaddr", Share_Wishlist.imei));
//	            params.add(new BasicNameValuePair("mail", email));
	            //params.add(new BasicNameValuePair("pass", "xyz"));
	            UrlEncodedFormEntity ent = new UrlEncodedFormEntity(params,HTTP.UTF_8);
	            httppost.setEntity(ent);
	          
			
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity httpentity = response.getEntity();
			is = httpentity.getContent();
 
		} catch (Exception e) {
			Log.e("log_tag", "Error in http connection " + e.toString());
		}
 
		// Convert response to string
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "iso-8859-1"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			result = sb.toString();
		} catch (Exception e) {
			Log.e("log_tag", "Error converting result " + e.toString());
		}
 
		try {
 
			jArray = new JSONObject(result);
		} catch (JSONException e) {
			Log.e("log_tag", "Error parsing data " + e.toString());
		}
 
		return jArray;
	}
}
