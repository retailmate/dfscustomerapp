package com.cognizant.iot.wishlist.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cognizant.iot.mycart.JsonFuntionCart;
import com.cognizant.iot.utils.AccountState;
import com.cognizant.iot.wishlist.activity.ImageLoader_wish;
import com.cognizant.iot.wishlist.activity.JSONfunctions_wish_deleteproduct;
import com.cognizant.iot.wishlist.activity.Product_description_wishlistscreen;
import com.cognizant.iot.wishlist.activity.Wishlist_Screen;
import com.cognizant.retailmate.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ListViewAdapter_wish extends BaseAdapter implements Filterable {

    // Declare Variables
    Context context;
    LayoutInflater inflater;
    ArrayList<HashMap<String, String>> data;
    ImageLoader_wish imageLoader;
    ArrayList<HashMap<String, String>> initialDisplayedValues;
    HashMap<String, String> resultp = new HashMap<String, String>();
    ProgressDialog mProgressDialog;
    JSONObject jsonobject;
    JSONArray jsonarray;
    private SalesPartFilter filter;
    ArrayList<HashMap<String, String>> arraylist;
    String prodid1;
    String lineId;
    int product_should_be_added = 0;

    public ListViewAdapter_wish(Context context,
                                ArrayList<HashMap<String, String>> arraylistpassed) {
        this.context = context;
        data = arraylistpassed;
        imageLoader = new ImageLoader_wish(context);
        inflater = LayoutInflater.from(context);
        initialDisplayedValues = arraylistpassed;

    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        // Declare Variables
        TextView category;
        TextView unit;
        TextView prodname;
        TextView prodprice, offer, offer_in_tag;
        ImageView flag;
        ImageButton delete_img_btn;
        final ImageButton tag_cart_icon;
        ImageView tag;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View itemView = inflater.inflate(R.layout.listview_item_wish_vertical,
                parent, false);
        // Get the position
        resultp = data.get(position);

        // Locate the TextViews in listview_item.xml
        category = (TextView) itemView.findViewById(R.id.prodcategory_wish);
        prodname = (TextView) itemView.findViewById(R.id.prodname_wish);
        prodprice = (TextView) itemView.findViewById(R.id.prodprice_wish1);
        delete_img_btn = (ImageButton) itemView
                .findViewById(R.id.delete_small_trash1);
        tag_cart_icon = (ImageButton) itemView.findViewById(R.id.tag_cart_icon);
        // Locate the ImageView in listview_item.xml
        flag = (ImageView) itemView.findViewById(R.id.flag_wish);
        offer = (TextView) itemView.findViewById(R.id.offers_wish);
        offer_in_tag = (TextView) itemView.findViewById(R.id.offer_in_tag);
        unit = (TextView) itemView.findViewById(R.id.unit);
        tag = (ImageView) itemView.findViewById(R.id.tag);

        // Capture position and set results to the TextViews
        category.setText(resultp.get(Wishlist_Screen.CATEGORY));
        prodname.setText(resultp.get(Wishlist_Screen.PRODNAME));
        prodprice.setText(resultp.get(Wishlist_Screen.PRODPRICE));
        unit.setText(resultp.get(Wishlist_Screen.UNIT));

        if (resultp.get(Wishlist_Screen.OFFER).length() > 0) {
            offer.setTextSize(15);
            offer_in_tag.setText(resultp.get(Wishlist_Screen.OFFER));
            // if (resultp.get(Wishlist_Screen.LASTDATE).length() > 0)
            // offer.setText(resultp.get(Wishlist_Screen.OFFER)
            // + " Available till "
            // + resultp.get(Wishlist_Screen.LASTDATE));
            // else
            // offer.setText(resultp.get(Wishlist_Screen.OFFER));
        } else {

            tag.setVisibility(View.INVISIBLE);
            offer_in_tag.setVisibility(View.INVISIBLE);
            offer.setTextColor(Color.DKGRAY);
            offer.setText("No offers for this product");
        }

        // Capture position and set results to the ImageView
        // Passes flag images URL into ImageLoader.class
        imageLoader.DisplayImage(resultp.get(Wishlist_Screen.FLAG), flag);
        // Capture ListView item click

        tag_cart_icon.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if (R.drawable.cart_red_icon == tag_cart_icon.getId()) {
                    tag_cart_icon.setImageResource(R.drawable.no_crl_cart_icon);
                } else {
                    tag_cart_icon.setImageResource(R.drawable.cart_red_icon);
                }

                resultp = data.get(position);
                prodid1 = resultp.get(Wishlist_Screen.PRODID);
                lineId = resultp.get(Wishlist_Screen.LINEID);
                new DownloadJSONforaddCart().execute();

            }
        });

		/*
         * delete_img_btn.setBackgroundColor(Color.RED);
		 */
        delete_img_btn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                resultp = data.get(position);
                lineId = resultp.get(Wishlist_Screen.LINEID);


                new DownloadJSONfordelete().execute();

                data.remove(position);
                notifyDataSetChanged();

            }
        });

        itemView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // Get the position
                resultp = data.get(position);

                Intent intent = new Intent(context,
                        Product_description_wishlistscreen.class);
                // Pass all data prodid
                intent.putExtra("prodid", resultp.get(Wishlist_Screen.PRODID));
                // Pass all data prodname
                intent.putExtra("prodname",
                        resultp.get(Wishlist_Screen.PRODNAME));
                // Pass all data prodprice
                intent.putExtra("prodprice",
                        resultp.get(Wishlist_Screen.PRODPRICE));
                // Pass all data flag
                intent.putExtra("flag", resultp.get(Wishlist_Screen.FLAG));
                intent.putExtra("desc", resultp.get(Wishlist_Screen.DESC));
                intent.putExtra("beacon", resultp.get(Wishlist_Screen.BEACON));
                intent.putExtra("offer", resultp.get(Wishlist_Screen.OFFER));
                intent.putExtra("category",
                        resultp.get(Wishlist_Screen.CATEGORY));
                intent.putExtra("wish_present", "1");
                intent.putExtra("unit", resultp.get(Wishlist_Screen.UNIT));
                // Start SingleItemView Class
                context.startActivity(intent);

            }
        });
        return itemView;
    }

    /*
     * add to cart
     */
    // DownloadJSON AsyncTask
    private class DownloadJSONforaddCart extends AsyncTask<Void, Void, Void> {
        String str4 = null, str3 = null, str5 = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {

			/*
             * System.setProperty("https.proxyHost", "10.243.115.76");
			 * System.setProperty("https.proxyPort", "6050");
			 * System.setProperty("http.proxyHost", "10.243.115.76");
			 * System.setProperty("http.proxyPort", "6050");
			 */

            // Create an array
            arraylist = new ArrayList<HashMap<String, String>>();
            // Retrieve JSON Objects from the given URL address
            jsonobject = JsonFuntionCart
                    .getJSONfromURL(
                            "http://a92c7aa3a5d5429cba31efe555e0cb58.cloudapp.net/api/ns/Product/AddProductToCart",
                            prodid1, "1");

            try {

                JSONObject obj1 = new JSONObject(jsonobject.getString("header"));
                String message = obj1.getString("message");
                if (message.equalsIgnoreCase("Request processed successfully")) {

                    String status = obj1.getString("status");
                    if (status.equalsIgnoreCase("success")) {

                        JSONArray obj3 = (JSONArray) jsonobject
                                .get("cusdetails");
                        try {
                            str3 = (String) obj3.getJSONObject(0).get("prodid");

                        } catch (JSONException j) {
                            j.printStackTrace();
                        }
                    }

                }
            } catch (JSONException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            Toast.makeText(
                    context,
                    "Request processed successfully\nProduct "
                            + " Added to the Cart", Toast.LENGTH_LONG).show();
            // mProgressDialog.dismiss();
        }
    }

    // DownloadJSON AsyncTask
    private class DownloadJSONfordelete extends AsyncTask<Void, JSONObject, JSONObject> {
        String str4 = null, str3 = null, str5 = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Create a progressdialog
            mProgressDialog = new ProgressDialog(context);
            // Set progressdialog title
            mProgressDialog.setTitle("Deleting");
            // Set progressdialog message
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            // Show progressdialog
            mProgressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(Void... params) {


            // Create an array
            arraylist = new ArrayList<HashMap<String, String>>();
            // Retrieve JSON Objects from the given URL address
            String deleteWishListUrl = "http://ethcustomervalidtionax.azurewebsites.net/api/Commerce/DeleteWishListItems?idToken=" + AccountState.getTokenID();
            Log.d("@@##", "lineId = " + lineId);
            jsonobject = JSONfunctions_wish_deleteproduct.getJSONfromURL(deleteWishListUrl, lineId);


            return jsonobject;
        }

        @Override
        protected void onPostExecute(JSONObject jsonobject) {
            mProgressDialog.dismiss();

            try {
                if (jsonobject.has("Data")) {
                    JSONObject dataObject = jsonobject.getJSONObject("Data");
                    if (dataObject.getBoolean("success")) {
                        Toast.makeText(context, "Wishlist deleted successfully",
                                Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(context, "Unable to delete wishlist item",
                                Toast.LENGTH_LONG).show();
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
//            Intent hh = new Intent(context, Wishlist_Screen.class);
//            hh.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            context.startActivity(hh);

        }
    }

    public Filter getFilter() {
        if (filter == null) {
            filter = new SalesPartFilter();
        }
        return filter;
    }

    ArrayList<HashMap<String, String>> productDisplayedValues;

    private class SalesPartFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            constraint = constraint.toString().toLowerCase();
            FilterResults result = new FilterResults();
            if (data == null) {
                data = new ArrayList<HashMap<String, String>>(
                        initialDisplayedValues);
            }
            if (constraint == null || constraint.length() == 0) {
                result.count = data.size();
                result.values = data;
            }
            if (constraint != null && constraint.toString().length() > 0) {
                productDisplayedValues = new ArrayList<HashMap<String, String>>();

                for (HashMap<String, String> map : data) {
                    product_should_be_added = 0;
                    for (Map.Entry<String, String> entry : map.entrySet()) {

                        if (entry.getKey() == Wishlist_Screen.PRODNAME
                                && entry.getValue().toString().toLowerCase()
                                .contains(constraint)) {
                            product_should_be_added = 1;
                        }
                    }
                    if (product_should_be_added == 1) {
                        productDisplayedValues.add(map);
                    }
                }

                result.count = productDisplayedValues.size();
                result.values = productDisplayedValues;
            } else {
                synchronized (this) {
                    result.values = initialDisplayedValues;
                    result.count = data.size();
                }
            }

            return result;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            if (results.count > 0) {
                data = (ArrayList<HashMap<String, String>>) results.values;
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }

        }
    }

}