package com.cognizant.iot.wishlist.activity;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.cognizant.iot.activity.CatalogActivity;
import com.cognizant.iot.activityfindproduct.JSONFunctionsDetails_findprod;
import com.cognizant.iot.activitywishlist.JSONfunctions_wish;
import com.cognizant.iot.utils.AccountState;
import com.cognizant.iot.wishlist.adapter.ListViewAdapter_wish;
import com.cognizant.retailmate.R;

public class Wishlist_Screen extends AppCompatActivity {
    // Declare Variables
    JSONObject jsonobject, jsonobject1, jsonobjectdetail;
    JSONArray jsonarray, jsonarraydetails;
    ListView listview;
    ListViewAdapter_wish adapter;
    ProgressDialog mProgressDialog;
    ArrayList<HashMap<String, String>> arraylist;
    public static String PRODID = "prodid";
    public static String LINEID = "LineId";
    public static String CATEGORY = "category";
    public static String UNIT = "unit";
    public static String PRODNAME = "prodname";
    public static String PRODPRICE = "prodprice";
    public static String FLAG = "flag";
    Boolean arrayempty = false;
    public static String DESC = "desc";
    public static String BEACON = "beacon";
    public static String OFFER = "offer";
    public static String LASTDATE = "lastdate";
    public static String imei = "";
    EditText wishlistSearch;
    Intent wishlist_intent;
    SearchView mSearchView;

    @Override
    public void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        // Get the view from listview_main.xml
        setContentView(R.layout.listview_main_wish);
        // Execute DownloadJSON AsyncTask

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarWishlist);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        imei = getIntent().getStringExtra("imeinmbr");
        initialize();
        new DownloadJSONAX().execute();

    }

    private void initialize() {
        // wishlistSearch= (EditText) findViewById(R.id.search_wish_list);
        listview = (ListView) findViewById(R.id.listview_wish);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        /*
		 * getMenuInflater().inflate(R.menu.main, menu); return true;
		 */


        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        switch (item.getItemId()) {
            case R.id.share_wish_btn:
//                wishlist_intent = new Intent(this, Share_Wishlist.class);
//                startActivity(wishlist_intent);

                break;

            case android.R.id.home:
                finish();
                return true;

            default:
                break;
        }
        return true;

    }

    // DownloadJSON AsyncTask
//	private class DownloadJSON extends AsyncTask<Void, Void, Void> {
//
//		@Override
//		protected void onPreExecute() {
//			super.onPreExecute();
//			// Create a progressdialog
//			mProgressDialog = new ProgressDialog(Wishlist_Screen.this);
//			// Set progressdialog title
//			mProgressDialog.setTitle("My WishList");
//			// Set progressdialog message
//			mProgressDialog.setMessage("Loading...");
//			mProgressDialog.setIndeterminate(false);
//			// Show progressdialog
//			mProgressDialog.show();
//		}
//
//		@Override
//		protected Void doInBackground(Void... params) {
//
//			/*
//			 * System.setProperty("https.proxyHost", "10.243.115.76");
//			 * System.setProperty("https.proxyPort", "6050");
//			 * System.setProperty("http.proxyHost", "10.243.115.76");
//			 * System.setProperty("http.proxyPort", "6050");
//			 */// Create an array
//			arraylist = new ArrayList<HashMap<String, String>>();
//			// Retrieve JSON Objects from the given URL address
//			jsonobject1 = JSONfunctions_wish
//					.getJSONfromURL(CatalogActivity.urlPart
//							+ "/api/WishList/GetWishListDetails");
//
//			try {
//				// Locate the array name in JSON
//				jsonarray = jsonobject1.getJSONArray("cusdetails");
//
//				if (jsonarray.length() == 0)
//					arrayempty = true;
//				else {
//					for (int i = 0; i < jsonarray.length(); i++) {
//						HashMap<String, String> map = new HashMap<String, String>();
//						jsonobject = jsonarray.getJSONObject(i);
//						// Retrive JSON Objects
//						map.put("prodid", jsonobject.getString("prodid"));
//						map.put("category", jsonobject.getString("Category"));
//						map.put("unit", jsonobject.getString("Unit"));
//						map.put("prodname", jsonobject.getString("asset"));
//						map.put("prodprice", "$" + jsonobject.getString("Cost"));
////						map.put("prodprice", "$" + jsonobject.getString("Cost"));
//						map.put("flag", jsonobject.getString("Thumb"));
//						map.put("desc", jsonobject.getString("Desc"));
//						map.put("beacon", jsonobject.getString("Beacon"));
//						map.put("offer", jsonobject.getString("Offer"));
//						map.put("lastdate", jsonobject.getString("Lastdate"));
//						// Set the JSON Objects into the array
//						arraylist.add(map);
//
//					}
//				}
//			} catch (JSONException e) {
//				Log.e("Error", e.getMessage());
//				e.printStackTrace();
//			}
//			return null;
//		}
//
//		@Override
//		protected void onPostExecute(Void args) {
//
//			// Toast.makeText(getApplicationContext(),jsonobject1.toString() ,
//			// Toast.LENGTH_LONG).show();
//
//			if (arrayempty == true)
//				Toast.makeText(getApplicationContext(),
//						"Your wishlist is empty", Toast.LENGTH_LONG).show();
//
//			// Locate the listview in listview_main.xml
//			// listview = (ListView) findViewById(R.id.listview_wish);
//			// Pass the results into ListViewAdapter.java
//			adapter = new ListViewAdapter_wish(Wishlist_Screen.this, arraylist);
//			// Set the adapter to the ListView
//			adapter.notifyDataSetChanged();
//			listview.setAdapter(adapter);
//			// Close the progressdialog
//			mProgressDialog.dismiss();
//		}
//	}

    /*
     *
     * AX wishlist
     */
    // DownloadJSON AsyncTask
    private class DownloadJSONAX extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Create a progressdialog
            mProgressDialog = new ProgressDialog(Wishlist_Screen.this);
            // Set progressdialog title
            mProgressDialog.setTitle("My WishList");
            // Set progressdialog message
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            // Show progressdialog
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            // Create an array
            arraylist = new ArrayList<HashMap<String, String>>();

            //String jsonUrl = "https://etihaddemodevret.cloudax.dynamics.com/data/tests?$filter=Customer";
            String jsonUrl = "http://ethcustomervalidtionax.azurewebsites.net/api/Commerce/GetWishList?idToken=" + AccountState.getTokenID();
            try {


                jsonobject1 = JSONfunctions_wish
                        .getJSONfromAXURLGETWishlist(jsonUrl);

                System.out.println("whislist returned \n" + jsonobject1);


                JSONObject dataObject = jsonobject1.getJSONObject("Data");
                if (dataObject != null) {
                    if (dataObject.get("success") != null && (boolean) dataObject.get("success")) {
                        JSONObject wishListObject = dataObject.getJSONObject("wishlistdata");
                        JSONArray jsonarray = wishListObject.getJSONArray("CommerceListLines");

                        Log.d("@@##", "jsonarray length = " + jsonarray.length());
                        if (jsonarray.length() == 0) {
                            arrayempty = true;
                        } else {
                            for (int i = 0; i < jsonarray.length(); i++) {
                                HashMap<String, String> map = new HashMap<String, String>();
                                jsonobject = jsonarray.getJSONObject(i);
                                // Retrive JSON Objects
                                String productId = Long.toString((Long) jsonobject.get("ProductId"));
                                String lineId = Long.toString((Long) jsonobject.get("LineId"));
                                Log.d("@@##", "Wishlist_screen lineId@ = " + lineId);
                                map.put("prodid", productId);
                                map.put("LineId", lineId);
                                System.out.println("prodid \n"
                                        + productId);
                                map.put("itemid", "");

                                map.put("desc", "");
                                // map.put("beacon", jsonobject.getString("Beacon"));
                                map.put("offer", "");
                                // Set the JSON Objects into the array

                                JSONObject jsonobject1detail = JSONFunctionsDetails_findprod
                                        .getJSONfromURLAX(
                                                "https://etihaddemodevret.cloudax.dynamics.com/Commerce/Products/Search?$top=250&api-version=7.1",
                                                productId,
                                                CatalogActivity.imei);

                                System.out.println("jsonObject Wishlist Details \n");

                                try {

                                    jsonarraydetails = jsonobject1detail
                                            .getJSONArray("value");

                                    for (int ii = 0; ii < jsonarraydetails.length(); ii++) {
                                        jsonobjectdetail = jsonarraydetails
                                                .getJSONObject(ii);

                                        JSONObject insideImage = new JSONObject();
                                        insideImage = jsonobjectdetail
                                                .getJSONObject("Image");
                                        JSONArray insideitemimage = new JSONArray();
                                        insideitemimage = insideImage
                                                .getJSONArray("Items");

                                        JSONObject insideImageURL = new JSONObject();
                                        for (int j = 0; j < insideitemimage.length(); j++) {

                                            insideImageURL = insideitemimage
                                                    .getJSONObject(ii);
                                        }
                                        map.put("prodname", jsonobjectdetail
                                                .getString("ProductName"));

                                        map.put("category", jsonobjectdetail
                                                .getString("SearchName"));
                                        map.put("unit", "Each");
                                        map.put("prodprice",
                                                Html.fromHtml("<small>$</small>")
                                                        + jsonobjectdetail
                                                        .getString("AdjustedPrice"));

                                        map.put("beacon", "CE:0B:89:EE:21:9A");
                                        map.put("offer", "0");

                                        map.put("flag",
                                                "https://etihaddemodevret.cloudax.dynamics.com/MediaServer/"
                                                        + insideImageURL
                                                        .getString("Url"));

                                    }

                                } catch (JSONException e) {

                                    Log.e("Error", e.getMessage());
                                    e.printStackTrace();
                                }

                                arraylist.add(map);
                            }
                        }

                    }
                }

            } catch (NullPointerException | IndexOutOfBoundsException | JSONException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }


            return null;

        }

        @Override
        protected void onPostExecute(Void args) {

            if (arrayempty == true)
                Toast.makeText(getApplicationContext(),
                        "Your wishlist is empty", Toast.LENGTH_LONG).show();

            adapter = new ListViewAdapter_wish(Wishlist_Screen.this, arraylist);
            // Set the adapter to the ListView
            adapter.notifyDataSetChanged();
            listview.setAdapter(adapter);
            // Close the progressdialog
            mProgressDialog.dismiss();
        }
    }
}