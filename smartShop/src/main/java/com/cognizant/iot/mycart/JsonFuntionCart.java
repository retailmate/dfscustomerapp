package com.cognizant.iot.mycart;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.cognizant.iot.activity.CatalogActivity;
import com.cognizant.iot.orderhistory.activity.Order_Screen;
import com.cognizant.iot.utils.AccountState;
import com.cognizant.iot.utils.MySSLSocketFactory;

import android.util.Log;

public class JsonFuntionCart {

	/*
            ADD to Cart from wishlist
	 */

    public static JSONObject getJSONfromURL(String url, String prodid1,
                                            String quantity) {
        InputStream is = null;
        String result = "";
        JSONObject jArray = null;
        ArrayList<NameValuePair> postParameters;

        // Download JSON data from URL
        try {
            HttpClient httpclient = MySSLSocketFactory.getNewHttpClient();
            HttpPost httppost = new HttpPost(url);

            JSONObject parentData = new JSONObject();

            parentData.put("ProductId", prodid1);
            parentData.put("Quantity", quantity);


            StringEntity params = new StringEntity(parentData.toString());
            System.out.println("@@## req params " + parentData.toString());

            httppost.setEntity(params);
            httppost.setHeader("Content-Type", "application/json");


            HttpResponse response = httpclient.execute(httppost);
            HttpEntity httpentity = response.getEntity();
            is = httpentity.getContent();

        } catch (Exception e) {
            Log.e("log_tag", "Error in http connection " + e.toString());
        }

        // Convert response to string
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            result = sb.toString();
        } catch (Exception e) {
            Log.e("log_tag", "Error converting result " + e.toString());
        }

        try {

            jArray = new JSONObject(result);
        } catch (JSONException e) {
            Log.e("log_tag", "Error parsing data " + e.toString());
        }

        return jArray;
    }


    public static JSONObject deleteAXCartURL(String url, String prodid1
    ) {
        InputStream is = null;
        String result = "";
        JSONObject jArray = null;
        ArrayList<NameValuePair> postParameters;

        // Download JSON data from URL
        try {
            HttpClient httpclient = MySSLSocketFactory.getNewHttpClient();
            HttpPost httppost = new HttpPost(url);

            JSONObject parentData = new JSONObject();

            parentData.put("lineid", prodid1);


            StringEntity params = new StringEntity(parentData.toString());
            System.out.println("@@## req params " + parentData.toString());

            httppost.setEntity(params);
            httppost.setHeader("Content-Type", "application/json");


            HttpResponse response = httpclient.execute(httppost);
            HttpEntity httpentity = response.getEntity();
            is = httpentity.getContent();

        } catch (Exception e) {
            Log.e("log_tag", "Error in http connection " + e.toString());
        }

        // Convert response to string
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            result = sb.toString();
        } catch (Exception e) {
            Log.e("log_tag", "Error converting result " + e.toString());
        }

        try {

            jArray = new JSONObject(result);
        } catch (JSONException e) {
            Log.e("log_tag", "Error parsing data " + e.toString());
        }

        return jArray;
    }

}
