package com.cognizant.iot.mycart;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.cognizant.iot.activityfindproduct.JSONFunctionsDetails_findprod;
import com.cognizant.iot.loyaltypoints.simpleloyalty.LoyaltyPoints;
import com.cognizant.iot.orderhistory.activity.JSONfunctions_order;
import com.cognizant.iot.utils.AccountState;
import com.cognizant.iot.utils.Constants;
import com.cognizant.iot.utils.GlobalClass;
import com.cognizant.retailmate.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class MyCart extends AppCompatActivity {
    ProgressDialog mProgressDialog;
    ArrayList<HashMap<String, String>> arraylist;
    JSONObject jsonobject;
    ListView listview;
    JSONArray jsonarray;
    MyCartAdapter adapter2;

    ImageView checkoutCart;

    public static TextView totalValueCart, totalItemsCart;

    String whichClass;
    public static int loyaltyPoints;

    public static ImageView loyaltyRedeem;
    public static TextView loyaltyPointsNumber;

    public static double value1;

    GlobalClass mApplication;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

        setContentView(R.layout.mycart);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarCart);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);



		/*
         * For lotyalty dummy call
		 */
        mApplication = (GlobalClass) getApplicationContext();
        mApplication.setLp(false);

		/*
         *
		 */

        checkoutCart = (ImageView) findViewById(R.id.checkoutCart);
        totalItemsCart = (TextView) findViewById(R.id.totalItemsCart);
        totalValueCart = (TextView) findViewById(R.id.totalValueCart);
        loyaltyRedeem = (ImageView) findViewById(R.id.loyaltyRedeem);
        loyaltyPointsNumber = (TextView) findViewById(R.id.loyaltyPointsNumber);

        whichClass = getIntent().getStringExtra("whichClass");

        if (whichClass.equals("loyalty")) {
            loyaltyPoints = getIntent().getIntExtra("loyaltyPoints", 0);

            loyaltyRedeem.setVisibility(View.INVISIBLE);
            loyaltyPointsNumber.setVisibility(View.VISIBLE);

        }

        loyaltyRedeem.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(getApplicationContext(),
                        LoyaltyPoints.class);
                intent.putExtra("FromCart", true);
                startActivity(intent);
                // finish();
            }
        });


        new DownloadJSON().execute();

    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();

        whichClass = getIntent().getStringExtra("whichClass");

        if (whichClass.equals("catalog")) {
        } else {
            loyaltyPoints = getIntent().getIntExtra("loyaltyPoints", 0);

            loyaltyRedeem.setVisibility(View.INVISIBLE);
            loyaltyPointsNumber.setVisibility(View.VISIBLE);

            // int value = Integer.parseInt((String) totalValueCart.getText());
            // int value = 0;
            System.out.println("@@@## VCART VALUE " + value1);
            System.out.println("@@@## loyalty points GOT " + loyaltyPoints);

            loyaltyPointsNumber.setText(String.valueOf(loyaltyPoints)
                    .toString());


        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar app icon/title

        // Handle action bar actions click
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // DownloadJSON AsyncTask
    private class DownloadJSON extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Create a progressdialog
            mProgressDialog = new ProgressDialog(MyCart.this);
            // Set progressdialog title
            mProgressDialog.setTitle("Fetching your Cart..");
            // Set progressdialog message
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            // Show progressdialog
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            // Create an array
            arraylist = new ArrayList<HashMap<String, String>>();
            // Retrieve JSON Objects from the given URL address
            try {
                if (AccountState.getOfflineMode()) {
                    jsonobject = new JSONObject(getResponse("cartoffline"));
                } else {
                    jsonobject = JSONfunctions_order
                            .getJSONfromAXURL(Constants.CART_LIST_ITEM_REQUEST + AccountState.getTokenID());
                }
                System.out.println("@@## JSON response for GETTING THE CART  " + jsonobject.toString());

                JSONObject dataObject = jsonobject.getJSONObject("Data");
                JSONObject cartDataObject = dataObject.getJSONObject("CartData");
                // Locate the array name in JSON
                jsonarray = cartDataObject.getJSONArray("CartLines");

                System.out.println("@@## cusdetails " + jsonarray);

                for (int i = 0; i < jsonarray.length(); i++) {
                    HashMap<String, String> map = new HashMap<String, String>();
                    jsonobject = jsonarray.getJSONObject(i);
                    // Retrive JSON Objects
                    map.put("prodid", jsonobject.getString("ProductId"));
                    map.put("prodcategory", jsonobject.getString("WarehouseId"));
                    map.put("prodname", jsonobject.getString("Description"));
                    map.put("prodprice", jsonobject.getString("Price"));
                    map.put("prodquantity", jsonobject.getString("Quantity"));
                    map.put("lineid", jsonobject.getString("LineId"));
                    map.put("itemid", jsonobject.getString("ItemId"));

                    // map.put("flag", jsonobject.getString("Thumb"));
                    /*
                     * dummy removing from wishlist change this
					 */
                    map.put("wish_prsnt", "0");
                    // Set the JSON Objects into the array
                    arraylist.add(map);
                }
            } catch (NullPointerException | JSONException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {

            new DownloadJSONprodAX().execute();


            // Locate the listview in listview_main.xml
            listview = (ListView) findViewById(R.id.cartList);
            // Pass the results into ListViewAdapter.java
            adapter2 = new MyCartAdapter(MyCart.this, arraylist,
                    totalItemsCart, totalValueCart, checkoutCart,
                    loyaltyPointsNumber);
            // Set the adapter to the ListView
            listview.setAdapter(adapter2);


            // Close the progressdialog
            mProgressDialog.dismiss();


        }
    }

	/*
     * NEW
	 */

    private class DownloadJSONprodAX extends AsyncTask<Void, Void, Void> {
        String[] products = new String[(arraylist.size())];
        String productIds = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            Collections.reverse(arraylist);

            for (int i = 0; i < arraylist.size(); i++) {
                products[i] = arraylist.get(i).get("prodid");

            }

        }

        @Override
        protected Void doInBackground(Void... params) {
            String productRequestUrl = String.format("https://etihaddemodevret.cloudax.dynamics.com/Commerce/Products/Search?$top=250&api-version=7.1");
            Log.d("@@##", "productRequestUrl = " + productRequestUrl);
            ArrayList<HashMap<String, String>> arraylistprod = new ArrayList<HashMap<String, String>>();

            JSONObject cartResponseObject = null;
            try {

                if (AccountState.getOfflineMode()) {
                    cartResponseObject = new JSONObject(getResponse("cartextraoffline"));
                } else {

                    cartResponseObject = JSONFunctionsDetails_findprod
                            .getMultipleJSONfromURLAX(productRequestUrl, products, "");
                }
                System.out.println("@@## cartResponseObject for products = " + cartResponseObject.toString());
                JSONArray jsonarrayprod;
                JSONObject jsonobject1;

                // Locate the array name in JSON
                jsonarrayprod = cartResponseObject.getJSONArray("value");

                for (int i = 0; i < jsonarrayprod.length(); i++) {
                    HashMap<String, String> map = new HashMap<String, String>();
                    jsonobject1 = jsonarrayprod.getJSONObject(i);

                    arraylist.get(i).put("prodname",
                            jsonobject1.getString("ProductName"));


                    JSONObject imageObj = new JSONObject();
                    imageObj = jsonobject1.getJSONObject("Image");
                    JSONArray jsonarrayAXImage = imageObj
                            .getJSONArray("Items");
                    for (int j = 0; j < 1; j++) {
                        JSONObject jsonobjectAX = jsonarrayAXImage
                                .getJSONObject(j);


                        arraylist.get(i).put(
                                "flag",
                                "https://etihaddemodevret.cloudax.dynamics.com/MediaServer/"
                                        + jsonobjectAX
                                        .getString("Url"));
                        System.out.println("@@##IMAGE URL FOR CART" + jsonobjectAX
                                .getString("Url"));
//						}

                    }
                }
            } catch (JSONException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return null;

        }

        @Override
        protected void onPostExecute(Void args) {

            adapter2.notifyDataSetChanged();

        }
    }

    public String getResponse(String type) {
        String json = null;
        InputStream is = null;
        try {

            switch (type) {

                case "cartoffline":
                    is = getAssets().open("cartoffline.json");
                    break;
                case "cartextraoffline":
                    is = getAssets().open("cartextraoffline.json");
                    break;

            }
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }

        return json;
    }

}
