package com.cognizant.iot.mycart;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cognizant.iot.activity.CatalogActivity;
import com.cognizant.iot.activityproductlist.JSONfunctions_deletefrmwishlist_prod;
import com.cognizant.iot.activityproductlist.MainActivityprod;
import com.cognizant.iot.bopis.RmBopis;
import com.cognizant.iot.orderhistory.activity.ImageLoader_Orders;
import com.cognizant.iot.orderhistory.activity.Order_Products;
import com.cognizant.iot.utils.AccountState;
import com.cognizant.retailmate.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import cz.msebera.android.httpclient.Header;

public class MyCartAdapter extends BaseAdapter {

    // Declare Variables
    Context context;
    LayoutInflater inflater;
    ArrayList<HashMap<String, String>> data;
    ImageLoader_Orders imageLoader;
    HashMap<String, String> resultp = new HashMap<String, String>();
    HashMap<String, String> resultp2 = new HashMap<String, String>();
    ArrayList<HashMap<String, String>> arraylist;
    JSONObject jsonobject;
    JSONArray jsonarray;

    Boolean har = false;
    ProgressDialog prgDialog;

    String prodid1;

    //    final TextView prodquantity;
    TextView totalValue, totalCountItems, loyaltyPointsNumber;
    ImageView checkout;

    double value = 0;
    int totalCount = 0;

    public MyCartAdapter(final Context context,
                         ArrayList<HashMap<String, String>> arraylist,
                         TextView totalItemsCart, final TextView totalValueCart,
                         ImageView checkoutCart, final TextView loyaltyPointsNumber) {
        this.context = context;
        data = arraylist;
        imageLoader = new ImageLoader_Orders(context);

        checkout = checkoutCart;
        totalValue = totalValueCart;
        totalCountItems = totalItemsCart;

        checkoutCart.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                AlertDialog.Builder alertbox = new AlertDialog.Builder(v
                        .getRootView().getContext(), R.style.MyDatePickerDialogTheme);
                alertbox.setMessage("Amount Payable: $"
                        + String.valueOf((Double.parseDouble(totalValueCart
                        .getText().toString()) - (Double
                        .parseDouble(loyaltyPointsNumber.getText()
                                .toString())))));
                alertbox.setTitle("Cart Value");
                alertbox.setIcon(R.drawable.cart_icon);
                alertbox.setNegativeButton("cancel", null);
                alertbox.setPositiveButton("Continue",
                        new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface arg0, int arg1) {
                                System.out.println("@@### CHECKOUT CLICKED : "
                                        + totalValueCart.getText().toString()
                                        + " payable ammount : $"
                                        + String.valueOf(String.format(
                                        "%.2f",
                                        (Double.parseDouble(totalValueCart
                                                .getText().toString()) - (Double
                                                .parseDouble(loyaltyPointsNumber
                                                        .getText()
                                                        .toString()))))));

                                Intent intent = new Intent(context, RmBopis.class);
                                intent.putExtra("productList", data);
                                intent.putExtra("lpReedem", Double
                                        .parseDouble(loyaltyPointsNumber
                                                .getText().toString()));

                                context.startActivity(intent);
                                ((Activity) context).finish();
                            }
                        });
                alertbox.show();
            }
        });


    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        // Declare Variables
        TextView prodid, prodcategorytv;
        TextView prodname;
        TextView prodprice;
        ImageView flag;
        final TextView prodquantity;

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View itemView = inflater.inflate(R.layout.cart_listview_item, parent,
                false);
        // Get the position
        resultp = data.get(position);

        // Locate the TextViews in listview_item.xml
        // prodid = (TextView) itemView.findViewById(R.id.order_product_prodid);
        prodcategorytv = (TextView) itemView
                .findViewById(R.id.order_product_prodcategory_cart);
        prodname = (TextView) itemView.findViewById(R.id.order_product_cart);
        prodprice = (TextView) itemView
                .findViewById(R.id.order_product_prodprice1_cart);
        prodquantity = (TextView) itemView
                .findViewById(R.id.product_quantity_main1_cart);
        // Locate the ImageView in listview_item.xml
        flag = (ImageView) itemView
                .findViewById(R.id.image_order_products_cart);

        final ImageView wishlist_cart = (ImageView) itemView
                .findViewById(R.id.wishlist_cart);

        ImageView cartIcon_cart = (ImageView) itemView
                .findViewById(R.id.cartIcon_cart);


        ImageView plus_cart = (ImageView) itemView.findViewById(R.id.plus_cart);
        ImageView minus_cart = (ImageView) itemView.findViewById(R.id.minus_cart);

        plus_cart.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
//                prodquantity.setText(String.valueOf(Integer.parseInt((prodquantity.getText().toString())) + 1));
                prodquantity.setText(String.valueOf(new Double((prodquantity.getText().toString())).intValue() + 1));

                data.get(position).put(Order_Products.PRODQAUNTITY, String.valueOf(new Double(data.get(position).get(Order_Products.PRODQAUNTITY)).intValue() + 1));

                notifyDataSetChanged();
            }
        });


        minus_cart.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((new Double((prodquantity.getText().toString())).intValue()) > 1) {
//                    prodquantity.setText(String.valueOf(Integer.parseInt(prodquantity.getText().toString()) - 1));
                    prodquantity.setText(String.valueOf(new Double((prodquantity.getText().toString())).intValue() - 1));

                    data.get(position).put(Order_Products.PRODQAUNTITY, String.valueOf(new Double(data.get(position).get(Order_Products.PRODQAUNTITY)).intValue() - 1));
                    notifyDataSetChanged();
                }
            }
        });


        // Capture position and set results to the TextViews
        // prodid.setText(resultp.get(Order_Products.PRODID));
//        prodcategorytv.setText("Category : "
//                + resultp.get(Order_Products.PRODCATEGORY));

        prodcategorytv.setText("Category : "
                + "Houseware");

        prodname.setText(resultp.get(Order_Products.PRODNAME));
        // prodprice.setText("$"
        // + String.format(
        // "%.2f",
        // Double.parseDouble(resultp
        // .get(Order_Products.PRODPRICE))).toString());
        prodprice.setText("$"
                + String.format(
                "%.2f",
                Double.parseDouble(resultp
                        .get(Order_Products.PRODPRICE))).toString());
        prodquantity.setText(resultp.get(Order_Products.PRODQAUNTITY));
        // Capture position and set results to the ImageView
        // Passes flag images URL into ImageLoader.class

//        double value = 0;
//        String valueString;
//        for (int i = 0; i < data.size(); i++) {
//            valueString = data.get(i).get("prodprice");
//            System.out.println("@@## valuestring " + valueString);
//            // value = value + Integer.parseInt(valueString);
//            value = value + Double.parseDouble(valueString);
//
//            MyCart.value1 = value;
//        }

//        totalValue.setText(String.format("%.2f",
//                Double.parseDouble(String.valueOf(value))).toString());

        totalValue.setText(String.format("%.2f",
                Double.parseDouble(String.valueOf(calculateCartTotal()))).toString());


        if (Double.parseDouble((String) MyCart.loyaltyPointsNumber.getText()) > Double
                .parseDouble((String) MyCart.totalValueCart.getText())) {
            MyCart.loyaltyPointsNumber.setText(MyCart.totalValueCart.getText()
                    .toString());
        }

        // totalValue.setText(String.valueOf(value));


//        totalCountItems.setText(String.valueOf(data.size()));
        totalCountItems.setText(String.valueOf(totalCount));

        imageLoader.DisplayImage(resultp.get(Order_Products.FLAG), flag);

        if (resultp.get(MainActivityprod.WISH_PRSNT).toString().equals("0")) {
            wishlist_cart.setImageResource(R.drawable.no_crl_mywish_blu_icon);
            har = false;
        } else {
            wishlist_cart.setImageResource(R.drawable.hearticonnn);
            har = true;
        }

        cartIcon_cart.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                System.out.println("@@## MAP OF CART before " + data.size());

                // MyCart.loyaltyPointsNumber
                // .setText(String.valueOf(0).toString());
                MyCart.loyaltyPointsNumber.setVisibility(View.INVISIBLE);
                MyCart.loyaltyRedeem.setVisibility(View.VISIBLE);

                resultp2 = data.get(position);
                prodid1 = resultp2
                        .get(com.cognizant.iot.activity.CatalogActivity.PRODID);
                new DeleteFromCart().execute();
                data.remove(position);

                System.out.println("@@## MAP OF CART after " + data.size());
                notifyDataSetChanged();

            }
        });

        wishlist_cart.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (R.drawable.hearticonnn == wishlist_cart.getId()) {
                    wishlist_cart
                            .setImageResource(R.drawable.no_crl_mywish_icon);
                } else {
                    wishlist_cart.setImageResource(R.drawable.hearticonnn);
                }

                resultp2 = data.get(position);
                prodid1 = resultp2
                        .get(com.cognizant.iot.activity.CatalogActivity.PRODID);

                if (resultp2.get(MainActivityprod.WISH_PRSNT).toString()
                        .equals("0")) {
                    RequestParams params = new RequestParams();
                    params.put("macaddr", CatalogActivity.imei);
                    params.put("wishname", "Marriage");
                    params.put("products", prodid1);
                    addtowishlistService(params);

                    // Intent in = new Intent(context, CatalogActivity.class);
                    // in.putExtra("imeinmbr", ime);
                    // in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    // context.startActivity(in);
                } else {
                    new DownloadJSONfordelete().execute();
                    // Intent hh = new Intent(context, CatalogActivity.class);
                    // hh.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    // hh.putExtra("imeinmbr", ime);
                    // context.startActivity(hh);
                }
            }
        });

        return itemView;
    }


    double calculateCartTotal() {
        value = 0;
        totalCount = 0;
        String valueString;
        String countString;
        for (int i = 0; i < data.size(); i++) {
            valueString = data.get(i).get("prodprice");
            countString = data.get(i).get("prodquantity");
            System.out.println("@@## valuestring " + valueString);
            // value = value + Integer.parseInt(valueString);
            value = value + (Double.parseDouble(valueString) * Double.parseDouble(countString));
            totalCount = totalCount + (int) Double.parseDouble(countString);
            MyCart.value1 = value;
        }
        return value;
    }

    // DownloadJSON AsyncTask
    private class DownloadJSONforcartdelete extends AsyncTask<Void, Void, Void> {
        String str4 = null, str3 = null, str5 = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {

			/*
             * System.setProperty("https.proxyHost", "10.243.115.76");
			 * System.setProperty("https.proxyPort", "6050");
			 * System.setProperty("http.proxyHost", "10.243.115.76");
			 * System.setProperty("http.proxyPort", "6050");
			 */

            // Create an array
            arraylist = new ArrayList<HashMap<String, String>>();
            // Retrieve JSON Objects from the given URL address
            jsonobject = JsonFuntionCart
                    .getJSONfromURL(
                            "http://a92c7aa3a5d5429cba31efe555e0cb58.cloudapp.net/api/ns/Product/DeleteProductFromCart",
                            prodid1, "1");

            try {

                JSONObject obj1 = new JSONObject(jsonobject.getString("header"));
                String message = obj1.getString("message");
                if (message.equalsIgnoreCase("Request processed successfully")) {

                    String status = obj1.getString("status");
                    if (status.equalsIgnoreCase("success")) {

                        JSONArray obj3 = (JSONArray) jsonobject
                                .get("cusdetails");
                        try {
                            str3 = (String) obj3.getJSONObject(0).get("prodid");

                        } catch (JSONException j) {
                            j.printStackTrace();
                        }
                    }

                }
            } catch (JSONException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            Toast.makeText(
                    context,
                    "Request processed successfully\nProduct "
                            + " removed from the Cart", Toast.LENGTH_LONG)
                    .show();
            // mProgressDialog.dismiss();
        }
    }

    // DownloadJSON AsyncTask
    private class DownloadJSONfordelete extends AsyncTask<Void, Void, Void> {
        String str4 = null, str3 = null, str5 = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {

			/*
             * System.setProperty("https.proxyHost", "10.243.115.76");
			 * System.setProperty("https.proxyPort", "6050");
			 * System.setProperty("http.proxyHost", "10.243.115.76");
			 * System.setProperty("http.proxyPort", "6050");
			 */

            // Create an array
            arraylist = new ArrayList<HashMap<String, String>>();
            // Retrieve JSON Objects from the given URL address
            jsonobject = JSONfunctions_deletefrmwishlist_prod.getJSONfromURL(
                    CatalogActivity.urlPart
                            + "/api/WishList/DeleteWishlistProducts", prodid1);

            try {

                JSONObject obj1 = new JSONObject(jsonobject.getString("header"));
                String message = obj1.getString("message");
                if (message.equalsIgnoreCase("Request processed successfully")) {

                    String status = obj1.getString("status");
                    if (status.equalsIgnoreCase("success")) {

                        JSONArray obj3 = (JSONArray) jsonobject
                                .get("cusdetails");
                        try {
                            str3 = (String) obj3.getJSONObject(0).get("prodid");

                        } catch (JSONException j) {
                            j.printStackTrace();
                        }
                    }

                }
            } catch (JSONException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            Toast.makeText(
                    context,
                    "Request processed successfully\nProduct " + str3
                            + " from the wishlist", Toast.LENGTH_LONG).show();
            // mProgressDialog.dismiss();
        }
    }

    // This is Async task for adding to wishlist

    // Web service for ADD to WISHLIST

    /**
     * Method that performs RESTful webservice invocations
     *
     * @param params
     */
    public void addtowishlistService(RequestParams params) {

        // Toast.makeText(getApplicationContext(),"JSON request params is" +
        // params.toString(),Toast.LENGTH_LONG).show(); //to show the REQUEST

        AsyncHttpClient client = new AsyncHttpClient();

        client.post(CatalogActivity.urlPart + "/api/WishList/Add", params,
                new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        try {
                            String response = new String(responseBody, "UTF-8");
                            // JSON Object
                            JSONObject obj = new JSONObject(response);

                            JSONObject obj1 = new JSONObject(obj
                                    .getString("header"));

                            String msg = obj1.getString("message");
                            // Toast.makeText(getApplicationContext(),"Response is\n"
                            // +obj1.toString(),Toast.LENGTH_LONG).show();

                            if (msg.equalsIgnoreCase("Request processed successfully")) {

                                // Set Default Values for Edit View controls
                                // setDefaultValues();

                                // Toast.makeText(getApplicationContext(),"Request processed successfully",Toast.LENGTH_LONG).show();

                                String status = obj1.getString("status");

                                String str4 = null, str3 = null, str5 = null;
                                if (status.equalsIgnoreCase("success")) {

                                    JSONArray obj3 = (JSONArray) obj
                                            .get("cusdetails");

                                    try {
                                        str3 = (String) obj3.getJSONObject(0)
                                                .get("customer");
                                        str4 = (String) obj3.getJSONObject(0)
                                                .get("Products added");
                                        Toast.makeText(
                                                context,
                                                "Request processed successfully\nProduct "
                                                        + str4
                                                        + " added to wishlist for Customer: "
                                                        + str3,
                                                Toast.LENGTH_LONG).show();
                                    } catch (JSONException j) {

                                        str5 = (String) obj3.getJSONObject(0)
                                                .get("product");
                                        Toast.makeText(
                                                context,
                                                "Product " + str5
                                                        + " in the wishlist",
                                                Toast.LENGTH_LONG).show();
                                    }
                                }

                            }
                        } catch (IOException io) {
                            io.printStackTrace();
                        } catch (JSONException e) {

                            // TODO Auto-generated catch block
                            Toast.makeText(
                                    context,
                                    "Error Occured [Server's JSON response might be invalid]!",
                                    Toast.LENGTH_LONG).show();
                            e.printStackTrace();

                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

                        // Hide Progress Dialog
                        prgDialog.hide();

                        if (statusCode == 404) {
                            Toast.makeText(context,
                                    "Requested resource not found",
                                    Toast.LENGTH_LONG).show();
                        }
                        // When Http response code is '500'
                        else if (statusCode == 500) {
                            Toast.makeText(context,
                                    "Something went wrong at server end",
                                    Toast.LENGTH_LONG).show();
                        }
                        // When Http response code other than 404, 500
                        else {
                            Toast.makeText(
                                    context,
                                    "Device might not be connected to Internet",
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    // DeleteFromCart AsyncTask
    private class DeleteFromCart extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {

            // Create an array
            arraylist = new ArrayList<HashMap<String, String>>();
            // Retrieve JSON Objects from the given URL address
            jsonobject = JsonFuntionCart
                    .deleteAXCartURL("http://ethcustomervalidtionax.azurewebsites.net/api/cart/RemoveCartItems?idToken=" + AccountState.getTokenID(), resultp2
                            .get("lineid"));

            System.out.println("@@## jsonobject " + jsonobject.toString());
            try {
                JSONObject dataObject = jsonobject.getJSONObject("Data");

            } catch (JSONException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {


        }
    }

}