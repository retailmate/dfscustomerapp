package com.cognizant.iot.mycart;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.cognizant.iot.Network.VolleyHelper;
import com.cognizant.iot.Network.VolleyRequest;
import com.cognizant.iot.homepage.models.cart.CartLineModel;
import com.cognizant.iot.homepage.models.cart.CartModel;
import com.cognizant.iot.homepage.models.productsearch.ProductSearchModel;
import com.cognizant.iot.homepage.models.wishlist.WishlistCommerceListLineModel;
import com.cognizant.iot.homepage.models.wishlist.WishlistModel;
import com.cognizant.iot.loyaltypoints.simpleloyalty.LoyaltyPoints;
import com.cognizant.iot.mycart.adapter.BagRecyclerAdapter;
import com.cognizant.iot.orderhistory.model.OrderModel;
import com.cognizant.iot.utils.AccountState;
import com.cognizant.iot.utils.Constants;
import com.cognizant.iot.utils.GlobalClass;
import com.cognizant.retailmate.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 452781 on 2/26/2017.
 */
public class MyBagFragment extends Fragment {

    RecyclerView recyclerView;
    TextView mycart_tv, myorders_tv;
    ImageView mywishlist_tv;
    List<WishlistCommerceListLineModel> wishlistLineModels;

    private GridLayoutManager lLayout;
    LinearLayoutManager layoutManager;
    RelativeLayout cartHeader;
    TextView ordersHeader;
    LinearLayout wishlistHeader;
    /*
    CART
    */

    ImageView checkoutCart;

    public static TextView totalValueCart, totalItemsCart;

    String whichClass;
    public static int loyaltyPoints;

    public static ImageView loyaltyRedeem;
    public static TextView loyaltyPointsNumber;

    public static double value1;

    GlobalClass mApplication;

    BagRecyclerAdapter bagRecyclerAdapter;
    List<CartLineModel> cartLineModels;

    ProgressBar progressBar;

    /*
    CART END
     */

    SwipeRefreshLayout swipeRefreshLayout;
    View bag_recycler_cover;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = null;

        try {
            v = inflater.inflate(R.layout.bag_layout, container, false);
            ((AppCompatActivity) getActivity()).getSupportActionBar().hide();

        } catch (InflateException | IllegalArgumentException | NullPointerException e) {

        }

        swipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh_layout);
        progressBar = (ProgressBar) v.findViewById(R.id.progressBar);
        cartHeader = (RelativeLayout) v.findViewById(R.id.cartHeader);
        ordersHeader = (TextView) v.findViewById(R.id.ordersHeader);
        wishlistHeader = (LinearLayout) v.findViewById(R.id.wishlistHeader);

        layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        lLayout = new GridLayoutManager(getContext(), 2);

        recyclerView = (RecyclerView) v.findViewById(R.id.bagList);


        bag_recycler_cover = v.findViewById(R.id.bag_recycler_cover);

        mycart_tv = (TextView) v.findViewById(R.id.mycart_tv);
        myorders_tv = (TextView) v.findViewById(R.id.myorders_tv);
        mywishlist_tv = (ImageView) v.findViewById(R.id.mywishlist_tv);


        recyclerView.setLayoutManager(layoutManager);
        mycart_tv.setBackgroundColor(getResources().getColor(R.color.bgdark));
        mycart_tv.setTextColor(getResources().getColor(R.color.white));

        mycart_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerView.setLayoutManager(layoutManager);
                cartHeader.setVisibility(View.VISIBLE);
                wishlistHeader.setVisibility(View.GONE);
                ordersHeader.setVisibility(View.GONE);
                cartApiCall();
                mycart_tv.setBackgroundColor(getResources().getColor(R.color.bgdark));
                mycart_tv.setTextColor(getResources().getColor(R.color.white));
                myorders_tv.setBackgroundColor(getResources().getColor(R.color.white));
                myorders_tv.setTextColor(getResources().getColor(R.color.bgdark));
                mywishlist_tv.setImageResource(R.drawable.heart_red_outline_icon);
                mywishlist_tv.setColorFilter(getResources().getColor(R.color.bgdark));
            }
        });
        myorders_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerView.setLayoutManager(layoutManager);
                cartHeader.setVisibility(View.GONE);
                wishlistHeader.setVisibility(View.GONE);
                ordersHeader.setVisibility(View.VISIBLE);
                ordersApiCall();
                myorders_tv.setBackgroundColor(getResources().getColor(R.color.bgdark));
                myorders_tv.setTextColor(getResources().getColor(R.color.white));
                mycart_tv.setBackgroundColor(getResources().getColor(R.color.white));
                mycart_tv.setTextColor(getResources().getColor(R.color.bgdark));
                mywishlist_tv.setImageResource(R.drawable.heart_red_outline_icon);
                mywishlist_tv.setColorFilter(getResources().getColor(R.color.bgdark));
            }
        });
        mywishlist_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerView.setLayoutManager(lLayout);
                cartHeader.setVisibility(View.GONE);
                wishlistHeader.setVisibility(View.VISIBLE);
                ordersHeader.setVisibility(View.GONE);
                wishlistApiCall();
                mywishlist_tv.setImageResource(R.drawable.green_filled_heart);
                mywishlist_tv.setColorFilter(getResources().getColor(R.color.bgdark));
                myorders_tv.setBackgroundColor(getResources().getColor(R.color.white));
                myorders_tv.setTextColor(getResources().getColor(R.color.bgdark));
                mycart_tv.setBackgroundColor(getResources().getColor(R.color.white));
                mycart_tv.setTextColor(getResources().getColor(R.color.bgdark));

            }
        });

        /*
        Cart
         */
        /*
         * For lotyalty dummy call
		 */
        mApplication = (GlobalClass) getActivity().getApplicationContext();
        mApplication.setLp(false);

		/*
         *
		 */

        checkoutCart = (ImageView) v.findViewById(R.id.checkoutCart);
        totalItemsCart = (TextView) v.findViewById(R.id.totalItemsCart);
        totalValueCart = (TextView) v.findViewById(R.id.totalValueCart);
        loyaltyRedeem = (ImageView) v.findViewById(R.id.loyaltyRedeem);
        loyaltyPointsNumber = (TextView) v.findViewById(R.id.loyaltyPointsNumber);

        whichClass = getArguments().getString("whichClass");

        if (whichClass.equals("loyalty")) {
            loyaltyPoints = getActivity().getIntent().getIntExtra("loyaltyPoints", 0);

            loyaltyRedeem.setVisibility(View.INVISIBLE);
            loyaltyPointsNumber.setVisibility(View.VISIBLE);

        }

        loyaltyRedeem.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(getActivity().getApplicationContext(),
                        LoyaltyPoints.class);
                intent.putExtra("FromCart", true);
                startActivity(intent);
                // finish();
            }
        });
        cartApiCall();
        /*
        Cart End
         */
        return v;
    }


    @Override
    public void onResume() {
        super.onResume();

        whichClass = getArguments().getString("whichClass");

        if (whichClass.equals("catalog")) {
        } else {
            loyaltyPoints = getArguments().getInt("loyaltyPoints", 0);

            loyaltyRedeem.setVisibility(View.INVISIBLE);
            loyaltyPointsNumber.setVisibility(View.VISIBLE);

            // int value = Integer.parseInt((String) totalValueCart.getText());
            // int value = 0;
            System.out.println("@@@## VCART VALUE " + value1);
            System.out.println("@@@## loyalty points GOT " + loyaltyPoints);

            loyaltyPointsNumber.setText(String.valueOf(loyaltyPoints)
                    .toString());


        }
    }


    void cartApiCall() {
        bag_recycler_cover.setVisibility(View.VISIBLE);

        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(true);
            }
        });


        Map<String, String> headers = new HashMap<>();
        JSONObject params = null;
        try {

            headers.put("Content-Type", "application/json");

            params = new JSONObject();
            params.put("UserID", AccountState.getUserID());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        VolleyRequest<CartModel> recommendationModelVolleyRequest =
                new VolleyRequest<CartModel>(Request.Method.POST, Constants.CART_LIST_ITEM_REQUEST + AccountState.getTokenID(), CartModel.class,

                        new com.android.volley.Response.Listener<CartModel>() {
                            @Override
                            public void onResponse(CartModel response) {
//                                Toast.makeText(HomePageActivity.this, "Your Recommendations \n" + response.getValue(), Toast.LENGTH_SHORT).show();

//                                bagRecyclerAdapter = new BagRecyclerAdapter(getContext(), response.getCartBriefDataModel().getCartDatamodel().getCartLineModels(), totalItemsCart, totalValueCart, checkoutCart, loyaltyPointsNumber);

//                                recyclerView.setAdapter(bagRecyclerAdapter);

                                try {
                                    System.out.println("@@## cartProductDetailsApiCall " + response.getCartBriefDataModel().getCartDatamodel().getCartLineModels());
                                    cartProductDetailsApiCall(response.getCartBriefDataModel().getCartDatamodel().getCartLineModels());
                                } catch (NullPointerException e) {
                                    Toast.makeText(getContext(), "Your Cart is empty\n", Toast.LENGTH_SHORT).show();
                                    swipeRefreshLayout.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            swipeRefreshLayout.setRefreshing(false);
                                        }
                                    });
                                }


                            }
                        }
                        //API CALL GETTING MADE FOR Dynamic Version contd

                        , new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null && networkResponse.statusCode != 200) {
                            Log.e("TAG", "No network on update.");
                            Toast.makeText(getContext(), "Network Error, please check internet", Toast.LENGTH_SHORT).show();

                        }

                    }
                });


        RetryPolicy policy = new DefaultRetryPolicy(VolleyHelper.Timeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        recommendationModelVolleyRequest.setRetryPolicy(policy);
        VolleyHelper.getInstance(getContext()).addToRequestQueue(recommendationModelVolleyRequest);
    }

    void cartProductDetailsApiCall(List<CartLineModel> cartLineModel) {

        this.cartLineModels = cartLineModel;
        Collections.reverse(cartLineModels);


        String[] products = new String[(cartLineModels.size())];
        for (int i = 0; i < cartLineModels.size(); i++) {
            products[i] = cartLineModels.get(i).getProductId();

        }


        Map<String, String> headers = new HashMap<>();
        JSONObject finalObject = null;
        try {

            headers.put("Content-Type", "application/json");
            headers.put("Authorization", "id_token " + AccountState.getTokenID());
            Log.e("TAG", "AccountState.getTokenID()" + AccountState.getTokenID());
            headers.put(
                    "OUN",
                    "092");

            JSONArray parentData = new JSONArray();

            for (int i = 0; i < products.length; i++) {


                parentData.put(Double.valueOf(products[i]));
            }

            JSONObject childData = new JSONObject();

            childData.put("Ids", parentData);
            childData.put("DataLevelValue", 4);

            finalObject = new JSONObject();
            finalObject.put("productSearchCriteria", childData);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        VolleyRequest<ProductSearchModel> recommendationModelVolleyRequest =
                new VolleyRequest<ProductSearchModel>(Request.Method.POST, Constants.PRODUCT_SEARCH_BY_ID_API, ProductSearchModel.class, headers, finalObject,

                        new com.android.volley.Response.Listener<ProductSearchModel>() {
                            @Override
                            public void onResponse(ProductSearchModel response) {
//                                System.out.println("@@## cartLineModel NAMNE" + response.getProductSearchValueModel().get(0).getProductName());
                                bag_recycler_cover.setVisibility(View.GONE);

                                for (int i = 0; i < response.getProductSearchValueModel().size(); i++) {
                                    cartLineModels.get(i).setProductSearchValueModel(response.getProductSearchValueModel().get(i));
//                                    System.out.println("@@## cartLineModel " + response.getProductSearchValueModel().get(i));
                                }
//                                System.out.println("@@## calling BagRecyclerAdapter");
                                bagRecyclerAdapter = new BagRecyclerAdapter(getContext(), cartLineModels, totalItemsCart, totalValueCart, checkoutCart, loyaltyPointsNumber);

                                recyclerView.setAdapter(bagRecyclerAdapter);
//                                progressBar.setVisibility(View.GONE);
                                swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(false);
                                    }
                                });
                            }
                        }
                        //API CALL GETTING MADE FOR Dynamic Version contd

                        , new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null && networkResponse.statusCode != 200) {
                            Log.e("TAG", "No network on update.");
                            Toast.makeText(getContext(), "Network Error, please check internet", Toast.LENGTH_SHORT).show();
                            swipeRefreshLayout.post(new Runnable() {
                                @Override
                                public void run() {
                                    swipeRefreshLayout.setRefreshing(false);
                                }
                            });
                        }

                    }
                });


        RetryPolicy policy = new DefaultRetryPolicy(VolleyHelper.Timeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        recommendationModelVolleyRequest.setRetryPolicy(policy);
        VolleyHelper.getInstance(getContext()).addToRequestQueue(recommendationModelVolleyRequest);
    }

    void ordersApiCall() {
        bag_recycler_cover.setVisibility(View.VISIBLE);

        recyclerView.removeAllViews();

        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(true);
            }
        });
        Map<String, String> headers = new HashMap<>();
        JSONObject params = null;
        try {

            headers.put("Content-Type", "application/json");

            params = new JSONObject();

            params.put("_custAccount", AccountState.getUserID());
            params.put("_historyCount", "2");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        VolleyRequest<OrderModel[]> recommendationModelVolleyRequest =
                new VolleyRequest<OrderModel[]>(Request.Method.POST, Constants.GET_ORDERS_HISTORY_REQUEST, OrderModel[].class, headers, params,

                        new com.android.volley.Response.Listener<OrderModel[]>() {
                            @Override
                            public void onResponse(OrderModel[] response) {
//                                Toast.makeText(HomePageActivity.this, "Your Recommendations \n" + response.getValue(), Toast.LENGTH_SHORT).show();
                                bag_recycler_cover.setVisibility(View.GONE);

                                bagRecyclerAdapter = new BagRecyclerAdapter(getContext(), response);

                                recyclerView.setAdapter(bagRecyclerAdapter);
//                                progressBar.setVisibility(View.GONE);
//                                recommendationProductDetailsApiCall(response);
                                swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(false);
                                    }
                                });


                            }
                        }
                        //API CALL GETTING MADE FOR Dynamic Version contd

                        , new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null && networkResponse.statusCode != 200) {
                            Log.e("TAG", "No network on update.");
                            Toast.makeText(getContext(), "Network Error, please check internet", Toast.LENGTH_SHORT).show();
                            swipeRefreshLayout.post(new Runnable() {
                                @Override
                                public void run() {
                                    swipeRefreshLayout.setRefreshing(false);
                                }
                            });
                        }

                    }
                });


        RetryPolicy policy = new DefaultRetryPolicy(VolleyHelper.Timeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        recommendationModelVolleyRequest.setRetryPolicy(policy);
        VolleyHelper.getInstance(getContext()).addToRequestQueue(recommendationModelVolleyRequest);
    }

    void wishlistApiCall() {
        bag_recycler_cover.setVisibility(View.VISIBLE);

        recyclerView.removeAllViews();

//        progressBar.setVisibility(View.VISIBLE);
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(true);
            }
        });

        Map<String, String> headers = new HashMap<>();
        JSONObject params = null;
        try {

            headers.put("Content-Type", "application/json");

            params = new JSONObject();
            params.put("UserID", AccountState.getUserID());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        VolleyRequest<WishlistModel> recommendationModelVolleyRequest =
                new VolleyRequest<WishlistModel>(Request.Method.POST, Constants.WISHLIST_ITEM_REQUEST + AccountState.getTokenID(), WishlistModel.class,

                        new com.android.volley.Response.Listener<WishlistModel>() {
                            @Override
                            public void onResponse(WishlistModel response) {

//                                Toast.makeText(HomePageActivity.this, "Your Recommendations \n" + response.getValue(), Toast.LENGTH_SHORT).show();

                                try {

                                    wishlistProductDetailsApiCall(response.getData().getWishlistdata().getCommerceListLines());
//                                    bagRecyclerAdapter = new BagRecyclerAdapter(getContext(), response.getData().getWishlistdata().getCommerceListLines());
//
//                                    recyclerView.setAdapter(bagRecyclerAdapter);


                                } catch (NullPointerException e) {
                                    Toast.makeText(getContext(), "Your Wishlist is empty\n", Toast.LENGTH_SHORT).show();
                                    swipeRefreshLayout.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            swipeRefreshLayout.setRefreshing(false);
                                        }
                                    });
                                }
                            }
                        }
                        //API CALL GETTING MADE FOR Dynamic Version contd

                        , new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null && networkResponse.statusCode != 200) {
                            Log.e("TAG", "No network on update.");
                            Toast.makeText(getContext(), "Network Error, please check internet", Toast.LENGTH_SHORT).show();
                            swipeRefreshLayout.post(new Runnable() {
                                @Override
                                public void run() {
                                    swipeRefreshLayout.setRefreshing(false);
                                }
                            });
                        }

                    }
                });


        RetryPolicy policy = new DefaultRetryPolicy(VolleyHelper.Timeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        recommendationModelVolleyRequest.setRetryPolicy(policy);
        VolleyHelper.getInstance(getContext()).addToRequestQueue(recommendationModelVolleyRequest);
    }

    void wishlistProductDetailsApiCall(final List<WishlistCommerceListLineModel> wishlistLineModel) {

        this.wishlistLineModels = wishlistLineModel;
        Collections.reverse(wishlistLineModels);

        System.out.println("@@## wishlistLineModels count" + wishlistLineModels.size());
        String[] products = new String[(wishlistLineModels.size())];
        for (int i = 0; i < wishlistLineModels.size(); i++) {
            products[i] = wishlistLineModels.get(i).getProductId();

        }


        Map<String, String> headers = new HashMap<>();
        JSONObject finalObject = null;
        try {

            headers.put("Content-Type", "application/json");
            headers.put("Authorization", "id_token " + AccountState.getTokenID());
            Log.e("TAG", "AccountState.getTokenID()" + AccountState.getTokenID());
            headers.put(
                    "OUN",
                    "092");

            JSONArray parentData = new JSONArray();

            for (int i = 0; i < products.length; i++) {


                parentData.put(Double.valueOf(products[i]));
            }

            JSONObject childData = new JSONObject();

            childData.put("Ids", parentData);
            childData.put("DataLevelValue", 4);

            finalObject = new JSONObject();
            finalObject.put("productSearchCriteria", childData);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        VolleyRequest<ProductSearchModel> wishlistproductsModelVolleyRequest =
                new VolleyRequest<ProductSearchModel>(Request.Method.POST, Constants.PRODUCT_SEARCH_BY_ID_API, ProductSearchModel.class, headers, finalObject,

                        new com.android.volley.Response.Listener<ProductSearchModel>() {
                            @Override
                            public void onResponse(ProductSearchModel response) {

                                bag_recycler_cover.setVisibility(View.GONE);

                                System.out.println("@@## wishlistLineModel NAME" + response.getProductSearchValueModel().get(0).getProductName());

                                for (int i = 0; i < response.getProductSearchValueModel().size(); i++) {
                                    wishlistLineModels.get(i).setProductSearchValueModel(response.getProductSearchValueModel().get(i));
                                    System.out.println("@@## wishlistLineModel " + response.getProductSearchValueModel().get(i).getProductName());
                                }
//                                System.out.println("@@## wishlistLineModel " + wishlistLineModels.get(0).getProductSearchValueModel().getProductName());
                                System.out.println("@@## calling BagRecyclerAdapter");
                                bagRecyclerAdapter = new BagRecyclerAdapter(getContext(), wishlistLineModels);

                                recyclerView.setAdapter(bagRecyclerAdapter);
//                                progressBar.setVisibility(View.GONE);
                                swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(false);
                                    }
                                });
                            }
                        }
                        //API CALL GETTING MADE FOR Dynamic Version contd

                        , new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null && networkResponse.statusCode != 200) {
                            Log.e("TAG", "No network on update.");
                            Toast.makeText(getContext(), "Network Error, please check internet", Toast.LENGTH_SHORT).show();

                        }

                    }
                });


        RetryPolicy policy = new DefaultRetryPolicy(VolleyHelper.Timeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        wishlistproductsModelVolleyRequest.setRetryPolicy(policy);
        VolleyHelper.getInstance(getContext()).addToRequestQueue(wishlistproductsModelVolleyRequest);
    }

}
