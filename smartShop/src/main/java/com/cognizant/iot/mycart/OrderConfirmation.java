package com.cognizant.iot.mycart;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.cognizant.iot.activity.CatalogActivity;
import com.cognizant.iot.insidestore.AssociateCall;
import com.cognizant.iot.utils.AccountState;
import com.cognizant.iot.utils.GlobalClass;
import com.cognizant.retailmate.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.entity.ByteArrayEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class OrderConfirmation extends AppCompatActivity {

    ArrayList<HashMap<String, String>> data;
    ProgressBar orderconfirmProgress;
    TextView thankyounote, thankyounote1;

    GlobalClass mApplication;

    Boolean islp = false;
    double lpredeem;
    String orderType, messsge;

    String itemlistAndQty = "";

    LinearLayout linearLayoutAssociateAssigned;
    Button clickforAssociateDetails;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.orderconfirmed);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarOrderConfirm);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);


        mApplication = (GlobalClass) getApplicationContext();
        /*
         * For lotyalty dummy call
		 */

        islp = mApplication.isLp();

		/*
         *
		 */


        data = (ArrayList<HashMap<String, String>>) getIntent()
                .getSerializableExtra("productList");

        lpredeem = getIntent().getDoubleExtra("lpReedem", 0);

        orderType = getIntent().getStringExtra("ordertype");

        messsge = getIntent().getStringExtra("message");

        linearLayoutAssociateAssigned = (LinearLayout) findViewById(R.id.linearLayoutAssociateAssigned);
        clickforAssociateDetails = (Button) findViewById(R.id.clickforAssociateDetails);

        thankyounote = (TextView) findViewById(R.id.thankyounote);
        thankyounote1 = (TextView) findViewById(R.id.thankyounote1);
        orderconfirmProgress = (ProgressBar) findViewById(R.id.orderconfirmProgress);
        thankyounote.setVisibility(View.INVISIBLE);


        clickforAssociateDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OrderConfirmation.this, AssociateCall.class);
                intent.putExtra("whichClass", "order");
                startActivity(intent);
                finish();
            }
        });

        System.out.println("@@## Ready to send CRM " + data.size());
        createArrayList(data);
        // Execute DownloadJSON AsyncTask
//        sendNotification();
//        CreatePurchaseOrder();

    }

    private void createArrayList(ArrayList<HashMap<String, String>> data) {
        try {
            for (int i = 0; i < data.size(); i++) {


                if (i == data.size() - 1) {
                    itemlistAndQty = itemlistAndQty + data.get(i).get("itemid") + ":" + (int) Double.parseDouble(data.get(i).get("prodquantity"));
                } else {
                    itemlistAndQty = itemlistAndQty + data.get(i).get("itemid") + ":" + (int) Double.parseDouble(data.get(i).get("prodquantity")) + ":";
                }
            }
        } catch (NullPointerException | IndexOutOfBoundsException e) {

        }
        if (!data.isEmpty()) {
            System.out.println("@@## itemlistAndQty " + itemlistAndQty);
            if (AccountState.getOfflineMode()) {
                orderconfirmProgress.setProgress(100);
                orderconfirmProgress.setVisibility(View.INVISIBLE);
                thankyounote.setVisibility(View.VISIBLE);


            } else {
                CreatePurchaseOrder();
            }
            if (orderType.equals("bopis")) {

                String storename = getIntent().getStringExtra("storename");
                linearLayoutAssociateAssigned.setVisibility(View.VISIBLE);
                thankyounote1.setText(storename + " Store");
            } else if (orderType.equals("locker")) {
                String lockername = getIntent().getStringExtra("lockername");
                thankyounote1.setText(lockername + " Store");
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar app icon/title

        // Handle action bar actions click
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void sendNotification() {
        // TODO Auto-generated method stub

        orderconfirmProgress.setProgress(50);
        // RequestParams params = new RequestParams();

        // params.put("CustomerId", "000000000000011");
        //
        // params.put("CurrencyCode", "USD");
        // params.put("ProductId", "ACC9912");
        ByteArrayEntity entity = null;
        try {
            JSONObject json = new JSONObject();
            RequestParams params = new RequestParams();
            params.put("CustomerId", CatalogActivity.imei);
            params.put("CurrencyCode", "USD");

            JSONArray veg = new JSONArray();

            for (int i = 0; i < data.size(); i++) {
                JSONObject vegData = new JSONObject();
                vegData.put("ProductId", data.get(i).get("prodid"));
                veg.put(vegData);
            }
            params.put("ProductIds", veg);
            sendMe(params);
//			entity = new ByteArrayEntity(json.toString().getBytes("UTF-8"));
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void sendMe(RequestParams json) {
        // TODO Auto-generated method stub
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(70000); // set the timeout in milliseconds

//		client.post(
//				OrderConfirmation.this,
//				"http://a92c7aa3a5d5429cba31efe555e0cb58.cloudapp.net/api/MSCRM/Customer/AddPoints",
//				entity, "application/json", new AsyncHttpResponseHandler() {

        client.post("http://a92c7aa3a5d5429cba31efe555e0cb58.cloudapp.net/api/MSCRM/Customer/AddPoints", json,
                new AsyncHttpResponseHandler() {

                    @Override
                    public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
                        System.out.println("@@##LOYALY SERVER RESPONSE"
                                + responseBody.toString());

                        orderconfirmProgress.setProgress(100);
                        orderconfirmProgress.setVisibility(View.INVISIBLE);
                        thankyounote.setVisibility(View.VISIBLE);
                        /*
                         *
						 * dummy part for loyalty redeem
						 */
                        mApplication.setLp(true);
                        mApplication.setLpRedeemed(lpredeem);
                    }

                    @Override
                    public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                        System.out.println("@@##LOYALY SERVER FAILURE RESPONSE"
                                + error);

                        orderconfirmProgress.setProgress(100);
                        orderconfirmProgress.setVisibility(View.INVISIBLE);
                        thankyounote.setVisibility(View.VISIBLE);
                        /*
                         *
						 * dummy part for loyalty redeem
						 */
                        mApplication.setLp(true);
                        mApplication.setLpRedeemed(lpredeem);
                    }
                });
    }

    public void CreatePurchaseOrder() {
        System.out.println("@@## CreatePurchaseOrder Called");


        /*
        new call
         */

        String url = "http://rmethapi.azurewebsites.net/api/JDAServices/CreateCustomerOrderAPI";
        StringRequest req = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("@@## inbound:%n %s", response);
//                    reply=response;

                System.out.println("@@## Response of order creation " + response);
                orderconfirmProgress.setProgress(100);
                orderconfirmProgress.setVisibility(View.INVISIBLE);
                thankyounote.setVisibility(View.VISIBLE);

                if (orderType.equals("bopis")) {
                    linearLayoutAssociateAssigned.setVisibility(View.VISIBLE);

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                Log.e("Error: ", error.getMessage());
//                callCount = 0;
//                    reply="server not working";
                System.out.println("@@## Response of order creation error" + error);
                orderconfirmProgress.setProgress(100);
                orderconfirmProgress.setVisibility(View.INVISIBLE);
                thankyounote.setVisibility(View.VISIBLE);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("_custAccount", AccountState.getUserID());
//                params.put("_itemIdQty", "81301:2:81302:1");

                System.out.println("@@## product list for order creation " + itemlistAndQty);
                params.put("_itemIdQty", itemlistAndQty);
                params.put("_DlvMode", orderType);
                params.put("_Message", messsge);


                return params;


            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(OrderConfirmation.this);
        requestQueue.add(req);


    }


}
