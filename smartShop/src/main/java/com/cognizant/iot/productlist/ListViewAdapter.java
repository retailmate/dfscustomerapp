package com.cognizant.iot.productlist;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cognizant.iot.activity.CatalogActivity;
import com.cognizant.retailmate.R;

public class ListViewAdapter extends BaseAdapter {

	// Declare Variables
	Context context;
	LayoutInflater inflater;
	ArrayList<HashMap<String, String>> data;
	ImageLoader imageLoader;
	HashMap<String, String> resultp = new HashMap<String, String>();

	public ListViewAdapter(Context context,
			ArrayList<HashMap<String, String>> arraylist) {
		this.context = context;
		data = arraylist;
		imageLoader = new ImageLoader(context);
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		// Declare Variables
		TextView prodid;
		TextView prodname;
		TextView prodprice;
		ImageView flag;

		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View itemView = inflater.inflate(R.layout.listview_item, parent, false);
		// Get the position
		resultp = data.get(position);

		// Locate the TextViews in listview_item.xml
		// prodid = (TextView) itemView.findViewById(R.id.prodid);
		prodname = (TextView) itemView.findViewById(R.id.prodname);
		// prodprice = (TextView) itemView.findViewById(R.id.prodprice);

		// Locate the ImageView in listview_item.xml
		flag = (ImageView) itemView.findViewById(R.id.flag);

		// Capture position and set results to the TextViews
		// prodid.setText(resultp.get(CatalogActivity.PRODID));
		prodname.setText(resultp.get(CatalogActivity.PRODNAME));
		// prodprice.setText(resultp.get(CatalogActivity.PRODPRICE));
		// Capture position and set results to the ImageView
		// Passes flag images URL into ImageLoader.class
		imageLoader.DisplayImage(resultp.get(CatalogActivity.FLAG), flag);
		// Capture ListView item click
		prodname.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// Get the position
				resultp = data.get(position);
//				Intent intent = new Intent(context, SingleItemView.class);
				// Pass all data prodid
//				intent.putExtra("prodid", resultp.get(CatalogActivity.PRODID));
//				// Pass all data prodname
//				intent.putExtra("prodname",
//						resultp.get(CatalogActivity.PRODNAME));
//				// Pass all data prodprice
//				intent.putExtra("prodprice",
//						resultp.get(CatalogActivity.PRODPRICE));
//				// Pass all data flag
//				intent.putExtra("flag", resultp.get(CatalogActivity.FLAG));
//				// Start SingleItemView Class
//				context.startActivity(intent);

			}
		});
		return itemView;
	}
}