package com.cognizant.iot.json;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import com.cognizant.iot.activity.CatalogActivity;
import com.cognizant.iot.estimote.ListBeaconsActivity;
import com.cognizant.iot.estimote.NotifyDemoActivity;
import com.cognizant.iot.model.AssetModel;
import com.cognizant.iot.utils.AccountState;

public class AssetsExtracter extends AsyncTask<String, Void, String> {
    Context mContext;
    ArrayList<AssetModel> assetList = new ArrayList<AssetModel>();
    ArrayList<String> wishList = new ArrayList<String>();
    String assetID, jsonUrl;
    String jresponse = null;
    JSONParser jsonparser = new JSONParser();

    private ProgressDialog progress;

    String imei;

    public AssetsExtracter(String arg, Context context) {
        // TODO Auto-generated constructor stub
        mContext = context;
        assetID = arg;
        progress = new ProgressDialog(context);
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {

            InputStream is = mContext.getAssets().open("beaconoffersoffline.json");

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");
            System.out.println("JSON FILE READ" + json);

        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;

    }

    @Override
    protected void onPreExecute() {
        // Extract all assets and overwrite existing files if debug build
        progress.setMessage("Please wait..");

        progress.setIndeterminate(true);
        progress.show();
    }

    @Override
    protected String doInBackground(String... arg0) {


		/*
         * BEACON IN AX
		 */
//		jsonUrl = "https://etihaddemodevret.cloudax.dynamics.com/data/BeaconMasters";
        jsonUrl = "http://rmethapi.azurewebsites.net/api/BeaconServices/GetBeaconOffersAPI";


        System.out.println("JSON URL:" + jsonUrl);
        try {

            if (AccountState.getOfflineMode()) {

            } else {
                jresponse = jsonparser.makeHttpRequestAX(jsonUrl,
                        CatalogActivity.imei);

            }
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        }
        return jresponse;
        // return null;
    }

    @Override
    protected void onPostExecute(String doc) {

        progress.dismiss();

        JSONObject obj = null;
        JSONArray m_jArry = null;
        JSONArray w_jArry = null;

        try {

            if (AccountState.getOfflineMode()) {
//                jresponse = loadJSONFromAsset();
                obj = new JSONObject(loadJSONFromAsset());
            } else {
                obj = new JSONObject(doc);
            }
            m_jArry = obj.getJSONArray("value");

            // m_jArry = new JSONArray(doc);

            System.out.println("JSON array RECIEVED" + m_jArry);

            for (int i = 0; i < m_jArry.length(); i++) {
                JSONObject jo_inside;
                AssetModel am = new AssetModel();
                try {
                    jo_inside = m_jArry.getJSONObject(i);
                    //am.setAssetBrand(jo_inside.getString("Brands"));
                    //am.setAssetDate(jo_inside.getString("Startdate"));
                    am.setAssetName(jo_inside.getString("CategoryName"));
                    am.setAssetBeaconId(jo_inside.getString("BeaconId"));
                    am.setAssetDesc(jo_inside.getString("OfferDescription"));
                    //am.setAssetTime(jo_inside.getString("time"));
                    am.setOffers(jo_inside.getString("OfferId"));
                    try {
                        String encodedurl = URLEncoder.encode(jo_inside.getString("CategoryImage"),
                                HTTP.UTF_8).replace("+", "%20");
                        am.setUrl("https://etihaddemodevret.cloudax.dynamics.com/MediaServer/" + encodedurl);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }


                    am.setNotfied(false);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                // Add your values in your `ArrayList` as below:

                assetList.add(am);
                System.out.println("LIST " + assetList.get(i).getAssetName());
                // Same way for other value...
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        // jo_insidewish = w_jArry.getJSONObject(i);
        wishList.add("B9407F30-F5F8-466E-AFF9-25556B57FE6D");


        Intent intent = new Intent(mContext, ListBeaconsActivity.class);
        intent.putExtra("list", assetList);
        intent.putExtra("wishlist", wishList);

        intent.putExtra(ListBeaconsActivity.EXTRAS_TARGET_ACTIVITY,
                NotifyDemoActivity.class.getName());
        intent.putExtra("assetID", assetID);
        mContext.startActivity(intent);

    }


    /*
    OFFLINE FUNCTION FOR BEACONS
     */


    public String getbeaconoffersResponse() {
        String json = null;
        try {


            InputStream is = mContext.getAssets().open("beaconoffersoffline.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        System.out.println("@@## jresponse offline before " + jresponse);
        return json;
    }
}