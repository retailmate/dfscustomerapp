package com.cognizant.iot.json;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.cognizant.iot.estimote.NotifyDemoActivity;
import com.cognizant.iot.model.BrandModel;
import com.cognizant.iot.model.OfferModel;
import com.cognizant.iot.utils.AccountState;
import com.cognizant.retailmate.R;

public class BrandCall extends AsyncTask<String, Void, String> {
    Context mContext;
    public static ArrayList<BrandModel> offerList = new ArrayList<BrandModel>();
    ArrayList<String> itemList = new ArrayList<String>();
    String assetID, jsonUrl;
    JSONObject jresponse = null;
    BrandHttpCall brandcall = new BrandHttpCall();
    CountHttpCall countcall = new CountHttpCall();
    private int mProgressStatus = 0;

    ProgressBar progress;
    static Spinner spinner;

    String adr, bID, beacon;

    JSONParser jsonparser = new JSONParser();

    public BrandCall(String arg, Context context, String imei, String brandID, String beacon) {
        // TODO Auto-generated constructor stub
        mContext = context;
        assetID = arg;
        adr = imei;
        bID = brandID;
        this.beacon = beacon;
        // bID = "bata";

        progress = (ProgressBar) ((Activity) mContext)
                .findViewById(R.id.progressBar1);
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {

            InputStream is = mContext.getAssets().open("assets.json");

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");
            System.out.println("JSON FILE READ" + json);

        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;

    }

    @Override
    protected void onPreExecute() {
        // Extract all assets and overwrite existing files if debug build

        progress.setProgress(mProgressStatus);

    }

    @Override
    protected String doInBackground(String... arg0) {

//		jsonUrl = "https://etihaddemodevret.cloudax.dynamics.com/data/Brandproducts?$filter=BrandID";
        jsonUrl = "http://rmethapi.azurewebsites.net/api/BeaconServices/GetBeaconProductsAPI";

        try {
            if (AccountState.getOfflineMode()) {

            } else {
                jresponse = countcall.getJSONfromURLBrandItems(jsonUrl, beacon, bID);
                System.out.println("@@## Response for BRAND ITEMS " + jresponse);
            }

            progress.setProgress(50);
            return jresponse.toString();
        } catch (NullPointerException | ArrayIndexOutOfBoundsException e) {

        }
        return null;
    }

    @Override
    protected void onPostExecute(String doc) {

        progress.setProgress(100);
        progress.setVisibility(View.INVISIBLE);

        LinearLayout progresslayout = (LinearLayout) ((Activity) mContext)
                .findViewById(R.id.progressbar_layout);
        progresslayout.setVisibility(View.GONE);

        JSONObject obj = null;
        JSONArray m_jArry = null;
        JSONArray w_jArry = null;

        offerList.clear();

        try {

            if (AccountState.getOfflineMode()) {
                obj = new JSONObject(getbeaconproductsResponse());
            } else {

                obj = new JSONObject(doc);
            }
            m_jArry = obj.getJSONArray("value");

            // m_jArry = new JSONArray(doc);

            System.out.println("JSON array RECIEVED" + m_jArry);

            for (int i = 0; i < m_jArry.length(); i++) {
                JSONObject jo_inside;
                BrandModel am = new BrandModel();
                try {
                    jo_inside = m_jArry.getJSONObject(i);
                    am.setName(jo_inside.getString("ProductName"));
                    am.setCode(jo_inside.getString("Product"));

                    if (jo_inside.getString("HasVariant").equals("Yes")) {
                        am.setVariantExists(true);
                    } else {
                        am.setVariantExists(false);
                    }


                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                // Add your values in your `ArrayList` as below:

                offerList.add(am);
                // Same way for other value...
                itemList.clear();
            }
        } catch (JSONException | NullPointerException | ArrayIndexOutOfBoundsException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        int i = 0;
        while (i < offerList.size()) {
            itemList.add(offerList.get(i).getName());
            i++;
        }


        TextView statusTextView = (TextView) ((Activity) mContext)
                .findViewById(R.id.code);

        spinner = (Spinner) ((Activity) mContext).findViewById(R.id.spinner2);
        ArrayAdapter<String> adapter;
        adapter = new ArrayAdapter<String>(mContext, R.layout.spinner, itemList);

        adapter.setDropDownViewResource(R.layout.spinner);
        spinner.setAdapter(adapter);


    }

    public static String getProductID() {
        // TODO Auto-generated method stub
        return getProductID(String.valueOf(spinner.getSelectedItem()));
    }

    public static String getProductID(String name) {
        int i = 0;
        while (i < offerList.size()) {

            if (offerList.get(i).getName().equals(name)) {
                return offerList.get(i).getCode();
            }
            i++;
        }

        return "NOID";
    }

    /*
    OFFLINE FUNCTION FOR BEACONS
     */


    public String getbeaconproductsResponse() {
        String json = null;
        try {
            InputStream is = mContext.getAssets().open("beaconproductsoffline.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

}
