package com.cognizant.iot.bopis;

import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.cognizant.retailmate.R;
import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by 452781 on 11/9/2016.
 */
public class RmStoreSelect extends AppCompatActivity {

    Gson gson;
    String locationString;
    LocationModel locationModel;
    Geocoder geocoder;
    List<Address> addresses;
    RmStoreModel rmStoreModel;
    private List<RmStoreModel> storeList = new ArrayList<>();
    private RecyclerView recyclerView;
    private RmStoreAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bopis_store_select_layout);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        locationString = loadJSONFromAsset("location.json");
        gson = new Gson();
        locationModel = gson.fromJson(locationString, LocationModel.class);
        geocoder = new Geocoder(this, Locale.getDefault());

        for (int i = 0; i < locationModel.getLocations().size(); i++) {
            rmStoreModel = new RmStoreModel();
            try {
//                String addressString="";
//                addresses = geocoder.getFromLocation(locationModel.getLocations().get(i).getLat(), locationModel.getLocations().get(i).getLng(), 1);
//                for (int j = 0; j <addresses.get(0).getMaxAddressLineIndex() ; j++) {
//                    addressString=addressString+" "+addresses.get(0).getAddressLine(j);
//                    if (j<addresses.get(0).getMaxAddressLineIndex()-1){
//                        addressString=addressString+"\n";
//                    }
//                }

                rmStoreModel.setAddress(locationModel.getLocations().get(i).getAddress());
                rmStoreModel.setName(locationModel.getLocations().get(i).getPlace());
                storeList.add(rmStoreModel);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }


        mAdapter = new RmStoreAdapter(storeList, RmStoreSelect.this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(mAdapter);

    }


    public String loadJSONFromAsset(String s) {
        String json = null;
        try {
            InputStream is = getApplicationContext().getAssets().open(s);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}
