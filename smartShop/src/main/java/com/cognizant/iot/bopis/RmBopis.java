package com.cognizant.iot.bopis;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.cognizant.iot.mycart.OrderConfirmation;
import com.cognizant.retailmate.R;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by 452781 on 11/9/2016.
 */


public class RmBopis extends AppCompatActivity {

    ProgressDialog progressDialog;


    PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

    int PLACE_PICKER_REQUEST = 1;

    RadioButton homeRadio, storeRadio, lockerRadio;
    ImageView locationPicker, storePicker;

    TextView storeAddress, lockerAddress;
    EditText homeAddress;

    ArrayList<HashMap<String, String>> data;
    Double lpPoints;
    CheckBox order_message_checkbox;
    EditText order_message;

    TimePickerDialog timePickerDialog;
    private int mYear, mMonth, mDay, mHour, mMinute;

    String datentime = "";

    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rm_bopis);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        homeRadio = (RadioButton) findViewById(R.id.homeRadio);
        storeRadio = (RadioButton) findViewById(R.id.storeRadio);
        lockerRadio = (RadioButton) findViewById(R.id.lockerRadio);
        locationPicker = (ImageView) findViewById(R.id.locationPicker);
        storePicker = (ImageView) findViewById(R.id.storeLocator);
        homeAddress = (EditText) findViewById(R.id.homeLocationText);
        storeAddress = (TextView) findViewById(R.id.storeLocationText);
        lockerAddress = (TextView) findViewById(R.id.lockerLocationText);

        order_message_checkbox = (CheckBox) findViewById(R.id.order_message_checkbox);
        order_message = (EditText) findViewById(R.id.order_message);


        order_message_checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                                                              @Override
                                                              public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                                                  if (isChecked) {
//                                                                      order_message_checkbox.setChecked(false);
                                                                      order_message.setVisibility(View.VISIBLE);
                                                                  } else {
//                                                                      order_message_checkbox.setChecked(true);
                                                                      order_message.setVisibility(View.GONE);
                                                                  }
                                                              }
                                                          }
        );


        homeAddress.setFocusable(false);
        homeAddress.setFocusableInTouchMode(false);
        homeAddress.setClickable(false);


        data = (ArrayList<HashMap<String, String>>) getIntent()
                .getSerializableExtra("productList");

        lpPoints = getIntent().getDoubleExtra("lpReedem", 0);

    }

    public void showLocationPicker(View view) {
        new mapTask().execute();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                Log.e("TAG", "Address: " + place.getAddress());
                homeAddress.setText(place.getAddress());

                homeAddress.setFocusable(true);
                homeAddress.setFocusableInTouchMode(true);
                homeAddress.setClickable(true);
                homeAddress.requestFocus();

            }
        } else if (requestCode == 2) {
            if (null != data) {
                String message = data.getStringExtra("storeLocation");
                Log.e("TAG", "Store: " + message);
                storeAddress.setText(message);
            }
        } else if (requestCode == 3) {
            if (null != data) {
                String message = data.getStringExtra("storeLocation");
                Log.e("TAG", "Store: " + message);
                lockerAddress.setText(message);
            }
        }
    }


    public void showStorePicker(View view) {
        Log.e("TAG", "showStorePicker");
        Intent intent = new Intent(RmBopis.this, RmStoreSelect.class);
        startActivityForResult(intent, 2);
    }


    public void showLockerPicker(View view) {
        Log.e("TAG", "showLockerPicker");
        Intent intent = new Intent(RmBopis.this, RmLockerSelect.class);
        startActivityForResult(intent, 3);
    }


    public void radioClick(View view) {
        Log.e("TAG", "Radio Clicked." + view.getId() + R.id.storeRadio + "store" + R.id.homeRadio + "Home");
        if (view.getId() == R.id.storeRadio) {

            homeRadio.setChecked(false);
            lockerRadio.setChecked(false);
        } else if (view.getId() == R.id.homeRadio) {
            lockerRadio.setChecked(false);
            storeRadio.setChecked(false);
        } else if (view.getId() == R.id.lockerRadio) {
            homeRadio.setChecked(false);
            storeRadio.setChecked(false);
        }
    }

    public void proceedToPayment(View view) {
        String homeAddressString = homeAddress.toString();
        String storeAddressString = (String) storeAddress.getText();
        if (homeRadio.isChecked() && !homeAddressString.equalsIgnoreCase("Please select a location")) {
            Toast.makeText(RmBopis.this, "Will be delivered to your home.", Toast.LENGTH_SHORT).show();
            /*Start order conformation intent here*/
            Intent intent = new Intent(RmBopis.this,
                    OrderConfirmation.class);

            intent.putExtra("productList", data);
            intent.putExtra("lpReedem", lpPoints);
            intent.putExtra("ordertype", "delivery");

            startActivity(intent);
            finish();
        } else if (storeRadio.isChecked() && !storeAddressString.equalsIgnoreCase("Please select a store")) {
//            Toast.makeText(RmBopis.this, "You can pick it up at the store.", Toast.LENGTH_SHORT).show();
            /*Start order conformation intent here*/
            intent = new Intent(RmBopis.this,
                    OrderConfirmation.class);
            intent.putExtra("productList", data);
            intent.putExtra("lpReedem", lpPoints);
            intent.putExtra("storename",storeAddress.getText());
//            intent.putExtra("ordertype", "bopis");
            dateAndTime("bopis");


//            startActivity(intent);
//            finish();
        } else if (lockerRadio.isChecked() && !storeAddressString.equalsIgnoreCase("Please select a location")) {
//            Toast.makeText(RmBopis.this, "You can collect it from your locker.", Toast.LENGTH_SHORT).show();
            /*Start order conformation intent here*/
            intent = new Intent(RmBopis.this,
                    OrderConfirmation.class);
            intent.putExtra("productList", data);
            intent.putExtra("lpReedem", lpPoints);
//            intent.putExtra("ordertype", "locker");
            intent.putExtra("lockername",lockerAddress.getText());
            dateAndTime("locker");

//            startActivity(intent);
//            finish();
        } else {
            Toast.makeText(RmBopis.this, "Please select a valid address/store.", Toast.LENGTH_LONG).show();

        }


    }

    class mapTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(RmBopis.this);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Loading...");
            progressDialog.show();

        }

        @Override
        protected Void doInBackground(Void... arg0) {
            try {
                startActivityForResult(builder.build(RmBopis.this), PLACE_PICKER_REQUEST);
            } catch (GooglePlayServicesRepairableException e) {
                e.printStackTrace();
//                Toast.makeText(RmBopis.this, "Server error. Please try after sometime.", Toast.LENGTH_LONG).show();
            } catch (GooglePlayServicesNotAvailableException e) {
                e.printStackTrace();
//                Toast.makeText(RmBopis.this, "Unexpected error. Please try after sometime.", Toast.LENGTH_LONG).show();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressDialog.dismiss();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar app icon/title

        // Handle action bar actions click
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    /*

    Choose date and time
     */

    void dateAndTime(final String type) {


        final Calendar c = Calendar.getInstance();
//        mHour = c.get(Calendar.HOUR_OF_DAY);
//        mMinute = c.get(Calendar.MINUTE);
//        timePickerDialog = new TimePickerDialog(this,
//                new TimePickerDialog.OnTimeSetListener() {
//
//                    @Override
//                    public void onTimeSet(TimePicker view, int hourOfDay,
//                                          int minute) {
//
//
////                        txtTime.setText(hourOfDay + ":" + minute);
//                    }
//                }, mHour, mMinute, false);


        // Get Current Date
//        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datea = new DatePickerDialog(this, R.style.MyDatePickerDialogTheme, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
//                txtDate.setText(dayOfMonth + "-" + (month + 1) + "-" + year);
                datentime = dayOfMonth + "-" + (month + 1) + "-" + year;

                timePickerDialog.setTitle("Select time for pick up:");
                timePickerDialog.show();
            }
        }, mYear, mMonth, mDay);
        datea.setTitle("Select date for pick up:");
        datea.show();


        // Get Current Time
//        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        timePickerDialog = new TimePickerDialog(this, R.style.MyDatePickerDialogTheme,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {
                        datentime = datentime + "at " + hourOfDay + ":" + minute;
                        intent.putExtra("ordertype", type);
                        intent.putExtra("datentime", datentime);
                        intent.putExtra("message", order_message.getText());
                        startActivity(intent);
                        finish();

//                        txtTime.setText(hourOfDay + ":" + minute);
                    }
                }, mHour, mMinute, false);


    }
}
