package com.cognizant.iot.aboutus;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.cognizant.retailmate.R;

public class AboutRetailMate extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

        setContentView(R.layout.aboutretailmate);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarAboutRetailmate);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        // final Drawable upArrow =
        // getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        // upArrow.setColorFilter(getResources().getColor(R.color.grey),
        // PorterDuff.Mode.SRC_ATOP);

//		getActionBar().setHomeAsUpIndicator(R.drawable.back_aro);
//		getActionBar().setDisplayHomeAsUpEnabled(true);
//		getActionBar().setHomeButtonEnabled(true);
//		getActionBar().setDisplayShowTitleEnabled(true);
//		getActionBar().setBackgroundDrawable(
//				new ColorDrawable(Color.parseColor("#B5D000")));
//		getActionBar().setIcon(
//				new ColorDrawable(getResources().getColor(
//						android.R.color.transparent)));
//
//		int titleId = getResources().getIdentifier("action_bar_title", "id",
//				"android");
//
//		TextView abTitle = (TextView) findViewById(titleId);
//		abTitle.setTextColor(Color.parseColor("#ffffff"));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.aboutusmenu, menu);
        // setMenuBackground();

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar app icon/title

        // Handle action bar actions click
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            case R.id.mail_aboutus:
                Intent intent = new Intent(Intent.ACTION_SEND);
                String[] recipients = {"retailmate_customersupport@gmail.com"};
                intent.putExtra(Intent.EXTRA_EMAIL, recipients);
                intent.putExtra(Intent.EXTRA_SUBJECT, "Retail Mate Subject");
                intent.putExtra(Intent.EXTRA_TEXT, "Retail Mate Sample message");
                intent.putExtra(Intent.EXTRA_CC, "retailmate_admin@gmail.com");
                intent.setType("text/html");
                startActivity(Intent.createChooser(intent, "Send mail"));
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
