
package com.cognizant.iot.loyaltypoints.multipleloyalty.loyaltymodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class LoyaltyResponseModel {


    @SerializedName("TransactionDetail")
    @Expose
    private List<LoyaltyTransactionDetail> transactionDetail = null;
    @SerializedName("TierPoint")
    @Expose
    private String tierPoint;
    @SerializedName("ReedemptionPoint")
    @Expose
    private String reedemptionPoint;

    public List<LoyaltyTransactionDetail> getTransactionDetail() {
        return transactionDetail;
    }

    public void setTransactionDetail(List<LoyaltyTransactionDetail> transactionDetail) {
        this.transactionDetail = transactionDetail;
    }

    public String getTierPoint() {
        return tierPoint;
    }

    public void setTierPoint(String tierPoint) {
        this.tierPoint = tierPoint;
    }

    public String getReedemptionPoint() {
        return reedemptionPoint;
    }

    public void setReedemptionPoint(String reedemptionPoint) {
        this.reedemptionPoint = reedemptionPoint;
    }

}