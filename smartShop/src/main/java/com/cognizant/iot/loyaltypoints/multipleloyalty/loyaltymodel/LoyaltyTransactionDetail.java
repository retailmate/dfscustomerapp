
package com.cognizant.iot.loyaltypoints.multipleloyalty.loyaltymodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoyaltyTransactionDetail {

    @SerializedName("AccrualPoints")
    @Expose
    private String accrualPoints;
    @SerializedName("RedemptionPoints")
    @Expose
    private String redemptionPoints;
    @SerializedName("Transactiontype")
    @Expose
    private String transactiontype;
    @SerializedName("TransactionDate")
    @Expose
    private String transactionDate;

    public String getAccrualPoints() {
        return accrualPoints;
    }

    public void setAccrualPoints(String accrualPoints) {
        this.accrualPoints = accrualPoints;
    }

    public String getRedemptionPoints() {
        return redemptionPoints;
    }

    public void setRedemptionPoints(String redemptionPoints) {
        this.redemptionPoints = redemptionPoints;
    }

    public String getTransactiontype() {
        return transactiontype;
    }

    public void setTransactiontype(String transactiontype) {
        this.transactiontype = transactiontype;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

}
