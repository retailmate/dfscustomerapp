package com.cognizant.iot.loyaltypoints.multipleloyalty.loyaltyfragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.TextView;

import com.cognizant.retailmate.R;
import com.hookedonplay.decoviewlib.DecoView;
import com.hookedonplay.decoviewlib.charts.SeriesItem;
import com.hookedonplay.decoviewlib.events.DecoEvent;


/**
 * Created by 543898 on 1/23/2017.
 */

public class MilestoneFragment extends Fragment {
    DecoView arcView1, arcView2, arcView3, arcView4, arcView5;
    int series1Index, series1Index2, series1Index3, series1Index4, series1Index5;
    ImageView imageView2, imageView3, imageView4, imageView5, imageView6;

    public MilestoneFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.loyalty_multiple_milestone, container, false);
        Bundle bundle = getArguments();


        arcView1 = (DecoView) layout.findViewById(R.id.dynamicArcView1);
        arcView2 = (DecoView) layout.findViewById(R.id.dynamicArcView2);
        arcView3 = (DecoView) layout.findViewById(R.id.dynamicArcView3);
        arcView4 = (DecoView) layout.findViewById(R.id.dynamicArcView4);
        arcView5 = (DecoView) layout.findViewById(R.id.dynamicArcView5);

        imageView2 = (ImageView) layout.findViewById(R.id.imageView2);
        imageView3 = (ImageView) layout.findViewById(R.id.imageView3);
        imageView4 = (ImageView) layout.findViewById(R.id.imageView4);
        imageView5 = (ImageView) layout.findViewById(R.id.imageView5);
        imageView6 = (ImageView) layout.findViewById(R.id.imageView6);

        final CardView cardViewOpen = (CardView) layout.findViewById(R.id.open);
        final CardView cardViewNotOpen = (CardView) layout.findViewById(R.id.notopen);

        final TextView textView1 = (TextView) layout.findViewById(R.id.text1);
        final TextView textView2 = (TextView) layout.findViewById(R.id.text2);
        final TextView textView3 = (TextView) layout.findViewById(R.id.text3);
        final TextView textView4 = (TextView) layout.findViewById(R.id.text4);
        final ImageView imageView = (ImageView) layout.findViewById(R.id.imageViewMile);

        imageView4.setVisibility(View.VISIBLE);
        imageView3.setVisibility(View.INVISIBLE);
        imageView2.setVisibility(View.INVISIBLE);
        imageView5.setVisibility(View.INVISIBLE);
        imageView6.setVisibility(View.INVISIBLE);

        arcView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                cardViewNotOpen.setVisibility(View.INVISIBLE);
                cardViewOpen.setVisibility(View.VISIBLE);
                textView1.setText(" No More Newbie");
                textView2.setText(" Shop for 300 loyalty points.");
                textView3.setText("Get a gift voucher worth 11% of your first transaction.");
                textView4.setText("Challenge Completed.");
                textView4.setTextColor(getResources().getColor(R.color.green));
                imageView.setVisibility(View.VISIBLE);
                imageView.setImageResource(R.drawable.mile_one);

                imageView2.setVisibility(View.VISIBLE);
                imageView3.setVisibility(View.INVISIBLE);
                imageView4.setVisibility(View.INVISIBLE);
                imageView5.setVisibility(View.INVISIBLE);
                imageView6.setVisibility(View.INVISIBLE);


            }
        });

        arcView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cardViewNotOpen.setVisibility(View.INVISIBLE);
                cardViewOpen.setVisibility(View.VISIBLE);
                textView1.setText(" Shopping Spree");
                textView2.setText(" Shop in three or more stores withing a  week.");
                textView3.setText("Get 7% off in one of the stores which were visited during the milestone.");
                textView4.setText("Challenge Completed.");
                textView4.setTextColor(getResources().getColor(R.color.green));
                imageView.setVisibility(View.VISIBLE);
                imageView.setImageResource(R.drawable.mile_two);

                imageView3.setVisibility(View.VISIBLE);
                imageView2.setVisibility(View.INVISIBLE);
                imageView4.setVisibility(View.INVISIBLE);
                imageView5.setVisibility(View.INVISIBLE);
                imageView6.setVisibility(View.INVISIBLE);

            }
        });

        arcView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cardViewNotOpen.setVisibility(View.INVISIBLE);
                cardViewOpen.setVisibility(View.VISIBLE);
                textView1.setText(" Go International");
                textView2.setText(" Shop in at least four different nations.");
                textView3.setText("A chance to win a four day trip to Amsterdam.");
                textView4.setText("Shop in two more nations.");
                textView4.setTextColor(getResources().getColor(R.color.orange));
                imageView.setVisibility(View.VISIBLE);
                imageView.setImageResource(R.drawable.mile_three);

                imageView4.setVisibility(View.VISIBLE);
                imageView3.setVisibility(View.INVISIBLE);
                imageView2.setVisibility(View.INVISIBLE);
                imageView5.setVisibility(View.INVISIBLE);
                imageView6.setVisibility(View.INVISIBLE);

            }
        });

        arcView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cardViewNotOpen.setVisibility(View.VISIBLE);
                cardViewOpen.setVisibility(View.INVISIBLE);

                imageView5.setVisibility(View.VISIBLE);
                imageView3.setVisibility(View.INVISIBLE);
                imageView4.setVisibility(View.INVISIBLE);
                imageView2.setVisibility(View.INVISIBLE);
                imageView6.setVisibility(View.INVISIBLE);

            }
        });

        arcView5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cardViewNotOpen.setVisibility(View.VISIBLE);
                cardViewOpen.setVisibility(View.INVISIBLE);

                imageView6.setVisibility(View.VISIBLE);
                imageView3.setVisibility(View.INVISIBLE);
                imageView4.setVisibility(View.INVISIBLE);
                imageView5.setVisibility(View.INVISIBLE);
                imageView2.setVisibility(View.INVISIBLE);


            }
        });
        final HorizontalScrollView horizontalScrollView = (HorizontalScrollView) layout.findViewById(R.id.hsv);

        ViewTreeObserver vto = horizontalScrollView.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                horizontalScrollView.scrollTo(110, 0);
            }
        });


        // Create background track
        arcView1.addSeries(new SeriesItem.Builder(Color.argb(255, 218, 218, 218))
                .setRange(0, 100, 100)
                .setInitialVisibility(false)
                .setLineWidth(12f)
                .build());

        //Create data series track
        SeriesItem seriesItem1 = new SeriesItem.Builder(Color.argb(255, 64, 196, 0))
                .setRange(0, 100, 0)
                .setLineWidth(12f)
                .build();

        series1Index = arcView1.addSeries(seriesItem1);


        //arcview2

        arcView2.addSeries(new SeriesItem.Builder(Color.argb(255, 218, 218, 218))
                .setRange(0, 100, 100)
                .setInitialVisibility(false)
                .setLineWidth(12f)
                .build());

        //Create data series track
        SeriesItem seriesItem2 = new SeriesItem.Builder(Color.argb(255, 64, 196, 0))
                .setRange(0, 100, 0)
                .setLineWidth(12f)
                .build();

        series1Index2 = arcView2.addSeries(seriesItem2);


        //arcview3

        arcView3.addSeries(new SeriesItem.Builder(Color.argb(255, 218, 218, 218))
                .setRange(0, 100, 100)
                .setInitialVisibility(false)
                .setLineWidth(22f)
                .build());

        //Create data series track
        SeriesItem seriesItem3 = new SeriesItem.Builder(Color.argb(255, 255, 140, 0))
                .setRange(0, 100, 0)
                .setLineWidth(22f)
                .build();

        series1Index3 = arcView3.addSeries(seriesItem3);


        //arcview4

        arcView4.addSeries(new SeriesItem.Builder(Color.argb(255, 218, 218, 218))
                .setRange(0, 100, 100)
                .setInitialVisibility(false)
                .setLineWidth(12f)
                .build());

        //Create data series track
        SeriesItem seriesItem4 = new SeriesItem.Builder(Color.argb(255, 218, 218, 218))
                .setRange(0, 100, 0)
                .setLineWidth(12f)
                .build();

        series1Index4 = arcView4.addSeries(seriesItem4);


        //


        arcView5.addSeries(new SeriesItem.Builder(Color.argb(255, 218, 218, 218))
                .setRange(0, 100, 100)
                .setInitialVisibility(false)
                .setLineWidth(12f)
                .build());

        //Create data series track
        SeriesItem seriesItem5 = new SeriesItem.Builder(Color.argb(255, 218, 218, 218))
                .setRange(0, 100, 0)
                .setLineWidth(12f)
                .build();

        series1Index5 = arcView5.addSeries(seriesItem5);

        return layout;


    }


    @Override
    public void setMenuVisibility(final boolean visible) {
        if (visible) {
            //Do your stuff here
            showAnimation();

        }

        super.setMenuVisibility(visible);
    }


    public void showAnimation() {

        arcView1.addEvent(new DecoEvent.Builder(DecoEvent.EventType.EVENT_SHOW, true)
//                .setDelay(1000)
                .setDuration(500)
                .build());

        arcView1.addEvent(new DecoEvent.Builder(100).setIndex(series1Index).build());

        arcView2.addEvent(new DecoEvent.Builder(DecoEvent.EventType.EVENT_SHOW, true)
//                .setDelay(1000)
                .setDuration(500)
                .build());

        arcView2.addEvent(new DecoEvent.Builder(100).setIndex(series1Index2).build());

        arcView3.addEvent(new DecoEvent.Builder(DecoEvent.EventType.EVENT_SHOW, true)
//                .setDelay(1000)
                .setDuration(2000)
                .build());

        arcView3.addEvent(new DecoEvent.Builder(50).setIndex(series1Index3).build());

        arcView4.addEvent(new DecoEvent.Builder(DecoEvent.EventType.EVENT_SHOW, true)
//                .setDelay(1000)
                .setDuration(1000)
                .build());

        arcView4.addEvent(new DecoEvent.Builder(25).setIndex(series1Index4).build());

        arcView5.addEvent(new DecoEvent.Builder(DecoEvent.EventType.EVENT_SHOW, true)
//                .setDelay(1000)
                .setDuration(1000)
                .build());

        arcView5.addEvent(new DecoEvent.Builder(25).setIndex(series1Index5).build());
    }

}