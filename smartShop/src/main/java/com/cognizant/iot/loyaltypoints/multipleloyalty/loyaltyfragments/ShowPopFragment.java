package com.cognizant.iot.loyaltypoints.multipleloyalty.loyaltyfragments;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.cognizant.retailmate.R;


/**
 * Created by 452781 Ankur Bhatt
 */

public class ShowPopFragment extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loyalty_multiple_pop_up);
    }

    public void closePopup(View view) {
        finish();
    }

    public void showCart(View view) {
    }

}