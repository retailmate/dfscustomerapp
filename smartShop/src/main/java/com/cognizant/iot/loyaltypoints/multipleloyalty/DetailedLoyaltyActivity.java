package com.cognizant.iot.loyaltypoints.multipleloyalty;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.cognizant.iot.loyaltypoints.multipleloyalty.loyaltyfragments.DetailsFragment;
import com.cognizant.iot.loyaltypoints.multipleloyalty.loyaltyfragments.HistoryFragment;
import com.cognizant.iot.loyaltypoints.multipleloyalty.loyaltyfragments.RedeemFragment;
import com.cognizant.iot.loyaltypoints.multipleloyalty.loyaltyfragments.ShowPopFragment;
import com.cognizant.retailmate.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 452781 Ankur Bhatt
 */

public class DetailedLoyaltyActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loyalty_multiple_activity_main);

//        toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new RedeemFragment(), getResources().getString(R.string.redeem));
        adapter.addFragment(new HistoryFragment(DetailedLoyaltyActivity.this), getResources().getString(R.string.history));
        adapter.addFragment(new DetailsFragment(), getResources().getString(R.string.details));
        viewPager.setAdapter(adapter);
    }

    public void showRedeemPopup(View view) {

        Intent intent = new Intent(DetailedLoyaltyActivity.this, ShowPopFragment.class);
        startActivity(intent);


    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.aboutusmenu, menu);
        // setMenuBackground();

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar app icon/title

        // Handle action bar actions click
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}