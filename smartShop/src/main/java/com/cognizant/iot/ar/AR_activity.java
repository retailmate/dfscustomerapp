package com.cognizant.iot.ar;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.cognizant.iot.activity.CatalogActivity;
import com.cognizant.iot.mycart.JsonFuntionCart;
import com.cognizant.iot.utils.AccountState;
import com.cognizant.retailmate.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.metaio.sdk.ARViewActivity;
import com.metaio.sdk.GestureHandlerAndroid;
import com.metaio.sdk.MetaioDebug;
import com.metaio.sdk.jni.EPLAYBACK_STATUS;
import com.metaio.sdk.jni.GestureHandler;
import com.metaio.sdk.jni.IGeometry;
import com.metaio.sdk.jni.IMetaioSDKCallback;
import com.metaio.sdk.jni.MovieTextureStatus;
import com.metaio.sdk.jni.TrackingValuesVector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;

//public class AR_activity extends Activity {
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        // set up notitle
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        // set up full screen
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        super.onCreate(savedInstanceState);
//    }
//
//}

public class AR_activity extends ARViewActivity implements TextToSpeech.OnInitListener {
    private IGeometry mMetaioMan;
    private IGeometry mImagePlane, mImagePlane1, mImagePlane2, mImagePlane3,
            mImagePlane4, mImagePlane5;
    private IGeometry mMoviePlane;
    private int mSelectedModel;

    private GestureHandlerAndroid mGestureHandler;
    private int mGestureMask;
    private MetaioSDKCallbackHandler mCallbackHandler;
    final File dir = Environment.getExternalStorageDirectory();

    String assetPath;

    /*
     * // XML node keys
     */
    static final String KEY_MODEL = "model"; // parent node
    static final String KEY_FILE = "file";
    static final String KEY_TEXTURE = "texture";
    static final String KEY_MARKER = "marker";
    static final String KEY_ROTATION = "rotation";
    static final String KEY_TRANSLATION = "translation";
    static final String KEY_SCALE = "scale";
    static final String KEY_PROCEDURE = "procedure";
    static final String KEY_STEP = "step";
    static final String KEY_TEXT = "text";
    static final String KEY_TTS = "tts";
    static final String KEY_ANIM = "anim";

    ArrayList<String[]> stepsList;
    static int currentStep;
    private TextToSpeech tts;

    TextView tv;

    // ProgressDialog prgDialog;
    /*
     * END
	 */

	/*
     * (non-Javadoc)
	 *
	 * @see com.metaio.sdk.ARViewActivity#onCreate(android.os.Bundle) for cart
	 */

    ArrayList<HashMap<String, String>> arraylist;

    JSONObject jsonobject;
    JSONArray jsonarray;
    String prodid1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // set up notitle
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        // set up full screen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);

        mMetaioMan = null;
        mImagePlane = null;
        mMoviePlane = null;

        mGestureMask = GestureHandler.GESTURE_ALL;
        mGestureHandler = new GestureHandlerAndroid(metaioSDK, mGestureMask);

        mCallbackHandler = new MetaioSDKCallbackHandler();

		/*
         * NEW
		 */
        stepsList = new ArrayList<String[]>();
//        tts = new TextToSpeech(this, this);
        currentStep = 0;

        mGUIView.findViewById(R.id.btnNext).setVisibility(View.INVISIBLE);
        /*
         * END
		 */

    }

    /*
     * NEW addition
     */
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {
            int result = tts.setLanguage(Locale.US);
            // If your device doesn't support language you set above
            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {

                Toast.makeText(this, "Language not supported",
                        Toast.LENGTH_LONG).show();

            }
        } else {
            Toast.makeText(this, "TTS Initilization Failed", Toast.LENGTH_LONG)
                    .show();
        }

    }

	/*
     * END
	 */

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        super.onTouch(v, event);

        mGestureHandler.onTouch(v, event);

        return true;
    }

    public void startBrowser(String path) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(path));
        startActivity(browserIntent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mCallbackHandler.delete();
        mCallbackHandler = null;
    }

    @Override
    protected int getGUILayout() {
        return R.layout.ar_layout;
    }

    public void onBrowserButtonClick(View v) {

        String path = "http://localhost:8080/iotAR_nrf/server/demos/md2viewer.html";

        startBrowser(path);
        currentStep = 0;
        nextStep();
        finish();
    }

    public void onModelButtonClick(View v) {
        setActiveModel(0);
        // setActiveModel(4);
        mGUIView.setVisibility(View.VISIBLE);

        mGUIView.findViewById(R.id.btnNext).setVisibility(View.VISIBLE);

    }

    public void onImageButtonClick(View v) {
        setActiveModel(1);

        mGUIView.findViewById(R.id.btnNext).setVisibility(View.INVISIBLE);
        mGUIView.findViewById(R.id.txtStepText).setVisibility(View.INVISIBLE);
        currentStep = 0;
        nextStep();

    }

    public void onMovieButtonClick(View v) {
        setActiveModel(3);

        mGUIView.findViewById(R.id.btnNext).setVisibility(View.INVISIBLE);
        mGUIView.findViewById(R.id.txtStepText).setVisibility(View.INVISIBLE);
        // currentStep = 0;
        // nextStep();
    }

    @Override
    protected void loadContents() {
        try {
            // Load desired tracking data for planar marker tracking
            // final File trackingConfigFile = AssetsManager
            // .getAssetPathAsFile(getApplicationContext(),
            // "TutorialContentTypes/Assets/TrackingData_MarkerlessFast.xml");
            final File trackingConfigFile = new File(dir,
                    "/iotAR_nrf/tracking/TrackingData_MarkerlessFast.xml");

            final boolean result = metaioSDK
                    .setTrackingConfiguration(trackingConfigFile);
            MetaioDebug.log("Tracking data loaded: " + result);

			/*
             * Starting parsing the XML here
			 */

//            assetPath = (Environment.getExternalStorageDirectory() + "/iotAR_nrf/models");
//            DocumentBuilderFactory factory = DocumentBuilderFactory
//                    .newInstance();
//            DocumentBuilder builder = factory.newDocumentBuilder();
//            Document dom = builder.parse(new File(assetPath + "/"
//                    + "acocremoval.xml"));
//
//            Element root = dom.getDocumentElement();
//
//            // load steps into arraylist
//            NodeList n = root.getElementsByTagName(KEY_STEP);
//            for (int i = 0; i < n.getLength(); i++) {
//                Node item = n.item(i);
//                NodeList properties = item.getChildNodes();
//
//                String[] content = new String[3];
//                for (int j = 0; j < properties.getLength(); j++) {
//                    Node property = properties.item(j);
//                    String name = property.getNodeName();
//
//                    if (name.equalsIgnoreCase(KEY_TEXT)) {
//                        content[0] = property.getFirstChild().getNodeValue();
//                    } else if (name.equalsIgnoreCase(KEY_TTS)) {
//                        content[1] = property.getFirstChild().getNodeValue();
//                    } else if (name.equalsIgnoreCase(KEY_ANIM)) {
//                        content[2] = property.getFirstChild().getNodeValue();
//                    }
//
//                }
//                stepsList.add(content);
//
//            }

			/*
             * END OF PARSING
			 */

			/*
             * For models in asset file uncomment // Load all the geometries.
			 * First - Model
			 */

            // final File modelPath = AssetsManager.getAssetPathAsFile(
            // getApplicationContext(),
            // "TutorialContentTypes/Assets/models/acoc.md2");

//            final File modelPath = new File(dir, "/iotAR_nrf/models/acoc.md2");
//            if (modelPath != null) {
//                mMetaioMan = metaioSDK.createGeometry(modelPath);
//                if (mMetaioMan != null) {
//                    mMetaioMan.setCoordinateSystemID(1);
//                    // mMetaioMan.setTranslation(new Vector3d(0f, 10f, 0f));
//                    mMetaioMan.setTranslation(new Vector3d(0f, 0f, 0f));
//                    mGestureHandler.addObject(mMetaioMan, 1);
//                    mMetaioMan.setScale(0.1f);
//					/*
//					 * New animation added
//					 */
//                    mMetaioMan.setAnimationSpeed(5);
//					/*
//					 * END
//					 */
//
//                    MetaioDebug.log("Loaded geometry " + modelPath);
//                } else {
//                    MetaioDebug.log(Log.ERROR, "Error loading geometry: "
//                            + modelPath);
//                }
//
//            }

			/*
             * Use when image is inside asset folder for Loading image geometry
			 */

            // final File imagePath = AssetsManager.getAssetPathAsFile(
            // getApplicationContext(),
            // "TutorialContentTypes/Assets/models/frame.png");

            final File imagePath = new File(dir,
                    "/iotAR_nrf/image/shirts_offer.png");

            if (imagePath != null) {
                mImagePlane = metaioSDK.createGeometryFromImage(imagePath);
                if (mImagePlane != null) {
                    mImagePlane.setCoordinateSystemID(1);
                    mImagePlane.setScale(2.5f);
                    MetaioDebug.log("Loaded geometry " + imagePath);
                } else {
                    MetaioDebug.log(Log.ERROR, "Error loading geometry: "
                            + imagePath);
                }
            }

			/*
             * new image added
			 */
            final File imagePath1 = new File(dir,
                    "/iotAR_nrf/image/jeans_offer.png");

            if (imagePath1 != null) {
                mImagePlane1 = metaioSDK.createGeometryFromImage(imagePath1);
                if (mImagePlane1 != null) {
                    mImagePlane1.setCoordinateSystemID(2);
                    mImagePlane1.setScale(1.5f);
                    MetaioDebug.log("Loaded geometry " + imagePath);
                } else {
                    MetaioDebug.log(Log.ERROR, "Error loading geometry: "
                            + imagePath1);
                }
            }

            final File imagePath2 = new File(dir, "/iotAR_nrf/image/hats_offer.png");

            if (imagePath2 != null) {
                mImagePlane2 = metaioSDK.createGeometryFromImage(imagePath2);
                if (mImagePlane2 != null) {
                    mImagePlane2.setCoordinateSystemID(4);
                    mImagePlane2.setScale(1.5f);
                    MetaioDebug.log("Loaded geometry " + imagePath);
                } else {
                    MetaioDebug.log(Log.ERROR, "Error loading geometry: "
                            + imagePath2);
                }
            }
//            final File imagePath3 = new File(dir, "/iotAR_nrf/image/levis_logo.png");
//
//            if (imagePath3 != null) {
//                mImagePlane3 = metaioSDK.createGeometryFromImage(imagePath3);
//                if (mImagePlane3 != null) {
//                    mImagePlane3.setCoordinateSystemID(3);
//                    mImagePlane3.setScale(5.0f);
//                    MetaioDebug.log("Loaded geometry " + imagePath3);
//                } else {
//                    MetaioDebug.log(Log.ERROR, "Error loading geometry: "
//                            + imagePath3);
//                }
//            }
            final File imagePath4 = new File(dir,
                    "/iotAR_nrf/image/promo_offer.png");

            if (imagePath4 != null) {
                mImagePlane4 = metaioSDK.createGeometryFromImage(imagePath4);
                if (mImagePlane4 != null) {
                    mImagePlane4.setCoordinateSystemID(5);
                    mImagePlane4.setScale(2.5f);
                    MetaioDebug.log("Loaded geometry " + imagePath4);
                } else {
                    MetaioDebug.log(Log.ERROR, "Error loading geometry: "
                            + imagePath4);
                }
            }

            final File imagePath5 = new File(dir,
                    "/iotAR_nrf/image/shirt_specific_offer.png");
            if (imagePath5 != null) {
                mImagePlane5 = metaioSDK.createGeometryFromImage(imagePath5);
                if (mImagePlane5 != null) {
                    mImagePlane5.setCoordinateSystemID(6);
                    mImagePlane5.setScale(1.5f);
                    MetaioDebug.log("Loaded geometry " + imagePath5);
                } else {
                    MetaioDebug.log(Log.ERROR, "Error loading geometry: "
                            + imagePath5);
                }
            }
            /*
             * END
			 */

            final File moviePath = new File(dir,
                    "/iotAR_nrf/videos/demo_movie_real.3g2");

            if (moviePath != null) {
                mMoviePlane = metaioSDK
                        .createGeometryFromMovie(moviePath, true);
                if (mMoviePlane != null) {
                    mMoviePlane.setScale(2.0f);
                    // mMoviePlane.setRotation(new Rotation(0f, 0f,
                    // (float) -Math.PI / 2));
                    MetaioDebug.log("Loaded geometry " + moviePath);
                } else {
                    MetaioDebug.log(Log.ERROR, "Error loading geometry: "
                            + moviePath);
                }
            }

            setActiveModel(1);

        } catch (Exception e) {
            e.printStackTrace();
            MetaioDebug.log(Log.ERROR, "loadContents failed, see stack trace");
        }
    }

    @Override
    protected void onGeometryTouched(IGeometry geometry) {

        if (geometry.equals(mMetaioMan)) {
            // NEW
            nextStep();

            // END
        }
        // NEW
//        if (tts.isSpeaking())
//            tts.stop();
        // END

        if (geometry.equals(mMoviePlane)) {
            final MovieTextureStatus status = mMoviePlane
                    .getMovieTextureStatus();
            if (status.getPlaybackStatus() == EPLAYBACK_STATUS.EPLAYBACK_STATUS_PLAYING) {
                mMoviePlane.pauseMovieTexture();
            } else {
                mMoviePlane.startMovieTexture(true);
            }
        }

        if (geometry.equals(mImagePlane2)) {
            System.out.println("@@##   SINGLE CLICLED ");
            RequestParams params = new RequestParams();
            params.put("macaddr", CatalogActivity.imei);
            params.put("wishname", "Marriage");
            params.put("products", "ACC9911");
            // addtowishlistService(params);

            if (AccountState.getOfflineMode()) {
                Toast.makeText(
                        getApplicationContext(),
                        "Request processed successfully\nProduct "
                                + " added to the Cart", Toast.LENGTH_LONG).show();
            } else {
                new DownloadJSONforaddCart().execute();
            }
        }

    }

    @Override
    protected IMetaioSDKCallback getMetaioSDKCallbackHandler() {
        return mCallbackHandler;
    }

    private void setActiveModel(int modelIndex) {
        mSelectedModel = modelIndex;

//        if (mSelectedModel == 0) {
//            mMetaioMan.setVisible(true);
//            mImagePlane.setVisible(false);
//            mMoviePlane.setVisible(false);
//
//        } else {
//            mMetaioMan.setVisible(false);
//        }
        if (modelIndex == 1) {
            mImagePlane.setVisible(true);
            mImagePlane1.setVisible(true);
            mMoviePlane.setVisible(false);
        }
        if (modelIndex == 3) {
            mMoviePlane.setVisible(true);
            mImagePlane.setVisible(false);
        }
        if (modelIndex != 3) {
            mMoviePlane.stopMovieTexture();

        }

        // Start or pause movie according to tracking state
        mCallbackHandler.onTrackingEvent(metaioSDK.getTrackingValues());
    }

    final private class MetaioSDKCallbackHandler extends IMetaioSDKCallback {
        @Override
        public void onSDKReady() {
            // show GUI after SDK is ready
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mGUIView.setVisibility(View.VISIBLE);
                    mGUIView.findViewById(R.id.btnNext).setOnClickListener(
                            new View.OnClickListener() {

                                @Override
                                public void onClick(View arg0) {
                                    nextStep();

                                }
                            });
                }
            });

        }

        @Override
        public void onTrackingEvent(TrackingValuesVector trackingValues) {
            super.onTrackingEvent(trackingValues);

            // We only have one COS, so there can only ever be one
            // TrackingValues structure passed.
            // Play movie if the movie button was selected and we're currently
            // tracking.
            if (trackingValues.isEmpty()
                    || !trackingValues.get(0).isTrackingState()) {
                if (mMoviePlane != null) {
                    mMoviePlane.pauseMovieTexture();
                }
            } else {
                if (mMoviePlane != null && mSelectedModel == 3) {
                    mMoviePlane.startMovieTexture(true);
                }
            }
        }
    }

    @Override
    public void onDrawFrame() {
        super.onDrawFrame();

        if (metaioSDK != null) { // get all detected poses/targets
            TrackingValuesVector poses = metaioSDK.getTrackingValues();

            // if we have detected one, attach our metaio man to this coordinate
            // system Id
            if (poses.size() != 0) {
//                mMetaioMan.setCoordinateSystemID(1);
            }
        }
    }

    /*
     * NEW CODE
     */
    public void nextStep() {

//        mMetaioMan.stopAnimation();
//        mMetaioMan.startAnimation(stepsList.get(currentStep)[2], true);
//        if (mGUIView != null) {
//            // tv = (TextView) ((RelativeLayout) ((RelativeLayout) mGUIView)
//            // .getChildAt(2)).getChildAt(1);
//
//            tv = (TextView) findViewById(R.id.txtStepText);
//            runOnUiThread(new Runnable() {
//                public void run() {
//                    // mGUIView.setVisibility(View.VISIBLE);
//                    tv.setText("" + (stepsList.get(currentStep)[0]));
//
//                }
//            });
//            tts.speak(stepsList.get(currentStep)[1], TextToSpeech.QUEUE_FLUSH,
//                    null);
//        }
//        MetaioDebug.log(currentStep + ":::" + stepsList.get(currentStep)[0]);
//        currentStep++;
//        if (currentStep == stepsList.size()) {
//            currentStep--;
//            runOnUiThread(new Runnable() {
//                public void run() {
//                    mGUIView.findViewById(R.id.btnNext).setVisibility(
//                            View.INVISIBLE);
//
//                }
//            });

//        }
    }

	/*
     * END
	 */

	/*
     * New for interactive add
	 */


    // Web service for ADD to CART

    /**
     * Method that performs RESTful webservice invocations
     *
     * @param params
     */
    public void addtowishlistService(RequestParams params) {

        // Toast.makeText(getApplicationContext(),"JSON request params is" +
        // params.toString(),Toast.LENGTH_LONG).show(); //to show the REQUEST

        AsyncHttpClient client = new AsyncHttpClient();

        client.post(CatalogActivity.urlPart + "/api/WishList/Add", params,
                new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        try {
                            String response = new String(responseBody, "UTF-8");

                            // JSON Object
                            JSONObject obj = new JSONObject(response);

                            JSONObject obj1 = new JSONObject(obj
                                    .getString("header"));

                            String msg = obj1.getString("message");
                            // Toast.makeText(getApplicationContext(),"Response is\n"
                            // +obj1.toString(),Toast.LENGTH_LONG).show();

                            if (msg.equalsIgnoreCase("Request processed successfully")) {

                                // Set Default Values for Edit View controls
                                // setDefaultValues();

                                // Toast.makeText(getApplicationContext(),"Request
                                // processed
                                // successfully",Toast.LENGTH_LONG).show();

                                String status = obj1.getString("status");

                                String str4 = null, str3 = null, str5 = null;
                                if (status.equalsIgnoreCase("success")) {

                                    JSONArray obj3 = (JSONArray) obj
                                            .get("cusdetails");

                                    try {
                                        str3 = (String) obj3.getJSONObject(0)
                                                .get("customer");
                                        str4 = (String) obj3.getJSONObject(0)
                                                .get("Products added");
                                        Toast.makeText(
                                                AR_activity.this,
                                                "Request processed successfully\nProduct "
                                                        + str4
                                                        + " added to wishlist for Customer: "
                                                        + str3,
                                                Toast.LENGTH_LONG).show();
                                    } catch (JSONException j) {

                                        str5 = (String) obj3.getJSONObject(0)
                                                .get("product");
                                        Toast.makeText(
                                                AR_activity.this,
                                                "Product " + str5
                                                        + " in the wishlist",
                                                Toast.LENGTH_LONG).show();
                                    }
                                }
                            }
                        } catch (IOException io) {
                            io.printStackTrace();
                        } catch (JSONException e) {
                            Toast.makeText(
                                    AR_activity.this,
                                    "Error Occured [Server's JSON response might be invalid]!",
                                    Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                        }
                    }


                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        if (statusCode == 404) {
                            Toast.makeText(AR_activity.this,
                                    "Requested resource not found",
                                    Toast.LENGTH_LONG).show();
                        }
                        // When Http response code is '500'
                        else if (statusCode == 500) {
                            Toast.makeText(AR_activity.this,
                                    "Something went wrong at server end",
                                    Toast.LENGTH_LONG).show();
                        }
                        // When Http response code other than 404, 500
                        else {
                            Toast.makeText(
                                    AR_activity.this,
                                    "Device might not be connected to Internet",
                                    Toast.LENGTH_LONG).show();
                        }
                    }

                });
    }

	/*
     * new to add to cart
	 */

    /*
     * add to cart
     */
    // DownloadJSON AsyncTask
    private class DownloadJSONforaddCart extends AsyncTask<Void, Void, Integer> {
        String str4 = null, str3 = null, str5 = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Integer doInBackground(Void... params) {

			/*
             * System.setProperty("https.proxyHost", "10.243.115.76");
			 * System.setProperty("https.proxyPort", "6050");
			 * System.setProperty("http.proxyHost", "10.243.115.76");
			 * System.setProperty("http.proxyPort", "6050");
			 */
            try {
                // Create an array
                arraylist = new ArrayList<HashMap<String, String>>();
                // Retrieve JSON Objects from the given URL address
                jsonobject = JsonFuntionCart
                        .getJSONfromURL(
                                "http://ethcustomervalidtionax.azurewebsites.net/api/cart/CreateCart?idToken=" + AccountState.getTokenID(),
                                "68719483123", "1");

                System.out.println("@@## ADD CART AX " + jsonobject);


                String message = jsonobject.getString("Id");

                return 1;

            } catch (JSONException | NullPointerException e) {
                Log.e("Error", e.getMessage());


                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Integer args) {

            if (args != null) {
                Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(
                        getApplicationContext(),
                        "Request processed successfully\nProduct "
                                + " added to the Cart", Toast.LENGTH_LONG).show();
            }
        }
    }

}
