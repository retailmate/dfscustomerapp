package com.cognizant.iot.activityfindproduct;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.cognizant.iot.activity.CatalogActivity;
import com.cognizant.iot.activityproductlist.ImageLoaderprod;
import com.cognizant.iot.activitywishlist.JSONfunctions_wish_deleteproduct;
import com.cognizant.iot.json.AssetsExtracter;
import com.cognizant.iot.mycart.JsonFuntionCart;
import com.cognizant.iot.utils.AccountState;
import com.cognizant.iot.utils.MySSLSocketFactory;
import com.cognizant.retailmate.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ProductDetails_findprod extends AppCompatActivity {
    private static final String TAG = "@@##";
    public String value, distinctProductVariantId, recommendedProductId;

    Intent myIntent = getIntent();

    JSONObject jsonobject, jsonobjectAllergy;

    JSONArray jsonarray, jsonarrayAllergy;

    String imageUrl;

    String wish_present, offer, prodid, itemid;

    TextView product_id, product_name, product_description, product_price,
            product_offer, product_category, product_unit, pdt_details,
            allergic;

    String Allergic;

    ImageView imageViewObject, i1, i2, i3, i4, i5;

    ImageButton wish_btn, shop_cart2;

    ImageView offer_tag;

    TextView et, st;

    Bitmap mIcon11;

    AssetsExtracter mTask;

    int min = 0, smin = 0;

    int max = 10, smax = 10;

    String res1, res2;

    int Qval, Sval;

    Integer valqty, valQ, valsize, valS;

    ArrayList<HashMap<String, String>> arraylist;

    ImageButton qinc;
    ImageButton qdec;
    ImageButton sinc;
    ImageButton sdec;

    Boolean fromBeaconClass;
    /*
     * Bitmap color change
     */
    ImageView image11;
    ImageView image12, image13, image14, image15;

    /*
     * (non-Javadoc)
     *
     * @see android.app.Activity#onCreate(android.os.Bundle)
     */
    ImageView tag_dropdown_icon;
    TextView view_offer_tv;

    /*
     *
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 * 
	 * NEW AX
	 */

    TextView suggestionProductName;
    ImageView suggestionProductImage;

    ImageLoaderprod imageLoader;

    ProgressBar progressSearchAX;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.productdescriptionfindproduct);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarProductDescription);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);


        product_id = (TextView) findViewById(R.id.pdtID_text_view);
        product_name = (TextView) findViewById(R.id.product_name);
        product_description = (TextView) findViewById(R.id.description_text_view);
        product_price = (TextView) findViewById(R.id.product_cost);
        product_unit = (TextView) findViewById(R.id.product_unit);
        product_category = (TextView) findViewById(R.id.category_text_view);
        product_offer = (TextView) findViewById(R.id.offer_details_tv);
        allergic = (TextView) findViewById(R.id.allergic);
        imageViewObject = (ImageView) findViewById(R.id.product_image1);
        shop_cart2 = (ImageButton) findViewById(R.id.shop_cart2);
        suggestionProductName = (TextView) findViewById(R.id.suggestedProductName);
        suggestionProductImage = (ImageView) findViewById(R.id.suggestedProductImage);
        progressSearchAX = (ProgressBar) findViewById(R.id.progressSearchAX);
        offer_tag = (ImageView) findViewById(R.id.offer_details);
        wish_btn = (ImageButton) findViewById(R.id.wishlist);
        tag_dropdown_icon = (ImageView) findViewById(R.id.tag_dropdown_icon);
        view_offer_tv = (TextView) findViewById(R.id.view_offer_tv);


        Bundle extras = getIntent().getExtras();

        try {
            if (extras != null) {
                value = extras.getString("ProductId");
                System.out.println("@@##VALUE" + value);
                value = value.replaceAll("\\s", "");
                System.out.println("@@$$VALUE NEW:" + value);


                if (extras.get("class").equals("BEACON")) {
                    shop_cart2.setVisibility(View.INVISIBLE);
                    wish_btn.setVisibility(View.INVISIBLE);
                    fromBeaconClass = true;
                } else {
                    fromBeaconClass = false;
                }

            }
        } catch (NullPointerException e) {

        }


        imageLoader = new ImageLoaderprod(ProductDetails_findprod.this);


        if (fromBeaconClass) {
            tag_dropdown_icon.setVisibility(View.INVISIBLE);
            view_offer_tv.setText("Offer Availed!");
            view_offer_tv.setTextColor(Color.parseColor("#000000"));
        }

        i1 = (ImageView) findViewById(R.id.image1);
        i2 = (ImageView) findViewById(R.id.image2);
        i3 = (ImageView) findViewById(R.id.image3);
        i4 = (ImageView) findViewById(R.id.image4);
        i5 = (ImageView) findViewById(R.id.image5);

        new AdapterUpdaterTaskAX7().execute();

		/*
         * bitmap color change
		 */
        image11 = (ImageView) findViewById(R.id.image1);

        image12 = (ImageView) findViewById(R.id.image2);
        image13 = (ImageView) findViewById(R.id.image3);
        image14 = (ImageView) findViewById(R.id.image4);
        image15 = (ImageView) findViewById(R.id.image5);
        /*
         * END
		 */


        pdt_details = (TextView) findViewById(R.id.heading_pdt_name);
        String udata = "PRODUCT DETAILS";
        // SpannableString content = new SpannableString(udata);
        // content.setSpan(new UnderlineSpan(), 0, udata.length(), 0);
        pdt_details.setText(udata);

        et = (TextView) findViewById(R.id.qtynumber);
        res1 = et.getText().toString();
        valqty = Integer.parseInt(res1);
        Qval = valqty.intValue();

        st = (TextView) findViewById(R.id.pdtsize);
        res2 = st.getText().toString();
        valsize = Integer.parseInt(res2);
        Sval = valsize.intValue();

        qinc = (ImageButton) findViewById(R.id.bqty_inc);
        qinc.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {

                if (Qval <= max - 1) {
                    Qval = Qval + 1;
                    valQ = new Integer(Qval);
                    et.setText(valQ.toString());
                }
            }
        });

        qdec = (ImageButton) findViewById(R.id.bqty_dec);
        qdec.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (Qval >= min + 1) {
                    Qval = Qval - 1;
                    valQ = new Integer(Qval);
                    et.setText(valQ.toString());
                }
            }
        });

        sinc = (ImageButton) findViewById(R.id.size_inc);
        sinc.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (Sval <= smax - 1) {
                    Sval = Sval + 1;
                    valS = new Integer(Sval);
                    st.setText(valS.toString());
                }
            }
        });

        sdec = (ImageButton) findViewById(R.id.size_dec);
        sdec.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (Sval >= smin + 1) {
                    Sval = Sval - 1;
                    valS = new Integer(Sval);
                    st.setText(valS.toString());
                }
            }
        });



		/*
         * DIALOG ADDED
		 */
        view_offer_tv.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {

                // Create custom dialog object
                final Dialog dialog = new Dialog(ProductDetails_findprod.this);
                // hide to default title for Dialog

                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                // inflate the layout dialog_layout.xml and set it as
                // contentView
                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view = inflater.inflate(R.layout.dilogbox, null, false);
                dialog.setCanceledOnTouchOutside(false);
                dialog.setContentView(view);


                Button btninstore = (Button) dialog
                        .findViewById(R.id.check_instore_offer);
                btninstore.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        // Open the browser
                        // Intent In = new Intent(
                        // Product_description_wishlistscreen.this,
                        // ListBeaconsActivity.class);
                        // startActivity(In);

                        mTask = new AssetsExtracter("empty",
                                ProductDetails_findprod.this);
                        mTask.execute();

                        // Dismiss the dialog
                        dialog.dismiss();
                    }
                });
                Button btndismiss = (Button) dialog.findViewById(R.id.dismiss);
                btndismiss.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        // Open the browser
                        // Intent In = new Intent(MainActivity.this,
                        // Availoffer.class);
                        // startActivity(In);
                        // Dismiss the dialog
                        dialog.dismiss();
                    }
                });

                // Display the dialog
                dialog.show();
            }
        });
        /*
         * END
		 */

        shop_cart2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (R.drawable.cart_red_icon == shop_cart2.getId()) {
                    shop_cart2.setImageResource(R.drawable.no_crl_cart_icon);
                } else {
                    shop_cart2.setImageResource(R.drawable.cart_red_icon);
                }

                new DownloadJSONforAXaddCartfrombeaconORZbar().execute();
            }
        });

        // Delete this
        wish_btn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                if (R.drawable.hearticonnn == wish_btn.getId()) {
                    wish_btn.setImageResource(R.drawable.no_crl_mywish_icon);
                } else {
                    wish_btn.setImageResource(R.drawable.hearticonnn);
                }
                new DownloadJSONforAXADDwishlistFromBeaconORZbar().execute();

            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // setMenuBackground();

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar app icon/title

        // Handle action bar actions click
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

	/*
     * new for ax7
	 */

    private class AdapterUpdaterTaskAX7 extends AsyncTask<Void, Void, Integer> {

        List<ProductData_findprod> ListData = new ArrayList<ProductData_findprod>();

        String category, description, suggestionID, relationName;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Integer doInBackground(Void... params) {
            Log.i("UPDATE", "1");

            System.out.println("@@#$$PRODUCT ID ADD INDICATOR:" + value);

            JSONObject jsonobject1 = JSONFunctionsDetails_findprod
                    .getJSONfromURLAX(
                            "https://etihaddemodevret.cloudax.dynamics.com/Commerce/Products/Search?$top=250&api-version=7.1",
                            value, CatalogActivity.imei);


            System.out.println("@@##JSON AX SEARCH RESULT OBJECT: "
                    + jsonobject1);

            try { // jsonobject = new
                // JSONObject(jsonobject1.getString("assets")) ;

				/*
                 * newly added for allergy
				 */

                jsonarray = jsonobject1.getJSONArray("value");

                for (int i = 0; i < jsonarray.length(); i++) {
                    jsonobject = jsonarray.getJSONObject(i);


                    JSONObject insideImage = new JSONObject();
                    insideImage = jsonobject.getJSONObject("Image");
                    JSONArray insideitemimage = new JSONArray();
                    insideitemimage = insideImage.getJSONArray("Items");

                    JSONObject insideImageURL = new JSONObject();
                    for (int j = 0; j < insideitemimage.length(); j++) {

                        insideImageURL = insideitemimage.getJSONObject(i);
                    }

                    ListData.add(new ProductData_findprod(jsonobject
                            .getString("RecordId"), jsonobject
                            .getString("ProductName"), jsonobject
                            .getString("Price"), jsonobject
                            .getString("Description"),
                            "https://etihaddemodevret.cloudax.dynamics.com/MediaServer/"
                                    + insideImageURL.getString("Url"), "", "",
                            "", "", "0", jsonobject.getString("ItemId")));


                    // FOR GETTING THE VARIANT ID
                    try {
                        JSONObject compositionInfo = new JSONObject();

                        compositionInfo = jsonobject.getJSONObject("CompositionInformation");
                        JSONObject variantInfo = new JSONObject();

                        variantInfo = compositionInfo.getJSONObject("VariantInformation");

                        JSONArray variants = new JSONArray();
                        variants = variantInfo.getJSONArray("Variants");

                        JSONObject insideVariantObj = new JSONObject();


                        // Loop kept just to iterate once as we are picking up the first variant ID
                        for (int jk = 0; jk < 1; jk++) {

                            insideVariantObj = variants
                                    .getJSONObject(jk);

                            distinctProductVariantId = insideVariantObj
                                    .getString("DistinctProductVariantId");

                        }
                    } catch (NullPointerException | org.json.JSONException e) {
                        distinctProductVariantId = jsonobject
                                .getString("RecordId");
                    }

                    //FOR RECOMMENDED PRODUCT DETAILS
                    JSONArray insiderelatedproductArray = new JSONArray();
                    insiderelatedproductArray = jsonobject
                            .getJSONArray("RelatedProducts");


                    JSONObject insideRelatedProduct = new JSONObject();
                    for (int j = 0; j < insiderelatedproductArray.length(); j++) {

                        insideRelatedProduct = insiderelatedproductArray.getJSONObject(i);
                    }

                    category = "Houseware";
                    description = jsonobject.getString("Description");

                    suggestionID = insideRelatedProduct
                            .getString("RelatedProductRecordId");

                    relationName = insideRelatedProduct.getString("RelationName");

                }


            } catch (JSONException | NullPointerException | ArrayIndexOutOfBoundsException e) {

                Log.e("Error", e.getMessage());


                e.printStackTrace();

                return 0;
            }

            Allergic = "no";


            Log.i("UPDATE", "2");
            return 1;
        }

        @Override
        protected void onPostExecute(Integer aVoid) {

            if (aVoid == 0) {
                Toast.makeText(getApplicationContext(), "Not a valid product, please try again", Toast.LENGTH_SHORT).show();
                finish();
            } else {

                int size = ListData.size();
                if (size > 0) {
                    for (int i = 0; i < ListData.size(); i++) {

                        product_id.setText(ListData.get(i).getId());
                        product_name.setText(ListData.get(i).getName());

                        System.out.println("@@## PRODUCT NAME: \n"
                                + ListData.get(i).getName());
                        product_description.setText(ListData.get(i).getDesc());
                        product_unit.setText(ListData.get(i).getUnit());
                        // product_price.setText("$" + ListData.get(i).getPrice());
                        product_price.setText("$" + ListData.get(i).getPrice());
                        product_category.setText(ListData.get(i).getCategory());
                        if (Allergic.equals("no")) {
                            allergic.setVisibility(View.INVISIBLE);
                        } else {
                            allergic.setText("You are allergic to " + Allergic
                                    + " (Not Recommended)");
                        }

                        wish_present = ListData.get(i).getFlag();
                        offer = ListData.get(i).getOffer();
                        prodid = ListData.get(i).getId();
                        itemid = ListData.get(i).getitemID();


                        if (ListData.get(i).getOffer().isEmpty()) {
                            product_offer.setText("No offer available");
                        } else {
                            product_offer.setText(ListData.get(i).getOffer() + "\n"
                                    + "ends " + ListData.get(i).getLastdate());
                        }

                        if (wish_present.toString().equals("1")) {
                            wish_btn.setImageResource(R.drawable.hearticonbig);
                        } else {
                            wish_btn.setImageResource(R.drawable.bbb);
                        }

                        if (offer.length() == 0) {
                            offer_tag.setVisibility(View.INVISIBLE);
                            product_offer.setVisibility(View.INVISIBLE);
                        }


                        imageUrl = ListData.get(i).getImageUrl();
                        new DownloadImageTask(
                                (ImageView) findViewById(R.id.product_image1))
                                .execute(imageUrl);

                    }
                    Log.i("UPDATE", "4");
                } else {
                    Toast.makeText(getApplicationContext(), "No detail found",
                            Toast.LENGTH_SHORT).show();
                }


                //

                product_category.setText(category);
                product_description.setText(description);
                recommendedProductId = suggestionID;

            }


            super.onPostExecute(aVoid);

        }
    }

	/*
     *
	 * END AX7
	 */

//    private class AdapterUpdaterTask extends AsyncTask<Void, Void, Void> {
//
//        List<ProductData_findprod> ListData = new ArrayList<ProductData_findprod>();
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//
//        }
//
//        @Override
//        protected Void doInBackground(Void... params) {
//            Log.i("UPDATE", "1");
//
//            System.out.println("@@#$$PRODUCT ID ADD INDICATOR:" + value);
//
//            JSONObject jsonobject1 = JSONFunctionsDetails_findprod
//                    .getJSONfromURL(CatalogActivity.urlPart
//                                    + "/api/Product/GetAllergicProductDetails", value,
//                            CatalogActivity.imei);
//
//            // JSONObject jsonobject1 = JSONFunctionsDetails_findprod
//            // .getJSONfromURL(CatalogActivity.urlPart
//            // + "/api/WishList/AddIndicator", value,
//            // CatalogActivity.imei);
//
//            System.out.println("@@##JSON OBJECT: " + jsonobject1);
//
//            try { // jsonobject = new
//                // JSONObject(jsonobject1.getString("assets")) ;
//
//				/*
//				 * newly added for allergy
//				 */
//
//                jsonarray = jsonobject1.getJSONArray("assets");
//
//                for (int i = 0; i < jsonarray.length(); i++) {
//                    jsonobject = jsonarray.getJSONObject(i);
//                    // jsonobjectAllergy = jsonarrayAllergy.getJSONObject(i);
//                    ListData.add(new ProductData_findprod(jsonobject
//                            .getString("ProductID"), jsonobject
//                            .getString("ItemId"), jsonobject.getString("Name"),
//                            jsonobject.getString("Cost"), jsonobject
//                            .getString("Desc"), jsonobject
//                            .getString("thumbnail"), jsonobject
//                            .getString("Category"), jsonobject
//                            .getString("Offer"), jsonobject
//                            .getString("Lastdate"), jsonobject
//                            .getString("Unit"), jsonobject
//                            .getString("Wishlist")));
//                }
//
//            } catch (JSONException e) {
//
//                Log.e("Error", e.getMessage());
//                e.printStackTrace();
//            }
//            try {
//                jsonarrayAllergy = jsonobject1
//                        .getJSONArray("Allergic to contents");
//
//                for (int i = 0; i < jsonarray.length(); i++) {
//                    jsonobjectAllergy = jsonarrayAllergy.getJSONObject(i);
//                    Allergic = jsonobjectAllergy.getString("AllergictoContent");
//                }
//            } catch (NullPointerException e) {
//                // TODO Auto-generated catch block
//                Allergic = "no";
//            } catch (JSONException e) {
//                // TODO Auto-generated catch block
//                Allergic = "no";
//                e.printStackTrace();
//            } catch (IndexOutOfBoundsException e) {
//                Allergic = "no";
//            }
//
//            Log.i("UPDATE", "2");
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(Void aVoid) {
//
//            int size = ListData.size();
//            if (size > 0) {
//                for (int i = 0; i < ListData.size(); i++) {
//
//                    product_id.setText(ListData.get(i).getId());
//                    product_name.setText(ListData.get(i).getName());
//                    product_description.setText(ListData.get(i).getDesc());
//                    product_unit.setText(ListData.get(i).getUnit());
//                    // product_price.setText("$" + ListData.get(i).getPrice());
//                    product_price.setText("$" + ListData.get(i).getPrice());
//                    product_category.setText(ListData.get(i).getCategory());
//                    if (Allergic.equals("no")) {
//                        allergic.setVisibility(View.INVISIBLE);
//                    } else {
//                        allergic.setText("You are allergic to " + Allergic
//                                + " (Not Recommended)");
//                    }
//
//                    wish_present = ListData.get(i).getFlag();
//                    offer = ListData.get(i).getOffer();
//                    prodid = ListData.get(i).getId();
//
//                    // Toast.makeText(getApplicationContext(),"wish present :
//                    // "+wish_present
//                    // , Toast.LENGTH_LONG).show();
//                    // Toast.makeText(getApplicationContext(),"offer : "+offer ,
//                    // Toast.LENGTH_LONG).show();
//                    // Toast.makeText(getApplicationContext(),"Product id :
//                    // "+prodid,
//                    // Toast.LENGTH_LONG).show();
//
//                    if (ListData.get(i).getOffer().isEmpty()) {
//                        product_offer.setText("No offer available");
//                    } else {
//                        product_offer.setText(ListData.get(i).getOffer() + "\n"
//                                + "ends " + ListData.get(i).getLastdate());
//                    }
//
//                    if (wish_present.toString().equals("1")) {
//                        wish_btn.setImageResource(R.drawable.hearticonbig);
//                    } else {
//                        wish_btn.setImageResource(R.drawable.bbb);
//                    }
//
//                    if (offer.length() == 0) {
//                        offer_tag.setVisibility(View.INVISIBLE);
//                        product_offer.setVisibility(View.INVISIBLE);
//                    }
//                    // Delete this
//                    wish_btn.setOnClickListener(new OnClickListener() {
//
//                        @Override
//                        public void onClick(View v) {
//
//                            if (R.drawable.hearticonnn == wish_btn.getId()) {
//                                wish_btn.setImageResource(R.drawable.no_crl_mywish_icon);
//                            } else {
//                                wish_btn.setImageResource(R.drawable.hearticonnn);
//                            }
//
//                            if (wish_present.equals("1")) {
//                                new DownloadJSONfordelete().execute();
//                                // Intent hh = new Intent(
//                                // ProductDetails_findprod.this,
//                                // CatalogActivity.class);
//                                // hh.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                // startActivity(hh);
//                            } else {
//                                new DownloadJSONforAXADDwishlist().execute();
//                                // new
//                                // DownloadJSONforAddingToWishList().execute();
//                                // Intent in = new Intent(
//                                // ProductDetails_findprod.this,
//                                // CatalogActivity.class);
//                                // in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                // startActivity(in);
//                            }
//                        }
//                    });
//
//                    imageUrl = ListData.get(i).getImageUrl();
//                    new DownloadImageTask(
//                            (ImageView) findViewById(R.id.product_image1))
//                            .execute(imageUrl);
//
//                }
//                Log.i("UPDATE", "4");
//            } else {
//                Toast.makeText(getApplicationContext(), "No detail found",
//                        Toast.LENGTH_SHORT).show();
//            }
//            super.onPostExecute(aVoid);
//
//        }
//    }


    // Function to download image

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        ProgressDialog pDialog;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(ProductDetails_findprod.this);
            pDialog.setMessage("Loading Image ....");
            pDialog.show();

            progressSearchAX.setProgress(50);
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            mIcon11 = null;
            try {
                MySSLSocketFactory.doTrustToCertificates();
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);

            pDialog.dismiss();

            Bitmap bitmap11;

            Matrix matrix;

            Bitmap rotated;

            int width, height;
            height = result.getHeight();
            width = result.getWidth();

            final int REQUIRED_SIZE = 170;

            int width_tmp = width, height_tmp = height;

            while (true) {
                if (width_tmp / 2 < REQUIRED_SIZE
                        || height_tmp / 2 < REQUIRED_SIZE)
                    break;
                width_tmp /= 2;
                height_tmp /= 2;

            }

            bitmap11 = Bitmap.createScaledBitmap(result, width_tmp, height_tmp,
                    true);

            // i1.setImageDrawable(imageViewObject.getDrawable());
            i1.setBackgroundColor(Color.rgb(255, 255, 255));
            // Bitmap object=mIcon11.copy(mIcon11.getConfig(), true);
            matrix = new Matrix();
            matrix.postRotate(30);
            rotated = Bitmap.createBitmap(bitmap11, 0, 0, bitmap11.getWidth(),
                    bitmap11.getHeight(), matrix, true);
            i1.setImageBitmap(rotated);

            // i2.setImageDrawable(imageViewObject.getDrawable());
            i2.setBackgroundColor(Color.rgb(255, 255, 255));
            // object=mIcon11.copy(mIcon11.getConfig(), true);
            matrix = new Matrix();
            matrix.postRotate(60);
            rotated = Bitmap.createBitmap(bitmap11, 0, 0, bitmap11.getWidth(),
                    bitmap11.getHeight(), matrix, true);
            i2.setImageBitmap(rotated);

            // i3.setImageDrawable(imageViewObject.getDrawable());
            i3.setBackgroundColor(Color.rgb(255, 255, 255));
            // object=mIcon11.copy(mIcon11.getConfig(), true);
            matrix = new Matrix();
            matrix.postRotate(90);
            rotated = Bitmap.createBitmap(bitmap11, 0, 0, bitmap11.getWidth(),
                    bitmap11.getHeight(), matrix, true);
            i3.setImageBitmap(rotated);

            // i4.setImageDrawable(imageViewObject.getDrawable());
            i4.setBackgroundColor(Color.rgb(255, 255, 255));
            // object=mIcon11.copy(mIcon11.getConfig(), true);
            matrix = new Matrix();
            matrix.postRotate(-30);
            rotated = Bitmap.createBitmap(bitmap11, 0, 0, bitmap11.getWidth(),
                    bitmap11.getHeight(), matrix, true);
            i4.setImageBitmap(rotated);

            // i5.setImageDrawable(imageViewObject.getDrawable());
            i5.setBackgroundColor(Color.rgb(255, 255, 255));
            // object=mIcon11.copy(mIcon11.getConfig(), true);
            matrix = new Matrix();
            matrix.postRotate(-60);
            rotated = Bitmap.createBitmap(bitmap11, 0, 0, bitmap11.getWidth(),
                    bitmap11.getHeight(), matrix, true);
            i5.setImageBitmap(rotated);


            new AdapterSuggestedProductTaskAX7(recommendedProductId).execute();
        }

    }

    // Function of adding to wish list

    private class DownloadJSONforAddingToWishList extends
            AsyncTask<Void, Void, Void> {
        String str4 = null, str3 = null, str5 = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {

            arraylist = new ArrayList<HashMap<String, String>>();
            jsonobject = JSONfunctions_addToWishList_find_product
                    .getJSONfromURL(CatalogActivity.urlPart
                            + "/api/WishList/Add", "Marriage", prodid);

            try {
                JSONObject obj1 = new JSONObject(jsonobject.getString("header"));
                String message = obj1.getString("message");
                if (message.equalsIgnoreCase("Request processed successfully")) {
                    String status = obj1.getString("status");
                    if (status.equalsIgnoreCase("success")) {
                        JSONArray obj3 = (JSONArray) jsonobject
                                .get("cusdetails");
                        try {
                            str3 = (String) obj3.getJSONObject(0).get(
                                    "customer");
                            str4 = (String) obj3.getJSONObject(0).get(
                                    "Products added");
                        } catch (JSONException j) {
                            j.printStackTrace();
                        }
                    }

                }
            } catch (JSONException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            Toast.makeText(
                    getApplicationContext(),
                    "Request processed successfully\nProduct " + str4
                            + " added to wishlist for Customer: " + str3,
                    Toast.LENGTH_LONG).show();
        }
    }


	/*
     * FOR CHANGING BITMAP COLORS
	 */

    public static ArrayList<Bitmap> LIST1 = new ArrayList<Bitmap>();

//    private class LoadImage extends
//            AsyncTask<String, String, ArrayList<Bitmap>> {
//        Bitmap bitmap1, bitmap2, bitmap3, bitmap12, bitmap13, bitmap11,
//                bitmap14, bitmap15;
//        ProgressDialog pDialog;
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            pDialog = new ProgressDialog(ProductDetails_findprod.this);
//            pDialog.setMessage("Loading Image ....");
//            pDialog.show();
//
//        }
//
//        protected ArrayList<Bitmap> doInBackground(String... args) {
//            try {
//                bitmap1 = BitmapFactory.decodeStream((InputStream) new URL(
//                        args[0]).getContent());
//
//                int width, height;
//                height = bitmap1.getHeight();
//                width = bitmap1.getWidth();
//
//                Bitmap bmpGrayscale = Bitmap.createBitmap(width, height,
//                        Bitmap.Config.ARGB_8888);
//                Bitmap bmpGrayscale2 = Bitmap.createBitmap(width, height,
//                        Bitmap.Config.ARGB_8888);
//                Bitmap bmpGrayscale3 = Bitmap.createBitmap(width, height,
//                        Bitmap.Config.ARGB_8888);
//                Bitmap bmpGrayscale4 = Bitmap.createBitmap(width, height,
//                        Bitmap.Config.ARGB_8888);
//                Bitmap bmpGrayscale5 = Bitmap.createBitmap(width, height,
//                        Bitmap.Config.ARGB_8888);
//                Canvas c = new Canvas(bmpGrayscale);
//                Canvas c2 = new Canvas(bmpGrayscale2);
//                Canvas c3 = new Canvas(bmpGrayscale3);
//                Canvas c4 = new Canvas(bmpGrayscale4);
//                Canvas c5 = new Canvas(bmpGrayscale5);
//                Paint paint = new Paint();
//                Paint paint2 = new Paint();
//                Paint paint3 = new Paint();
//                Paint paint4 = new Paint();
//                Paint paint5 = new Paint();
//				/*
//				 * ColorMatrix cm = new ColorMatrix(); cm.setSaturation(10);
//				 */
//
//                // Paint p = new Paint(Color.RED);
//                ColorFilter filter = new LightingColorFilter(Color.RED, 50);
//                paint.setColorFilter(filter);
//                ColorFilter filter2 = new LightingColorFilter(Color.YELLOW, 50);
//                paint2.setColorFilter(filter2);
//                ColorFilter filter3 = new LightingColorFilter(Color.GREEN, 50);
//                paint3.setColorFilter(filter3);
//                ColorFilter filter4 = new LightingColorFilter(Color.GRAY, 50);
//                paint4.setColorFilter(filter4);
//                ColorFilter filter5 = new LightingColorFilter(Color.rgb(50, 74,
//                        89), 50);
//                paint5.setColorFilter(filter5);
//
//				/*
//				 * ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
//				 * paint.setColorFilter(f);
//				 */
//                c.drawBitmap(bitmap1, 0, 0, paint);
//                bitmap11 = bmpGrayscale;
//                LIST1.add(bitmap11);
//
//                c2.drawBitmap(bitmap1, 0, 0, paint2);
//                bitmap12 = bmpGrayscale2;
//                LIST1.add(bitmap12);
//
//                c3.drawBitmap(bitmap1, 0, 0, paint3);
//                bitmap13 = bmpGrayscale3;
//                LIST1.add(bitmap13);
//
//                c4.drawBitmap(bitmap1, 0, 0, paint4);
//                bitmap14 = bmpGrayscale4;
//                LIST1.add(bitmap14);
//
//                c5.drawBitmap(bitmap1, 0, 0, paint5);
//                bitmap15 = bmpGrayscale5;
//                LIST1.add(bitmap15);
//
//				/*
//				 * LIST1.add(bitmap1); LIST1.add(bitmap1); LIST1.add(bitmap1);
//				 */
//
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            return LIST1;
//        }
//
//        protected void onPostExecute(ArrayList<Bitmap> image) {
//
//            pDialog.dismiss();
//
//            // image11.setImageBitmap(image.get(0));
//            // image12.setImageBitmap(image.get(1));
//            // image13.setImageBitmap(image.get(2));
//            // image14.setImageBitmap(image.get(3));
//            // image15.setImageBitmap(image.get(4));
//
//            i1.setImageBitmap(image.get(0));
//            i2.setImageBitmap(image.get(1));
//            i3.setImageBitmap(image.get(2));
//            i4.setImageBitmap(image.get(3));
//            i5.setImageBitmap(image.get(4));
//
//            LIST1.clear();
//
//            new AdapterDetailsTaskAX7().execute();
//
//        }
//    }

	/*
     * END
	 */

	/*
     * NEW FOR AX7
	 */
    /*
     * new for ax7
	 */

//    private class AdapterDetailsTaskAX7 extends AsyncTask<Void, Void, Void> {
//
//        List<ProductData_findprod> ListData = new ArrayList<ProductData_findprod>();
//        String category, description, suggestionID, relationName;
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//
//        }
//
//        @Override
//        protected Void doInBackground(Void... params) {
//            Log.i("UPDATE", "1");
//
//            System.out.println("@@#$$PRODUCT ID AdapterDetailsTaskAX7:"
//                    + prodid);
//
//            JSONObject jsonobject1 = JSONFunctionsDetails_findprod
//                    .getJSONfromURLAX(
//                            "https://etihaddemodevret.cloudax.dynamics.com/Commerce/Products/Search?$top=250&api-version=7.1",
//                            prodid, CatalogActivity.imei);
//
//
//            System.out.println("@@##JSON AX SEARCH RESULT OBJECT: "
//                    + jsonobject1);
//
//            try { // jsonobject = new
//
//                jsonarray = jsonobject1.getJSONArray("value");
//
//                for (int i = 0; i < jsonarray.length(); i++) {
//                    jsonobject = jsonarray.getJSONObject(i);
//
//                    JSONArray insideitemimage = new JSONArray();
//                    insideitemimage = jsonobject
//                            .getJSONArray("RelatedProducts");
//
//
//                    JSONObject insideImageURL = new JSONObject();
//                    for (int j = 0; j < insideitemimage.length(); j++) {
//
//                        insideImageURL = insideitemimage.getJSONObject(i);
//                    }
//
//                    category = "Electronics";
//                    description = jsonobject.getString("Description");
//
//                    suggestionID = insideImageURL
//                            .getString("RelatedProductRecordId");
//
//                    relationName = insideImageURL.getString("RelationName");
//
//                }
//
//            } catch (JSONException e) {
//
//                Log.e("Error", e.getMessage());
//                e.printStackTrace();
//            }
//
//            Log.i("UPDATE", "2");
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(Void aVoid) {
//
//            product_category.setText(category);
//            product_description.setText(description);
//            // suggestionProductName.setText(relationName);
//            new AdapterSuggestedProductTaskAX7(suggestionID).execute();
//            super.onPostExecute(aVoid);
//
//        }
//    }

	/*
     *
	 * END AX7
	 */

	/*
     * NEW FOR AX7
	 */
    /*
     * GETTING SUGGESTED PRODUCT
	 */

    private class AdapterSuggestedProductTaskAX7 extends
            AsyncTask<Void, Void, Void> {

        String productID, suggestedProductName, suggestionImageURL;

        public AdapterSuggestedProductTaskAX7(String suggestionID) {
            // TODO Auto-generated constructor stub
            productID = suggestionID;
            System.out.println("@@## RECEIVED SUGGESTED ID " + productID);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        public void execute(String suggestionID) {
            // TODO Auto-generated method stub

        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.i("UPDATE", "1");

            System.out.println("@@#$$PRODUCT ID AdapterDetailsTaskAX7:"
                    + productID);

            JSONObject jsonobject1 = JSONFunctionsDetails_findprod
                    .getJSONfromURLAX(
                            "https://etihaddemodevret.cloudax.dynamics.com/Commerce/Products/Search?$top=250&api-version=7.1",
                            productID, CatalogActivity.imei);

            try {

                jsonarray = jsonobject1.getJSONArray("value");

                for (int i = 0; i < jsonarray.length(); i++) {
                    jsonobject = jsonarray.getJSONObject(i);

                    JSONObject insideImage = new JSONObject();
                    insideImage = jsonobject.getJSONObject("Image");
                    JSONArray insideitemimage = new JSONArray();
                    insideitemimage = insideImage.getJSONArray("Items");

                    JSONObject insideImageURL = new JSONObject();
                    for (int j = 0; j < insideitemimage.length(); j++) {

                        insideImageURL = insideitemimage.getJSONObject(i);
                    }

                    suggestedProductName = jsonobject.getString("ProductName");

                    suggestionImageURL = "https://etihaddemodevret.cloudax.dynamics.com/MediaServer/"
                            + insideImageURL.getString("Url");

                }

            } catch (JSONException e) {

                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }

            Log.i("UPDATE", "2");
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            suggestionProductName.setText(suggestedProductName);

            if (isStoragePermissionGranted()) {
                imageLoader
                        .DisplayImage(suggestionImageURL, suggestionProductImage);
            }
            progressSearchAX.setProgress(100);
            progressSearchAX.setVisibility(View.GONE);

            super.onPostExecute(aVoid);
            if (fromBeaconClass) {
                new DownloadJSONforAXaddCartfrombeaconORZbar().execute();
            }
        }
    }

	/*
     * ADD FROM AX wishlist
	 */

    // DownloadJSON AsyncTask
    private class DownloadJSONforAXADDwishlist extends
            AsyncTask<Void, Void, Void> {
        String str4 = null, str3 = null, str5 = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {
            /*
             * System.setProperty("https.proxyHost", "10.243.115.76");
			 * System.setProperty("https.proxyPort", "6050");
			 * System.setProperty("http.proxyHost", "10.243.115.76");
			 * System.setProperty("http.proxyPort", "6050");
			 */

            // Create an array
            arraylist = new ArrayList<HashMap<String, String>>();
            // Retrieve JSON Objects from the given URL address
            jsonobject = JSONfunctions_wish_deleteproduct
                    .getJSONfromURLAXADDwishlist(
                            "https://etihaddemodevret.cloudax.dynamics.com/data/tests",
                            prodid, itemid);

            try {

                System.out.println("@@## REsponse addwishlist obj 1\n"
                        + jsonobject);


                String message = jsonobject.getString("Customer");

                if (!message.isEmpty()) {


                }
            } catch (JSONException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {


            Toast.makeText(ProductDetails_findprod.this,
                    "Request processed successfully\nProduct ",
                    Toast.LENGTH_LONG).show();

        }
    }

	/*
     *
	 * END AX7
	 */

    /*
For permission to access storage
 */
    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG, "Permission is granted");
                return true;
            } else {

                Log.v(TAG, "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG, "Permission is granted");
            return true;
        }


    }

    /*
        Add to cart function Latest
     */

    /*
     * add to cart LATEST
     */
    // DownloadJSON AsyncTask
    public class DownloadJSONforAXaddCartfrombeaconORZbar extends AsyncTask<Void, Void, JSONObject> {
        String str4 = null, str3 = null, str5 = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected JSONObject doInBackground(Void... params) {

            // Create an array
            arraylist = new ArrayList<HashMap<String, String>>();

            jsonobject = JsonFuntionCart
                    .getJSONfromURL(
                            "http://ethcustomervalidtionax.azurewebsites.net/api/cart/CreateCart?idToken=" + AccountState.getTokenID(),
                            distinctProductVariantId, "1");

            System.out.println("@@## ADD CART AX " + jsonobject);

            try {


                String message = jsonobject.getString("Id");

            } catch (JSONException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return jsonobject;
        }

        @Override
        protected void onPostExecute(JSONObject jsonobject) {
            try {
                if (jsonobject.has("Data")) {
                    JSONObject dataObject = jsonobject.getJSONObject("Data");
                    if (dataObject.getBoolean("success")) {
                        Toast.makeText(getApplicationContext(), "Item has been added to your Cart",
                                Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "Unable to update cart, please try again",
                                Toast.LENGTH_LONG).show();
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    /*
        Adding to wishlist Latest
     */
    private class DownloadJSONforAXADDwishlistFromBeaconORZbar extends
            AsyncTask<Void, Void, JSONObject> {
        String str4 = null, str3 = null, str5 = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected JSONObject doInBackground(Void... params) {

            arraylist = new ArrayList<HashMap<String, String>>();


            jsonobject = JSONfunctions_wish_deleteproduct
                    .getJSONfromURLAXADDwishlist(
                            "http://ethcustomervalidtionax.azurewebsites.net/api/Commerce/CreateWishList?idtoken=" + AccountState.getTokenID(),
                            value, "1");

            System.out.println("@@## REsponse addwishlist \n"
                    + jsonobject);

            return jsonobject;
        }

        @Override
        protected void onPostExecute(JSONObject jsonobject) {

            try {
                if (jsonobject.has("Data")) {
                    JSONObject dataObject = jsonobject.getJSONObject("Data");
                    if (dataObject.getBoolean("success")) {
                        Toast.makeText(getApplicationContext(), "Item has been added to your Wishlist",
                                Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "Unable to update Wishlist, please try again",
                                Toast.LENGTH_LONG).show();
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
}
