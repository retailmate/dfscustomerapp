package com.cognizant.iot.activityfindproduct;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.cognizant.iot.activity.CatalogActivity;
import com.cognizant.iot.utils.AccountState;
import com.cognizant.iot.utils.MySSLSocketFactory;

import android.util.Log;

public class JSONFunctionsDetails_findprod {

    public static JSONObject getJSONfromURLAX(String url, String id, String imei) {
        InputStream is = null;
        String result = "";
        JSONObject jArray = null;

        try {
            HttpClient httpclient = MySSLSocketFactory.getNewHttpClient();
            // HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(url);

            System.out.println("@@##macaddr: " + imei + " productid:" + id);

            JSONArray parentData = new JSONArray();


            parentData.put(Double.valueOf(id));


            JSONObject childData = new JSONObject();

            childData.put("Ids", parentData);
            childData.put("DataLevelValue", 4);

            JSONObject finalObject = new JSONObject();
            finalObject.put("productSearchCriteria", childData);

            System.out
                    .println("@@## JSON SEARCH PRODUCT OBJECT " + finalObject);


            StringEntity params = new StringEntity(finalObject.toString());
            System.out.println("@@## params " + finalObject.toString());

            httppost.setEntity(params);

            httppost.setHeader("Content-Type", "application/json");

            httppost.addHeader("Authorization", CatalogActivity.google_token);
            httppost.addHeader(
                    "OUN",
                    "092");
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity httpentity = response.getEntity();
            is = httpentity.getContent();

        } catch (Exception e) {
            Log.e("log_tag", "Error in http connection " + e.toString());
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            result = sb.toString();
            Log.d("log_tag", "product detail result " + result);
        } catch (Exception e) {
            Log.e("log_tag", "Error converting result " + e.toString());
        }

        try {
            jArray = new JSONObject(result);
        } catch (JSONException e) {
            Log.e("log_tag", "Error parsing data " + e.toString());
        }

        return jArray;
    }

    public static JSONObject getJSONfromURL(String url, String id, String imei) {
        InputStream is = null;
        String result = "";
        JSONObject jArray = null;

        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(url);
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("macaddr", imei));
            params.add(new BasicNameValuePair("product", id));

            System.out.println("@@##macaddr: " + imei + " productid:" + id);

            UrlEncodedFormEntity ent = new UrlEncodedFormEntity(params,
                    HTTP.UTF_8);
            httppost.setEntity(ent);
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity httpentity = response.getEntity();
            is = httpentity.getContent();

        } catch (Exception e) {
            Log.e("log_tag", "Error in http connection " + e.toString());
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            result = sb.toString();
        } catch (Exception e) {
            Log.e("log_tag", "Error converting result " + e.toString());
        }

        try {
            jArray = new JSONObject(result);
        } catch (JSONException e) {
            Log.e("log_tag", "Error parsing data " + e.toString());
        }

        return jArray;
    }

    /*
        USed in getting Product details for WISHLIST and CART LATEST AX API
     */
    public static JSONObject getMultipleJSONfromURLAX(String url, String[] id, String imei) {
        InputStream is = null;
        String result = "";
        JSONObject jArray = null;

        try {
            HttpClient httpclient = MySSLSocketFactory.getNewHttpClient();
            // HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(url);

            System.out.println("@@##macaddr: " + imei + " productid:" + id);

            JSONArray parentData = new JSONArray();

            for (int i = 0; i < id.length; i++) {


                parentData.put(Double.valueOf(id[i]));
            }

            JSONObject childData = new JSONObject();

            childData.put("Ids", parentData);
            childData.put("DataLevelValue", 4);

            JSONObject finalObject = new JSONObject();
            finalObject.put("productSearchCriteria", childData);

            System.out
                    .println("@@## JSON SEARCH PRODUCT OBJECT " + finalObject);


            StringEntity params = new StringEntity(finalObject.toString());
            System.out.println("@@## params " + finalObject.toString());

            httppost.setEntity(params);

            httppost.setHeader("Content-Type", "application/json");

            httppost.addHeader("Authorization", "id_token " + AccountState.getTokenID());
            httppost.addHeader(
                    "OUN",
                    "092");
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity httpentity = response.getEntity();
            is = httpentity.getContent();

        } catch (Exception e) {
            Log.e("log_tag", "Error in http connection " + e.toString());
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            result = sb.toString();
            Log.d("log_tag", "product detail result " + result);
        } catch (Exception e) {
            Log.e("log_tag", "Error converting result " + e.toString());
        }

        try {
            jArray = new JSONObject(result);
        } catch (JSONException e) {
            Log.e("log_tag", "Error parsing data " + e.toString());
        }

        return jArray;
    }
}
