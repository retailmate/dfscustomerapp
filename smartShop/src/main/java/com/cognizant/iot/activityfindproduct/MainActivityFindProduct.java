package com.cognizant.iot.activityfindproduct;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Filter;
import android.widget.ImageButton;
import android.widget.Toast;

import com.cognizant.iot.activity.CatalogActivity;
import com.cognizant.retailmate.R;

public class MainActivityFindProduct extends Activity {
	AutoCompleteTextView autoText;
	ArrayAdapter<String> adapter;

	public static String imei;
	private Filter filter;
	final Context context = this;

	JSONObject jsonobject;
	JSONArray jsonarray;

	private static final int ADDRESS_TRESHOLD = 1;

	Intent intentObject;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.product_find);
		autoText = (AutoCompleteTextView) findViewById(R.id.autoComplete_tv);
		TelephonyManager mngr = (TelephonyManager) context
				.getSystemService(context.TELEPHONY_SERVICE);
		imei = mngr.getDeviceId();
		Toast.makeText(getApplicationContext(),
				"IMEI number of thos device is : " + imei, Toast.LENGTH_SHORT)
				.show();

		TextWatcher textChecker = new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence charSequence, int i,
					int i2, int i3) {

			}

			@Override
			public void onTextChanged(CharSequence charSequence, int i, int i2,
					int i3) {
				if (autoText.length() > 1) {
					new AdapterUpdaterTask().execute();
				}
			}

			@Override
			public void afterTextChanged(Editable editable) {
				if (autoText.length() > 1) {
					autoText.showDropDown();
					Log.i("UPDATE", "showDropDown");
				}
			}
		};
		autoText.setThreshold(ADDRESS_TRESHOLD);
		autoText.setHint("Search your Product");

		filter = new Filter() {
			@Override
			protected void publishResults(CharSequence constraint,
					FilterResults results) {

			}

			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				Log.i("Filter",
						"Filter:" + constraint + " thread: "
								+ Thread.currentThread());
				if (constraint != null
						&& constraint.length() > ADDRESS_TRESHOLD) {
					Log.i("Filter", "doing a search ..");
					new AdapterUpdaterTask().execute();
				}
				return null;
			}
		};

		adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_dropdown_item_1line) {
			public android.widget.Filter getFilter() {
				return filter;
			}
		};

		autoText.setAdapter(adapter);
		adapter.setNotifyOnChange(false);

		ImageButton button = (ImageButton) findViewById(R.id.imageButton1);
		button.setOnClickListener(new Button.OnClickListener() {
			public void onClick(View v) {
				String information = autoText.getText().toString();
				String pid = information.substring(information.lastIndexOf("/") + 1);
				Toast.makeText(getApplicationContext(), pid, Toast.LENGTH_SHORT)
						.show();
				intentObject = new Intent(getApplicationContext(),
						ProductDetails_findprod.class);
				intentObject.putExtra("ProductId", pid);
				startActivity(intentObject);
				finish();

			}
		});
	}

	private class AdapterUpdaterTask extends AsyncTask<Void, Void, Void> {
		/*
		 * ProgressDialog mProgressDialog=new ProgressDialog(Sample.this);
		 * 
		 * mProgressDialog.setTitle("Search Results");
		 * 
		 * mProgressDialog.setMessage("Loading...");
		 * 
		 * mProgressDialog.setIndeterminate(false);
		 * 
		 * mProgressDialog.show();
		 */
		List<SuggestGetSet_findprod> ListData = new ArrayList<SuggestGetSet_findprod>();

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

		}

		@Override
		protected Void doInBackground(Void... params) {
			Log.i("UPDATE", "1");

			/*
			 * System.setProperty("https.proxyHost", "10.243.115.76");
			 * System.setProperty("https.proxyPort", "6050");
			 * System.setProperty("http.proxyHost", "10.243.115.76");
			 * System.setProperty("http.proxyPort", "6050");
			 */
			jsonobject = JSONFunctions_findprod.getJSONfromURL(
					CatalogActivity.urlPart + "/api/Product/FindProduct",
					autoText.getText().toString(), imei);

			try {
				jsonarray = jsonobject.getJSONArray("assets");
				for (int i = 0; i < jsonarray.length(); i++) {
					jsonobject = jsonarray.getJSONObject(i);
					ListData.add(new SuggestGetSet_findprod(jsonobject
							.getString("ProductID"), jsonobject
							.getString("name"), jsonobject.getString("purc")));
				}
			} catch (JSONException e) {

				Log.e("Error", e.getMessage());
				e.printStackTrace();
			}
			Log.i("UPDATE", "2");
			return null;
		}

		@Override
		protected void onPostExecute(Void aVoid) {
			// mProgressDialog.dismiss();
			int size = ListData.size();
			if (size > 0) {
				adapter.clear();
				Log.i("ADAPTER_SIZE", "" + size);
				List<String> suggestions = new ArrayList<String>();
				suggestions.clear();
				for (int i = 0; i < ListData.size(); i++) {
					suggestions.add(ListData.get(i).getName() + " / "
							+ ListData.get(i).getId());
				}
				for (int i = 0; i < suggestions.size(); i++) {
					adapter.add(suggestions.get(i));
				}

				Log.i("UPDATE", "4");
				adapter.notifyDataSetChanged();
				autoText.showDropDown();

			} else {
				Toast.makeText(getApplicationContext(),
						"No products found/Search field empty",
						Toast.LENGTH_SHORT).show();
			}
			super.onPostExecute(aVoid);
		}
	}
}