package com.cognizant.iot.activityproductlist;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.cognizant.iot.activity.CatalogActivity;
import com.cognizant.iot.adapter.ListViewAdapter_prod_full;
import com.cognizant.retailmate.R;

public class MainActivityprod extends AppCompatActivity {
    // Declare Variables
    JSONObject jsonobject, jsonobject1;
    JSONArray jsonarray;
    ListView listview;
    ListViewAdapter_prod_full adapter;
    ProgressDialog mProgressDialog;
    ArrayList<HashMap<String, String>> arraylist;
    public static String PRODID = "prodid";
    public static String PRODNAME = "prodname";
    public static String PRODPRICE = "prodprice";
    public static String FLAG = "flag";
    public static String WISH_PRSNT = "wish_prsnt";
    public static String CATEGORY = "category";
    public static String UNIT = "unit";
    public static String OFFER = "offers";
    public static String DESC = "desc";
    public static String LASTDATE = "lastdate";
    public static String imei = "";
    // EditText editTextObject;
    EditText productSearch;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Get the view from listview_main.xml
        setContentView(R.layout.listview_mainprod);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarFullProductlist);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        imei = CatalogActivity.imei;

        // Execute DownloadJSON AsyncTask
        initialize();
        new DownloadJSONAX().execute();

    }

    private void initialize() {
        listview = (ListView) findViewById(R.id.listview);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:

                break;
        }

        return true;
    }

    // DownloadJSON AsyncTask
    private class DownloadJSON extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Create a progressdialog
            mProgressDialog = new ProgressDialog(MainActivityprod.this);
            // Set progressdialog title
            mProgressDialog.setTitle("Product Catalog");
            // Set progressdialog message
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            // Show progressdialog
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            // Create an array
            arraylist = new ArrayList<HashMap<String, String>>();
            // Retrieve JSON Objects from the given URL address
            jsonobject1 = JSONfunctionsprod
                    .getJSONfromURL(CatalogActivity.urlPart
                            + "/api/Product/GetMoreProducts");

            try {
                jsonobject = new JSONObject(
                        jsonobject1.getString("ProductResponse"));
                jsonarray = jsonobject.getJSONArray("assets");
                for (int i = 0; i < jsonarray.length(); i++) {
                    HashMap<String, String> map = new HashMap<String, String>();
                    jsonobject = jsonarray.getJSONObject(i);
                    map.put("prodid", jsonobject.getString("ProductID"));
                    map.put("prodname", jsonobject.getString("name"));
                    // map.put("prodprice", "$" + jsonobject.getString("Cost"));
                    map.put("prodprice", "$" + jsonobject.getString("Cost"));
                    map.put("flag", jsonobject.getString("Image"));
                    map.put("wish_prsnt", jsonobject.getString("Wishlist"));
                    map.put("category", jsonobject.getString("Category"));
                    map.put("unit", jsonobject.getString("Unit"));
                    map.put("desc", jsonobject.getString("purc"));
                    map.put("offers", jsonobject.getString("Offer"));
                    map.put("lastdate", jsonobject.getString("Lastdate"));
                    arraylist.add(map);
                }
            } catch (JSONException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {

            // Locate the listview in listview_main.xml
            // listview = (ListView) findViewById(R.id.listview);
            // Pass the results into ListViewAdapter.java
            // Toast.makeText(getApplicationContext(),
            // jsonobject1.toString(),Toast.LENGTH_LONG).show();
            adapter = new ListViewAdapter_prod_full(MainActivityprod.this,
                    arraylist);
            // Set the adapter to the ListView
            listview.setAdapter(adapter);
            // Close the progressdialog
            mProgressDialog.dismiss();
        }
    }

    /*
     * AX product list
     */
    // DownloadJSON AsyncTask
    private class DownloadJSONAX extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Create a progressdialog
            mProgressDialog = new ProgressDialog(MainActivityprod.this);
            // Set progressdialog title
            mProgressDialog.setTitle("Product Catalog");
            // Set progressdialog message
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            // Show progressdialog
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            // Create an array
            arraylist = new ArrayList<HashMap<String, String>>();
            // Retrieve JSON Objects from the given URL address
            // jsonobject1 = JSONfunctionsprod
            // .getJSONfromURL(CatalogActivity.urlPart
            // + "/api/Product/GetMoreProducts");
//            jsonobject1 = JSONfunctionsprod
//                    .getJSONfromAXURLGETProducts("https://etihaddemodevret.cloudax.dynamics.com/Commerce/Products/SearchByText(channelId=0,catalogId=0,searchText='digital,jeans')?$top=20&api-version=7.1");
            jsonobject1 = JSONfunctionsprod
                    .getJSONfromAXURLGETProducts("https://etihaddemodevret.cloudax.dynamics.com/Commerce/Products/SearchByCategory(channelId=68719476778,catalogId=0,categoryId=68719477291)?$top=60&api-version=7.1");
            System.out.println("@@## FULL PRODUCT LIST " + jsonobject1);

            try {
                // jsonobject = new JSONObject(
                // jsonobject1.getString("ProductResponse"));
                jsonarray = jsonobject1.getJSONArray("value");
                for (int i = 0; i < jsonarray.length(); i++) {
                    HashMap<String, String> map = new HashMap<String, String>();
                    jsonobject = jsonarray.getJSONObject(i);
                    map.put("prodid", jsonobject.getString("RecordId"));
                    map.put("itemid", jsonobject.getString("ItemId"));
                    map.put("prodname", jsonobject.getString("Name"));
                    // map.put("prodprice", "$" + jsonobject.getString("Cost"));
                    map.put("prodprice", "$" + jsonobject.getString("Price"));
                    map.put("flag",
                            "https://etihaddemodevret.cloudax.dynamics.com/MediaServer/"
                                    + jsonobject.getString("PrimaryImageUrl"));
                    map.put("wish_prsnt", "0");
                    map.put("category", "Electronics");
                    map.put("unit", "Each");
                    // map.put("desc", jsonobject.getString("purc"));
                    map.put("offers", "");
                    // map.put("lastdate", jsonobject.getString("Lastdate"));
                    arraylist.add(map);
                }
            } catch (JSONException | NullPointerException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {


            adapter = new ListViewAdapter_prod_full(MainActivityprod.this,
                    arraylist);
            // Set the adapter to the ListView
            listview.setAdapter(adapter);
            // Close the progressdialog
            mProgressDialog.dismiss();
        }
    }
}