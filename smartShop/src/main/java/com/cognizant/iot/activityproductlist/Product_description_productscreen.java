package com.cognizant.iot.activityproductlist;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.cognizant.iot.activity.CatalogActivity;
import com.cognizant.iot.activityfindproduct.JSONFunctionsDetails_findprod;
import com.cognizant.iot.activityfindproduct.ProductData_findprod;
import com.cognizant.iot.activitywishlist.JSONfunctions_wish_deleteproduct;
import com.cognizant.iot.json.AssetsExtracter;
import com.cognizant.iot.mycart.JsonFuntionCart;
import com.cognizant.iot.receiver.ConnectivityReceiver;
import com.cognizant.iot.utils.AccountState;
import com.cognizant.iot.utils.MySSLSocketFactory;
import com.cognizant.retailmate.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Product_description_productscreen extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {

    private static final String TAG = "@@##";
    TextView et, st;
    int min = 0, smin = 0;
    int max = 10, smax = 10;
    String res1, res2;
    int Qval, Sval;
    TextView pdt_details;
    ImageView cart;
    Integer valqty, valQ, valsize, valS;
    ImageButton qinc;
    ImageButton qdec;
    ImageButton sinc;
    ImageButton sdec;
    TextView offernew;

    String prodid;
    String prodname;
    String prodprice;
    String flag;
    String beacon;
    String desc;
    String position, category;
    String wish_present, offer, unit;

    AssetsExtracter mTask;

    ProgressDialog mProgressDialog;
    JSONObject jsonobject;
    JSONArray jsonarray;
    ArrayList<HashMap<String, String>> arraylist;

    /*
     * Bitmap color change
     */
    ImageView image11;
    ImageView image12, image13, image14, image15;

    /*
     * (non-Javadoc)
     *
     * @see android.app.Activity#onCreate(android.os.Bundle)
     */
    /*
     * NEW FOR AX
	 */
    TextView description_text_view;
    TextView category_text_view;

    TextView suggestionProductName;
    ImageView suggestionProductImage;

    ImageLoaderprod imageLoader;

    ProgressBar progressSearchAX;

    String distinctProductVariantId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_descriptionnew);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarproductscreen);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        Intent i = getIntent();
        // Get the result of prodid
        prodid = i.getStringExtra("prodid");
        // Get the result of prodname
        prodname = i.getStringExtra("prodname");
        // Get the result of prodprice
        prodprice = i.getStringExtra("prodprice");
        // Get the result of flag
        flag = i.getStringExtra("flag");
        desc = i.getStringExtra("desc");

        System.out.println("@@## DESCRIPTION DETAIL PAGE" + desc);
        beacon = i.getStringExtra("beacon");

        category = i.getStringExtra("category");


        TextView prodname_tv = (TextView) findViewById(R.id.pdt_name);
        TextView pdt_cost = (TextView) findViewById(R.id.pdt_cost);
        TextView offer_details_tv = (TextView) findViewById(R.id.offer_details_tv);
        TextView pdtID_text_view = (TextView) findViewById(R.id.pdtID_text_view);
        category_text_view = (TextView) findViewById(R.id.category_text_view);
        description_text_view = (TextView) findViewById(R.id.description_text_view);
        ImageView product_image1 = (ImageView) findViewById(R.id.product_image1);
        final ImageButton wish_btn = (ImageButton) findViewById(R.id.wishlist);
        ImageView offer_tag = (ImageView) findViewById(R.id.offer_details);
        TextView unit_text_view = (TextView) findViewById(R.id.unit);
        cart = (ImageView) findViewById(R.id.shop_cart2);
        suggestionProductName = (TextView) findViewById(R.id.suggstionProductName);
        suggestionProductImage = (ImageView) findViewById(R.id.suggestionProductImage);
        progressSearchAX = (ProgressBar) findViewById(R.id.progressSearchAX);

        if (checkConnection()) {
            imageLoader = new ImageLoaderprod(
                    Product_description_productscreen.this);
        }
        prodname_tv.setText(prodname);
        pdt_cost.setText(prodprice);
        offer_details_tv.setText(offer);
        pdtID_text_view.setText(prodid);
        category_text_view.setText(category);
        description_text_view.setText(desc);
        unit_text_view.setText(unit);

		/*
         * bitmap color change
		 */
        image11 = (ImageView) findViewById(R.id.image1);

        image12 = (ImageView) findViewById(R.id.image2);
        image13 = (ImageView) findViewById(R.id.image3);
        image14 = (ImageView) findViewById(R.id.image4);
        image15 = (ImageView) findViewById(R.id.image5);
        /*
         * END
		 */

        if (checkConnection() && !flag.isEmpty()) {
            new LoadImage().execute(flag);
        }
        offernew = (TextView) findViewById(R.id.view_offer_tv);
        offernew.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {

                // Create custom dialog object
                final Dialog dialog = new Dialog(
                        Product_description_productscreen.this);
                // hide to default title for Dialog

                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                // inflate the layout dialog_layout.xml and set it as
                // contentView
                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view = inflater.inflate(R.layout.dilogbox, null, false);
                dialog.setCanceledOnTouchOutside(false);
                dialog.setContentView(view);


                Button btninstore = (Button) dialog
                        .findViewById(R.id.check_instore_offer);
                btninstore.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        if (checkConnection()) {
                            mTask = new AssetsExtracter("empty",
                                    Product_description_productscreen.this);
                            mTask.execute();

                        }
                        // Dismiss the dialog
                        dialog.dismiss();
                    }
                });
                Button btndismiss = (Button) dialog.findViewById(R.id.dismiss);
                btndismiss.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        // Open the browser
                        // Intent In = new Intent(MainActivity.this,
                        // Availoffer.class);
                        // startActivity(In);
                        // Dismiss the dialog
                        dialog.dismiss();
                    }
                });


                // Display the dialog
                dialog.show();
            }
        });

        if (isStoragePermissionGranted()) {
            ImageLoaderprod imageLoader = new ImageLoaderprod(this);
            imageLoader.DisplayImage(flag, product_image1);
        }


        pdt_details = (TextView) findViewById(R.id.heading_pdt_name);
        String udata = "PRODUCT DETAILS";
        SpannableString content = new SpannableString(udata);
        content.setSpan(new UnderlineSpan(), 0, udata.length(), 0);
        pdt_details.setText(content);

        et = (TextView) findViewById(R.id.qtynumber);
        res1 = et.getText().toString();
        valqty = Integer.parseInt(res1);
        Qval = valqty.intValue();


        st = (TextView) findViewById(R.id.pdtsize);
        res2 = st.getText().toString();
        valsize = Integer.parseInt(res2);
        Sval = valsize.intValue();
        // Toast.makeText(getApplicationContext(), "size "+Sval, 1000).show();

        qinc = (ImageButton) findViewById(R.id.bqty_inc);
        qinc.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {

                if (Qval <= max - 1) {
                    Qval = Qval + 1;
                    valQ = new Integer(Qval);
                    // Toast.makeText(getApplicationContext(),
                    // "after converting to string inc: "+valQ.toString(),
                    // 1000).show();
                    et.setText(valQ.toString());
                }
            }
        });

        qdec = (ImageButton) findViewById(R.id.bqty_dec);
        // decrement quantity
        qdec.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (Qval >= min + 1) {
                    Qval = Qval - 1;
                    valQ = new Integer(Qval);
                    // Toast.makeText(getApplicationContext(),
                    // "after converting to string dec: "+valQ.toString(),
                    // 1000).show();
                    et.setText(valQ.toString());
                }
            }
        });

        sinc = (ImageButton) findViewById(R.id.size_inc);
        // increment size
        sinc.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (Sval <= smax - 1) {
                    Sval = Sval + 1;
                    valS = new Integer(Sval);
                    // Toast.makeText(getApplicationContext(),
                    // "after converting to string: "+aryaM.toString(),
                    // 1000).show();
                    st.setText(valS.toString());
                }
            }
        });

        sdec = (ImageButton) findViewById(R.id.size_dec);
        // decrement size
        sdec.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (Sval >= smin + 1) {
                    Sval = Sval - 1;
                    valS = new Integer(Sval);
                    // Toast.makeText(getApplicationContext(),
                    // "after converting to string: "+aryaM.toString(),
                    // 1000).show();
                    st.setText(valS.toString());
                }
            }
        });

        cart.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Log.d("@@##", "cart initiated");
                if (R.drawable.cart_red_icon == cart.getId()) {
                    cart.setImageResource(R.drawable.no_crl_cart_blu_icon);
                } else {
                    cart.setImageResource(R.drawable.cart_red_icon);
                }
                if (checkConnection()) {
                    new DownloadJSONforAXaddCartfromProductlistORRecommended().execute();
                }
            }
        });

        wish_btn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if (R.drawable.hearticonnn == wish_btn.getId()) {
                    wish_btn.setImageResource(R.drawable.no_crl_mywish_blu_icon);
                } else {
                    wish_btn.setImageResource(R.drawable.hearticonnn);
                }
//                RequestParams params = new RequestParams();
//
//                params.put("Quantity", "1");
//                params.put("ProductId", prodid);
//                addtowishlistService(params);

                if (checkConnection()) {
                    new DownloadJSONforAXADDwishlistFromProductORRecommended().execute();
                }

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // setMenuBackground();

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar app icon/title

        // Handle action bar actions click
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public ArrayList<Bitmap> LIST1 = new ArrayList<Bitmap>();

    private class LoadImage extends
            AsyncTask<String, String, ArrayList<Bitmap>> {

        Bitmap bitmap1, bitmap11;
        Matrix matrix1, matrix2, matrix3, matrix4, matrix5;
        Bitmap rotated1;

        ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Product_description_productscreen.this);
            pDialog.setMessage("Loading...");
            pDialog.show();
            progressSearchAX.setProgress(50);

        }

        protected ArrayList<Bitmap> doInBackground(String... args) {

            try {
                MySSLSocketFactory.doTrustToCertificates();

                bitmap1 = BitmapFactory.decodeStream((InputStream) new URL(
                        args[0]).getContent());

                int width, height;
                height = bitmap1.getHeight();
                width = bitmap1.getWidth();

                final int REQUIRED_SIZE = 170;
                int width_tmp = width, height_tmp = height;

                while (true) {
                    if (width_tmp / 2 < REQUIRED_SIZE
                            || height_tmp / 2 < REQUIRED_SIZE)
                        break;
                    width_tmp /= 2;
                    height_tmp /= 2;

                }

                bitmap11 = Bitmap.createScaledBitmap(bitmap1, width_tmp,
                        height_tmp, true);

                matrix1 = new Matrix();
                matrix1.setRotate(30);
                rotated1 = Bitmap.createBitmap(bitmap11, 0, 0,
                        bitmap11.getWidth(), bitmap11.getHeight(), matrix1,
                        true);
                LIST1.add(rotated1);

                matrix2 = new Matrix();
                matrix2.setRotate(60);
                rotated1 = Bitmap.createBitmap(bitmap11, 0, 0,
                        bitmap11.getWidth(), bitmap11.getHeight(), matrix2,
                        true);
                LIST1.add(rotated1);

                matrix3 = new Matrix();
                matrix3.setRotate(90);
                rotated1 = Bitmap.createBitmap(bitmap11, 0, 0,
                        bitmap11.getWidth(), bitmap11.getHeight(), matrix3,
                        true);
                LIST1.add(rotated1);

                matrix4 = new Matrix();
                matrix4.setRotate(-30);
                rotated1 = Bitmap.createBitmap(bitmap11, 0, 0,
                        bitmap11.getWidth(), bitmap11.getHeight(), matrix4,
                        true);
                LIST1.add(rotated1);

                matrix5 = new Matrix();
                matrix5.setRotate(-60);
                rotated1 = Bitmap.createBitmap(bitmap11, 0, 0,
                        bitmap11.getWidth(), bitmap11.getHeight(), matrix5,
                        true);
                LIST1.add(rotated1);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return LIST1;
        }

        protected void onPostExecute(ArrayList<Bitmap> image) {

            pDialog.dismiss();

            try {
                image11.setBackgroundColor(Color.rgb(255, 255, 255));
                image11.setImageBitmap(image.get(0));

                image12.setBackgroundColor(Color.rgb(255, 255, 255));
                image12.setImageBitmap(image.get(1));

                image13.setBackgroundColor(Color.rgb(255, 255, 255));
                image13.setImageBitmap(image.get(2));

                image14.setBackgroundColor(Color.rgb(255, 255, 255));
                image14.setImageBitmap(image.get(3));

                image15.setBackgroundColor(Color.rgb(255, 255, 255));
                image15.setImageBitmap(image.get(4));
            } catch (NullPointerException | IndexOutOfBoundsException e) {

            }


            LIST1.clear();

            new AdapterDetailsTaskAX7().execute();

        }
    }

	/*
     * END
	 */


	/*
     * NEW FOR AX7
	 */
    /*
     * new for ax7
	 */

    private class AdapterDetailsTaskAX7 extends AsyncTask<Void, Void, Void> {

        List<ProductData_findprod> ListData = new ArrayList<ProductData_findprod>();
        String category, description, suggestionID, relationName;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.i("UPDATE", "1");

            System.out.println("@@#$$PRODUCT ID AdapterDetailsTaskAX7:"
                    + prodid);

            JSONObject jsonobject1 = JSONFunctionsDetails_findprod
                    .getJSONfromURLAX(
                            "https://etihaddemodevret.cloudax.dynamics.com/Commerce/Products/Search?$top=250&api-version=7.1",
                            prodid, CatalogActivity.imei);


            System.out.println("@@##JSON AX SEARCH RESULT OBJECT: "
                    + jsonobject1);

            try { // jsonobject = new
                // JSONObject(jsonobject1.getString("assets")) ;

				/*
                 * newly added for allergy
				 */

                jsonarray = jsonobject1.getJSONArray("value");

                for (int i = 0; i < jsonarray.length(); i++) {
                    jsonobject = jsonarray.getJSONObject(i);


                    // FOR GETTING THE VARIANT ID
                    try {
                        JSONObject compositionInfo = new JSONObject();

                        compositionInfo = jsonobject.getJSONObject("CompositionInformation");
                        JSONObject variantInfo = new JSONObject();

                        variantInfo = compositionInfo.getJSONObject("VariantInformation");

                        JSONArray variants = new JSONArray();
                        variants = variantInfo.getJSONArray("Variants");

                        JSONObject insideVariantObj = new JSONObject();


                        // Loop kept just to iterate once as we are picking up the first variant ID
                        for (int jk = 0; jk < 1; jk++) {

                            insideVariantObj = variants
                                    .getJSONObject(jk);

                            distinctProductVariantId = insideVariantObj
                                    .getString("DistinctProductVariantId");

                        }
                    } catch (NullPointerException | org.json.JSONException e) {
                        distinctProductVariantId = jsonobject
                                .getString("RecordId");
                    }


                    JSONArray insideitemimage = new JSONArray();
                    insideitemimage = jsonobject
                            .getJSONArray("RelatedProducts");


                    JSONObject insideImageURL = new JSONObject();
                    for (int j = 0; j < insideitemimage.length(); j++) {

                        insideImageURL = insideitemimage.getJSONObject(i);
                    }

                    category = "Electronics";
                    description = jsonobject.getString("Description");

                    suggestionID = insideImageURL
                            .getString("RelatedProductRecordId");

                    relationName = insideImageURL.getString("RelationName");

                }

            } catch (JSONException e) {

                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }

            Log.i("UPDATE", "2");
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            category_text_view.setText(category);
            description_text_view.setText(description);
            // suggestionProductName.setText(relationName);
            new AdapterSuggestedProductTaskAX7(suggestionID).execute();
            super.onPostExecute(aVoid);

        }
    }

	/*
     *
	 * END AX7
	 */

	/*
     * NEW FOR AX7
	 */
    /*
     * GETTING SUGGESTED PRODUCT
	 */

    private class AdapterSuggestedProductTaskAX7 extends
            AsyncTask<Void, Void, Void> {

        String productID, suggestedProductName, suggestionImageURL;

        public AdapterSuggestedProductTaskAX7(String suggestionID) {
            // TODO Auto-generated constructor stub
            productID = suggestionID;
            System.out.println("@@## RECEIVED SUGGESTED ID " + productID);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        public void execute(String suggestionID) {
            // TODO Auto-generated method stub

        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.i("UPDATE", "1");

            System.out.println("@@#$$PRODUCT ID AdapterDetailsTaskAX7:"
                    + productID);

            JSONObject jsonobject1 = JSONFunctionsDetails_findprod
                    .getJSONfromURLAX(
                            "https://etihaddemodevret.cloudax.dynamics.com/Commerce/Products/Search?$top=250&api-version=7.1",
                            productID, CatalogActivity.imei);

            try {

                jsonarray = jsonobject1.getJSONArray("value");

                for (int i = 0; i < jsonarray.length(); i++) {
                    jsonobject = jsonarray.getJSONObject(i);

                    JSONObject insideImage = new JSONObject();
                    insideImage = jsonobject.getJSONObject("Image");
                    JSONArray insideitemimage = new JSONArray();
                    insideitemimage = insideImage.getJSONArray("Items");

                    JSONObject insideImageURL = new JSONObject();
                    for (int j = 0; j < insideitemimage.length(); j++) {

                        insideImageURL = insideitemimage.getJSONObject(i);
                    }

                    suggestedProductName = jsonobject.getString("ProductName");

                    suggestionImageURL = "https://etihaddemodevret.cloudax.dynamics.com/MediaServer/"
                            + insideImageURL.getString("Url");

                }

            } catch (NullPointerException | JSONException e) {

                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }

            Log.i("UPDATE", "2");
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            suggestionProductName.setText(suggestedProductName);

            imageLoader
                    .DisplayImage(suggestionImageURL, suggestionProductImage);

            progressSearchAX.setProgress(100);
            progressSearchAX.setVisibility(View.GONE);

            super.onPostExecute(aVoid);

        }
    }


	/*
     *
	 * END AX7
	 */

    /*
For permission to access storage
 */
    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG, "Permission is granted");
                return true;
            } else {

                Log.v(TAG, "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG, "Permission is granted");
            return true;
        }


    }

    /*
     * add to cart LATEST
     */
    // DownloadJSON AsyncTask
    public class DownloadJSONforAXaddCartfromProductlistORRecommended extends AsyncTask<Void, Void, JSONObject> {
        String str4 = null, str3 = null, str5 = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected JSONObject doInBackground(Void... params) {

            // Create an array
            arraylist = new ArrayList<HashMap<String, String>>();

            jsonobject = JsonFuntionCart
                    .getJSONfromURL(
                            "http://ethcustomervalidtionax.azurewebsites.net/api/cart/CreateCart?idToken=" + AccountState.getTokenID(),
                            distinctProductVariantId, "1");

            System.out.println("@@## ADD CART AX " + jsonobject);

            try {


                String message = jsonobject.getString("Id");

            } catch (JSONException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return jsonobject;
        }

        @Override
        protected void onPostExecute(JSONObject jsonobject) {

            try {
                if (jsonobject.has("Data")) {
                    JSONObject dataObject = jsonobject.getJSONObject("Data");
                    if (dataObject.getBoolean("success")) {
                        Toast.makeText(getApplicationContext(), "Item has been added to your Cart",
                                Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "Unable to update cart, please try again",
                                Toast.LENGTH_LONG).show();
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
    }


    /*
        New Wishlist add latest
     */
     /*
        Adding to wishlist Latest
     */
    private class DownloadJSONforAXADDwishlistFromProductORRecommended extends
            AsyncTask<Void, Void, JSONObject> {
        String str4 = null, str3 = null, str5 = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected JSONObject doInBackground(Void... params) {

            arraylist = new ArrayList<HashMap<String, String>>();

            System.out.println("@@## Product ID for wishlist in productandREcommend DETAIL " + prodid);
            jsonobject = JSONfunctions_wish_deleteproduct
                    .getJSONfromURLAXADDwishlist(
                            "http://ethcustomervalidtionax.azurewebsites.net/api/Commerce/CreateWishList?idtoken=" + AccountState.getTokenID(),
                            prodid, "1");

            System.out.println("@@## REsponse addwishlist \n"
                    + jsonobject);


            return jsonobject;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {

            try {
                if (jsonobject.has("Data")) {
                    JSONObject dataObject = jsonobject.getJSONObject("Data");
                    if (dataObject.getBoolean("success")) {
                        Toast.makeText(getApplicationContext(), "Item has been added to your Wishlist",
                                Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "Unable to update Wishlist, please try again",
                                Toast.LENGTH_LONG).show();
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        makeToast(isConnected);
    }


    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        makeToast(isConnected);

        return isConnected;
    }

    // Showing the status in Toast
    private void makeToast(boolean isConnected) {
        String message = "";
        int color;
        if (isConnected) {

        } else {
            message = "Sorry! Not connected to internet";
            color = Color.RED;
            Toast.makeText(Product_description_productscreen.this, message, Toast.LENGTH_SHORT).show();
        }
    }

}
