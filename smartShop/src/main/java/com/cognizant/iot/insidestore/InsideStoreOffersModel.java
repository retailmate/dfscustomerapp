package com.cognizant.iot.insidestore;

/**
 * Created by 452781 on 12/6/2016.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InsideStoreOffersModel {

    @SerializedName("PROMO_ID")
    @Expose
    private String pROMOID;
    @SerializedName("PROMO_DESC")
    @Expose
    private String pROMODESC;
    @SerializedName("LEVEL")
    @Expose
    private String lEVEL;

    /**
     * @return The pROMOID
     */
    public String getPROMOID() {
        return pROMOID;
    }

    /**
     * @param pROMOID The PROMO_ID
     */
    public void setPROMOID(String pROMOID) {
        this.pROMOID = pROMOID;
    }

    /**
     * @return The pROMODESC
     */
    public String getPROMODESC() {
        return pROMODESC;
    }

    /**
     * @param pROMODESC The PROMO_DESC
     */
    public void setPROMODESC(String pROMODESC) {
        this.pROMODESC = pROMODESC;
    }

    /**
     * @return The lEVEL
     */
    public String getLEVEL() {
        return lEVEL;
    }

    /**
     * @param lEVEL The LEVEL
     */
    public void setLEVEL(String lEVEL) {
        this.lEVEL = lEVEL;
    }


}
