package com.cognizant.iot.insidestore;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.cognizant.iot.utils.AccountState;
import com.cognizant.retailmate.R;
import com.google.gson.Gson;

import java.net.HttpURLConnection;

/**
 * Created by 452781 on 12/6/2016.
 */
public class AssociateCall extends AppCompatActivity {

    Gson gson;

    TextView assoName, assoID, assoCallTag;
    RelativeLayout associateDetails, status_bar_layout;

    FloatingActionButton fab_p1us, fab_call, fab_chat, fab_location;
    Animation FabOpen, FabClose, FabClockwise, FabAntiClockwise;

    String whichClass;
    boolean isOpen = false;
    boolean location = true;

    Intent callIntent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.insidestore_associatecallscreen);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        gson = new Gson();


        whichClass = getIntent().getStringExtra("whichClass");
        assoName = (TextView) findViewById(R.id.associate_name);
        assoID = (TextView) findViewById(R.id.associate_id);
        assoCallTag = (TextView) findViewById(R.id.associate_assignment_tag);
        associateDetails = (RelativeLayout) findViewById(R.id.associateDetails);
        status_bar_layout = (RelativeLayout) findViewById(R.id.status_bar_layout);

        fab_p1us = (FloatingActionButton) findViewById(R.id.fab_plus);
        fab_chat = (FloatingActionButton) findViewById(R.id.fab_chat);
        fab_call = (FloatingActionButton) findViewById(R.id.fab_call);
        fab_location = (FloatingActionButton) findViewById(R.id.fab_location);
        FabOpen = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_open);
        FabClose = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_close);
        FabClockwise = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_clockwise);
        FabAntiClockwise = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_anticlockwise);

        associateDetails.setVisibility(View.INVISIBLE);
        if (AccountState.getOfflineMode()) {

            assoName.setText("Katie");
            assoID.setText("E002");

            assoCallTag.setVisibility(View.GONE);
            associateDetails.setVisibility(View.VISIBLE);

        } else {
            associateCalltoOneRetail();
        }

        if (whichClass.equals("order")) {
            status_bar_layout.setVisibility(View.VISIBLE);

        } else {
            status_bar_layout.setVisibility(View.INVISIBLE);
        }


        fab_p1us.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isOpen) {
                    fab_call.startAnimation(FabClose);
                    fab_chat.startAnimation(FabClose);
                    fab_location.startAnimation(FabClose);
                    fab_p1us.startAnimation(FabAntiClockwise);
                    fab_call.setClickable(false);
                    fab_chat.setClickable(false);
                    fab_location.setClickable(false);
                    isOpen = false;
                } else {
                    fab_call.startAnimation(FabOpen);
                    fab_chat.startAnimation(FabOpen);
                    fab_location.startAnimation(FabOpen);
                    fab_p1us.startAnimation(FabClockwise);
                    fab_call.setClickable(true);
                    fab_chat.setClickable(true);
                    fab_location.setClickable(true);
                    isOpen = true;
                }
            }

        });


        fab_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:0377778888"));

                if (ActivityCompat.checkSelfPermission(AssociateCall.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(AssociateCall.this,
                            Manifest.permission.CALL_PHONE)) {
                    } else {
                        ActivityCompat.requestPermissions(AssociateCall.this,
                                new String[]{Manifest.permission.CALL_PHONE},
                                200);
                    }
                } else {
                    startActivity(callIntent);
                }
            }

        });

        if (location && isOpen) {
            fab_location.setVisibility(View.VISIBLE);
        } else {
            fab_location.setVisibility((View.INVISIBLE));
        }


        fab_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(AssociateCall.this, "location", Toast.LENGTH_SHORT).show();
            }

        });
        fab_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(AssociateCall.this, "chat here", Toast.LENGTH_SHORT).show();
            }

        });
    }


    public void associateCalltoOneRetail() {
        {


//            String url = "http://104.43.192.216:8180/api/Assistance?CID=%271%27&DID=%27D001%27&BID=%27B002%27";


//            String url = "http://oneretailsql.southcentralus.cloudapp.azure.com/api/SalesAssociate?msgId=1&grpId=1";

            String url = "http://oneretail.southcentralus.cloudapp.azure.com:8084/api/Assistance?CID=%271%27&DID=%27D001%27&BID=%27B002%27";

            StringRequest req = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    AssociateDetailsModel[] associateDetailsModel = gson.fromJson(response, AssociateDetailsModel[].class);

                    assoName.setText(associateDetailsModel[0].getAssociateName());
                    assoID.setText(associateDetailsModel[0].getAssociateId());

                    assoCallTag.setVisibility(View.GONE);
                    associateDetails.setVisibility(View.VISIBLE);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }) {


                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    int mStatusCode = response.statusCode;

                    Log.d("@@##", "Response Back " + response.statusCode);
                    if (mStatusCode == HttpURLConnection.HTTP_OK) {

                    } else {

                    }
                    return super.parseNetworkResponse(response);
                }

            };

            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(req);

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.aboutusmenu, menu);
        // setMenuBackground();

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar app icon/title

        // Handle action bar actions click
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 200: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    startActivity(callIntent);

                }
                return;
            }

        }
    }

}
