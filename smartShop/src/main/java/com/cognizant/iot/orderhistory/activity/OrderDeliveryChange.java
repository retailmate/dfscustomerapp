package com.cognizant.iot.orderhistory.activity;

import android.content.Context;
import android.os.AsyncTask;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.cognizant.iot.activity.CatalogActivity;
import com.cognizant.iot.activityfindproduct.JSONFunctionsDetails_findprod;
import com.cognizant.iot.activityproductlist.ImageLoaderprod;
import com.cognizant.iot.utils.AccountState;
import com.cognizant.retailmate.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by 452781 on 1/17/2017.
 */
public class OrderDeliveryChange {

    Context context;


    ImageView btnDismiss, okButon;


    String name;
    String deliveryMode;

    JSONObject jsonobject;
    JSONArray jsonarray;

    ImageLoaderprod imageLoader;

    RadioGroup radioGroupdeliveryMode;
    RadioButton radioDlvyMode;

    public OrderDeliveryChange(String deliveryMode, Context context) {

        this.context = context;
        this.deliveryMode = deliveryMode;
        imageLoader = new ImageLoaderprod(context);
        if (AccountState.getOfflineMode()) {

        } else {
//            new UpdateDeliveryMode(deliveryMode).execute();
        }
    }


    public void getPopUp(View parent, final Context context) {

        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.80);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View popUpView = inflater.inflate(R.layout.ordermodechange, null, false);
        final PopupWindow popup = new PopupWindow(popUpView, screenWidth,
                WindowManager.LayoutParams.WRAP_CONTENT, true);
        popup.setContentView(popUpView);
        btnDismiss = (ImageView) popUpView.
                findViewById(R.id.cancelButton);


        okButon = (ImageView) popUpView.findViewById(R.id.okButon);
        radioGroupdeliveryMode = (RadioGroup) popUpView.findViewById(R.id.radioGroupdeliveryMode);


        popup.showAtLocation(parent, Gravity.CENTER_HORIZONTAL, 10, 10);

        if (AccountState.getOfflineMode()) {
            getRecommdedproductDetailsOffline();
        }


        okButon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int selectedId = radioGroupdeliveryMode.getCheckedRadioButtonId();

                radioDlvyMode = (RadioButton) popUpView.findViewById(selectedId);

                if (radioDlvyMode.getText().equals("Store Pickup")) {
                    Order_Products.dlvType = "bopis";
                } else if (radioDlvyMode.getText().equals("Locker Pickup")) {
                    Order_Products.dlvType = "locker";

                } else {
                    Order_Products.dlvType = "delivery";
                }

                Toast.makeText(context, "Delivery mode updated to " + radioDlvyMode.getText(), Toast.LENGTH_SHORT).show();
                popup.dismiss();

            }
        });
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                popup.dismiss();
            }
        });

    }

    private class UpdateDeliveryMode extends
            AsyncTask<Void, Void, Void> {

        String productID, suggestedProductName, suggestionImageURL;

        public UpdateDeliveryMode(String suggestionID) {
            // TODO Auto-generated constructor stub
            productID = suggestionID;
            System.out.println("@@## RECEIVED SUGGESTED ID " + productID);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        public void execute(String suggestionID) {
            // TODO Auto-generated method stub

        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.i("UPDATE", "1");

            System.out.println("@@#$$PRODUCT ID OneretailRecommended product call:"
                    + productID);

            JSONObject jsonobject1 = JSONFunctionsDetails_findprod
                    .getJSONfromURLAX(
                            "https://etihaddemodevret.cloudax.dynamics.com/Commerce/Products/Search?$top=250&api-version=7.1",
                            productID, CatalogActivity.imei);

            try {

                jsonarray = jsonobject1.getJSONArray("value");

                for (int i = 0; i < jsonarray.length(); i++) {
                    jsonobject = jsonarray.getJSONObject(i);

                    JSONObject insideImage = new JSONObject();
                    insideImage = jsonobject.getJSONObject("Image");
                    System.out.println("@@## Oneretail Recommended product image:"
                            + insideImage);

                    JSONArray insideitemimage = new JSONArray();
                    insideitemimage = insideImage.getJSONArray("Items");


                    JSONObject insideImageURL = new JSONObject();
                    for (int j = 0; j < insideitemimage.length(); j++) {

                        insideImageURL = insideitemimage.getJSONObject(i);
                    }

                    suggestedProductName = jsonobject.getString("ProductName");
                    System.out.println("@@#$$One retail Recommended product name:"
                            + suggestedProductName);


                    suggestionImageURL = "https://etihaddemodevret.cloudax.dynamics.com/MediaServer/"
                            + insideImageURL.getString("Url");

                }

            } catch (JSONException e) {

                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }

            Log.i("UPDATE", "2");
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {


            super.onPostExecute(aVoid);

        }
    }

    /*
    OFFLINE FUNCTION
     */
    private void getRecommdedproductDetailsOffline() {


    }

}
