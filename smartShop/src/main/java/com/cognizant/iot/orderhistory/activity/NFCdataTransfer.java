package com.cognizant.iot.orderhistory.activity;


import android.app.Activity;
import android.content.Context;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcEvent;
import android.os.AsyncTask;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.cognizant.iot.activity.CatalogActivity;
import com.cognizant.iot.activityfindproduct.JSONFunctionsDetails_findprod;
import com.cognizant.iot.activityproductlist.ImageLoaderprod;
import com.cognizant.iot.utils.AccountState;
import com.cognizant.retailmate.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by 452781 on 1/23/2017.
 */
public class NFCdataTransfer {

    String lockerPassword = "1234";


    /**
     * Created by 452781 on 1/17/2017.
     */


    Context context;


    ImageView btnDismiss;

    TextView lbl_exchange;

    LinearLayout settings;


    public NFCdataTransfer(String deliveryMode, Context context) {

        this.context = context;

        if (AccountState.getOfflineMode()) {

        } else {
//            new UpdateDeliveryMode(deliveryMode).execute();
        }
    }


    public void getPopUp(View parent, final Context context) {

        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.80);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View popUpView = inflater.inflate(R.layout.nfc_layout, null, false);
        final PopupWindow popup = new PopupWindow(popUpView, screenWidth,
                WindowManager.LayoutParams.WRAP_CONTENT, true);
        popup.setContentView(popUpView);
        btnDismiss = (ImageView) popUpView.
                findViewById(R.id.cancelButton);

        lbl_exchange = (TextView) popUpView.findViewById(R.id.lbl_exchange);
        settings = (LinearLayout) popUpView.findViewById(R.id.settings);


        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                popup.dismiss();
            }
        });

        popup.showAtLocation(parent, Gravity.CENTER_HORIZONTAL, 10, 10);

        if (AccountState.getOfflineMode()) {
            getRecommdedproductDetailsOffline();
        }


        NfcAdapter mAdapter = NfcAdapter.getDefaultAdapter(context);
        if (mAdapter == null) {
            lbl_exchange.setText("Sorry this device does not support NFC.");
            settings.setVisibility(View.GONE);
            return;
        } else if (!mAdapter.isEnabled()) {
            lbl_exchange.setText("Please enable NFC via Settings");
            settings.setVisibility(View.GONE);
//            Toast.makeText(context, "Please enable NFC via Settings.", Toast.LENGTH_LONG).show();
        } else {

            NfcAdapter.CreateNdefMessageCallback callback = new NfcAdapter.CreateNdefMessageCallback() {
                @Override
                public NdefMessage createNdefMessage(NfcEvent event) {
                    String message = lockerPassword;
                    NdefRecord ndefRecord = NdefRecord.createMime("text/plain", message.getBytes());
                    NdefMessage ndefMessage = new NdefMessage(ndefRecord);
                    return ndefMessage;
                }
            };
            mAdapter.setNdefPushMessageCallback(callback, (Activity) context);


        }

    }


    /*
    OFFLINE FUNCTION
     */
    private void getRecommdedproductDetailsOffline() {


    }

}


