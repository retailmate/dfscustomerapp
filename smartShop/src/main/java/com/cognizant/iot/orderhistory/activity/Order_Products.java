package com.cognizant.iot.orderhistory.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cognizant.iot.orderhistory.adapter.ListViewAdapter_order_product;
import com.cognizant.iot.orderhistory.model.OrderModel;
import com.cognizant.iot.orderhistory.qrActivity;
import com.cognizant.iot.utils.AccountState;
import com.cognizant.retailmate.R;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

public class Order_Products extends AppCompatActivity {
    // Declare Variables
    JSONObject jsonobject;
    JSONArray jsonarray;
    ListView listview;
    ListViewAdapter_order_product adapter2;
    ProgressDialog mProgressDialog;
    ArrayList<HashMap<String, String>> arraylist;
    //    ArrayList<HashMap<String, String>> orderProductsArraylist;
    public static String PRODID = "prodid";
    public static String PRODNAME = "prodname";
    public static String PRODPRICE = "prodprice";
    public static String PRODQAUNTITY = "prodquantity";
    public static String PRODCATEGORY = "prodcategory";
    public static String FLAG = "flag";

    String orderid;
    String orderdate;
    int orderprice;
    String flag;
    String position;
    String orderstatus;

    public static FloatingActionButton fab;

    OrderModel orderModel;

    public static String dlvType;
    FloatingActionButton fab_p1us;
    FloatingActionButton fab_one;
    FloatingActionButton fab_two;
    FloatingActionButton fab_three;
    Animation FabOpen, FabClose, FabClockwise, FabAntiClockwise;
    boolean isOpen = false;

    View orderstatusbar;
    RelativeLayout status_bar_layout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Get the view from listview_main.xml
        setContentView(R.layout.order_products);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarOrderdetails);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        orderModel = (OrderModel) getIntent().getSerializableExtra("OrderData");

        orderprice = getIntent().getIntExtra("OrderPrice", 0);
        System.out.println("@@## orderprice in detail screen " + orderprice);
        dlvType = orderModel.getDlvMode();

        // Locate the TextViews in singleitemview.xml
        TextView txtorderid = (TextView) findViewById(R.id.order_id_products1);
        TextView txtorderdate = (TextView) findViewById(R.id.order_products_date2);
        TextView txtorderstatus = (TextView) findViewById(R.id.order_products_status);
        TextView txtorderprice = (TextView) findViewById(R.id.order_products_price1);

        orderstatusbar = findViewById(R.id.orderstatusbar);
        status_bar_layout = (RelativeLayout) orderstatusbar.findViewById(R.id.status_bar_layout);

//        fab = (FloatingActionButton) findViewById(R.id.fab);

        fab_p1us = (FloatingActionButton) findViewById(R.id.fab_plus);
        fab_one = (FloatingActionButton) findViewById(R.id.fab_one);
        fab_two = (FloatingActionButton) findViewById(R.id.fab_two);
        fab_three = (FloatingActionButton) findViewById(R.id.fab_three);
        FabOpen = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_open);
        FabClose = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_close);
        FabClockwise = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_clockwise);
        FabAntiClockwise = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_anticlockwise);


        System.out.println("@@## dvlymode" + dlvType);

        if (dlvType.equals("bopis")) {
            orderstatusbar.setVisibility(View.VISIBLE);
        } else {
            orderstatusbar.setVisibility(View.GONE);
        }

        fab_p1us.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (isOpen) {
                    fab_one.startAnimation(FabClose);
                    fab_two.startAnimation(FabClose);
                    fab_three.startAnimation(FabClose);
                    fab_p1us.startAnimation(FabAntiClockwise);
                    fab_one.setClickable(false);
                    fab_two.setClickable(false);
                    fab_three.setClickable(false);
                    isOpen = false;
                } else {
                    fab_one.startAnimation(FabOpen);
                    fab_two.startAnimation(FabOpen);
                    fab_three.startAnimation(FabOpen);
                    fab_p1us.startAnimation(FabClockwise);
                    fab_one.setClickable(true);
                    fab_two.setClickable(true);
                    fab_three.setClickable(true);
                    isOpen = true;
                }
            }

        });


        txtorderid.setText("INV-" + orderModel.getSalesId());
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd'T'HH:mm:ss");


            Date newdate = sdf.parse(orderModel.getDateCreated());

            SimpleDateFormat sdftwo = new SimpleDateFormat("dd/MM/yyyy");
            txtorderdate.setText(sdftwo.format(newdate));
        } catch (ParseException e) {
            e.printStackTrace();
        }


//        txtorderprice.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (isDlvyModeEditable()) {
//                    new OrderDeliveryChange(orderModel.getDlvMode(), Order_Products.this).getPopUp(v, Order_Products.this);
//                }
//            }
//        });


        Double orderprice = 0.0;
        for (int ki = 0; ki < orderModel.getItems().size(); ki++) {

            orderprice = orderprice + orderModel.getItems().get(ki).getLineAmount();
        }
        txtorderprice.setText(String.valueOf(orderprice));


        // Locate the listview in listview_main.xml
        listview = (ListView) findViewById(R.id.order_products_listview);
        // Pass the results into ListViewAdapter.java
//        adapter2 = new ListViewAdapter_order_product(Order_Products.this,
//                orderProductsArraylist);

        adapter2 = new ListViewAdapter_order_product(Order_Products.this,
                orderModel);

        // Set the adapter to the ListView
        listview.setAdapter(adapter2);

        /*
        FAB
         */
        fab_one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(Order_Products.this, "Qrcode Generated", Toast.LENGTH_SHORT).show();
//                String text2Qr = "23145";//editText.getText().toString();

//                String text2Qr = orderProductsArraylist.get(0).get("salesid") + ":" + AccountState.getUserID() + ":" + AccountState.getUserName();

                if (dlvType.equals("bopis")) {


                    String text2Qr = orderModel.getSalesId() + ":" + AccountState.getUserID() + ":" + AccountState.getUserName();

                    //validating string if null displaying a toast message
                    if (text2Qr.matches("")) {
                        Toast.makeText(Order_Products.this, "Enter a valid string", Toast.LENGTH_SHORT).show();
                    } else {
                        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
                        try {
                            BitMatrix bitMatrix = multiFormatWriter.encode(text2Qr, BarcodeFormat.QR_CODE, 200, 200);
                            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
                            Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
                            Intent intent = new Intent(Order_Products.this, qrActivity.class);
                            intent.putExtra("pic", bitmap);
                            startActivity(intent);
                        } catch (WriterException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });


        fab_three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isDlvyModeEditable()) {
                    new OrderDeliveryChange(orderModel.getDlvMode(), Order_Products.this).getPopUp(v, Order_Products.this);
                }

            }
        });

        fab_two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dlvType.equals("locker") || dlvType.equals("bopis")) {
                    new NFCdataTransfer(orderModel.getDlvMode(), Order_Products.this).getPopUp(v, Order_Products.this);
                }

            }
        });

    }

    private boolean isDlvyModeEditable() {


        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd'T'HH:mm:ss");

        sdf.getTimeInstance();
        sdf.setTimeZone(TimeZone.getTimeZone("gmt"));
        String currentDateTimeString = sdf.format(new Date());

//        String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());


        try {

            Date date1 = sdf.parse(currentDateTimeString);
            Date date2 = sdf.parse(orderModel.getDateCreated());

            long diff = calculateDifference(date1, date2);

            if (120 - diff > 0) {
                return true;
            } else {
                return false;
            }


        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }


    }


    long calculateDifference(Date startDate, Date endDate) {
        //1 minute = 60 seconds
        //1 hour = 60 x 60 = 3600
        //1 day = 3600 x 24 = 86400

        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        System.out.println("startDate : " + startDate);
        System.out.println("endDate : " + endDate);
        System.out.println("different : " + different);

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        System.out.printf(
                "@@## %d days, %d hours, %d minutes, %d seconds%n",
                elapsedDays,
                elapsedHours, elapsedMinutes, elapsedSeconds);

        return (-1 * minutesInMilli);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
//                Intent in = new Intent(this, Order_Screen.class);
//                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                startActivity(in);
                finish();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // DownloadJSON AsyncTask
//    private class DownloadJSON extends AsyncTask<Void, Void, Void> {
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            // Create a progressdialog
//            mProgressDialog = new ProgressDialog(Order_Products.this);
//            // Set progressdialog title
//            mProgressDialog.setTitle("Getting Products for the selected Order");
//            // Set progressdialog message
//            mProgressDialog.setMessage("Loading...");
//            mProgressDialog.setIndeterminate(false);
//            // Show progressdialog
//            mProgressDialog.show();
//        }
//
//        @Override
//        protected Void doInBackground(Void... params) {
//
//            // Create an array
//            arraylist = new ArrayList<HashMap<String, String>>();
//            // Retrieve JSON Objects from the given URL address
//            jsonobject = JSONfunctions_order.getJSONfromURL_order_products(
//                    CatalogActivity.urlPart + "/api/Order/GetOrderDetails",
//                    orderid);
//
//            try {
//                // Locate the array name in JSON
//                jsonarray = jsonobject.getJSONArray("cusdetails");
//
//                for (int i = 0; i < jsonarray.length(); i++) {
//                    HashMap<String, String> map = new HashMap<String, String>();
//                    jsonobject = jsonarray.getJSONObject(i);
//                    // Retrive JSON Objects
//                    map.put("prodid", jsonobject.getString("prodid"));
//                    map.put("prodcategory", jsonobject.getString("Category"));
//                    map.put("prodname", jsonobject.getString("Name"));
//                    map.put("prodprice", jsonobject.getString("NetAmt"));
//                    map.put("prodquantity", jsonobject.getString("Quantity"));
//                    map.put("flag", jsonobject.getString("ImageURL"));
//                    // Set the JSON Objects into the array
//                    arraylist.add(map);
//                }
//            } catch (JSONException e) {
//                Log.e("Error", e.getMessage());
//                e.printStackTrace();
//            }
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(Void args) {
//
//            // Toast.makeText(getApplicationContext(), jsonobject.toString(),
//            // Toast.LENGTH_LONG).show();
//
//            // Locate the listview in listview_main.xml
//            listview = (ListView) findViewById(R.id.order_products_listview);
//            // Pass the results into ListViewAdapter.java
//            adapter2 = new ListViewAdapter_order_product(Order_Products.this,
//                    arraylist);
//            // Set the adapter to the ListView
//            listview.setAdapter(adapter2);
//            // Close the progressdialog
//            mProgressDialog.dismiss();
//        }
//    }
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.aboutusmenu, menu);
//        // setMenuBackground();
//
//        return true;
//    }


}