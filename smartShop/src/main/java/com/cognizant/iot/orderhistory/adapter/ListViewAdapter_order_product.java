package com.cognizant.iot.orderhistory.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cognizant.iot.orderhistory.activity.ImageLoader_Orders;
import com.cognizant.iot.orderhistory.model.OrderDetailModel;
import com.cognizant.iot.orderhistory.model.OrderModel;
import com.cognizant.retailmate.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ListViewAdapter_order_product extends BaseAdapter {

    // Declare Variables
    Context context;
    LayoutInflater inflater;
    ArrayList<HashMap<String, String>> data;
    ImageLoader_Orders imageLoader;
    HashMap<String, String> resultp = new HashMap<String, String>();

    OrderModel orderModel;

//    public ListViewAdapter_order_product(Context context,
//                                         ArrayList<HashMap<String, String>> arraylist) {
//        this.context = context;
//        data = arraylist;
//        imageLoader = new ImageLoader_Orders(context);
//    }

    public ListViewAdapter_order_product(Context context,
                                         OrderModel orderModel) {
        this.context = context;
        this.orderModel = orderModel;
        imageLoader = new ImageLoader_Orders(context);
    }

    @Override
    public int getCount() {
        return orderModel.getItems().size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        // Declare Variables
        TextView prodid, prodcategorytv;
        TextView prodname;
        TextView prodprice;
        ImageView flag;
        TextView prodquantity;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View itemView = inflater.inflate(R.layout.order_products_listview_item,
                parent, false);
        // Get the position
//        resultp = data.get(position);
//        System.out.println("@@## FLAG Image in Order Detail Screen " + resultp.get(Order_Products.FLAG));


        List<OrderDetailModel> orderDetailModels = orderModel.getItems();

        // Locate the TextViews in listview_item.xml
        // prodid = (TextView) itemView.findViewById(R.id.order_product_prodid);
        prodcategorytv = (TextView) itemView
                .findViewById(R.id.order_product_prodcategory);
        prodname = (TextView) itemView
                .findViewById(R.id.order_product_prodname1);
        prodprice = (TextView) itemView
                .findViewById(R.id.order_product_prodprice1);
        prodquantity = (TextView) itemView
                .findViewById(R.id.product_quantity_main1);
        // Locate the ImageView in listview_item.xml
        flag = (ImageView) itemView.findViewById(R.id.order_products_flag);

        // Capture position and set results to the TextViews
        // prodid.setText(resultp.get(Order_Products.PRODID));
//        prodcategorytv.setText("Category : "
//                + resultp.get(Order_Products.PRODCATEGORY));

        prodcategorytv.setText("Category : "
                + orderDetailModels.get(position).getProdCategory());

//        prodname.setText(resultp.get(Order_Products.PRODNAME));

        prodname.setText(orderDetailModels.get(position).getProductName());

//        prodprice.setText("$" + resultp.get(Order_Products.PRODPRICE));

        prodprice.setText("" + orderDetailModels.get(position).getLineAmount());

//        prodquantity.setText(resultp.get(Order_Products.PRODQAUNTITY));
        prodquantity.setText((orderDetailModels.get(position).getQty()).toString());
        System.out.println("@@##QTY " + orderDetailModels.get(position).getQty());
        // Capture position and set results to the ImageView
        // Passes flag images URL into ImageLoader.class


//        imageLoader.DisplayImage(resultp.get(Order_Products.FLAG), flag);

        imageLoader.DisplayImage("https://etihaddemodevret.cloudax.dynamics.com/MediaServer/" + orderDetailModels.get(position).getImage(), flag);

        return itemView;
    }
}