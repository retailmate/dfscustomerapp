package com.cognizant.iot.orderhistory.activity;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.cognizant.iot.activity.CatalogActivity;
import com.cognizant.iot.orderhistory.adapter.ListViewAdapter_order;
import com.cognizant.iot.orderhistory.adapter.ListViewAdapter_order_product;
import com.cognizant.iot.orderhistory.model.OrderModel;
import com.cognizant.iot.utils.AccountState;
import com.cognizant.retailmate.R;
import com.google.gson.Gson;

public class Order_Screen extends AppCompatActivity {
    // Declare Variables
    JSONObject jsonobject;
    JSONArray jsonarray;
    ListView listview;
    ListViewAdapter_order adapter;
    ProgressDialog mProgressDialog;
    ArrayList<HashMap<String, ArrayList>> arraylist;
    ArrayList<HashMap<String, String>> listofProducts;
    public static String ORDERID = "orderid";
    public static String ORDERDATE = "orderdate";
    public static String ORDERPRICE = "orderprice";
    public static String ORDERSTATUS = "orderstatus";
    Boolean arrayempty = false;

    ArrayList<String> list;
    static String imei = "000000000000788";

    String whichClass;

    Gson gson;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.listview_main_order);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarOrderList);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);


        imei = getIntent().getStringExtra("imeinmbr");

        whichClass = getIntent().getStringExtra("wichclass");


        gson = new Gson();

        if (AccountState.getOfflineMode()) {
            gethistoryoffline();
        } else {

            GetPurchaseHistory();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.normalsearchmenu, menu);
        // setMenuBackground();

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar app icon/title

        // Handle action bar actions click
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            case R.id.search_orders:

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    // DownloadJSON AsyncTask
    private class GetOrderHistory extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Create a progressdialog
            mProgressDialog = new ProgressDialog(Order_Screen.this);
            // Set progressdialog title
            mProgressDialog.setTitle("Order List");
            // Set progressdialog message
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            // Show progressdialog
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            // Create an array
            arraylist = new ArrayList<HashMap<String, ArrayList>>();
            listofProducts = new ArrayList<HashMap<String, String>>();
            // Retrieve JSON Objects from the given URL address
            jsonobject = JSONfunctions_order
                    .getJSONfromURL("http://rmethapi.azurewebsites.net/api/PurchaseServices/GetUserPurchaseHistoryAPI");

            System.out.println("@@## response of Order" + jsonobject);

            try {

                //list = new ArrayList<String>();
                // Locate the array name in JSON
                jsonarray = jsonobject.getJSONArray("value");

                if (jsonarray.length() == 0)
                    arrayempty = true;
                else {

                    HashMap<String, ArrayList> OrderMap;

                    OrderMap = new HashMap<String, ArrayList>();

                    String currentInvoiceId = null;

                    for (int i = 0; i < jsonarray.length(); i++) {

                        jsonobject = jsonarray.getJSONObject(i);
                        HashMap<String, String> map = new HashMap<String, String>();

                        if (currentInvoiceId == null || currentInvoiceId.equals(jsonobject.getString("InvoiceId"))) {
                            System.out.println("@@## InvoiceId" + jsonobject.getString("InvoiceId"));
                            currentInvoiceId = jsonobject.getString("InvoiceId");
                            // Retrive JSON Objects
                            map.put("orderid", jsonobject.getString("InvoiceId"));
                            map.put("orderdate", jsonobject.getString("Datedelivered"));
                            map.put("prodid", jsonobject.getString("ProductId"));
                            map.put("prodcategory", "");
                            map.put("prodname", jsonobject.getString("ProductName"));
                            map.put("prodprice", jsonobject.getString("UnitPrice"));
                            map.put("prodquantity", jsonobject.getString("Qty"));
                            map.put("flag", "https://etihaddemodevret.cloudax.dynamics.com/MediaServer/" + jsonobject.getString("Image"));
                            map.put("ordertype", jsonobject.getString("DlvMode"));
                            map.put("salesid", jsonobject.getString("SalesId"));


                            map.put("orderstatus", "");


                            listofProducts.add(map);

                            if (jsonarray.length() - i == 1) {
                                System.out.println("@@## InvoiceId adding to list");
                                OrderMap.put("invoice", listofProducts);
                                arraylist.add(OrderMap);
                            }
                        } else {

                            OrderMap.put("invoice", listofProducts);
                            arraylist.add(OrderMap);


                            if (jsonarray.length() - i == 0) {

                                currentInvoiceId = jsonobject.getString("InvoiceId");
                                // Retrive JSON Objects
                                map.put("orderid", jsonobject.getString("InvoiceId"));
                                map.put("orderdate", jsonobject.getString("Datedelivered"));
                                map.put("prodid", jsonobject.getString("ProductId"));
                                map.put("prodcategory", "");
                                map.put("prodname", jsonobject.getString("ProductName"));
                                map.put("prodprice", jsonobject.getString("UnitPrice"));
                                map.put("prodquantity", jsonobject.getString("Qty"));
                                map.put("salesid", jsonobject.getString("SalesId"));
                                map.put("flag", "https://etihaddemodevret.cloudax.dynamics.com/MediaServer/" + jsonobject.getString("Image"));
                                map.put("orderstatus", "");
                                listofProducts.add(map);
                                OrderMap.put("invoice", listofProducts);
                                arraylist.add(OrderMap);
                            } else if (jsonarray.length() - i > 0) {
                                currentInvoiceId = jsonobject.getString("InvoiceId");
                                // Retrive JSON Objects
                                map.put("orderid", jsonobject.getString("InvoiceId"));
                                map.put("orderdate", jsonobject.getString("Datedelivered"));
                                map.put("prodid", jsonobject.getString("ProductId"));
                                map.put("prodcategory", "");
                                map.put("prodname", jsonobject.getString("ProductName"));
                                map.put("prodprice", jsonobject.getString("UnitPrice"));
                                map.put("prodquantity", jsonobject.getString("Qty"));
                                map.put("salesid", jsonobject.getString("SalesId"));
                                map.put("flag", "https://etihaddemodevret.cloudax.dynamics.com/MediaServer/" + jsonobject.getString("Image"));


                                map.put("orderstatus", "");


                                listofProducts.add(map);
                            }
                        }


                        //list.add(jsonobject.getString("Orderid"));

                        // Set the JSON Objects into the array
                        //arraylist.add(map);
                    }
                }
            } catch (JSONException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {

            // Toast.makeText(getApplicationContext(),list.toString() ,
            // Toast.LENGTH_LONG).show();

            System.out.println("@@## InvoiceId ON POST EXECUTE");

            if (arrayempty == true)
                Toast.makeText(getApplicationContext(),
                        "Your Order List is empty", Toast.LENGTH_LONG).show();

            // Locate the listview in listview_main.xml
            listview = (ListView) findViewById(R.id.listview_order);
            // Pass the results into ListViewAdapter.java
//            adapter = new ListViewAdapter_order(Order_Screen.this, arraylist);
            // Set the adapter to the ListView
            adapter.notifyDataSetChanged();
            listview.setAdapter(adapter);
            // Close the progressdialog
            mProgressDialog.dismiss();
        }
    }

    /*
   ORDER Call for getting new order history
    */
    public void GetPurchaseHistory() {
        System.out.println("@@## GetPurchaseHistory Called");


        /*
        new call
         */

        String url = "http://rmethapi.azurewebsites.net/api/Order/OrderHistoryAPI";
        StringRequest req = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                System.out.println("@@## Response of order creation " + response);

                OrderModel[] orderModel = gson.fromJson(response, OrderModel[].class);

                if (arrayempty == true)
                    Toast.makeText(getApplicationContext(),
                            "Your Order List is empty", Toast.LENGTH_LONG).show();

                // Locate the listview in listview_main.xml
                listview = (ListView) findViewById(R.id.listview_order);
                // Pass the results into ListViewAdapter.java
//                adapter = new ListViewAdapter_order(Order_Screen.this, arraylist);
                adapter = new ListViewAdapter_order(Order_Screen.this, orderModel);

                // Set the adapter to the ListView
                adapter.notifyDataSetChanged();
                listview.setAdapter(adapter);
                // Close the progressdialog
//                mProgressDialog.dismiss();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                Log.e("Error: ", error.getMessage());
//                callCount = 0;
//                    reply="server not working";
                System.out.println("@@## Response of order creation error" + error);

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();


                params.put("_custAccount", AccountState.getUserID());
                params.put("_historyCount", "2");


                return params;


            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(Order_Screen.this);
        requestQueue.add(req);


    }

      /*
    Offline mode
     */


    public void gethistoryoffline() {

        OrderModel[] orderModel = gson.fromJson(gethistoryResponse(), OrderModel[].class);

        if (arrayempty == true)
            Toast.makeText(getApplicationContext(),
                    "Your Order List is empty", Toast.LENGTH_LONG).show();

        // Locate the listview in listview_main.xml
        listview = (ListView) findViewById(R.id.listview_order);
        // Pass the results into ListViewAdapter.java
//                adapter = new ListViewAdapter_order(Order_Screen.this, arraylist);
        adapter = new ListViewAdapter_order(Order_Screen.this, orderModel);

        // Set the adapter to the ListView
        adapter.notifyDataSetChanged();
        listview.setAdapter(adapter);
        // Close the progressdialog
//                mProgressDialog.dismiss();

    }

    public String gethistoryResponse() {
        String json = null;
        try {
            InputStream is = getAssets().open("orderhistoryoffline.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

}