package com.cognizant.iot.estimote;

import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cognizant.iot.json.CountCall;
import com.cognizant.iot.model.AssetModel;
import com.cognizant.retailmate.R;
import com.estimote.sdk.Beacon;
import com.estimote.sdk.Utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;

//import com.cognizant.iot.activity.GlobalClass;

public class LeDeviceListAdapter extends BaseAdapter {
    CountCall ccallobj;
    ListBeaconsActivity listbeaconsactivity;
    Context cxt;
    public static final String EXTRAS_BEACON = "extrasBeacon";

    private NotificationManager notificationManager;
    int NOTIFICATION_ID = 12345;

    private ArrayList<Beacon> beacons;
    private LayoutInflater inflater;

    ArrayList<AssetModel> assetList;
    ImageLoaderListBeacon imageLoader;

    // final GlobalClass globalVariable;

    int count = 0;


    public LeDeviceListAdapter(Context context, ArrayList<AssetModel> al) {
        this.inflater = LayoutInflater.from(context);
        this.beacons = new ArrayList<Beacon>();
        this.cxt = context;
        assetList = al;
        count = assetList.size();

        imageLoader = new ImageLoaderListBeacon(context);
        notificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        // globalVariable = (GlobalClass) context.getApplicationContext();
        this.listbeaconsactivity = new ListBeaconsActivity();
        System.out.println(listbeaconsactivity.assetList.get(0).getAssetName());
    }

    public void replaceWith(Collection<Beacon> newBeacons) {
        this.beacons.clear();
        this.beacons.addAll(newBeacons);

        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return beacons.size();
    }

    @Override
    public Beacon getItem(int position) {
        try {
            return beacons.get(position);
        } catch (IndexOutOfBoundsException e) {

        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        view = inflateIfRequired(view, position, parent);

        try {
            bind(getItem(position), view);
        } catch (NullPointerException e) {

        }

        return view;
    }

    private void bind(final Beacon beacon, View view) {
        try {
            ViewHolder holder = (ViewHolder) view.getTag();

            holder.offerlist.setText(listbeaconsactivity.assetList.get(
                    position(beacon.getProximityUUID().toString())).getOffers());
            holder.nameTextView.setText(listbeaconsactivity.assetList.get(
                    position(beacon.getProximityUUID().toString())).getAssetName());
            holder.descTextView.setText(listbeaconsactivity.assetList.get(
                    position(beacon.getProximityUUID().toString())).getAssetDesc());
            double roundOff = (double) Math
                    .round(Utils.computeAccuracy(beacon) * 100) / 100;
            holder.distanceTextView.setText("(" + roundOff + "m)");

            // holder.image.setImageResource(notifyIcon(beacon));

//        imageLoader.DisplayImage(
//                listbeaconsactivity.assetList.get(
//                        position(beacon.getProximityUUID())).getUrl(),
//                holder.image);
            imageLoader.DisplayImage(
                    listbeaconsactivity.assetList.get(
                            position(beacon.getProximityUUID().toString())).getUrl(),
                    holder.image, "notround");

            if (roundOff < 1
                    && !listbeaconsactivity.assetList.get(
                    position(beacon.getProximityUUID().toString())).getNotfied()) {
                postNotification("OFFERS!!", beacon);
                listbeaconsactivity.assetList.get(position(beacon.getProximityUUID().toString()))
                        .setNotfied(true);
            }

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(cxt,
                            NotifyDemoActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    intent.putExtra(EXTRAS_BEACON, beacon);
                    intent.putExtra("beaconDetails", assetList
                            .get(position(beacon.getProximityUUID().toString())));
                    intent.putExtra(
                            "msg",
                            assetList.get(
                                    position(beacon.getProximityUUID().toString()))
                                    .getAssetDesc());
                    cxt.startActivity(intent);
                }
            });


        } catch (NullPointerException | IndexOutOfBoundsException e) {

        }
    }

    public void postNotification(String msg, final Beacon beacon) {
        Random r = new Random();
        NOTIFICATION_ID = r.nextInt(10980 - 65) + 65;
        System.out.println("@@#### NOTIFY ID:" + NOTIFICATION_ID);

        Intent notifyIntent = new Intent(cxt, NotifyDemoActivity.class);
        // notifyIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        notifyIntent.putExtra(EXTRAS_BEACON, beacon);
        System.out.println(assetList.get(position(beacon.getProximityUUID().toString()))
                .getAssetBeaconId());

        notifyIntent.putExtra("beaconDetails",
                assetList.get(position(beacon.getProximityUUID().toString())));
        System.out.println("@@### MAC ID:"
                + beacon.getProximityUUID());
        System.out.println("@@### MAC ID:"
                + beacon.getProximityUUID());
        notifyIntent.putExtra("msg",
                assetList.get(position(beacon.getProximityUUID().toString())).getOffers());
        PendingIntent pendingIntent = PendingIntent.getActivities(cxt,
                NOTIFICATION_ID, new Intent[]{notifyIntent},
                PendingIntent.FLAG_UPDATE_CURRENT);

        Notification notification = new Notification.Builder(cxt)
                .setSmallIcon(notifyIcon(beacon))
                .setContentTitle(
                        assetList.get(position(beacon.getProximityUUID().toString()))
                                .getAssetName() + " Offers!!")
                .setContentText(msg).setAutoCancel(true)
                .setContentIntent(pendingIntent).build();
        notification.defaults |= Notification.DEFAULT_SOUND;
        notification.defaults |= Notification.DEFAULT_LIGHTS;
        notificationManager.notify(NOTIFICATION_ID, notification);

        if (listbeaconsactivity.alert) {
            new AlertDialog.Builder(cxt, R.style.MyDatePickerDialogTheme)
                    // Use an activity object here
                    .setMessage(
                            assetList.get(position(beacon.getProximityUUID().toString()))
                                    .getAssetDesc()).setCancelable(true)
                    .setNegativeButton("Never Mind", null)
                    .setPositiveButton("Open", new OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                            Intent intent = new Intent(cxt,
                                    NotifyDemoActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            intent.putExtra(EXTRAS_BEACON, beacon);
                            intent.putExtra("beaconDetails", assetList
                                    .get(position(beacon.getProximityUUID().toString())));
                            intent.putExtra(
                                    "msg",
                                    assetList.get(
                                            position(beacon.getProximityUUID().toString()))
                                            .getAssetDesc());
                            cxt.startActivity(intent);
                            assetList.get(position(beacon.getProximityUUID().toString()))
                                    .setNotfied(false);

                        }
                    }).create().show();
        }

    }

	/*
     * END of Debugging
	 */

    private int notifyIcon(Beacon beacon) {
        // TODO Auto-generated method stub


        return R.drawable.cognizant_retailmate_logo;
    }

    private int position(String macAddress) {
        // TODO Auto-generated method stub
        int i;
        for (i = 0; i < assetList.size(); i++) {
            if (macAddress.equals(assetList.get(i).getAssetBeaconId().toLowerCase())) {
                return i;
            }
        }
        return assetList.size();
    }


    private int offercount(ArrayList<Beacon> beacons) {
        // TODO Auto-generated method stub
        int count = 0;
        for (int j = 0; j < beacons.size(); j++) {
            for (int i = 0; i < assetList.size(); i++) {
                if (beacons.get(j).getMacAddress().equals(assetList.get(i).getAssetBeaconId().toLowerCase())) {
                    count = count + 1;
                }
            }
        }
        return count;
    }


    private View inflateIfRequired(View view, int position, ViewGroup parent) {
        if (view == null) {
            view = inflater.inflate(R.layout.beacons_list_row, null);
            view.setTag(new ViewHolder(view));
        }
        return view;
    }

    static class ViewHolder {
        final TextView nameTextView;
        final TextView distanceTextView;
        final TextView descTextView;
        final TextView offerlist;
        ImageView image;

        ViewHolder(View view) {
            offerlist = (TextView) view.findViewById(R.id.offers_list);
            nameTextView = (TextView) view.findViewById(R.id.name);
            distanceTextView = (TextView) view.findViewById(R.id.distance);
            descTextView = (TextView) view.findViewById(R.id.desc);
            image = (ImageView) view.findViewById(R.id.imageView1);
        }

    }
}
