package com.cognizant.iot.estimote;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cognizant.iot.activity.CatalogActivity;
import com.cognizant.iot.json.BrandCall;
import com.cognizant.iot.json.CountCall;
import com.cognizant.iot.model.AssetModel;
import com.cognizant.iot.model.OfferModel;
import com.cognizant.retailmate.R;
import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;
import com.estimote.sdk.Utils;

//import com.cognizant.iot.activity.GlobalClass;
/*
 * For compass implementation
 */
//implements SensorEventListener

public class NotifyDemoActivity extends AppCompatActivity {

    private static final String TAG = NotifyDemoActivity.class.getSimpleName();
    private static final int NOTIFICATION_ID = 123;

    private BeaconManager beaconManager;
    private NotificationManager notificationManager;
    private Region region, region1;
    TextView name, dateNtime, brands;
    ImageView shelf_image;
    Beacon beacon;
    public AssetModel assetmodel;
    double distance;
    CountDownTimer cdtimer;
    private int segmentLength = -1;
    private int startY = -1;
    public String msg;
    CountCall cCallObj;
    BrandCall bCallobj;

    String imei;

    Spinner spinner;

	/*
     * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreate(android.os.Bundle) for compass
	 * implementation
	 */

    public TextView statusTextView;
    private ImageView mPointer;
    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private Sensor mMagnetometer;
    private float[] mLastAccelerometer = new float[3];
    private float[] mLastMagnetometer = new float[3];
    private boolean mLastAccelerometerSet = false;
    private boolean mLastMagnetometerSet = false;
    private float[] mR = new float[9];
    private float[] mOrientation = new float[3];
    private float mCurrentDegree = 0f;
    /*
     * END
     */
    ImageLoaderListBeacon imageLoader;

	/*
     * Distance Activity
	 */

    // Y positions are relative to height of bg_distance image.
    private static final double RELATIVE_START_POS = 320.0 / 1110.0;
    private static final double RELATIVE_STOP_POS = 885.0 / 1110.0;

    private View dotView;
    TextView d1, distance_target, nameDot;

    public String productID = "";

	/*
     * END
	 */
    // GlobalClass globalVariable;

    public static ArrayAdapter<String> adapter;
    public static List<String> brandList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.shelfdetail_layout);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarBeaconDetail);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);


        imageLoader = new ImageLoaderListBeacon(this);

        // globalVariable = (GlobalClass) getApplicationContext();
        assetmodel = new AssetModel();

        assetmodel = (AssetModel) getIntent().getSerializableExtra(
                "beaconDetails");

        beacon = getIntent().getParcelableExtra(
                ListBeaconsActivity.EXTRAS_BEACON);

        // name = (TextView) findViewById(R.id.name);
        name = (TextView) findViewById(R.id.shelf_heading);
        shelf_image = (ImageView) findViewById(R.id.shelf_image);

        // dateNtime = (TextView) findViewById(R.id.timendate);
        // dateNtime = (TextView) findViewById(R.id.startdate_shelf);

        // brands = (TextView) findViewById(R.id.brands);
        brands = (TextView) findViewById(R.id.brands_shelf);

        // distance_target = (TextView) findViewById(R.id.distance);
        distance_target = (TextView) findViewById(R.id.distance_shelf);

        nameDot = (TextView) findViewById(R.id.nameDot);

        int power = beacon.getMeasuredPower();
        System.out.println("@@### POWER" + power);

        // Beacon beacon1 = getIntent().getParcelableExtra(
        // ListBeaconsActivity.EXTRAS_BEACONPLUS);
        region = new Region("regionId", beacon.getProximityUUID(),
                beacon.getMajor(), beacon.getMinor());
        // region1 = new Region("regionIdPLUS", beacon1.getProximityUUID(),
        // beacon1.getMajor(), beacon1.getMinor());
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        beaconManager = new BeaconManager(this);

        // Default values are 5s of scanning and 25s of waiting time to save CPU
        // cycles.
        // In order for this demo to be more responsive and immediate we lower
        // down those values.
        beaconManager.setBackgroundScanPeriod(TimeUnit.SECONDS.toMillis(1), 0);


		/*
         * Distance part
		 */

        dotView = findViewById(R.id.dot);
        d1 = (TextView) findViewById(R.id.dot);

        beacon = getIntent().getParcelableExtra(
                ListBeaconsActivity.EXTRAS_BEACON);
        region = new Region("regionid", beacon.getProximityUUID(),
                beacon.getMajor(), beacon.getMinor());

        d1.setText("You");
        // nameDot.setText(assetmodel.getAssetName());

        if (beacon == null) {
            Toast.makeText(this, "Beacon not found in intent extras",
                    Toast.LENGTH_LONG).show();
            finish();
        }

        beaconManager.setRangingListener(new BeaconManager.RangingListener() {
            @Override
            public void onBeaconsDiscovered(Region region,
                                            final List<Beacon> rangedBeacons) {
                // Note that results are not delivered on UI thread.
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // Just in case if there are multiple beacons with the
                        // same uuid, major, minor.
                        Beacon foundBeacon = null;
                        for (Beacon rangedBeacon : rangedBeacons) {
                            if (rangedBeacon.getProximityUUID().equals(
                                    beacon.getProximityUUID())) {
                                foundBeacon = rangedBeacon;
                            }
                        }
                        if (foundBeacon != null) {
                            updateDistanceView(foundBeacon);
                        }
                    }
                });
            }
        });

        final View view = findViewById(R.id.sonar);
        view.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        view.getViewTreeObserver()
                                .removeOnGlobalLayoutListener(this);

                        startY = (int) (RELATIVE_START_POS * view
                                .getMeasuredHeight());
                        int stopY = (int) (RELATIVE_STOP_POS * view
                                .getMeasuredHeight());
                        segmentLength = stopY - startY;

                        dotView.setVisibility(View.VISIBLE);
                        dotView.setTranslationY(computeDotPosY(beacon));
                    }
                });

		/*
         * END
		 */
        /*
         * Code for offer count
		 */

        System.out.println("@@## UUID GOING FOR BRAND CALL " + beacon.getProximityUUID());

        imei = CatalogActivity.imei;

        cCallObj = new CountCall("empty", NotifyDemoActivity.this, imei,
                beacon.getProximityUUID().toString(), assetmodel);
        cCallObj.execute();

    }

    void updateUI(String msg) {
        statusTextView.setText(msg);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /*
     * DisTance Activity
     */
    private void updateDistanceView(Beacon foundBeacon) {
        if (segmentLength == -1) {
            return;
        }

        dotView.animate().translationY(computeDotPosY(foundBeacon)).start();
    }

    private int computeDotPosY(Beacon beacon) {
        // Let's put dot at the end of the scale when it's further than 6m.
        double distance = Math.min(Utils.computeAccuracy(beacon), 6.0);

        System.out.println("notifyDemo Distance : "
                + String.format("%.2f", distance));
        distance_target.setText("(" + String.format("%.2f", distance) + " m)");

        return startY + (int) (segmentLength * (distance / 6.0));
    }

    public static double round(double value, int places) {
        if (places < 0)
            throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    /*
     * ENDs
     */
    @Override
    protected void onResume() {
        super.onResume();


        msg = getIntent().getStringExtra("msg");

        name.setText(assetmodel.getAssetName());


        imageLoader.DisplayImage(assetmodel.getUrl(), shelf_image, "round");


        String brands = "Fabrikam";

        brandList = Arrays.asList(brands.split(","));

        int i = 0;
        while (i < brandList.size()) {
            System.out.println("@#BRAND LIST" + brandList.get(i));
            i++;
        }
        /*
            Getting brand names from the first API
         */
//        spinner = (Spinner) findViewById(R.id.spinner1);
//
//        adapter = new ArrayAdapter<String>(getApplicationContext(),
//                R.layout.spinner, brandList);
//
//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spinner.setAdapter(adapter);
//        spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
//
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view,
//                                       int position, long id) {
//                // TODO Auto-generated method stub
//                Toast.makeText(getApplicationContext(), "CLicked" + position,
//                        Toast.LENGTH_SHORT);
//                System.out.println("Clicked");
//
//                bCallobj = new BrandCall("empty", NotifyDemoActivity.this,
//                        imei, String.valueOf(spinner.getSelectedItem()), assetmodel.getAssetBeaconId());
//                bCallobj.execute();
//
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//                // TODO Auto-generated method stub
//
//            }
//        });

        Button button1 = (Button) findViewById(R.id.button1);

        button1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if (productID.equals("NOID")) {
                    Toast.makeText(NotifyDemoActivity.this, "No Items available for this brand", Toast.LENGTH_SHORT).show();

                } else {

                    productID = BrandCall.getProductID();
                    System.out.println("@@## PRODUCT ID SPINNER" + productID);

                    Intent intentObject = new Intent(
                            getApplicationContext(),
                            com.cognizant.iot.activityfindproduct.ProductDetails_findprod.class);

                    System.out.println("@@## PRODUCT ID " + productID);
                    intentObject.putExtra("beacon", true);
                    intentObject.putExtra("ProductId", productID);
                    intentObject.putExtra("class", "BEACON");
                    startActivity(intentObject);
                    finish();
                }

            }
        });

        // statusTextView = (TextView) findViewById(R.id.status);
        statusTextView = (TextView) findViewById(R.id.shelf_offer);

        statusTextView.setText(msg);


    }


    protected void onPause() {
        super.onPause();
        /*
         * for compass
		 */
        // mSensorManager.unregisterListener(this, mAccelerometer);
        // mSensorManager.unregisterListener(this, mMagnetometer);
    }

	/*
     * FOR COMPASS
	 * 
	 * @see
	 * android.hardware.SensorEventListener#onSensorChanged(android.hardware
	 * .SensorEvent)
	 */
    // @Override
    // public void onSensorChanged(SensorEvent event) {
    // if (event.sensor == mAccelerometer) {
    // System.arraycopy(event.values, 0, mLastAccelerometer, 0,
    // event.values.length);
    // mLastAccelerometerSet = true;
    // } else if (event.sensor == mMagnetometer) {
    // System.arraycopy(event.values, 0, mLastMagnetometer, 0,
    // event.values.length);
    // mLastMagnetometerSet = true;
    // }
    // if (mLastAccelerometerSet && mLastMagnetometerSet) {
    // SensorManager.getRotationMatrix(mR, null, mLastAccelerometer,
    // mLastMagnetometer);
    // SensorManager.getOrientation(mR, mOrientation);
    // float azimuthInRadians = mOrientation[0];
    // float azimuthInDegress = (float) (Math.toDegrees(azimuthInRadians) + 360)
    // % 360;
    // RotateAnimation ra = new RotateAnimation(mCurrentDegree,
    // -azimuthInDegress, Animation.RELATIVE_TO_SELF, 0.5f,
    // Animation.RELATIVE_TO_SELF, 0.5f);
    //
    // ra.setDuration(250);
    //
    // ra.setFillAfter(true);
    //
    // mPointer.startAnimation(ra);
    // mCurrentDegree = -azimuthInDegress;
    // }
    // }

	/*
     * END
	 */

    @Override
    protected void onDestroy() {
        notificationManager.cancel(NOTIFICATION_ID);
        beaconManager.disconnect();

        super.onDestroy();
    }


	/*
     * for compass
	 */
    // @Override
    // public void onAccuracyChanged(Sensor sensor, int accuracy) {
    // // TODO Auto-generated method stub
    //
    // }

    /*
     * DisTance Activity
     */
    @Override
    protected void onStart() {
        super.onStart();

        beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
            @Override
            public void onServiceReady() {
                beaconManager.startRanging(region);
            }
        });
    }
    /*
     * END
	 */
}
