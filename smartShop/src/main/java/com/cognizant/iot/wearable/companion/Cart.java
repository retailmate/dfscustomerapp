package com.cognizant.iot.wearable.companion;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.cognizant.iot.activity.CatalogActivity;
import com.cognizant.iot.activity.Login_Screen;
import com.cognizant.iot.utils.MySSLSocketFactory;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

public class Cart {
	Context context = null;

	public Cart(Context context) {
		this.context = context;
	}

	public boolean isConnectingToInternet() {
		ConnectivityManager connectivity = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity != null) {
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null)
				for (int i = 0; i < info.length; i++)
					if (info[i].getState() == NetworkInfo.State.CONNECTED) {
						return true;
					}

		}
		return false;
	}

	public void addItemToCart() {
		System.out.println("\n \n @@## INSIDE CART  \n \n");
		class SendPostReqAsyncTask extends AsyncTask<String, Void, String> {

			@Override
			protected String doInBackground(String... params) {

				Log.d("@@##", "SendPostReqAsyncTask :: baseUrl ");

				HttpClient httpclient = MySSLSocketFactory.getNewHttpClient();
				HttpPost httppost = new HttpPost(
						"https://etihaddemodevret.cloudax.dynamics.com/Commerce/Carts('HOUSTON-HOUSTON-20-99')/AddCartLines?api-version=7.1");
				JSONObject parentData = new JSONObject();
				try {
					parentData.put("ProductId",
							(Double.valueOf("22565423553") + 1));
					parentData.put("Quantity", 1);

					JSONArray childData = new JSONArray();

					childData.put(parentData);

					JSONObject finalObject = new JSONObject();
					finalObject.put("cartLines", childData);
					System.out.println("@@## JSON ARRAY " + finalObject);
					/*
					 * DUMMY FOR AX
					 */
					Login_Screen.sendMessage("{'status':'Success'}",
							Constants.PATH_SERVER_RESPONSE);
					/*
					 * END OF DUMMY
					 */
					StringEntity params1;

					params1 = new StringEntity(finalObject.toString());

					System.out.println("@@## params " + finalObject.toString());
					httppost.setEntity(params1);
					httppost.setHeader("Content-Type", "application/json");
					httppost.addHeader("Authorization",
							CatalogActivity.access_token);
					// httppost.addHeader(
					// "DeviceToken",
					// "HOUSTON-20:QwLAwLPNYGMWHtEda7Lm792/PnwNbbjlIvphlaFk8lLoRYnQocGf1itJzWzui4/t3DtzO58iOKDfA9dXVJUz1Q==:5637144592:5637146202");
					httppost.addHeader(
							"DeviceToken",
							"HOUSTON-20:tmOvkmUrFIBWY2ypKGc/Y7KKuIy2/LHEcsouvq7vZsUIHJ8EOy40Z+P38LUtjqK5awQ6rVVn8FspYdDjGmQLkg==:5637144592:5637146202");

				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					HttpResponse httpResponse = httpclient.execute(httppost);

					InputStream inputStream = httpResponse.getEntity()
							.getContent();
					InputStreamReader inputStreamReader = new InputStreamReader(
							inputStream);
					BufferedReader bufferedReader = new BufferedReader(
							inputStreamReader);
					StringBuilder stringBuilder = new StringBuilder();
					String bufferedStrChunk = null;
					while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
						stringBuilder.append(bufferedStrChunk);
					}
					String responseString = stringBuilder.toString();
					Log.d("@@##", "Http Response = " + responseString);

					// Send message to wear
					MainActivity.sendMessage(responseString,
							Constants.PATH_SERVER_RESPONSE);

					return responseString;
				} catch (ClientProtocolException cpe) {
					Log.d("@@##", "Http exception :" + cpe);
					cpe.printStackTrace();
					MainActivity.sendMessage(cpe.toString(),
							Constants.PATH_SERVER_RESPONSE);
				} catch (IOException ioe) {
					Log.d("@@##", "Http exception :" + ioe);
					ioe.printStackTrace();
					MainActivity.sendMessage(ioe.toString(),
							Constants.PATH_SERVER_RESPONSE);
				}

				return null;
			}

			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
			}
		}

		if (isConnectingToInternet()) {
			SendPostReqAsyncTask sendPostReqAsyncTask = new SendPostReqAsyncTask();
			sendPostReqAsyncTask.execute();
		} else {
			MainActivity.sendMessage("{'status':'fail'}",
					Constants.PATH_SERVER_RESPONSE);
			Log.d("@@##", "Pls check network connection");
		}

	}
}
