package com.cognizant.iot.model;

/**
 * Created by Guest_User on 23/03/17.
 */

public class SpinnerModel {

    String name;
    Integer imageid;

    public SpinnerModel(String name, Integer imageid) {
        this.name = name;
        this.imageid = imageid;
    }

    public Integer getImageid() {
        return imageid;
    }

    public void setImageid(Integer imageid) {
        this.imageid = imageid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
