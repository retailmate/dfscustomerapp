package com.cognizant.iot.model;

/**
 * Created by 452781 on 3/20/2017.
 */
public class AddCustomerModel {


    String UserId;
    String BeaconId;
    String CaptureTime;
    Double XPosition;
    Double YPosition;

    public Double getXPosition() {
        return XPosition;
    }

    public void setXPosition(Double XPosition) {
        this.XPosition = XPosition;
    }

    public Double getYPosition() {
        return YPosition;
    }

    public void setYPosition(Double YPosition) {
        this.YPosition = YPosition;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getBeaconId() {
        return BeaconId;
    }

    public void setBeaconId(String beaconId) {
        BeaconId = beaconId;
    }

    public String getCaptureTime() {
        return CaptureTime;
    }

    public void setCaptureTime(String captureTime) {
        CaptureTime = captureTime;
    }
}
