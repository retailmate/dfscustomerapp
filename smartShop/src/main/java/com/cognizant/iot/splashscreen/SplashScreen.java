package com.cognizant.iot.splashscreen;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v4.app.ActivityOptionsCompat;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;

import com.cognizant.iot.activity.Login_Screen;
import com.cognizant.iot.utils.AccountState;
import com.cognizant.retailmate.R;

/**
 * Created by 452781 on 9/13/2016.
 */
public class SplashScreen extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.splashscreen);


        try {
            if (AccountState.getPrefBeaconId().equals(null)) {

            }
        } catch (NullPointerException e) {
            AccountState.setPrefBeaconId("id_token");
        }

        RotateAnimation anim = new RotateAnimation(0f, 360f, 15f, 15f);
        anim.setInterpolator(new LinearInterpolator());
        anim.setRepeatCount(Animation.INFINITE);
        anim.setDuration(3000);

        // Start animating the image
        final ImageView splash = (ImageView) findViewById(R.id.splashImage);
//        splash.startAnimation(anim);


        new CountDownTimer(3000, 1000) {

            public void onTick(long millisUntilFinished) {

            }

            public void onFinish() {

                Intent intent = new Intent(getApplicationContext(), Login_Screen.class);

                ActivityOptionsCompat options = ActivityOptionsCompat.
                        makeSceneTransitionAnimation(SplashScreen.this, splash, "profile");
                startActivity(intent, options.toBundle());

//                new-

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        finish();

                    }
                }, 2000);

            }
        }.start();

    }
}
