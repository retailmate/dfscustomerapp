package com.cognizant.iot.chatbot;

import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;

/**
 * Created by 540472 on 3/15/2017.
 */
public class TranslateTask extends AsyncTask<String,String,String> {

    @Override
    protected String doInBackground(String... params) {

        BaseConnection con = new BaseConnection();
        con.isTranslate=true;
        String jsonString = "server not working";
        try {
            Log.e("TAG","-------------?>>>>>>>>TranslateTask<<<<<<<<<<<<------------");
            jsonString = con.run(params[0]);
            Log.e("TAG",jsonString.toString());
            return jsonString;
        } catch (IOException e) {
            e.printStackTrace();
        }
        //Log.e("TAG",jsonString);
        return jsonString;
    }

}
