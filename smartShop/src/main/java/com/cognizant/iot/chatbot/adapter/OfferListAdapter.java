package com.cognizant.iot.chatbot.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.cognizant.iot.chatbot.model.OfferDataModel;
import com.cognizant.retailmate.R;

import java.util.ArrayList;

/**
 * Created by 452781 on 11/15/2016.
 */
public class OfferListAdapter extends RecyclerView.Adapter<OfferListAdapter.ProductHolder> {

    private ArrayList<OfferDataModel> itemsList;
    private Context mContext;

    public OfferListAdapter(Context context, ArrayList<OfferDataModel> itemsList) {
        this.itemsList = itemsList;
        this.mContext = context;
    }

    @Override
    public ProductHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.chatbot_offer_single_card, null);
        ProductHolder mh = new ProductHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(ProductHolder holder, int i) {

        OfferDataModel productData = itemsList.get(i);

        holder.offerDesc.setText(productData.getOfferDescription());
        holder.offerImage.setImageResource(productData.getImageSource());
//        holder.itemImage.setImageResource();
/*        Picasso.with(mContext)
                .load("https://etihaddemodevret.cloudax.dynamics.com/mediaserver/"+productData.getProductImageResource())
                .placeholder(R.drawable.loading)
                .error(R.drawable.loading)
                .into(holder.itemImage);*/
    }

    @Override
    public int getItemCount() {
        return (null != itemsList ? itemsList.size() : 0);
    }

    public class ProductHolder extends RecyclerView.ViewHolder {

        protected TextView offerDesc;

        protected ImageView offerImage;


        public ProductHolder(View view) {
            super(view);

            this.offerDesc = (TextView) view.findViewById(R.id.offerName);
            this.offerImage = (ImageView) view.findViewById(R.id.offerImage);



            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    Toast.makeText(v.getContext(), offerDesc.getText(), Toast.LENGTH_SHORT).show();

                }
            });


        }

    }

}