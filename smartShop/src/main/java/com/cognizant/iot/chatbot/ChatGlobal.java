package com.cognizant.iot.chatbot;

import android.content.Context;
import android.speech.tts.TextToSpeech;
import android.support.v7.widget.RecyclerView;
import android.widget.EditText;
import android.widget.ImageButton;

import com.cognizant.iot.chatbot.adapter.ChatAdapter;
import com.cognizant.iot.chatbot.model.ChatDataModel;
import com.cognizant.iot.chatbot.model.ProductDataModel;
import com.cognizant.iot.chatbot.model.SuggestDataModel;
import com.cognizant.iot.utils.AccountState;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Created by Guest_User on 22/11/16.
 */

public class ChatGlobal {
    public static HashSet<Character> arabicSet = new HashSet<>();
    public static boolean arabicSpeaking = false;

    /**
     * Refactored searchAPIToken to googleToken
     */

    public ChatGlobal(Context context) {
        this.mContext = context;
    }

    public static final int SEND = 0;
    public static final int RECEIVE = 1;
    public static final int RECEIVE_PRODUCT = 2;
    public static final int RECEIVE_OFFER = 3;
    public static final int RECEIVE_SEARCH = 4;
    public static final int RECEIVE_SUGGEST = 5;
    // Base url for Luis
//    public static String baseURL = "https://westus.api.cognitive.microsoft.com/luis/v2.0/apps/3af205c0-d01e-4b20-a67d-3ebdd7140791?subscription-key=665d662ecb614e40a54cf8d166989398&q=";
    public static String baseURL = "https://westus.api.cognitive.microsoft.com/luis/v2.0/apps/8854e3f2-fb89-444e-adbb-c6f325cad654?subscription-key=665d662ecb614e40a54cf8d166989398&timezoneOffset=0.0&verbose=true&q=";
    //token for search api
    public static String googleToken = AccountState.getTokenID();
    //
    //for YES/NO(means optional) response
    public static boolean isAsked = false;
    public static boolean askedSpecific = false;
    public static boolean intentIsRecommend = false;
    public static boolean intentIsAvailable = false;
    public static boolean intentIsPrice = false;
    public static boolean intentIsAddToCart = false;
    public static boolean isaddToCartAPI = false;
    //for mandatory response
    public static boolean isPrompt = false;
    public static String imageBaseUrl = "https://etihaddemodevret.cloudax.dynamics.com/MediaServer/";
    public static String recommendAPI = "http://rmethapi.azurewebsites.net/api/CognitiveServices/GetUserToItemRecommendationsAPI";
    public static String baseAddToCartAPI = "http://ethcustomervalidtionax.azurewebsites.net/api/cart/CreateCart?idToken=";
    public static String baseProductSearchAPI = "https://etihaddemodevret.cloudax.dynamics.com/Commerce/Products/SearchByText(channelId=68719476778,catalogId=0,searchText=%27";
    public static boolean tapToAdd = false;
    public static StringBuffer cartBuffer = new StringBuffer();
    public static String productID = "";
    public static String userId = AccountState.getUserID();
    public static String intent = "";

    public static TextToSpeech textToSpeech;
    public static int result;
    public static Context mContext;
    public static boolean online = !AccountState.getOfflineMode();

    //FOR GOOGLE SEARCH IN DEFAULT CASE.
    //https://www.googleapis.com/customsearch/v1?q=do+you+have+dell+laptop&cx=006896418020272592208%3Agznxrju2_ha&filter=1&num=5&safe=medium&key=AIzaSyAPXvMXt1MR5PMYMH7hoi2Pvfx2deHz6TQ
    public static String baseSearchUrl = "https://www.googleapis.com/customsearch/v1?q=";
    public static String endSearchUrl = "&cx=006896418020272592208%3Agznxrju2_ha&filter=1&num=5&safe=medium&key=AIzaSyAPXvMXt1MR5PMYMH7hoi2Pvfx2deHz6TQ";
    public static boolean hasSearchImage = false;

    //Intent specific Info
    public static String contextId;
    public static String lastIntent = "";

    //Layout Components
    public static RecyclerView mRecyclerView;
    public static ChatAdapter mAdapter;
    public static EditText chatText;
    public static ImageButton sendButton;
    public static RecyclerView.LayoutManager mLayoutManager;

    public static List<String> mDataset;
    public static ArrayList<Integer> mDatasetTypes;

    public static List<String> entityItemList = new ArrayList<>();
    public static List<String> entityBooleanList = new ArrayList<>();
    public static List<Integer> entityNumberList = new ArrayList<>();
    public static List<String> itemMatchedList = new ArrayList<>();
    public static List<String> itemMatchedIdList = new ArrayList<>();
    public static List<ProductDataModel> productList = new ArrayList<>();
    public static List<ProductDataModel> prevProductList = new ArrayList<>();
    public static List<SuggestDataModel> suggestDataModels;
    public static List<ChatDataModel> chatDataModels;

    public static boolean variantNeeded = true;
    public static boolean isVariantAPI = false;
    //for translation in arabic
    public static String eng2arabString = "https://translate.yandex.net/api/v1.5/tr.json/translate?lang=en-ar&key=trnsl.1.1.20170314T112944Z.0d019066b884a9c7.be1a27e78ecfc1319458aae64794fac4d10cdd3f&text=";
    public static String arab2engString = "https://translate.yandex.net/api/v1.5/tr.json/translate?lang=ar-en&key=trnsl.1.1.20170314T112944Z.0d019066b884a9c7.be1a27e78ecfc1319458aae64794fac4d10cdd3f&text=";

    public static String converted = "";

    public static int countPrompt = 0;
    public static String mSender = AccountState.getUserName();
    public static String mRecipient = "Raymont Brattly";


}
