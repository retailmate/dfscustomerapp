package com.cognizant.iot.chatbot.model;

/**
 * Created by 452781 on 11/15/2016.
 */
public class OfferDataModel {
    Integer imageSource;
    String offerDescription;

    public Integer getImageSource() {
        return imageSource;
    }

    public void setImageSource(Integer imageSource) {
        this.imageSource = imageSource;
    }

    public String getOfferDescription() {
        return offerDescription;
    }

    public void setOfferDescription(String offerDescription) {
        this.offerDescription = offerDescription;
    }
}