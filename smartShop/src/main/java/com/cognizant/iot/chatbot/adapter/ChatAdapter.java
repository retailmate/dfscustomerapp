package com.cognizant.iot.chatbot.adapter;


import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cognizant.iot.chatbot.ChatGlobal;

import com.cognizant.iot.chatbot.model.ChatDataModel;
import com.cognizant.retailmate.R;

import java.util.List;


/**Created by 543898 on 9/30/2016.**/
public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {
    private static final String TAG = "CustomAdapter";


    List<ChatDataModel> chatDataModelList;
    private static final String CustomTag="CustomTag";
    private Context mContext;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View v) {
            super(v);
            }
    }

    public class SendViewHolder extends ViewHolder {
        TextView sendMessage;
        TextView time;

        public SendViewHolder(View v) {
            super(v);
            this.sendMessage = (TextView) v.findViewById(R.id.textview_message_send);
            this.time= (TextView) v.findViewById(R.id.textview_time);
        }
    }

    public class ReceiveViewHolder extends ViewHolder {
        TextView receiveMessage;
        TextView time;

        public ReceiveViewHolder(View v) {
            super(v);
            this.receiveMessage = (TextView) v.findViewById(R.id.textview_message_receive);
            this.time= (TextView) v.findViewById(R.id.textview_time);
        }
    }

    public class ReceiveProductViewHolder extends ViewHolder {

        RecyclerView productRecyclerView;

        public ReceiveProductViewHolder(View v) {
            super(v);
            this.productRecyclerView= (RecyclerView) v.findViewById(R.id.product_list_recyc_view);
        }

    }

    public class ReceiveSearchViewHolder extends ViewHolder{
        RecyclerView searchRecyclerView;
        public ReceiveSearchViewHolder(View v) {
            super(v);
            this.searchRecyclerView=(RecyclerView) v.findViewById(R.id.search_list_recyc_view);
        }
    }

    public class SuggestionViewHolder extends ViewHolder{
        RecyclerView suggestionRecyclerView;
        public SuggestionViewHolder(View v) {
            super(v);
            this.suggestionRecyclerView=(RecyclerView) v.findViewById(R.id.suggestion_list_recyc_view);
        }
    }

    public ChatAdapter(Context context, List<ChatDataModel> chatDataList) {

    this.chatDataModelList=chatDataList;
        this.mContext=context;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        Log.e("TAG","(o)_(o)"+viewType);
        View v;
        if (viewType == ChatGlobal.SEND) {
            v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.chatbot_chat_user_send, viewGroup, false);

            return new SendViewHolder(v);
        }
        else if (viewType == ChatGlobal.RECEIVE_PRODUCT) {
            v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.chatbot_product_list, viewGroup, false);
            return new ReceiveProductViewHolder(v);
        }
        else if (viewType == ChatGlobal.RECEIVE_OFFER) {
            v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.chatbot_product_list, viewGroup, false);
            return new ReceiveProductViewHolder(v);
        }else if(viewType == ChatGlobal.RECEIVE_SEARCH){
            v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.chatbot_search_list,viewGroup,false);
            return  new ReceiveSearchViewHolder(v);
        }else if(viewType == ChatGlobal.RECEIVE_SUGGEST){
            v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.chatbot_suggestion_list,viewGroup,false);
            return  new SuggestionViewHolder(v);
        }else{
           v = LayoutInflater.from(viewGroup.getContext())
            .inflate(R.layout.chatbot_chat_user_receive, viewGroup, false);
            return new ReceiveViewHolder(v);
        }

    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        if (viewHolder.getItemViewType() == ChatGlobal.SEND) {
            SendViewHolder holder = (SendViewHolder) viewHolder;
            holder.sendMessage.setText(chatDataModelList.get(position).getmDataset());
            holder.time.setText(chatDataModelList.get(position).getmTime());
        }
        else if (viewHolder.getItemViewType() == ChatGlobal.RECEIVE_PRODUCT) {
            ReceiveProductViewHolder holder = (ReceiveProductViewHolder) viewHolder;

            ProductListAdapter productDataAdapter = new ProductListAdapter(mContext, chatDataModelList.get(position).getmProductArray());

            holder.productRecyclerView.setHasFixedSize(true);
            holder.productRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
            holder.productRecyclerView.setAdapter(productDataAdapter);

        }
        else if (viewHolder.getItemViewType() == ChatGlobal.RECEIVE_OFFER) {
            ReceiveProductViewHolder holder = (ReceiveProductViewHolder) viewHolder;

            OfferListAdapter offerDataAdapter = new OfferListAdapter(mContext, chatDataModelList.get(position).getmOfferArray());

            holder.productRecyclerView.setHasFixedSize(true);
            holder.productRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
            holder.productRecyclerView.setAdapter(offerDataAdapter);

        }else if (viewHolder.getItemViewType() == ChatGlobal.RECEIVE_SEARCH) {
            ReceiveSearchViewHolder holder = (ReceiveSearchViewHolder) viewHolder;

            SearchListAdapter searchDataAdapter = new SearchListAdapter(mContext, chatDataModelList.get(position).getmSearchArray());

            holder.searchRecyclerView.setHasFixedSize(true);
            holder.searchRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
            holder.searchRecyclerView.setAdapter(searchDataAdapter);

        }else if (viewHolder.getItemViewType() == ChatGlobal.RECEIVE_SUGGEST) {
            SuggestionViewHolder holder = (SuggestionViewHolder) viewHolder;

            SuggestionListAdapter suggestDataAdapter = new SuggestionListAdapter(mContext, chatDataModelList.get(position).getmSuggestArray());

            holder.suggestionRecyclerView.setHasFixedSize(true);
            holder.suggestionRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
            holder.suggestionRecyclerView.setAdapter(suggestDataAdapter);

        }else if (viewHolder.getItemViewType() == ChatGlobal.RECEIVE) {
            ReceiveViewHolder holder = (ReceiveViewHolder) viewHolder;
            holder.receiveMessage.setText(chatDataModelList.get(position).getmDataset());
            holder.time.setText(chatDataModelList.get(position).getmTime());
        }
    }

    @Override
    public int getItemCount() {
        Log.e("TAG","(o)_(o)"+chatDataModelList.size());
        return chatDataModelList.size();
    }

    @Override
    public int getItemViewType(int position) {
//
        return chatDataModelList.get(position).getmDatasetTypes();
    }


}
