package com.cognizant.iot.chatbot;

import android.util.Log;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;


public class MessageDataSource {
    private static final Firebase sRef = new Firebase("https://fireapp-c0df3.firebaseio.com/");
    private static SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
    private static final String TAG = "TAG";
    private static final String COLUMN_TEXT = "text";
    private static final String COLUMN_SENDER = "sender";
    ChatUtil util =new ChatUtil();

    public static void saveMessage(Message message, String convoId, String mSender){
        Log.e(TAG,"--------saveMessage-----------");
        Date date = message.getDate();
        String key = sDateFormat.format(date);
        HashMap<String, String> msg = new HashMap<>();
        msg.put(COLUMN_TEXT, message.getText());
        msg.put(COLUMN_SENDER,mSender);
        sRef.child(convoId).child(key).setValue(msg);
        Log.e(TAG,">>>>>>>>>>>>>>>>    after setting sRef");
    }

    public static MessagesListener addMessagesListener(String convoId){
        MessagesListener listener = new MessagesListener();
        sRef.child(convoId).addChildEventListener(listener);
        return listener;
    }

    public static void stop(MessagesListener listener){
        sRef.removeEventListener(listener);
    }

    public static class MessagesListener implements ChildEventListener {

        ChatUtil util =new ChatUtil();

        @Override
        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
            Log.e(TAG,"onChildAdded------");
            HashMap<String,String> msg = (HashMap)dataSnapshot.getValue();
            Message message = new Message();
            message.setSender(msg.get(COLUMN_SENDER));
            message.setText(msg.get(COLUMN_TEXT));
            try {
                message.setDate(sDateFormat.parse(dataSnapshot.getKey()));
            }catch (Exception e){
                Log.d(TAG, "Couldn't parse date"+e);
            }
            Log.e(TAG,">>>>>>>>>>>  Child Added <<<<<<<<<<<");
            if (message.getSender().equalsIgnoreCase(ChatGlobal.mSender)){
                util.setChatSendMessage(message);
            }else {
                ChatUtil.setChatReplyMessage(message);
            }
        }

        @Override
        public void onChildChanged(DataSnapshot dataSnapshot, String s) {

        }

        @Override
        public void onChildRemoved(DataSnapshot dataSnapshot) {


        }

        @Override
        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

        }

        @Override
        public void onCancelled(FirebaseError firebaseError) {

        }
    }

}
