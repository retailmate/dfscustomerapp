package com.cognizant.iot.chatbot;

import android.os.AsyncTask;

import com.cognizant.iot.chatbot.model.SearchDataModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 540472 on 12/27/2016.
 */
public class MySearchTask extends AsyncTask<String, String, String> {

    ChatUtil util=new ChatUtil();
    @Override
    protected String doInBackground(String... params) {
        BaseConnection con = new BaseConnection();
        String jsonString = "server not working";
        try {
            // Log.e("TAG","-------------?>>>>>>>>MyTask<<<<<<<<<<<<------------");
            jsonString = con.run(params[0]);
            // Log.e("TAG",jsonString.toString());
            return jsonString;
        } catch (IOException e) {
            e.printStackTrace();
        }
        //Log.e("TAG",jsonString);
        return jsonString;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

        JSONObject obj = null;
        try {
            obj = new JSONObject(s);
            List<SearchDataModel> searchDataModels = new ArrayList<>();
            JSONArray itemsArray = (JSONArray) obj.get("items");
            for (int i = 0; i < itemsArray.length(); i++) {
                JSONObject itemobj = (JSONObject) itemsArray.get(i);
                JSONArray imgArray = null;
                JSONObject imgObj = null;
                JSONObject pageMap = (JSONObject) itemobj.get("pagemap");
                if (pageMap.has("cse_thumbnail")) {
                    imgArray = pageMap.getJSONArray("cse_thumbnail");
                    imgObj = (JSONObject) imgArray.get(0);
                    ChatGlobal.hasSearchImage = true;
                }
                String titleName = itemobj.getString("title");
                String snippet = itemobj.getString("snippet");
                String link = itemobj.getString("link");
                SearchDataModel model = new SearchDataModel();
                model.setTitle(titleName);
                model.setSnippet(snippet);
                model.setLink(link);
                if (ChatGlobal.hasSearchImage && imgObj != null) {
                    String img = imgObj.getString("src");
                    model.setImgURL(img);
                    //Log.d("TAG","imgURL = "+model.getImgURL());
                }
                searchDataModels.add(model);
            }
            util.setReplySearch(searchDataModels);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}

