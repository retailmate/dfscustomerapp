package com.cognizant.iot.chatbot.model;

/**
 * Created by 543898 on 10/18/2016.
 */
public class ProductDataModel {
    String productName;
    String productImageResource;
    double price;
    String productId;
    Integer imageSource;

    public Integer getImageSource() {
        return imageSource;
    }

    public void setImageSource(Integer imageSource) {
        this.imageSource = imageSource;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }



    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getProductImageResource() {
        return productImageResource;
    }

    public void setProductImageResource(String productImageResource) {
        this.productImageResource = productImageResource;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }


}
