package com.cognizant.iot.chatbot;

import android.util.Log;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * Created by Guest_User on 22/09/16.
 * Class for API Call Using okHttp. It will return JSON Response Converted to String.
 */


public class BaseConnection {
    public boolean isrecommend=false;
    //used for searchByText API
    public boolean isavailable=false;
    public boolean isAddApi = false;
    public boolean isVariantApi = false;
    public boolean isTranslate=false;
    public String token="";
    private static final String CustomTag="CustomTag";


    OkHttpClient client = new OkHttpClient();
    Response response;

    String run(String url) throws IOException {
        String reply="server not working";
        if (isrecommend) {
            isrecommend=false;
            RequestBody formBody = new FormBody.Builder()
                    .add("UserID", ChatGlobal.userId)
                    .build();
            Request request = new Request.Builder()
                    .url(url)
                    .post(formBody)
                    .build();
            response = client.newCall(request).execute();
            reply= response.body().string();
        }else if (isAddApi){
            isAddApi=false;
/**
 * HAVE TO EDIT IT(ONLY FOR WHITE_LABEL version) ACCORDING TO VARIANT ID(USE PRODUCT SearchByProductId API  )
 * */
            Log.e("TAG","ChatGlobal.productID = "+ChatGlobal.productID);
            RequestBody formBody = new FormBody.Builder()
                    .add("ProductId",ChatGlobal.productID)
                    .add("Quantity", "1")
                    .build();
            Request request = new Request.Builder()
                    .url(url)
                    .post(formBody)
                    .build();
            response = client.newCall(request).execute();
            reply= response.body().string();

        }else if (isVariantApi){
            isVariantApi=false;
            Log.e("TAG","----------Variant id------");
            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, "{\"productSearchCriteria\":{\n\t\"Context\":{\n\t\t\"ChannelId\":68719476778,\n\t\t\"CatalogId\":0\n\t\t},\n\t\"Ids\":["+"68719482371"+"],\n\t\"DataLevelValue\":4,\n\t\"SkipVariantExpansion\":false\n\t}\n}");
            Request request = new Request.Builder()
                    .url("https://nikedevret.cloudax.dynamics.com/Commerce/Products/Search?%24top=250&api-version=7.1")
                    .post(body)
                    .addHeader("authorization", token)
                    .addHeader("oun", "091")
                    .addHeader("content-type", "application/json")
                    .build();

            response = client.newCall(request).execute();
            reply= response.body().string();

        }else if (isavailable){
            isavailable=false;
            Request request = new Request.Builder()
                    .url(url)
                    .addHeader("authorization", token)
                    .addHeader("oun", "091")
                    .build();

            response = client.newCall(request).execute();
            reply= response.body().string();
            Log.e(CustomTag,"reply from product search API = "+reply);
        }else if (isTranslate){
            isTranslate=false;
            Request request = new Request.Builder()
                    .url(url)
                    .build();

            response = client.newCall(request).execute();
            reply= response.body().string();
            Log.e(CustomTag,"reply from Translate API = "+reply);
        }else{
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            response = client.newCall(request).execute();
            reply= response.body().string();
        }


        int i = 0;
        while (reply.equalsIgnoreCase("server not working")) {

            i = i + 1;
        }
        return reply;
    }

}



