package com.cognizant.iot.chatbot;

import android.annotation.TargetApi;
import android.os.Build;
import android.speech.tts.TextToSpeech;
import android.text.format.DateFormat;
import android.util.Log;

import com.cognizant.iot.chatbot.model.AnalyticsDataModel;
import com.cognizant.iot.chatbot.model.ChatDataModel;
import com.cognizant.iot.chatbot.model.OfferDataModel;
import com.cognizant.iot.chatbot.model.ProductDataModel;
import com.cognizant.iot.chatbot.model.SearchDataModel;
import com.cognizant.iot.chatbot.model.SuggestDataModel;
import com.cognizant.retailmate.R;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;
import java.util.concurrent.ExecutionException;

/**
 * Created by 540472 on 12/27/2016.
 */
public class ChatUtil {

    private static final String CustomTag="CustomTag";
    AnalyticsDataModel analyticsDataModel;
    public String loadJSONFromAsset(String fileName) {
        String json = null;
        try {
            InputStream is = ChatGlobal.mContext.getAssets().open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public void clearList(){
        ChatGlobal.entityItemList.clear();
        ChatGlobal.entityBooleanList.clear();
        ChatGlobal.entityNumberList.clear();
        ChatGlobal.itemMatchedList.clear();
        ChatGlobal.itemMatchedIdList.clear();
        ChatGlobal.productList.clear();
    }

    public String stringToGoogleSearchUrl(String baseURL, String userSays,String endUrl) {
        String convertedString=null;
        StringBuffer searchbuffer=new StringBuffer(baseURL);
        Log.e(CustomTag,"usersaid = "+userSays);
        StringTokenizer st = new StringTokenizer(userSays," ");
        int length= st.countTokens();
        for (int i=0;i<length-1;i++){
            searchbuffer.append(st.nextToken());
            searchbuffer.append("+");
        }
        searchbuffer.append(st.nextToken());
        convertedString=searchbuffer.toString();
        convertedString=convertedString+endUrl;
        return convertedString;
    }

    //For setting suggestion texts.
    public void setSuggestDataModels(){
        SuggestDataModel suggestDataModel = new SuggestDataModel();
        suggestDataModel.setSuggestion(ChatGlobal.mContext.getResources().getString(R.string.bot_suggest_offer));
        Log.e("TAG", "#### setSuggestion" + suggestDataModel.getSuggestion());
        ChatGlobal.suggestDataModels.add(suggestDataModel);
        SuggestDataModel suggestDataModel1 = new SuggestDataModel();
        suggestDataModel1.setSuggestion(ChatGlobal.mContext.getResources().getString(R.string.bot_suggest_recommend));
        Log.e("TAG", "#### setSuggestion" + suggestDataModel.getSuggestion());
        ChatGlobal.suggestDataModels.add(suggestDataModel1);

        SuggestDataModel suggestDataModel2 = new SuggestDataModel();
        suggestDataModel2.setSuggestion(ChatGlobal.mContext.getResources().getString(R.string.bot_suggest_add_cart));
        Log.e("TAG", "#### setSuggestion" + suggestDataModel.getSuggestion());
        ChatGlobal.suggestDataModels.add(suggestDataModel2);

        SuggestDataModel suggestDataModel3 = new SuggestDataModel();
        suggestDataModel3.setSuggestion(ChatGlobal.mContext.getResources().getString(R.string.bot_suggest_checkout_cart));
        Log.e("TAG", "#### setSuggestion" + suggestDataModel.getSuggestion());
        ChatGlobal.suggestDataModels.add(suggestDataModel3);
    }

    //For URL of LUIS
    public String stringToUrl(String baseURL, String userSays) {
        String convertedString=null;
        StringBuffer sb=new StringBuffer(baseURL);

        StringTokenizer st = new StringTokenizer(userSays," ");
        while (st.hasMoreTokens()) {
            sb.append(st.nextToken());
            sb.append("%20");
        }
        convertedString=sb.toString();

        return convertedString;
    }

    public String stringToUrlContextId(String baseURL, String userSays, String contextId) {
        String convertedString=null;
        StringBuffer sb=new StringBuffer(baseURL);

        StringTokenizer st = new StringTokenizer(userSays," ");
        while (st.hasMoreTokens()) {
            sb.append(st.nextToken());
            sb.append("%20");
        }
        sb.append("&contextId="+contextId);
        convertedString=sb.toString();

        return convertedString;
    }

    public String makeProductSearchApi(String userSays){
        String api="";
        StringBuffer sb = new StringBuffer(ChatGlobal.baseProductSearchAPI);
        String query=userSays.replaceAll("[-+.^:,?]","");
        StringTokenizer st = new StringTokenizer(query, " ");
        while (st.hasMoreTokens()) {
            sb.append(st.nextToken());
            sb.append("%20");
        }
        sb.append("%27)?%24top=20&api-version=7.1");
        api = sb.toString();
        Log.e(CustomTag,"Api = "+api);
        return  api;
    }

    public void populateEntityList(JSONArray entityArray){
        Log.d(CustomTag,"entityArray.length() = "+entityArray.length());
        for (int i=0;i<entityArray.length();i++){
            JSONObject entityObj = null;


            try {
                entityObj = entityArray.getJSONObject(i);
                String type = entityObj.getString("type");
                if (type.equalsIgnoreCase("item")) {
                    String entity = entityObj.getString("entity").toLowerCase();
                    //for populating Analytics Data Model
                    populateADM(entity);
                    ChatGlobal.entityItemList.add(entity);
                    Log.d(CustomTag,"item = "+entity);
                }
                if (type.equalsIgnoreCase("boolean")) {
                    String entity = entityObj.getString("entity").toLowerCase();
                    ChatGlobal.entityBooleanList.add(entity);
                    Log.d(CustomTag,"boolean = "+entity);
                }
                if (type.equalsIgnoreCase("builtin.number")) {
                    //int number = entityObj.getInt("entity");
                    String number = entityObj.getString("entity").toLowerCase();
                    StringTokenizer tokenizer =new StringTokenizer(number," - ");
                    while(tokenizer.hasMoreTokens()){
                        int numb= Integer.parseInt(tokenizer.nextToken());
                        ChatGlobal.entityNumberList.add(numb);
                    }
                    Log.d(CustomTag,"number = "+number);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
/**
 * 1->  Call populate ADM from setReplyMessage if reply contains added to cart.
 * 2->  Have one common function for populateADM that can be called from any file.
 * 3->  Add productId in analyticDataModel.
 * */
    //for populating Analytics Data Model
    public void populateADM(String item){
        analyticsDataModel=new AnalyticsDataModel();
        analyticsDataModel.setIntent(ChatGlobal.intent);
        analyticsDataModel.setProduct_Name(item);
        analyticsDataModel.setUserId(ChatGlobal.userId);
        Date currentDateTime = new Date();
        analyticsDataModel.setTimeStamp(currentDateTime.toString());
        Gson gson=new Gson();
        Log.e(CustomTag,"ADM = "+gson.toJson(analyticsDataModel));

    }

    public void prompt(JSONObject dialogObj){
        ChatGlobal.isPrompt=true;
        String prompt = null;
        try {
            prompt = dialogObj.getString("prompt");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String reply = prompt;
        setReplyMessage(reply);
        Log.d(CustomTag, "prompt = " + prompt);

    }

    public static void setReplyMessage(String reply){
        //if arabic language
        if (ChatGlobal.arabicSpeaking){
            reply= translateToArabic(reply);
        }

        ChatDataModel chatSendData=new ChatDataModel();
        chatSendData.setmDataset(reply);
        chatSendData.setmDatasetTypes(ChatGlobal.RECEIVE);
        Date d = new Date();
        String time = (String) DateFormat.format("HH:mm", d.getTime());
        chatSendData.setmTime(time);
        ChatGlobal.chatDataModels.add(chatSendData);
        ChatGlobal.mAdapter.notifyDataSetChanged();
        ChatGlobal.mRecyclerView.scrollToPosition(ChatGlobal.chatDataModels.size()-1);

        //FOR TEXT TO SPEECH
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ttsGreater21(reply);
        } else {
            ttsUnder20(reply);
        }
    }

    @SuppressWarnings("deprecation")
    private static void ttsUnder20(String text) {
        HashMap<String, String> map = new HashMap<>();
        map.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "MessageId");
        ChatGlobal.textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, map);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private static void ttsGreater21(String text) {
        String utteranceId=ChatGlobal.mContext.hashCode() + "";
        ChatGlobal.textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null, utteranceId);
    }

    public void setReplySearch(List<SearchDataModel> productList){

        ChatDataModel chatReceiveData=new ChatDataModel();
        ArrayList<SearchDataModel> searchDataModelArrayList=new ArrayList<>();
        chatReceiveData.setmDatasetTypes(ChatGlobal.RECEIVE_SEARCH);
        Date d1 = new Date();
        String time1 = (String) DateFormat.format("HH:mm", d1.getTime());
        chatReceiveData.setmTime(time1);
        for (int i = 0; i <productList.size() ; i++) {

            SearchDataModel searchDataModel=new SearchDataModel();
            searchDataModel.setTitle(productList.get(i).getTitle());
            searchDataModel.setSnippet(productList.get(i).getSnippet());
            searchDataModel.setLink(productList.get(i).getLink());
            if (ChatGlobal.hasSearchImage){
                searchDataModel.setImgURL(productList.get(i).getImgURL());
            }
            searchDataModelArrayList.add(searchDataModel);
        }
        chatReceiveData.setmSearchArray(searchDataModelArrayList);
        ChatGlobal.chatDataModels.add(chatReceiveData);
        ChatGlobal.mAdapter.notifyDataSetChanged();
        ChatGlobal.mRecyclerView.scrollToPosition(ChatGlobal.chatDataModels.size()-1);
    }

    //Used for getting resource id of offer images
    public int getResourceId(String pVariableName, String pResourcename, String pPackageName)
    {
        try {
            return ChatGlobal.mContext.getResources().getIdentifier(pVariableName, pResourcename, pPackageName);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    public void setSendMessage(String userSays){
        ChatDataModel chatSendData=new ChatDataModel();
        chatSendData.setmDataset(userSays);
        chatSendData.setmDatasetTypes(ChatGlobal.SEND);
        Date d = new Date();
        String time = (String) DateFormat.format("HH:mm", d.getTime());
        chatSendData.setmTime(time);
        ChatGlobal.chatDataModels.add(chatSendData);
        ChatGlobal.mAdapter.notifyDataSetChanged();
        ChatGlobal.mRecyclerView.scrollToPosition(ChatGlobal.chatDataModels.size()-1);
    }

    public void setReplyProduct(List<ProductDataModel> productList){
        ChatDataModel chatReceiveData=new ChatDataModel();
        ArrayList<ProductDataModel> productDataModelArrayList=new ArrayList<>();
        chatReceiveData.setmDatasetTypes(ChatGlobal.RECEIVE_PRODUCT);
        Date d1 = new Date();
        String time1 = (String) DateFormat.format("HH:mm", d1.getTime());
        chatReceiveData.setmTime(time1);
        for (int i = 0; i <productList.size() ; i++) {
            ProductDataModel productDataModel=new ProductDataModel();
            productDataModel.setProductName(productList.get(i).getProductName());
            productDataModel.setPrice(productList.get(i).getPrice());
            productDataModel.setProductImageResource(productList.get(i).getProductImageResource());
            productDataModel.setProductId(productList.get(i).getProductId());
            productDataModelArrayList.add(productDataModel);
        }
        chatReceiveData.setmProductArray(productDataModelArrayList);
        ChatGlobal.chatDataModels.add(chatReceiveData);
        ChatGlobal.mAdapter.notifyDataSetChanged();
        ChatGlobal.mRecyclerView.scrollToPosition(ChatGlobal.chatDataModels.size()-1);
    }

    public void setSuggestion(List<SuggestDataModel> suggestList) {
        Log.e("TAG", "#### setSuggestion");
        ChatDataModel chatSuggestData = new ChatDataModel();
        ArrayList<SuggestDataModel> suggestDataModelArrayList = new ArrayList<>();
        chatSuggestData.setmDatasetTypes(ChatGlobal.RECEIVE_SUGGEST);
        for (int i = 0; i < suggestList.size(); i++) {
            SuggestDataModel suggestDataModel = new SuggestDataModel();
            suggestDataModel.setSuggestion(suggestList.get(i).getSuggestion());
            suggestDataModelArrayList.add(suggestDataModel);
        }
        System.out.println("#### suggestDataModelArrayList" + suggestDataModelArrayList.get(0).getSuggestion());
        chatSuggestData.setmSuggestArray(suggestDataModelArrayList);
        ChatGlobal.chatDataModels.add(chatSuggestData);
        ChatGlobal.mAdapter.notifyDataSetChanged();
        ChatGlobal.mRecyclerView.scrollToPosition(ChatGlobal.chatDataModels.size() - 1);
    }

    public void setReplyOffer(ArrayList<OfferDataModel> offerDataModelArrayList, ChatDataModel chatOfferData){
        chatOfferData.setmOfferArray(offerDataModelArrayList);
        ChatGlobal.chatDataModels.add(chatOfferData);
        ChatGlobal.mAdapter.notifyDataSetChanged();
        ChatGlobal.mRecyclerView.scrollToPosition(ChatGlobal.chatDataModels.size()-1);
    }

    public void setReplyProductOffline(ArrayList<ProductDataModel> productDataModelArrayList,ChatDataModel chatReceiveData ){

        chatReceiveData.setmProductArray(productDataModelArrayList);
        ChatGlobal.chatDataModels.add(chatReceiveData);
        ChatGlobal.mAdapter.notifyDataSetChanged();
        ChatGlobal.mRecyclerView.scrollToPosition(ChatGlobal.chatDataModels.size()-1);
    }

    public static void setChatReplyMessage(Message reply){
        ChatDataModel chatSendData=new ChatDataModel();
        chatSendData.setmDataset(reply.getText());
        chatSendData.setmDatasetTypes(ChatGlobal.RECEIVE);
        Date d = reply.getDate();
        String time = (String) DateFormat.format("HH:mm", d.getTime());
        chatSendData.setmTime(time);
        ChatGlobal.chatDataModels.add(chatSendData);
        ChatGlobal.mAdapter.notifyDataSetChanged();
        ChatGlobal.mRecyclerView.scrollToPosition(ChatGlobal.chatDataModels.size()-1);
    }

    public void setChatSendMessage(Message userSays){
        ChatDataModel chatSendData=new ChatDataModel();
        chatSendData.setmDataset(userSays.getText());
        chatSendData.setmDatasetTypes(ChatGlobal.SEND);
        Date d =userSays.getDate();
        String time = (String) DateFormat.format("HH:mm", d.getTime());
        chatSendData.setmTime(time);
        ChatGlobal.chatDataModels.add(chatSendData);
        ChatGlobal.mAdapter.notifyDataSetChanged();
        ChatGlobal.mRecyclerView.scrollToPosition(ChatGlobal.chatDataModels.size()-1);
    }

    public static void setArabicDataSet()
    {
        ChatGlobal.arabicSet.add('ض');
        ChatGlobal.arabicSet.add('ص');
        ChatGlobal.arabicSet.add('ث');
        ChatGlobal.arabicSet.add('ق');
        ChatGlobal.arabicSet.add('ف');
        ChatGlobal.arabicSet.add('غ');
        ChatGlobal.arabicSet.add('ع');
        ChatGlobal.arabicSet.add('ه');
        ChatGlobal.arabicSet.add('خ');
        ChatGlobal.arabicSet.add('ح');
        ChatGlobal.arabicSet.add('ج');
        ChatGlobal.arabicSet.add('ة');
        ChatGlobal.arabicSet.add('ش');
        ChatGlobal.arabicSet.add('س');
        ChatGlobal.arabicSet.add('ي');
        ChatGlobal.arabicSet.add('ب');
        ChatGlobal.arabicSet.add('ل');
        ChatGlobal.arabicSet.add('ا');
        ChatGlobal.arabicSet.add('ت');
        ChatGlobal.arabicSet.add('ن');
        ChatGlobal.arabicSet.add('م');
        ChatGlobal.arabicSet.add('ك');
        ChatGlobal.arabicSet.add('ظ');
        ChatGlobal.arabicSet.add('ط');
        ChatGlobal.arabicSet.add('ذ');
        ChatGlobal.arabicSet.add('د');
        ChatGlobal.arabicSet.add('ز');
        ChatGlobal.arabicSet.add('ر');
        ChatGlobal.arabicSet.add('و');
        ChatGlobal.arabicSet.add('١');
        ChatGlobal.arabicSet.add('٢');
        ChatGlobal.arabicSet.add('٣');
        ChatGlobal.arabicSet.add('٤');
        ChatGlobal.arabicSet.add('٥');
        ChatGlobal.arabicSet.add('٦');
        ChatGlobal.arabicSet.add('٧');
        ChatGlobal.arabicSet.add('٨');
        ChatGlobal.arabicSet.add('٩');
        ChatGlobal.arabicSet.add('٠');
    }

    public String translateToEnglish(String userSays) {
        //ChatGlobal.converted="";
        String url= ChatGlobal.arab2engString.concat(userSays);
        /*TranslateTask translateTask = new TranslateTask();
        translateTask.execute(url);*/
        String str_result="";
        try {
            str_result= new TranslateTask().execute(url).get();
            Log.e("###","translated text "+ str_result);
            JSONObject obj=new JSONObject(str_result);
            JSONArray textArray=obj.getJSONArray("text");
            str_result=textArray.getString(0);
            Log.e("###","translated text "+ str_result);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return str_result;

    }

    public static String translateToArabic(String userSays) {

        String url= ChatGlobal.eng2arabString.concat(userSays);
        String str_result="";
        try {
            str_result= new TranslateTask().execute(url).get();
            Log.e("###","translated text "+ str_result);
            JSONObject obj=new JSONObject(str_result);
            JSONArray textArray=obj.getJSONArray("text");
            str_result=textArray.getString(0);
            Log.e("###","translated text "+ str_result);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return str_result;

    }


    public static String getProductId(String id){
        String pId="";
        if (ChatGlobal.variantNeeded){
            ChatGlobal.isVariantAPI=true;

        }else{
            pId=id;
        }
        return pId;
    }
}
