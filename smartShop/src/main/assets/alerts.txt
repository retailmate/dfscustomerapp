{
    "alerts": [
        {
            "id": 3,
            "beaconId": "ED:65:22:6A:19:53",
            "type": "offer",
            "locationname": "Product Offer",
            "offers": [
                {
                    "description": "20% off Fabrikam SLR Camera X358",
                    "qrCode": "slr_generic"
                }
            ]
        }
    ]
}