{
	"@odata.context": "https://cts-ax7rtwupd1testdevaos.cloudax.dynamics.com/data/$metadata#CTS_CustomerRegistrationCategory",
	"value": [{
		"dataAreaId": "usrt",
		"Category": "Fashion Sunglasses",
		"CategoryHierarchy": "Fashion navigation hierarchy",
		"HasKnowledge":true
	}, {
		"dataAreaId": "usrt",
		"Category": "Mens Jeans",
		"CategoryHierarchy": "Fashion navigation hierarchy",
		"HasKnowledge":true
	}, {
		"dataAreaId": "usrt",
		"Category": "Fashion Sunglasses",
		"CategoryHierarchy": "Fashion navigation hierarchy",
		"HasKnowledge":false
	}, {
		"dataAreaId": "usrt",
		"Category": "Coats",
		"CategoryHierarchy": "Fashion navigation hierarchy",
		"HasKnowledge":true
	}, {
		"dataAreaId": "usrt",
		"Category": "Handbags",
		"CategoryHierarchy": "Fashion navigation hierarchy",
		"HasKnowledge":true
	}, {
		"dataAreaId": "usrt",
		"Category": "Jewelry",
		"CategoryHierarchy": "Fashion navigation hierarchy",
		"HasKnowledge":false
	}, {
		"dataAreaId": "usrt",
		"Category": "Watches",
		"CategoryHierarchy": "Fashion navigation hierarchy",
		"HasKnowledge":true
	}, {
		"dataAreaId": "usrt",
		"Category": "Digital SLR cameras",
		"CategoryHierarchy": "Channel navigation hierarchy",
		"HasKnowledge":true
	}]
}