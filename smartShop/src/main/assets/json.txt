{
    "assets": [
        {
            "beaconid": "2F234454-CF6D-4A0F-ADF2-F4911BA9FFA5",
            "direction": "north",
            "id": "A2241",
            "caseinfo": [
                {
                    "casetitle": "Solar Panel I",
                    "degree": "North 30 3 m",
                    "LastMaintenance": "07 Apr 2014",
                    "NextMaintenance": "27 May 2014",
                    "ticketcount": "2",
                    "TicketDes1": "Aluminium alloy frame is damaged",
                    "date": "10 Jan 2014",
                    "cost": "$500",
                    "TicketDes2": "Connection issues! Check Cables",
                    "video":"steamturbine.mp4"
                },	
                {
                    "casetitle": "Solar Panel II",
                    "degree": "North 41 2 m",
                    "LastMaintenance": "17 Mar 2014",
                    "NextMaintenance": "17 Apr 2014",
                    "ticketcount": "1",
                    "TicketDes1": "Issue reported with inverter batteries",
                    "date": "21 Jan 2014",
                    "cost": "$100",
                    "video":"multimeter.mp4"
                }
            ]
        },
        {
            "beaconid": "2F234454-CF6D-4A0F-ADF2-F4911BA9FFA7",
            "direction": "south",
            "id": "B2241",
            "caseinfo": [
                {
                    "casetitle": "Transformer Area A",
                    "degree": "South 45 3 m",
                    "LastMaintenance": "07 Mar 2014",
                    "NextMaintenance": "27 Apr 2014",
                    "ticketcount": "3",
                    "TicketDes1": "Short circuit in power lines of Unit 3",
                    "date": "15 Jan 2014",
                    "cost": "$50",
                    "TicketDes2": "Activate Magnetic Shielding near Unit 5",
                    "TicketDes3": "Calibrate primary winding with 2,000 in Unit 5",
                    "video":"transformer.mp4"
                },
                {
                    "casetitle": "Transformer Area B",
                    "degree": "South 55 3 m",
                    "LastMaintenance": "17 Mar 2014",
                    "NextMaintenance": "17 May 2014",
                    "ticketcount": "1",
                    "TicketDes1": "General maintenance",
                    "date": "20 Jan 2014",
                    "cost": "$53",
                    "video":"transformer.mp4"
                }
            ]
        },
        {
            "beaconid": "2F234454-CF6D-4A0F-ADF2-F4911BA9FFA8",
            "direction": "east",
            "id": "C2241",
            "caseinfo": [
                {
                    "casetitle": "Power Lines",
                    "degree": "East 35 2 m",
                    "LastMaintenance": "07 Apr 2014",
                    "NextMaintenance": "27 Jun 2014",
                    "ticketcount": "2",
                    "TicketDes1": "Short circuit in Area B",
                    "date": "17 Jan 2014",
                    "cost": "$40",
                    "TicketDes2": "Repair Voltage Regulator",
                    "video":"multimeter.mp4"
                },
                {
                    "casetitle": "Power Monitor",
                    "degree": "East 25 1 m",
                    "LastMaintenance": "17 Feb 2014",
                    "NextMaintenance": "17 May 2014",
                    "ticketcount": "4",
                    "TicketDes1": "Touch screen to be replaced",
                    "date": "14 Jan 2014",
                    "cost": "$50",
                    "TicketDes2": "Network cable to be replaced",
                    "TicketDes3": "Fix Booting Issues",
                    "TicketDes4": "Reload Software",
                    "video":"transformer.mp4"
                }
            ]
        },
        {
            "beaconid": "2F234454-CF6D-4A0F-ADF2-F4911BA9FFA9",
            "direction": "west",
            "id": "C2241",
            "caseinfo": [
                {
                    "casetitle": "Steam Turbine",
                    "degree": "West 65 4 m",
                    "LastMaintenance": "07 Apr 2014",
                    "NextMaintenance": "27 May 2014",
                    "ticketcount": "2",
                    "TicketDes1": "Pitting Corrosion & Pipe wall thinning",
                    "date": "14 Jan 2014",
                    "cost": "$51",
                    "TicketDes2": "Impurities deposits on HP Turbine Blades",
                    "video":"steamturbine.mp4"
                },
                {
                    "casetitle": "Gas Turbines",
                    "degree": "West 72 1 m",
                    "LastMaintenance": "17 Mar 2014",
                    "NextMaintenance": "17 Aug 2014",
                    "ticketcount": "1",
                    "TicketDes1": "Particle flow monitor for oxides, steam blow and water droplets",
                    "date": "12 Jan 2014",
                    "cost": "$40",
                    "video":"multimeter.mp4"
                }
            ]
        }
    ]
}